//
//  ApiCaller.swift
//  ConverterPro
//
//  Created by iApp on 29/07/20.
//  Copyright © 2020 iApp. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON
import Alamofire

struct NetworkImage {
    var imageUrl : URL?
    var title : String?
}
class BASE_URL {
    static let PixabayImageUrl      = "https://pixabay.com"
    static let UnsplashImageBase    = "https://api.unsplash.com/search/videos"
}
class KEYS{
    static let PixabayKey           = "17688780-e308f190464ee174bac1bba67"
    static let UnsplashAccessKey    = "g9XuLLKlEeLxHokJuA5Gl3dMKzfyFKZfco8l-93IEiI"

    
}
class ApiCaller{
    
    static let sharedInstance = ApiCaller()

    // MARK: - Request with codable
    
    
    
    
    public func requestPixabayData(_ search: String ,page : Int = 1,perPage : Int = 30, completion: @escaping ([PixabayObject]?) -> Void) {
        
        print("APi offset->\(page)")
        
//        let apiString = "\(BASE_URL.PixabayImageUrl)/api/?key=\(KEYS.PixabayKey)&q=\(search)&page=\(page)&per_page=\(perPage)&type=\(1)"
        let apiString = "\(BASE_URL.PixabayImageUrl)/api/videos/?key=\(KEYS.PixabayKey)&q=\(search)&page=\(page)&per_page=\(perPage)"
        //https://pixabay.com/api/videos/?key=17688780-e308f190464ee174bac1bba67&q=yellow+flowers
        let url = URL(string: apiString)!

        if NetworkReachabilityManager()!.isReachable {
            handleSearchQuery(search)
            
            // Alomafire version 4
            Alamofire.request(url, method: HTTPMethod.get, parameters: nil, headers: nil).responseJSON { (dataResponse:DataResponse) in
                
                guard let data = dataResponse.data else { return }
                let json = try? JSON(data:data)
                if let hits = json?["hits"].arrayObject as? [[String : Any]]{
                    print("pixa hits.count->",hits.count)
                    if let pixaModel = Mapper<PixabayObject>().mapArray(JSONObject: hits){
                        completion(pixaModel)
                    }
                }else{
                     completion(nil)
                }
            }
            
            
            /*
            // Alomafire version 5
            AF.request("\(url)").responseJSON { (response) in
                
                guard let data = response.data else { return }
                let json = try? JSON(data:data)
                if let hits = json?["hits"].arrayObject as? [[String : Any]]{
                    print("pixa hits.count->",hits.count)
                    
                    if let pixaModel = Mapper<PixabayObject>().mapArray(JSONObject: hits){
                        completion(pixaModel)
                    }

                }else{
                     completion(nil)
                }
                
            }
            */
        } else {
            completion(nil)
        }

    }
    /*
     Alamofire.request(url).responseData { (Afdata) in
                
                guard let data = Afdata.data else { return }
                do {
                    try data.write(to: CPDriveUtility.shared.temporaryFolderPathURL().appendingPathComponent("test").appendingPathExtension("mp4"))
                    
                }catch{
                    print(error.localizedDescription)
                    
                }
                return;
                

            }

    var medium : String?

        medium <- map["videos.large.url"]
     */
    public func requestPixabayLargeImage(_ search: String, completion: @escaping (URL?) -> Void) {
        
        
        let apiString = search
        let url = URL(string: apiString)!

        if NetworkReachabilityManager()!.isReachable {
            handleSearchQuery(search)
            
            
            Alamofire.request(url).responseData { (Afdata) in
                
                guard let data = Afdata.data else { return }
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                
                let filePath="\(documentsPath)/tempFile.mp4"
                    
                do {
                    try data.write(to: URL(fileURLWithPath: filePath))
                    completion(URL(fileURLWithPath: filePath))
                }catch{
                    print(error.localizedDescription)
                    completion(nil)

                }

            }
            
            /*
            AF.request("\(url)").responseData(completionHandler: { (Afdata) in
              
                guard let data = Afdata.data else { return }
                if let image = UIImage(data: data){
                    completion(image)
                }else{
                    completion(nil)
                }
            })
             */
            
        } else {
            completion(nil)
        }

    }
    
    // MARK: - Handle search query
    func handleSearchQuery(_ url: String!) {
        
        let sessionManager = Alamofire.SessionManager()
        sessionManager.session.getAllTasks { (tasks) -> Void in
            for task in tasks {
                if (task.originalRequest?.url?.absoluteString.contains(url))! {
                    task.cancel()
                }
            }
        }
        
        /*
        let sessionManager = Alamofire.Session()
        sessionManager.session.getAllTasks { (tasks) -> Void in
            for task in tasks {
                if (task.originalRequest?.url?.absoluteString.contains(url))! {
                    task.cancel()
                }
            }
        }
         */
    }
    
    public func requestUnsplashData(_ search: String ,page : Int = 1,perPage : Int = 30, completion: @escaping ([UnsplashModel]?) -> Void) {
        
        print("APi offset->\(page)")
        
        let urlStr = "\(BASE_URL.UnsplashImageBase)?client_id=\(KEYS.UnsplashAccessKey)&page=\(page)&per_page=\(perPage)&collections&query=\(search)"
        let url = URL(string: urlStr)!

        if NetworkReachabilityManager()!.isReachable {
            handleSearchQuery(search)
            
            Alamofire.request(url, method: HTTPMethod.get, parameters: nil, headers: nil).responseJSON { (response:DataResponse) in
                
                guard let data = response.data else { return }
                let json = try? JSON(data:data)
                if let results = json?["results"].arrayObject  {
                    
                    print("unsplah hits.count->",results.count)

                    let objcc = UnsplashModel.modelsFromDictionaryArray(array: results as NSArray)
                    completion(objcc)
                }else{
                    completion(nil)
                }
            }
            
            /*
            AF.request("\(url)").responseJSON { (response) in
                
                guard let data = response.data else { return }
                let json = try? JSON(data:data)
                if let results = json?["results"].arrayObject  {
                    
                    print("unsplah hits.count->",results.count)

                    let objcc = UnsplashModel.modelsFromDictionaryArray(array: results as NSArray)
                    completion(objcc)
                }else{
                    completion(nil)
                }
  
                
            }
            */
            
        } else {
            completion(nil)
        }

    }
    
    
}
extension String {

    /// convert JsonString to Dictionary
    func convertJsonStringToDictionary() -> [String: Any]? {
        if let data = data(using: .utf8) {
            return (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any]
        }

        return nil
    }
}

