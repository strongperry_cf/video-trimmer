//
//  Helper.swift
//  PDFScannerSample
//
//  Created by iApp on 14/11/18.
//  Copyright © 2018 iApp. All rights reserved.
//

import UIKit

protocol Evaluatable {
    // swiftlint:disable:next type_name
    associatedtype T
    func evaluate(with condition: T) -> Bool
}

class Helper{
    
    static func createFromJSON<T: Decodable>(_ data: Data) -> T? {
        guard let f = try? JSONDecoder().decode(T.self, from: data) else {
            return nil
        }
        return f
    }
    
//    func convert(pdfTextSticker: PDFTextSticker) -> TextSticker{
//        let textSticker = TextSticker(frame: pdfTextSticker.frame)
//        textSticker.label.text = pdfTextSticker.textView.text
//        textSticker.label.textColor = pdfTextSticker.textView.textColor
//        textSticker.isNew = pdfTextSticker.isNew
//        textSticker.transform = pdfTextSticker.transform
//        textSticker.center = pdfTextSticker.center
//
//        return textSticker
//    }
    
//    func convert(myTextView: MYTextView) -> TextSticker{
//        let textSticker = TextSticker(frame: myTextView.frame)
//        textSticker.label.text = myTextView.textView.text
//        textSticker.label.textColor = myTextView.textView.textColor
//        textSticker.transform = myTextView.transform
//        textSticker.center = myTextView.center
//        return textSticker
//    }
    
//    func convert(textSticker: TextSticker) -> PDFTextSticker{
//        let pdfTextSticker = PDFTextSticker(frame: textSticker.frame)
//        pdfTextSticker.textView.text = textSticker.label.text
//        pdfTextSticker.textView.textColor = textSticker.label.textColor
//        pdfTextSticker.isNew = textSticker.isNew
//        pdfTextSticker.transform = textSticker.transform
//        pdfTextSticker.center = textSticker.center
//        return pdfTextSticker
//    }
    
    
     func outputImage(image1: UIImage,container: UIView) -> UIImage? {
        var cImg:UIImage? = nil
        let picture = image1
        UIGraphicsBeginImageContextWithOptions(CGSize(width: picture.size.width, height: picture.size.height), true, 1.0)
        picture.draw(in: CGRect(x: 0, y: 0, width: picture.size.width, height: picture.size.height))
        container.drawHierarchy(in: CGRect(x: 0, y: 0, width: picture.size.width, height: picture.size.height), afterScreenUpdates: true)
        cImg = UIGraphicsGetImageFromCurrentImageContext()
        guard let newImg = cImg else{
//            let error = NSError(domain: "Error exporting Data", code: 1997, userInfo: nil)
            return nil
        }
        UIGraphicsEndImageContext()
        let imgData = newImg.jpegData(compressionQuality: 1.0)
        let finalImage = UIImage(data: imgData!)
        return finalImage
    }
    
    func showAlertViewController(title: String, message: String, btnTitle1: String, ok handler: ((UIAlertAction) -> Void)?, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: btnTitle1, style: .default, handler: handler));
        viewController.present(alert, animated: true, completion: nil);
    }
    
    //===========================================================================================
    func showAlertViewController(title: String, message: String, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
        viewController.present(alert, animated: true, completion: nil);
    }
    
    func showAlertViewController(title: String, message: String, buttonTitle: String, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .cancel, handler: nil));
        viewController.present(alert, animated: true, completion: nil);
    }
    
    func showAlertViewController(title: String, message: String, btnTitle1: String, btnTitle2: String, ok handler: ((UIAlertAction) -> Void)?,cancel handler1: ((UIAlertAction) -> Void)?, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: btnTitle1, style: .cancel, handler: handler1));
        var action2: UIAlertAction!
        if btnTitle2 == "Delete"{
            action2 = UIAlertAction(title: btnTitle2, style: .destructive, handler: handler)
        }else{
            action2 = UIAlertAction(title: btnTitle2, style: .default, handler: handler)
        }
        alert.addAction(action2);
        viewController.present(alert, animated: true, completion: nil);
    }
    
    
    func calculateRectOfImageInAspectFit(parentFrame: CGRect,childSize: CGSize?) -> CGRect {
        let imageViewSize = parentFrame.size
        let imgSize = childSize
        
        guard let imageSize = imgSize, imgSize != nil else {
            return CGRect.zero
        }
        
        let scaleWidth = imageViewSize.width / imageSize.width
        let scaleHeight = imageViewSize.height / imageSize.height
        let aspect = fmin(scaleWidth, scaleHeight)
        
        var imageRect = CGRect(x: 0, y: 0, width: imageSize.width * aspect, height: imageSize.height * aspect)
        // Center image
        imageRect.origin.x = (imageViewSize.width - imageRect.size.width) / 2
        imageRect.origin.y = (imageViewSize.height - imageRect.size.height) / 2
        
        // Add imageView offset
        imageRect.origin.x += parentFrame.origin.x
        imageRect.origin.y += parentFrame.origin.y
        
        return imageRect
    }
    
    static func getSmallSizeOfSameAspectRatio(of size: CGSize,minimumWH: CGFloat) -> CGSize {
        var newSize = CGSize(width: minimumWH, height: minimumWH)
            if size.width > size.height {
                let wToHRatio = size.width / size.height
                newSize = CGSize(width: wToHRatio * minimumWH, height: minimumWH)
            } else {
                let hToWRatio = size.height / size.width
                newSize = CGSize(width: minimumWH, height: minimumWH * hToWRatio)
            }
        return newSize
    }
    
    static func isAlphaNumeric(text: String) -> Bool {
        let regexp = "^[A-Z0-9a-z _:]+"
        return text.evaluate(with: regexp)
    }
    
    static func dispatchMain(execute : @escaping () -> Void) {
        DispatchQueue.main.async(execute: execute)
    }
    static func dispatchMainAfter(time : DispatchTime , execute :@escaping (() -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: time, execute: execute)
    }
    
    func color(withHexString hexColorString: String, alpha: CGFloat) -> UIColor {
        
        var colorValue: UInt32 = 0
        
        let valueScanner = Scanner(string: hexColorString)
        
        if Int((hexColorString as NSString).range(of: "#").location) != NSNotFound {
            valueScanner.scanLocation = 1
        }
        
        valueScanner.scanHexInt32(&colorValue)
        let color = UIColor(red: CGFloat((Int(colorValue) & 0xff0000) >> 16) / 255.0, green: CGFloat(((Int(colorValue) & 0xff00) >> 8)) / 255.0, blue: CGFloat(((Int(colorValue) & 0xff) >> 0)) / 255.0, alpha: alpha)
        return color
    }
    
    
    //MARK:- Stickers layr functions
//    static func createAttributedStringWith(_ text: StickerLayerData) -> NSMutableAttributedString? {
//        let range = NSRange(location: 0, length: (text.text != nil) ? text.text.count : text.placeHolderText.count)
//        let font = UIFont(name: text.fontName, size: text.fontSize)
//        let attribute = NSMutableAttributedString(string: text.text ?? text.placeHolderText)
//        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: text.fontColor ?? .white, range: range)
//        attribute.addAttribute(NSAttributedString.Key.font, value: font!, range: range)
//        attribute.addAttribute(NSAttributedString.Key.strokeWidth, value: text.strokeWidth ?? 0.0, range: range)
//        attribute.addAttribute(NSAttributedString.Key.strokeColor, value: text.strokeColor ?? UIColor.clear, range: range)
//        return attribute
//    }
    
    static func textSize(_ str: NSAttributedString) -> CGSize {
        var newSize = str.size()
        newSize.width += 10
        newSize.height += 10
        return newSize
    }
    static func xscale(_ transform: CGAffineTransform) -> CGFloat {
        let t: CGAffineTransform = transform
        return sqrt(t.a * t.a + t.c * t.c)
    }
    
    static func yscale(_ transform: CGAffineTransform) -> CGFloat {
        let t: CGAffineTransform = transform
        return sqrt(t.b * t.b + t.d * t.d)
    }
    
    static func zRotaion(_ transform: CGAffineTransform) -> CGFloat {
        let radians = atan2f(Float(transform.b), Float(transform.a))
        return CGFloat(radians)
    }
    

    static func getTopMostController() -> UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }

}



