//
//  SlidesCollectionViewCell.swift
//  VideoEditorial
//
//  Created by iApp on 18/04/19.
//  Copyright © 2019 iApp. All rights reserved.
//

import UIKit

class SlidesCollectionViewCell: UICollectionViewCell {

    @IBOutlet var thumbnail: UIImageView!
    @IBOutlet var themeName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    public func configure(with model: ANIMATIONS_CATEGORY) {
        
        DispatchQueue.main.async {

//            self.thumbnail.image = #imageLiteral(resourceName: "glitchThumbnail2")
            self.thumbnail.clipsToBounds = true
            self.themeName.text = model.rawValue.capitalized
            self.thumbnail.layer.cornerRadius = self.thumbnail.frame.width/2
        }
    }

}
