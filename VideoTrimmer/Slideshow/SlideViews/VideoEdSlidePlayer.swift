//
//  VideoEdSlidePlayer.swift
//  VideoEditorial
//
//  Created by iApp on 04/04/19.
//  Copyright © 2019 iApp. All rights reserved.
//

import UIKit

class VideoEdSlidePlayer: UIView {

    var imagesArray                 :   [UIImage]? = nil
    var currentPlayingAnimaionIndex :   Int = 0
    var player                      :   AVPlayer!
    var avPlayerItem                :   AVPlayerItem? = nil
    var avSynchLayer                :   AVSynchronizedLayer!
    var timeObserver                :   (Any)? = nil
    var manualScrolling             :   Bool = false


    @IBOutlet var playerSlider: UISlider!
    public func instanceFromNib() -> VideoEdSlidePlayer? {
        return UINib(nibName: "VideoEdSlidePlayer", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? VideoEdSlidePlayer
    }
    
    var playerStatus : ((_:Bool) -> ())?
    var playerUpdated : ((_:Bool) -> ())?
    var playerDurationClouser : ((CGFloat)->())?
    deinit{
        
        debugPrint("VideoEdSlidePlayer dealloc")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

        
    }
    func nillPlayerForcefully(){
        
        if player != nil && self.timeObserver != nil {
            self.player.removeTimeObserver(self.timeObserver)
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        self.player.pause()
        self.player = nil
        
    }
    func setupPlayer(){
        

        if self.avSynchLayer != nil{
            
            CATransaction.begin()
            CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
            self.avSynchLayer.removeFromSuperlayer()
            CATransaction.commit()
        }
        let imageView = UIImageView()
        imageView.frame = self.bounds
        imageView.image = imagesArray?.first
        imageView.contentMode = .scaleAspectFill
//        self.addSubview(imageView)
        
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        weak var weakSelf = self
        var  mediaImagesArray : [UIImage] = []
        mediaImagesArray = imagesArray!
        
        if imagesArray != nil{
            
            weakSelf?.avPlayerItem = nil
            let url = VideoEdVideoMakeManager.silenceAudioURL
            let asset:AVAsset = AVAsset.init(url: url)

            let composition = AVMutableComposition()
            
            let calcTimeForVideo = CGFloat((imagesArray?.count)!) * VideoEdVideoMakeManager.durationForOnePic
            let endTime:CMTime = CMTime(seconds: Double(calcTimeForVideo), preferredTimescale: 1)
            let assetTimeRange = CMTimeRangeMake(start: .zero, duration: endTime)
            
            do {
                try composition.insertTimeRange(assetTimeRange, of: asset, at: .zero)

            } catch {
                // Handle the error
                print("Inserting time range failed. ", error)
            }
            self.insertAudioInSlideshow(composition: composition)
            debugPrint("composition is",composition.duration)
            
            weakSelf?.avPlayerItem = AVPlayerItem.init(asset: composition)
            if player != nil && self.timeObserver != nil {
                self.player.removeTimeObserver(self.timeObserver)
            }

            if player == nil {
                player = AVPlayer(playerItem: self.avPlayerItem)
            } else {
                player!.replaceCurrentItem(with: self.avPlayerItem)
            }
            

            self.timeObserver = self.player?.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(0.05, preferredTimescale: Int32(100.0)), queue: DispatchQueue.main , using: { (currentTime) in
                
                if(self.manualScrolling){
                    return
                }
                if let closure = weakSelf?.playerDurationClouser {
                    closure(currentTime.durationInSeconds)
                }
            })

            
            self.avSynchLayer = AVSynchronizedLayer(playerItem: self.avPlayerItem!)
            self.avSynchLayer.frame = self.bounds
            self.layer.addSublayer(self.avSynchLayer)
            
            let masterlayerOnSynLayer = CALayer()
            masterlayerOnSynLayer.frame = self.avSynchLayer.bounds
            masterlayerOnSynLayer.opacity = 1.0
            masterlayerOnSynLayer.masksToBounds = true
            self.avSynchLayer.addSublayer(masterlayerOnSynLayer)

            let animateValue = globalMenuAnimationsArray[currentPlayingAnimaionIndex]
            
            for i in 0..<mediaImagesArray.count {

                AnimationsClass.shared.addAnimation(masterlayer: masterlayerOnSynLayer, images: mediaImagesArray, index: i, totalCount: mediaImagesArray.count, isSaving: false ,animateValue: animateValue)

            }
            DispatchQueue.main.async {
                
                self.player?.play()
                if self.playerStatus != nil {
                    self.playerStatus!(true)
                }
            }

            NotificationCenter.default.addObserver(self, selector: #selector(self.itemDidFinishPlaying(_:)),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayerItem!)


        }
    }

    func insertAudioInSlideshow(composition : AVMutableComposition ){
        
        let calcTimeForVideo = CGFloat((imagesArray?.count)!) * VideoEdVideoMakeManager.durationForOnePic
        let endTime:CMTime = CMTime(seconds: Double(calcTimeForVideo), preferredTimescale: 100)

        let audioAsset = AVAsset(url: VideoEdVideoMakeManager.audioUrl)
        let minTime = CMTimeMinimum(endTime, CMTime(seconds: Double(VideoEdVideoMakeManager.audioEndTime - VideoEdVideoMakeManager.audioStartTime), preferredTimescale: 100))
        let startTime = CMTime(seconds: Double(VideoEdVideoMakeManager.audioStartTime), preferredTimescale: 100)

        let audioTimeRange = CMTimeRange(start: startTime , duration:  minTime)
        let assetAudioTrack: AVAssetTrack = audioAsset.tracks(withMediaType: AVMediaType.audio)[0]
        
        if VideoEdVideoMakeManager.repeatAudio{
            
            if minTime<endTime{
                
                let compositionAudioVideo: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
                let divideTimeCount =  endTime.durationInSeconds /  minTime.durationInSeconds
//                CMTimeMultiplyByRatio(endTime, multiplier: 1, divisor: Int32(minTime.durationInSeconds))
                debugPrint("count=",divideTimeCount)
                
                var mutCmtime : CMTime = .zero
                for i in 0...Int(divideTimeCount) {
                    
                    
                    do {
                        try compositionAudioVideo.insertTimeRange(audioTimeRange, of: assetAudioTrack, at: mutCmtime)
                        
                    } catch {
                        // Handle the error
                        print("Inserting time range failed. ", error)
                    }
                    
                    mutCmtime = mutCmtime + minTime
                    debugPrint("mutCmtime=",mutCmtime)

                }

            }else{
                let compositionAudioVideo: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
                
                do {
                    try compositionAudioVideo.insertTimeRange(audioTimeRange, of: assetAudioTrack, at: .zero)
                    
                } catch {
                    // Handle the error
                    print("Inserting time range failed. ", error)
                }
            }
        }else{
            
            let compositionAudioVideo: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
            
            do {
                try compositionAudioVideo.insertTimeRange(audioTimeRange, of: assetAudioTrack, at: .zero)
                
            } catch {
                // Handle the error
                print("Inserting time range failed. ", error)
            }
        }

    }
    func updateAssetPlayer(){
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

        if self.avSynchLayer != nil{
            
//            CATransaction.begin()
//            CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
            self.avSynchLayer.removeFromSuperlayer()
//            CATransaction.commit()
        }
        
        
        weak var weakSelf = self
        var  mediaImagesArray : [UIImage] = []
        mediaImagesArray = imagesArray!
        
        if imagesArray != nil{
            
            weakSelf?.avPlayerItem = nil
            let url = VideoEdVideoMakeManager.silenceAudioURL
            
            let asset:AVAsset = AVAsset.init(url: url)
            
            let composition = AVMutableComposition()
            
            let calcTimeForVideo = CGFloat((imagesArray?.count)!) * VideoEdVideoMakeManager.durationForOnePic
            let endTime:CMTime = CMTime(seconds: Double(calcTimeForVideo), preferredTimescale: 1)
            let assetTimeRange = CMTimeRangeMake(start: .zero, duration: endTime)
            
            do {
                try composition.insertTimeRange(assetTimeRange, of: asset, at: .zero)
                
            } catch {
                // Handle the error
                print("Inserting time range failed. ", error)
            }
            
            
            self.insertAudioInSlideshow(composition: composition)
            debugPrint("composition is",composition.duration)
            
            weakSelf?.avPlayerItem = AVPlayerItem.init(asset: composition)
            
            if player == nil {
                player = AVPlayer(playerItem: self.avPlayerItem)
            } else {
                player!.replaceCurrentItem(with: self.avPlayerItem)
            }
//            player.removeObserver(self, forKeyPath: "rate")
//            player.addObserver(self, forKeyPath: "rate", options: [.new], context: nil)

            self.avSynchLayer.sublayers?.forEach{ layer in
                
                layer.sublayers?.forEach{ sublayer in
                    sublayer.removeAllAnimations()
                    sublayer.removeFromSuperlayer()
                    
                }
                layer.removeAllAnimations()
                layer.removeFromSuperlayer()
            }
            
            self.avSynchLayer.removeAllAnimations()
            self.avSynchLayer.removeFromSuperlayer()
            self.avSynchLayer = nil
            self.avSynchLayer = AVSynchronizedLayer(playerItem: self.avPlayerItem!)
            self.avSynchLayer.frame = self.bounds
            self.layer.addSublayer(self.avSynchLayer)
            
            let masterlayerOnSynLayer = CALayer()
            masterlayerOnSynLayer.frame = self.avSynchLayer.bounds
            masterlayerOnSynLayer.opacity = 1.0
            masterlayerOnSynLayer.masksToBounds = true
            self.avSynchLayer.addSublayer(masterlayerOnSynLayer)
            let animateValue = globalMenuAnimationsArray[currentPlayingAnimaionIndex]
            
            DispatchQueue.main.async {

                if weakSelf!.playerStatus != nil {
//                    weakSelf!.playerStatus!(false)
                }
            }

//            DispatchQueue.main.asyncAfter(deadline: .now() ) {
            
                for i in 0..<mediaImagesArray.count {
                    
                    AnimationsClass.shared.addAnimation(masterlayer: masterlayerOnSynLayer, images: mediaImagesArray, index: i, totalCount: mediaImagesArray.count, isSaving: false ,animateValue: animateValue)
                    
                }
                debugPrint("AnimationsClass finish")
                DispatchQueue.main.async {
                    
                    weakSelf!.player?.play()
                    //                weakSelf.layer.display()
                    //                weakSelf.avSynchLayer.display()
                    if weakSelf!.playerStatus != nil {
                        weakSelf!.playerStatus!(true)
                    }
                }
//            }
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayerItem!)
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object as AnyObject? === player {
//            debugPrint("keyPath=>",keyPath)

            if keyPath == "status" {
                if player.status == .readyToPlay {
//                    player.play()
                    debugPrint("player status")
                }
            } else if keyPath == "timeControlStatus" {
                if #available(iOS 10.0, *) {
                    if player.timeControlStatus == .playing {
//                        videoCell?.muteButton.isHidden = false
                    } else {
//                        videoCell?.muteButton.isHidden = true
                    }
                }
            } else if keyPath == "rate" {
                if player.rate > 0 {

                    debugPrint("rate 1=>")

//                    if self.playerUpdated != nil {
//                        self.playerUpdated!(true)
//                    }
                    
                } else {
//                    videoCell?.muteButton.isHidden = true
                }
            }
        }
    }

    func updatePlayer(){
        
        var  mediaImagesArray : [UIImage] = []
        mediaImagesArray = imagesArray!
        
        if imagesArray != nil{
            
            self.avSynchLayer.sublayers?.forEach{ layer in
                layer.removeAllAnimations()
                layer.removeFromSuperlayer()
            }
            
            self.avSynchLayer.removeAllAnimations()
            self.avSynchLayer.removeFromSuperlayer()
            self.avSynchLayer = nil
            self.avSynchLayer = AVSynchronizedLayer(playerItem: self.avPlayerItem!)
            self.avSynchLayer.frame = self.bounds
            self.layer.addSublayer(self.avSynchLayer)
            
            let masterlayerOnSynLayer = CALayer()
            masterlayerOnSynLayer.frame = self.avSynchLayer.bounds
            masterlayerOnSynLayer.opacity = 1.0
            masterlayerOnSynLayer.masksToBounds = true
            self.avSynchLayer.addSublayer(masterlayerOnSynLayer)
            let animateValue = globalMenuAnimationsArray[currentPlayingAnimaionIndex]
            
            for i in 0..<mediaImagesArray.count {
                
                AnimationsClass.shared.addAnimation(masterlayer: masterlayerOnSynLayer, images: mediaImagesArray, index: i, totalCount: mediaImagesArray.count, isSaving: false ,animateValue: animateValue)
                
            }
            DispatchQueue.main.async {
                
                self.avSynchLayer.display()
                self.layer.display()
                if self.playerStatus != nil {
                    self.playerStatus!(true)
                }

            }
        }
    }
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        DispatchQueue.main.async {
            self.player?.seek(to: CMTime.zero)
            self.player?.play()
        }
    }

    func addOnSyncLayer(syncLayer : AVSynchronizedLayer){
        
        let sampleLayer = CALayer()
        sampleLayer.frame = self.bounds
        sampleLayer.contents = UIImage(named: "dashboardImage.jpg")?.cgImage
        sampleLayer.opacity = 0.0
        
        let scaleAnimate:CABasicAnimation = CABasicAnimation(keyPath: "opacity")
        scaleAnimate.fromValue = 1.0
        scaleAnimate.toValue = 1.0
        scaleAnimate.duration = 1.0;
        scaleAnimate.beginTime = 0.0
        scaleAnimate.isRemovedOnCompletion = false
        sampleLayer.add(scaleAnimate, forKey: "scaleSmallAnimation")
        
        
        syncLayer.addSublayer(sampleLayer)
        
    }

}
