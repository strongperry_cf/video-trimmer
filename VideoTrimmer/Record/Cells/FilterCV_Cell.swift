//
//  FilterCV_Cell.swift
//  Video Editorial
//
//  Created by strongapps on 10/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//


import UIKit

class FilterCV_Cell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categorytitleLabel: UILabel!
    @IBOutlet weak var thumbnailView: UIImageView!
    @IBOutlet weak var effectNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
