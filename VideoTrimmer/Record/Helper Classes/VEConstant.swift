//  VEConstant.swift
//  VideoTrimmer
//
//  Created by strongapps on 03/04/19.
//  Copyright © 2019 strongapps. All rights reserved.


import Foundation

let AppNameGlobal:String = "Video Trimmer"
let AppIDGlobal:String = "1492918546"
//let GOOGLE_UNIT_AD = "ca-app-pub-3940256099942544/1033173712"//Unit Id: test
let GOOGLE_UNIT_AD = "ca-app-pub-2620068967527481/7441761507"//trimmer
//let GOOGLE_UNIT_AD = "ca-app-pub-2620068967527481/7475938606"

let FREE_GLITCH_EFFCTS          = 5000
let FREE_FILTERS                = 5000
let FREE_STICKERS_CATEGORIES    = 5000
let FREE_GIPHY_GIFS             = 5000
let FREE_GIPHY_STICKER          = 5000
let FREE_CAMERA_FILTERS         = 5000
let FREE_SLIDESHOWTHEMES        = 5000

let FONT_REGULAR    =       "GeorgeRoundedRegular"
let FONT_BOLD       =       "GeorgeRoundedBold"
let FONT_SEMIBOLD   =       "GeorgeRoundedSemibold"
let FONT_LIGHT      =       "GeorgeRoundedLight"

func print(_ object: Any) {
    #if DEBUG
    Swift.print(object)
    #endif
}

func print(_ item: @autoclosure () -> Any, separator: String = " ", terminator: String = "\n") {
    #if DEBUG
    Swift.print(item(), separator: separator, terminator: terminator)
    #endif
}


let SCREEN_WIDTH = ((UIApplication.shared.statusBarOrientation == .portrait) || (UIApplication.shared.statusBarOrientation == .portraitUpsideDown)) ? UIScreen.main.bounds.size.width : UIScreen.main.bounds.size.height

let SCREEN_HEIGHT = ((UIApplication.shared.statusBarOrientation == .portrait) || (UIApplication.shared.statusBarOrientation == .portraitUpsideDown)) ? UIScreen.main.bounds.size.height : UIScreen.main.bounds.size.width

let IS_IPAD             = (UI_USER_INTERFACE_IDIOM() == .pad)
let IS_IPHONE           = (UI_USER_INTERFACE_IDIOM() == .phone)
let IS_RETINA           = UIScreen.main.scale >= 2.0

let SCREEN_MAX_LENGTH   = Int(max(SCREEN_WIDTH, SCREEN_HEIGHT))
let SCREEN_MIN_LENGTH   = Int(min(SCREEN_WIDTH, SCREEN_HEIGHT))
let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
let IS_IPHONE_X_Series  = IS_IPHONE && SCREEN_MAX_LENGTH >= 812

let IMAGES_LIMIT         = 30
let VIDEO_MAXIMUM_LIMIT : CGFloat = 300 // In Seconds
let VIDEO_MAXIMUM_COUNT_LIMIT : CGFloat = 10 // In Count


struct FilePathManager {
    
    private init() {}
    
    static var docPath: String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    static func createNewPath(_ name:String) -> URL {
        
        let path = (FilePathManager.docPath as NSString).appendingPathComponent(name)
        if(FileManager.default.fileExists(atPath: path)) {
            guard (try? FileManager.default.removeItem(atPath: path)) != nil
                else {
                return URL(fileURLWithPath: path)
            }
        }
        let url = URL(fileURLWithPath: path)
        return url
    }
    

}

class constant: NSObject {
    
    static let sharedInstance : constant = {
        let instance = constant()
        return instance
    }()

    
    public func deviceHaveFlash() -> Bool {
        
        let device = AVCaptureDevice.default(for: .video)
        if (device?.hasTorch)!  {
            return true;
        }else
        {
            return false;
        }
    }
    
    
    
}

