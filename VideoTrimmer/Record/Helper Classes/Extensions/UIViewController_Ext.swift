

import UIKit

public extension UIViewController{
    
    func hideNavigationBar(_ animation:Bool = true){
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animation)
    }
    
    func showNavigationBar(_ animation:Bool = true) {
        
        // Show the navigation bar on other view controllers
        
        self.navigationController?.setNavigationBarHidden(false, animated: animation)
    }
   
    /*
    func updateConstraints(_ duration: TimeInterval = 0.3) {
        
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.view.layoutIfNeeded() ?? ()
        })
    }*/
    
    var isPresentingForFirstTime: Bool {
        return isBeingPresented || isMovingToParent
    }
 
    func updateConstraints(_ animation: Bool = true, _ duration: TimeInterval = 0.3) {
        
        if !animation {
            view.layoutIfNeeded()
        } else {
            weak var weakSelf = self
            UIView.animate(withDuration: duration, animations: {
                weakSelf!.view.layoutIfNeeded() // Called on parent view
            })
        }
    }

}

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
