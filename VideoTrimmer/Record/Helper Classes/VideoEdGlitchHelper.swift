//
//  VideoEdGlitchHelper.swift
//  VideoTrimmer
//
//  Created by strongapps on 15/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit


enum GlitchFilterType : Int
{
    case _None
    // case Brick_Pixellate
    // case Triangles
    
    case BrokenVideo1
    case Chromatic_Glitch1
    case Chromatic_Glitch2
    case BrokenVideo3
    case Chromatic_Glitch3
    case BinaryGlitch
    case TextureSlicer
    case Glitchy_Colours1
    case RandomZoom
    case Glitchy_Colours2 //10
    case Glitchy_Colours3
    case Glitch2
    case BrokenVideo4
    case Video_Glitch
    case Glitch_Shader_A
    
   // case Reflection_Map
    case Test_Glitch
    case Emix
    case Volumetrics
    case BrokenVideo2
//    case Drl011_glitch
//    case EndLess
//    case EndLess2
//    case SW_Glitch//20
//    case Frosted_Glass
//    case Hypnotic_Filter
//    case TV_CRT
//    case Stretch  //25
//    case Wave_Ripples
//    case FakeRipple
//    case RippleEffect
//    case Rainier_mood
//    case Ripple_Anastadunbar  //30
//    case Rotation
//    case BitForBit
//    case Glitch_Shader_B
//    case Broken_Pixelsort
//    case Broken_Pixelsort_2
//    case CMYK_Halftone   //35
//    case glitch_01_shampain
//    case GlitchPixel
//    case glitch_02_shampain
//    case VHS_Tape_Noise
//    case timOh  //40
//    case Dithering_Works_Wonders
//    case bit_of_Glitch
//    case yellow_Alien  //45
//    case Grate
//    case mod_Filter
//    case fresnel_Distortion
//    case DawnBringer
}

class VideoEdGlitchHelper: NSObject {

    
    func createGlitchFilter (_ indexpath :IndexPath ) -> GPUImageFilter?
    {
        
        var tempCustomFilter:GPUImageFilter?

        
        let selectedGlitchFilter:GlitchFilterType =  GlitchFilterType.init(rawValue: indexpath.item)!
//        debugPrint("selectedGlitchFilter is :",selectedGlitchFilter);
        
        switch selectedGlitchFilter {
        case ._None:
            
            tempCustomFilter = nil
            debugPrint("NO Filter Selected");
            tempCustomFilter = GPUImageFilter();
            return tempCustomFilter;
            
            
        case .Chromatic_Glitch1:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdChromatic_Glitch")
            tempCustomFilter?.setInteger(0, forUniformName: "Auto") // Enable - Disable
            
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
            tempCustomFilter?.setFloat(2.0, forUniformName: "power")

            
        case .Chromatic_Glitch2:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdChromatic_Glitch")
            tempCustomFilter?.setInteger(1, forUniformName: "Auto") // Enable - Disable
            
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.8, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.1, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue3")
            tempCustomFilter?.setFloat(2.0, forUniformName: "power")

            
        case .Chromatic_Glitch3:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdChromatic_Glitch")
            tempCustomFilter?.setInteger(1, forUniformName: "Auto") // Enable - Disable
            
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.8, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.1, forUniformName: "sliderValue3")
            tempCustomFilter?.setFloat(2.0, forUniformName: "power")

        case .BinaryGlitch:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdBinaryGlitch")
            
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.8, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
            tempCustomFilter?.setFloat(0.6, forUniformName: "power")
            
        case .TextureSlicer:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdTextureSlicer")
            
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.6, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")

        case .Glitchy_Colours1:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdGlitchy_Colours")
            tempCustomFilter?.setInteger(0, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
            
        case .Glitchy_Colours2:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdGlitchy_Colours")
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
            
        case .Glitchy_Colours3:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdGlitchy_Colours")
            
            tempCustomFilter?.setInteger(2, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
            
            
        case .Glitch2:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdGlitch2")
            
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")


        case .Video_Glitch:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdVideo_Glitch")
            
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")

            
        case .Glitch_Shader_A:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdGlitch_Shader_A")
            
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")

          
     /*   case .Reflection_Map:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Reflection_Map")
            
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
*/
            
        case .Test_Glitch:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdTest_Glitch")
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")

        case .RandomZoom :
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdRandomZoom")
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
        
        case .BrokenVideo1 :
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdBrokenVideo")
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
            
        case .BrokenVideo2 :
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdBrokenVideo")
            tempCustomFilter?.setInteger(2, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
            
        case .BrokenVideo3 :
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdBrokenVideo")
            tempCustomFilter?.setInteger(3, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
            
        case .BrokenVideo4 :
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VideoEdBrokenVideo")
            tempCustomFilter?.setInteger(4, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")

    /*    case .VHS_Effect :

            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "VHSEffect")
            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")

        case .Drl011_glitch:

            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "drl011_glitch")

            tempCustomFilter?.setInteger(1, forUniformName: "type")
            tempCustomFilter?.setInteger(1, forUniformName: "category")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue3")
            
            */
         /*
        case .Emix:
            
            /*let shaderURL = URL(string:"Emix.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["sliderValue"] =  1.0 ;*/
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Emix")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue")
            selectedFilter.sliderValue1 = 0.5;
            selectedFilter.SupportingSliders  = .OneSlider ;
            selectedFilter.timerSupport  = true ;
            
        case .Volumetrics:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "fake_Volumetrics")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue")
            tempCustomFilter?.setPoint(CGPoint(x: 0.5,y :0.5) , forUniformName: "iMouse")
            
            selectedFilter.sliderValue1 = 1.0;
            selectedFilter.SupportingSliders  = .OneSlider ;
            selectedFilter.FingerSupport = .Both
            selectedFilter.FingerPoint = CGPoint(x: 0.5, y: 0.5);
            
            
        case .SW_Glitch:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "SW_Glitch")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            tempCustomFilter?.setFloat(0.0, forUniformName: "sliderValue2")
            tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue3")
            selectedFilter.sliderValue1 = 0.5;
            selectedFilter.sliderValue2 = 0.0;
            selectedFilter.sliderValue3 = 1.0;
            selectedFilter.SupportingSliders  = .ThreeSlider ;
            selectedFilter.timerSupport  = true ;
            
        case .Frosted_Glass:
            
            /*let shaderURL = URL(string:"Frosted_Glass.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["sliderValue"] =  1.0 ;*/
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Frosted_Glass")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue")
            selectedFilter.sliderValue1 = 0.5;
            selectedFilter.SupportingSliders  = .OneSlider ;
            
            
            
        case .Hypnotic_Filter:
            
            /*let shaderURL = URL(string:"Hypnotic_Filter.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["iMouse"] = Position.init(point: CGPoint(x: 0.5,y :0.5));
             tempCustomFilter.uniformSettings["sliderValue"] =  1.0 ;
             tempCustomFilter.uniformSettings["sliderValue2"] =  1.0 ;
             tempCustomFilter.uniformSettings["sliderValue3"] =  1.0 ;*/
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Hypnotic_Filter")
            selectedFilter.sliderValue1 = 1.0;
            selectedFilter.sliderValue2 = 1.0;
            selectedFilter.SupportingSliders  = .TwoSlider ;
            selectedFilter.FingerSupport = .Both;
            selectedFilter.timerSupport  = true ;
            //selectedFilter.speedSupport  = true ;
            
            
        case .TV_CRT:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "TV_CRT")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue2")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue3")
            selectedFilter.sliderValue1 = 1.0;
            selectedFilter.sliderValue2 = 1.0;
            selectedFilter.SupportingSliders  = .TwoSlider ;
            selectedFilter.timerSupport  = true ;
            
            
        case .Stretch:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Stretch")
            selectedFilter.sliderValue1 = 1.0;
            selectedFilter.sliderValue2 = 1.0;
            selectedFilter.SupportingSliders  = .TwoSlider ;
            //selectedFilter.timerSupport  = true ;
            
            
        case .Wave_Ripples:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Wave_Ripples")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue2")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue3")
            selectedFilter.sliderValue1 = 1.0;
            selectedFilter.sliderValue2 = 1.0;
            selectedFilter.sliderValue3 = 1.0;
            selectedFilter.SupportingSliders  = .ThreeSlider ;
            selectedFilter.FingerSupport = .Both;
            selectedFilter.timerSupport  = true ;
            
        case .FakeRipple:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "FakeRipple")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue")
            selectedFilter.sliderValue1 = 1.0;
            selectedFilter.SupportingSliders  = .OneSlider ;
            selectedFilter.FingerSupport = .Both;
            selectedFilter.timerSupport  = true ;
            
        case .RippleEffect:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "RippleEffect")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue")
            selectedFilter.sliderValue1 = 1.0;
            selectedFilter.SupportingSliders  = .OneSlider ;
            selectedFilter.timerSupport  = true ;
            
        case .Rainier_mood:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Rainier_mood")
            //tempCustomFilter?.setFloat(0.3, forUniformName: "sliderValue")
            selectedFilter.sliderValue1 = 0.3;
            selectedFilter.SupportingSliders  = .OneSlider ;
            selectedFilter.timerSupport  = true ;
            
            
        case .Ripple_Anastadunbar:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Ripple_Anastadunbar")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue2")
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue3")
            selectedFilter.sliderValue1 = 1.0;
            selectedFilter.sliderValue2 = 1.0;
            selectedFilter.sliderValue3 = 1.0;
            selectedFilter.SupportingSliders  = .ThreeSlider ;
            selectedFilter.timerSupport  = true ;
            
        case .Rotation:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Rotation")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            selectedFilter.sliderValue1 = 0.5;
            selectedFilter.SupportingSliders  = .OneSlider ;
            // selectedFilter.timerSupport  = true ;
            
        case .BitForBit:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "BitForBit")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            selectedFilter.sliderValue1 = 0.5;
            selectedFilter.SupportingSliders  = .OneSlider ;
            //            selectedFilter.timerSupport  = true ;
            
        case .EndLess:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "EndLess")
            tempCustomFilter?.setInteger(0, forUniformName: "isSpiral")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            selectedFilter.sliderValue1 = 0.5;
            selectedFilter.sliderValue2 = 0.5;
            selectedFilter.SupportingSliders  = .OneSlider ;
            selectedFilter.FingerSupport = .Both;
            selectedFilter.timerSupport  = true ;
            selectedFilter.speedSupport  = true ;
            
            
        case .EndLess2:
            
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "EndLess")
            tempCustomFilter?.setInteger(1, forUniformName: "isSpiral")
            tempCustomFilter?.setFloat(0.5, forUniformName: "sliderValue")
            selectedFilter.sliderValue1 = 0.5;
            selectedFilter.sliderValue2 = 0.5;
            selectedFilter.SupportingSliders  = .OneSlider ;
            selectedFilter.FingerSupport = .Both;
            selectedFilter.timerSupport  = true ;
            selectedFilter.speedSupport  = true ;
            
            
        case .Glitch_Shader_B:
            
            /*let shaderURL = URL(string:"Glitch_Shader_B.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;*/
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Glitch_Shader_B")
            selectedFilter.SupportingSliders  = ._None ;
            
            //tempCustomFilter?.setFloat(1.0, forUniformName: "sliderValue")
            
        case .Broken_Pixelsort:
            
            /*let shaderURL = URL(string:"Broken_Pixelsort.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;*/
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Broken_Pixelsort")
            
        case .Broken_Pixelsort_2:
            
            /*let shaderURL = URL(string:"Broken_Pixelsort_2.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;*/
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "Broken_Pixelsort_2")
            
            
        case .CMYK_Halftone:
            
            /*let shaderURL = URL(string:"CMYK_Halftone.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;*/
            tempCustomFilter = GPUImageFilter(fragmentShaderFromFile: "CMYK_Halftone")
            
            */
            
            /*
             case .glitch_01_shampain:
             
             let shaderURL = URL(string:"glitch_01_shampain.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .GlitchPixel:
             
             let shaderURL = URL(string:"GlitchPixel.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .glitch_02_shampain:
             
             let shaderURL = URL(string:"glitch_02_shampain.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .VHS_Tape_Noise:
             
             let shaderURL = URL(string:"VHS_Tape_Noise.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .timOh:
             
             let shaderURL = URL(string:"timOh.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .Dithering_Works_Wonders:
             
             let shaderURL = URL(string:"Dithering_Works_Wonders.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .bit_of_Glitch:
             
             let shaderURL = URL(string:"bit_of_Glitch.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .yellow_Alien:
             
             let shaderURL = URL(string:"yellow_Alien.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .Grate:
             
             let shaderURL = URL(string:"Grate.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .mod_Filter:
             
             let shaderURL = URL(string:"mod_Filter.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .fresnel_Distortion:
             
             let shaderURL = URL(string:"fresnel_Distortion.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             
             case .DawnBringer:
             
             let shaderURL = URL(string:"DawnBringer.fsh", relativeTo:bundleURL)!
             tempCustomFilter = try!BasicOperation(fragmentShaderFile:shaderURL, numberOfInputs:1)
             tempCustomFilter.uniformSettings["constant"] =  1.0 ;
             */
        default:
            
            tempCustomFilter = nil
            debugPrint("NO Filter Selected");
            tempCustomFilter = GPUImageFilter();
            
            return tempCustomFilter;
            
        }


        return tempCustomFilter;
    }
    
}
