#import "GPUImageHighPassFilter2.h"

@implementation GPUImageHighPassFilter2

@synthesize filterStrength;


//- (id)initType:(int)type;
//- (id)init;


- (id)initType:(int)type
{
    if (!(self = [super init]))
    {
		return nil;
    }
    
    lowPassFilter = [[GPUImageLowPassFilter alloc] init];
    [self addFilter:lowPassFilter];
   
    
    
    if (type==1)
    {
    GPUImageSubtractBlendFilter * tempBlendFilter
        = [[GPUImageSubtractBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
    }
    else if(type==2)
    {
        GPUImageMultiplyBlendFilter * tempBlendFilter
        = [[GPUImageMultiplyBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==3)
    {
        GPUImageLightenBlendFilter * tempBlendFilter
        = [[GPUImageLightenBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==4)
    {
        GPUImageOverlayBlendFilter * tempBlendFilter
        = [[GPUImageOverlayBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==5)
    {
        GPUImageDarkenBlendFilter * tempBlendFilter
        = [[GPUImageDarkenBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==6)
    {
        GPUImageSourceOverBlendFilter * tempBlendFilter
        = [[GPUImageSourceOverBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==7)
    {
        GPUImageColorBurnBlendFilter * tempBlendFilter
        = [[GPUImageColorBurnBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        
        
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        
        
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==8)
    {
        GPUImageColorDodgeBlendFilter * tempBlendFilter
        = [[GPUImageColorDodgeBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==9)
    {
        GPUImageExclusionBlendFilter * tempBlendFilter
        = [[GPUImageExclusionBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==10)
    {
        GPUImageHardLightBlendFilter * tempBlendFilter
        = [[GPUImageHardLightBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==11)
    {
        GPUImageSoftLightBlendFilter * tempBlendFilter
        = [[GPUImageSoftLightBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }
    else if(type==12)
    {
        GPUImageColorBlendFilter * tempBlendFilter
        = [[GPUImageColorBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }    else if(type==13)
    {
        GPUImageHueBlendFilter * tempBlendFilter
        = [[GPUImageHueBlendFilter alloc] init];
        
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter,tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }
    else if(type==14)
    {
        GPUImageSaturationBlendFilter * tempBlendFilter
        = [[GPUImageSaturationBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }
    else if(type==15)
    {
        GPUImageLuminosityBlendFilter * tempBlendFilter
        = [[GPUImageLuminosityBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==16)
    {
        GPUImageChromaKeyBlendFilter * tempBlendFilter
        = [[GPUImageChromaKeyBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==17)
    {
        GPUImageAlphaBlendFilter * tempBlendFilter
        = [[GPUImageAlphaBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==18)
    {
        GPUImageNormalBlendFilter * tempBlendFilter
        = [[GPUImageNormalBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }else if(type==19)
    {
        GPUImageAddBlendFilter * tempBlendFilter
        = [[GPUImageAddBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }
    else if(type==20)
    {
        GPUImageDivideBlendFilter * tempBlendFilter
        = [[GPUImageDivideBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }
    else if(type==21)
    {
        GPUImageLinearBurnBlendFilter * tempBlendFilter
        = [[GPUImageLinearBurnBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }
    else if(type==22)
    {
        GPUImagePoissonBlendFilter * tempBlendFilter
        = [[GPUImagePoissonBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
        
    }
    
    else{
        GPUImageSubtractBlendFilter * tempBlendFilter
        = [[GPUImageSubtractBlendFilter alloc] init];
        [self addFilter:tempBlendFilter];
        [lowPassFilter addTarget:tempBlendFilter atTextureLocation:1];
        self.initialFilters = [NSArray arrayWithObjects:lowPassFilter, tempBlendFilter, nil];
        self.terminalFilter = tempBlendFilter;
    
    }
    
    NSLog(@"cusntum filter type is :%i",type);
    
    
    self.filterStrength = 0.5;
    
    return self;
}







#pragma mark -
#pragma mark Accessors

- (void)setFilterStrength:(CGFloat)newValue;
{
    lowPassFilter.filterStrength = newValue;
}

- (CGFloat)filterStrength;
{
    return lowPassFilter.filterStrength;
}

@end
