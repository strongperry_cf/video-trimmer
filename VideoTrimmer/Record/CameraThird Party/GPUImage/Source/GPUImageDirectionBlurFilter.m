#import "GPUImageDirectionBlurFilter.h"
#import "GPUImageFilter.h"
#import "GPUImageTwoInputFilter.h"
#import "GPUImageGaussianBlurFilter.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
NSString *const kGPUImageDirectionBlurFragmentShaderString = SHADER_STRING
( 
 varying highp vec2 textureCoordinate;
 varying highp vec2 textureCoordinate2;
 
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2; 
 
 uniform highp float focusLevel;
 
 //uniform highp float bottomFocusLevel;
 uniform highp float focusFallOffRate;
 uniform highp int direct;

 
 void main()
 {
     lowp vec4 sharpImageColor = texture2D(inputImageTexture, textureCoordinate);
     lowp vec4 blurredImageColor = texture2D(inputImageTexture2, textureCoordinate2);
     
     
//     lowp float blurIntensity = 1.0 - smoothstep(topFocusLevel - focusFallOffRate, topFocusLevel, textureCoordinate2.y);
     
//     blurIntensity += smoothstep(bottomFocusLevel, bottomFocusLevel + focusFallOffRate, textureCoordinate2.y);
     
//     lowp float blurIntensity = 1.0 - smoothstep(textureCoordinate2.x, topFocusLevel - focusFallOffRate, topFocusLevel);
     
//     @property(readwrite, nonatomic) int Direction;// 1 = Left ,2 =Right ,3 = Top ,4 = Bottom .


     lowp float blurIntensity= 1.0;
     
     if(direct == 1){
         
//        blurIntensity = 1.0 - smoothstep(focusLevel - focusFallOffRate, focusLevel, textureCoordinate2.x);
         
         blurIntensity = 1.0 - smoothstep(focusLevel, focusLevel + focusFallOffRate, textureCoordinate2.x);

     }else if(direct == 2){
         
//          blurIntensity = smoothstep(focusLevel, focusLevel + focusFallOffRate, textureCoordinate2.x);
         
         blurIntensity = smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel), textureCoordinate2.x);
     
     }else if(direct == 3){
         
         blurIntensity = 1.0 - smoothstep(focusLevel , focusLevel+focusFallOffRate, textureCoordinate2.y);

     }else if(direct == 4){

         blurIntensity = smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.y);
         
     }else if(direct == 5){
         
         blurIntensity = 1.0 - smoothstep(focusLevel, focusLevel + focusFallOffRate, textureCoordinate2.x);
         
         blurIntensity += 1.0 - smoothstep(focusLevel, focusLevel + focusFallOffRate, textureCoordinate2.y);
     
     }else if(direct == 6){
         
         blurIntensity = smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.x);
        
         blurIntensity += smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.y);

     }else if(direct == 7){
         
         blurIntensity = 1.0 - smoothstep(focusLevel , focusLevel+focusFallOffRate, textureCoordinate2.y);
        
         blurIntensity += smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.x);

//         blurIntensity += smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.y);

         
//         blurIntensity = 1.0 - smoothstep(focusLevel , focusLevel+focusFallOffRate, textureCoordinate2.x);

     }else if(direct == 8){
         
         blurIntensity = smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.y);

         blurIntensity += 1.0 - smoothstep(focusLevel, focusLevel + focusFallOffRate, textureCoordinate2.x);

         
//         blurIntensity += smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.x);

     }else if(direct == 9){
         
         blurIntensity = 1.0 - smoothstep(focusLevel , focusLevel+focusFallOffRate, textureCoordinate2.y);
         
        blurIntensity += smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.y);

         
     }else if(direct == 10){
     
//         blurIntensity = smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.x);
//        
//         blurIntensity +=1.0- smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.x);

         
         blurIntensity = 1.0 - smoothstep(focusLevel , focusLevel+focusFallOffRate, textureCoordinate2.x);
         
         blurIntensity += smoothstep((1.0-focusLevel)- focusFallOffRate, (1.0-focusLevel) , textureCoordinate2.x);

         
     
     }

     
     
     
     gl_FragColor = mix(sharpImageColor, blurredImageColor, blurIntensity);
//     gl_FragColor = mix(sharpImageColor, blurredImageColor, 0.5);

    }
);
#else
NSString *const kGPUImageDirectionBlurFragmentShaderString = SHADER_STRING
(
 varying vec2 textureCoordinate;
 varying vec2 textureCoordinate2;
 
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;
 
 uniform float topFocusLevel;
 uniform float bottomFocusLevel;
 uniform float focusFallOffRate;
 
 void main()
 {
     vec4 sharpImageColor = texture2D(inputImageTexture, textureCoordinate);
     vec4 blurredImageColor = texture2D(inputImageTexture2, textureCoordinate2);
     
     float blurIntensity = 1.0 - smoothstep(topFocusLevel - focusFallOffRate, topFocusLevel, textureCoordinate2.y);
     
     blurIntensity += smoothstep(bottomFocusLevel, bottomFocusLevel + focusFallOffRate, textureCoordinate2.y);
     
//          float blurIntensity = 1.0 - smoothstep(textureCoordinate2.x, topFocusLevel - focusFallOffRate, topFocusLevel);

     
     gl_FragColor = mix(sharpImageColor, blurredImageColor, blurIntensity);
 }
);
#endif

@implementation GPUImageDirectionBlurFilter

@synthesize blurRadiusInPixels;
@synthesize focusLevel = _focusLevel;
//@synthesize bottomFocusLevel = _bottomFocusLevel;
@synthesize focusFallOffRate = _focusFallOffRate;

- (id)init;
{
    if (!(self = [super init]))
    {
		return nil;
    }
    
    // First pass: apply a variable Gaussian blur
    blurFilter = [[GPUImageGaussianBlurFilter alloc] init];
    [self addFilter:blurFilter];
        
    // Second pass: combine the blurred image with the original sharp one
    tiltShiftFilter = [[GPUImageTwoInputFilter alloc] initWithFragmentShaderFromString:kGPUImageDirectionBlurFragmentShaderString];
    [self addFilter:tiltShiftFilter];
    
    // Texture location 0 needs to be the sharp image for both the blur and the second stage processing
    [blurFilter addTarget:tiltShiftFilter atTextureLocation:1];
    
    // To prevent double updating of this filter, disable updates from the sharp image side
//    self.inputFilterToIgnoreForUpdates = tiltShiftFilter;
    
    self.initialFilters = [NSArray arrayWithObjects:blurFilter, tiltShiftFilter, nil];
    self.terminalFilter = tiltShiftFilter;
    
    self.focusLevel = 0.5;
//    self.bottomFocusLevel = 0.6;
    self.Direction=1;
    self.focusFallOffRate = 0.15;
    self.blurRadiusInPixels = 7.0;
    
    return self;
}

#pragma mark -
#pragma mark Accessors

- (void)setBlurRadiusInPixels:(CGFloat)newValue;
{
    blurFilter.blurRadiusInPixels = newValue;
}

- (CGFloat)blurRadiusInPixels;
{
    return blurFilter.blurRadiusInPixels;
}

- (void)setFocusLevel:(CGFloat)newValue;
{
    _focusLevel = newValue;
    [tiltShiftFilter setFloat:newValue forUniformName:@"focusLevel"];
}

//- (void)setBottomFocusLevel:(CGFloat)newValue;
//{
//    _bottomFocusLevel = newValue;
//    [tiltShiftFilter setFloat:newValue forUniformName:@"bottomFocusLevel"];
//}

- (void)setDirection:(int)Direction;
{
    _Direction = Direction;
    [tiltShiftFilter setInteger:Direction forUniformName:@"direct"];
}

- (void)setFocusFallOffRate:(CGFloat)newValue;
{
    _focusFallOffRate = newValue;
    [tiltShiftFilter setFloat:newValue forUniformName:@"focusFallOffRate"];
}

@end
