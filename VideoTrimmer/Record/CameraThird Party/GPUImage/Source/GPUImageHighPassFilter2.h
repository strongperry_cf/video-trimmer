#import "GPUImageFilterGroup.h"
#import "GPUImageLowPassFilter.h"



#import "GPUImageSubtractBlendFilter.h"
#import "GPUImageMultiplyBlendFilter.h"
#import "GPUImageLightenBlendFilter.h"

#import "GPUImageOverlayBlendFilter.h"
#import "GPUImageDarkenBlendFilter.h"
#import "GPUImageSourceOverBlendFilter.h"
#import "GPUImageColorBurnBlendFilter.h"
#import "GPUImageColorDodgeBlendFilter.h"
#import "GPUImageScreenBlendFilter.h"
#import "GPUImageExclusionBlendFilter.h"
#import "GPUImageHardLightBlendFilter.h"
#import "GPUImageSoftLightBlendFilter.h"
#import "GPUImageColorBlendFilter.h"
#import "GPUImageHueBlendFilter.h"
#import "GPUImageSaturationBlendFilter.h"
#import "GPUImageLuminosityBlendFilter.h"
#import "GPUImageChromaKeyBlendFilter.h"
#import "GPUImageAlphaBlendFilter.h"
#import "GPUImageNormalBlendFilter.h"
#import "GPUImageAddBlendFilter.h"
#import "GPUImageDivideBlendFilter.h"
#import "GPUImageLinearBurnBlendFilter.h"
#import "GPUImagePoissonBlendFilter.h"
#import "GPUImageColorInvertFilter.h"




@interface GPUImageHighPassFilter2 : GPUImageFilterGroup
{
    GPUImageLowPassFilter *lowPassFilter;
    
//    GPUImageDifferenceBlendFilter *differenceBlendFilter;
//    GPUImageSubtractBlendFilter *tempBlendFilter;

//    GPUImageMultiplyBlendFilter * tempBlendFilter;
    
//    GPUImageLightenBlendFilter * tempBlendFilter;

    
    
}

// This controls the degree by which the previous accumulated frames are blended and then subtracted from the current one. This ranges from 0.0 to 1.0, with a default of 0.5.

- (id)initType:(int)type;

@property(readwrite, nonatomic) CGFloat filterStrength;

@end
