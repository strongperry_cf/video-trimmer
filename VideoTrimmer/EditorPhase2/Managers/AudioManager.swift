

import AVFoundation


class AudioManager: NSObject {
    
    func getAudioComposition(with audioModel: AudioModel) -> AVMutableComposition? {
        let asset = AVAsset(url: audioModel.audioUrl)
        guard let audioTrack = asset.tracks(withMediaType: .audio).first else {return nil}
        let startTime = CMTime(seconds: Double(audioModel.beginTime), preferredTimescale: asset.duration.timescale)
        let duration = CMTime(seconds: Double(audioModel.duration - audioModel.beginTime), preferredTimescale: asset.duration.timescale)
         let timeRange = CMTimeRange(start: startTime, duration: duration)
        

        let audioComposition = AVMutableComposition()
        var audioCompositionTrack = audioComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        do {
            try audioCompositionTrack?.insertTimeRange(timeRange, of: audioTrack, at: .zero)
//            try audioCompositionTrack?.insertTimeRange(timeRange, of: audioTrack, at: .zero)
            
        } catch {
            return nil
        }
        return audioComposition
    }
    
}
