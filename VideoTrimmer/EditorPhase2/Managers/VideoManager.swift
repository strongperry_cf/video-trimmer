
import UIKit

enum TrimAngle: Double {
    
    case kZero = 0, k90 = 90, k180 = 180, k270 = 270
    
    var cgFloat: CGFloat {
        switch self {
        case .kZero:
            return 0
        case .k90:
            return .pi / 2
        case .k180:
            return .pi
        case .k270:
            return .pi * 1.5
        }
    }
}


class VideoManager: NSObject{
    public static let shared = VideoManager()
    
    var exportSession : AVAssetExportSession!
    func exportVideoWith(mixComposition: AVMutableComposition,videoComposition: AVVideoComposition,range: CMTimeRange,progressBlock:((_ progress : Float) -> ())?,complitionBlock:@escaping (_ url : URL?) -> ()){
        
        let outputPath = DirectoryManager().createFileWith(fileName: .finalVideo)
        exportSession = AVAssetExportSession.init(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exportSession?.outputFileType = AVFileType.mp4
        exportSession?.outputURL = URL.init(fileURLWithPath: outputPath)
        exportSession?.videoComposition = videoComposition
        exportSession?.timeRange = range
        var exportProgress: Float = 0
        let queue = DispatchQueue(label: "Export Progress Queue")
        var exportProgressBarTimer = Timer() // initialize timer
        queue.async {[weak self] in
            exportProgressBarTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
                exportProgress = self?.exportSession?.progress ?? 0.0
                progressBlock?(exportProgress)
            })
        }
        exportSession?.exportAsynchronously(completionHandler: {[weak self] in
            switch(self?.exportSession?.status){
            case .failed?:
                exportProgressBarTimer.invalidate()
                print("Failed \(String(describing: self!.exportSession?.error?.localizedDescription))")
                self?.exportSession?.cancelExport()
                complitionBlock(nil)
                break
            case .completed?:
                exportProgressBarTimer.invalidate()
               
                self?.exportSession = nil
                print("Completed \(String(describing: outputPath))")
                let videoURL = URL(fileURLWithPath: outputPath)
                complitionBlock(videoURL)
                break
            case .exporting?:
                print("exporting")
                break
            case .cancelled?:
                exportProgressBarTimer.invalidate()
                complitionBlock(nil)
                break
            case .waiting?:
                break
            default:
                break
            }
        })
    }
    
    func cancelExport(){
        if exportSession != nil{
            exportSession.cancelExport()
            exportSession = nil
        }
    }
    
    static func videoCompositionWith(videoUrl: URL,playerFrame: CGRect,willShowWaterMark: Bool,startTime: CGFloat,duration: CGFloat,rotationAngle: CGFloat, complitionBlock:@escaping (_ url : URL?) -> ())  {
        
       let visibleRect = playerFrame
        let videoAsset = AVURLAsset(url: videoUrl, options: nil)
        // Get video track
        guard let videoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).first else {
            return
        }
        
        let finalSeconds = duration
        let finalDurationWithSpeed = CMTime(seconds: Double(finalSeconds), preferredTimescale: videoAsset.duration.timescale)
        
        // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        let mixComposition = AVMutableComposition()
        // Init video & audio composition track
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        let currentTime:CMTime = CMTime.zero;
        let previewTime:CMTime =  videoAsset.duration
        do {
            // ***** 1
            // Add video track to video composition at specific time
            try videoCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: currentTime, duration: previewTime), of: videoTrack, at: currentTime)
            
        } catch {
            print("Load track error")
        }
        
        let naturalSize = videoTrack.naturalSize
        let videoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        videoComposition.renderScale  = 1.0
        videoComposition.renderSize = naturalSize
        let frontLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
        var customTrans = videoAsset.tracks(withMediaType: .video).first?.preferredTransform
        //for Time-lapse videos
        if (customTrans?.b == 1.0 && (customTrans?.c)! == -1.0 &&  (customTrans?.tx)! == 0.0 &&   (customTrans?.ty)! == 0.0 )
        {
            customTrans =  customTrans?.translatedBy(x: 0.0, y: -naturalSize.width);
            frontLayerInstruction.setTransform(customTrans!.rotated(by: rotationAngle), at: CMTime.zero)
        }else{
            frontLayerInstruction.setTransform(videoTrack.preferredTransform.rotated(by: rotationAngle), at: CMTime.zero)
        }
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: finalDurationWithSpeed)
        mainInstruction.layerInstructions = [frontLayerInstruction]
        videoComposition.instructions = [mainInstruction]
        let videoLayer = CALayer.init()
        videoLayer.frame = CGRect(x: 0, y: 0, width: naturalSize.width, height: naturalSize.height)
        if willShowWaterMark{
            let textLayer  = CATextLayer()
            let ratio = naturalSize.height / visibleRect.height
            var textFrame : CGRect = .zero
            let size = 18.0 * ratio
            textLayer.font = UIFont(name: "SFUIDisplay-Bold", size: size)
            if let font =  UIFont(name: "SFUIDisplay-Bold", size: size){
                let size = NSAttributedString(string: "Drunk Cam", attributes:
                    [NSAttributedString.Key.font: font]).size()
                textFrame = CGRect(x: naturalSize.width - (size.width * 1.05),y: naturalSize.height - (size.height * 1.05), width: size.width, height:size.height)
            }
            textLayer.frame = textFrame
            textLayer.string = "Drunk Cam"
            textLayer.foregroundColor = UIColor.white.cgColor
            textLayer.fontSize = size
            textLayer.alignmentMode = .center
            textLayer.shouldRasterize = true
            textLayer.rasterizationScale = UIScreen.main.scale
            videoLayer.addSublayer(textLayer)
        }
        mixComposition.scaleTimeRange(CMTimeRange(start: .zero, end: videoAsset.duration), toDuration: finalDurationWithSpeed)
        let outputPath = DirectoryManager().createFileWith(fileName: .finalVideo)
        var exportSession = AVAssetExportSession.init(asset: mixComposition, presetName: AVAssetExportPreset1920x1080)
        exportSession?.outputFileType = AVFileType.mov
        exportSession?.outputURL = URL.init(fileURLWithPath: outputPath)
        exportSession?.videoComposition = videoComposition
        var exportProgress: Float = 0
        let queue = DispatchQueue(label: "Export Progress Queue")
        queue.async(execute: {() -> Void in
            while exportSession != nil {
                exportProgress = (exportSession?.progress)!
                print("current progress == \(exportProgress)")
            }
        })
        
        exportSession?.exportAsynchronously(completionHandler: {
            switch(exportSession?.status){
            case .failed?:
                print("Failed \(String(describing: exportSession?.error))")
                exportSession?.cancelExport()
                complitionBlock(nil)
                break
            case .completed?:
                exportSession = nil
                print("Completed \(String(describing: outputPath))")
                let videoURL = URL(fileURLWithPath: outputPath)
                complitionBlock(videoURL)
                break
            case .exporting?:
                break
            case .cancelled?:
                complitionBlock(nil)
                break
            case .waiting?:
                break
            default:
                break
            }
        })
    }
    
    
    static func getVideoSize(asset: AVAsset) -> CGSize{
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        var videoSize = assetTrack.naturalSize
        videoSize = videoSize.applying(assetTrack.preferredTransform)
        videoSize = CGSize(width: abs(videoSize.width), height: abs(videoSize.height))
        return videoSize
    }
    
    static func corrTransform(asset: AVAsset, standardSize: CGSize ) -> CGAffineTransform {
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        var finalTransform = assetTrack.preferredTransform
        var aspectFillRatio: CGFloat = 1
        var videoTrackSize = assetTrack.naturalSize.applying(assetTrack.preferredTransform)
        videoTrackSize = CGSize(width: abs(videoTrackSize.width), height: abs(videoTrackSize.height))
        
        if videoTrackSize.height > videoTrackSize.width {
            aspectFillRatio = standardSize.height / videoTrackSize.height
        } else {
            aspectFillRatio = standardSize.width / videoTrackSize.width
        }
        let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
        let posX = standardSize.width/2 - (videoTrackSize.width * aspectFillRatio)/2
        let posY = standardSize.height/2 - (videoTrackSize.height * aspectFillRatio)/2
        let moveFactor = CGAffineTransform(translationX: posX, y: posY)
        let concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
        finalTransform = concat
        return finalTransform
    }
    
    func correctTransform(asset: AVAsset, standardSize:CGSize ) -> (CGAffineTransform, Bool) {
        
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        let transform = assetTrack.preferredTransform
        var finalTransform = assetTrack.preferredTransform
        
        let assetInfo = orientationFromTransform(transform: transform)
        
        var aspectFillRatio:CGFloat = 1
        
        var videoSize = assetTrack.naturalSize;
        videoSize = videoSize.applying(assetTrack.preferredTransform)
        videoSize = CGSize(width: abs(videoSize.width), height: abs(videoSize.height))
        
      //  if videoSize.height > videoSize.width //Aspect Fit
        if videoSize.height < videoSize.width  //Aspect Fill
        {
            aspectFillRatio = standardSize.height / videoSize.height
        }
        else {
            aspectFillRatio = standardSize.width / videoSize.width
        }
        
        if assetInfo.isPortrait {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            
            let posX = standardSize.width/2 - (videoSize.width * aspectFillRatio)/2
            let posY = standardSize.height/2 - (videoSize.height * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor);
            
        } else {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            
            let posX = standardSize.width/2 - (videoSize.width * aspectFillRatio)/2
            let posY = standardSize.height/2 - (videoSize.height * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            
            let concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
            
            if assetInfo.orientation == .down {
                // let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                //concat = fixUpsideDown.concatenating(scaleFactor).concatenating(moveFactor)
            }
            
            finalTransform = concat;
        }
        
        return (finalTransform,assetInfo.isPortrait) ;
    }
    
    func orientationFromTransform(transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
            assetOrientation = .up
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
        }
        return (assetOrientation, isPortrait)
    }
    
    static func getComposition(videoAsset: AVAsset,from startTime: CGFloat,to endTime: CGFloat,rotation: TrimAngle = .kZero) -> VideoModel?{
        // Get video track
        let asset = videoAsset
        
        guard let videoTrack = asset.tracks(withMediaType: AVMediaType.video).first else {
            return nil
        }
        
        var audioTrack: AVAssetTrack?
        if let aTrack = asset.tracks(withMediaType: AVMediaType.audio).first{
           audioTrack = aTrack
        }
        // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        
        
        let mixComposition = AVMutableComposition()
        let sTime = CMTime(seconds: Double(startTime), preferredTimescale: videoAsset.duration.timescale)
        let eTime = CMTime(seconds: Double(endTime), preferredTimescale: videoAsset.duration.timescale)
        let timeRange = CMTimeRange(start: sTime, end: eTime)
        
        
        // Init video & audio composition track
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        do {
            // ***** 1
            // Add video track to video composition at specific time
            try videoCompositionTrack?.insertTimeRange(timeRange, of: videoTrack, at: .zero)
            // ***** 2
            // Add video track to video composition at specific time
            if audioTrack != nil{
                let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: Int32(1055))
                try audioCompositionTrack?.insertTimeRange(timeRange, of: audioTrack!, at: .zero)
            }
  
        } catch {
            print("Load track error")
        }
        var naturalSize = videoTrack.naturalSize
        naturalSize = naturalSize.applying(videoTrack.preferredTransform)
        naturalSize = CGSize(width: abs(naturalSize.width), height: abs(naturalSize.height))
        let frontLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
      
        //for Time-lapse videos
        let (transform,  isPortrait) = VideoManager.shared.correctTransform(asset: asset, standardSize: naturalSize)
        let newTransform = VideoManager.shared.rotateComposition(for: transform, isP: isPortrait, with: rotation, size: naturalSize)
        frontLayerInstruction.setTransform(newTransform.0, at: .zero)
        
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        videoComposition.renderScale  = 1.0
        videoComposition.renderSize = newTransform.1
        mixComposition.naturalSize = newTransform.1
    
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange =  (videoCompositionTrack?.timeRange)!
        mainInstruction.layerInstructions = [frontLayerInstruction]
        videoComposition.instructions = [mainInstruction]
        
        
        var videoModel = VideoModel()
        videoModel.composition = mixComposition
        videoModel.timeRange = timeRange
        videoModel.videoComposition = videoComposition
        videoModel.rotationAngle = rotation.cgFloat
        return videoModel
    }
    
    
  /*  static func getCompositionsFrom(asset: AVAsset, containerBgColor: UIColor, containerBounds: CGRect, startTime: CGFloat? = nil, endTime: CGFloat? = nil,isNewAudio: Bool, audioAssetModel: [AudioModel]) -> (AVMutableComposition, AVMutableVideoComposition)? {
        // Mutable Composition
        let mixComposition = AVMutableComposition()
        
        let videoAsset = asset
        
        
        // For trimming
        var timeRange = CMTimeRange(start: CMTime.zero, duration: videoAsset.duration)
        if startTime != nil && endTime != nil {
            let sTime = CMTime(seconds: Double(startTime!), preferredTimescale: videoAsset.duration.timescale)
            let eTime = CMTime(seconds: Double(endTime!), preferredTimescale: videoAsset.duration.timescale)
            timeRange = CMTimeRange(start: sTime, end: eTime)
        }
        // Video and Audio tracks
        guard let videoTrack = videoAsset.tracks(withMediaType: .video).first else { return nil }
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        do {
            try videoCompositionTrack?.insertTimeRange(timeRange, of: videoTrack, at: .zero)
            if audioAssetModel.count > 0 {
                let audioModel = audioAssetModel[0]
                let audioAsset = AVAsset(url: audioModel.audioUrl)
                if let audioTrack = audioAsset.tracks(withMediaType: .audio).first {
                    let startTime = CMTime(seconds: Double(audioModel.beginTime), preferredTimescale: asset.duration.timescale)
                    var durationSeconds = Double(audioModel.duration - audioModel.beginTime)
                    if durationSeconds > asset.duration.seconds {
                        let difference = durationSeconds - asset.duration.seconds
                        durationSeconds -= difference
                    }
                    let duration = CMTime(seconds: durationSeconds , preferredTimescale: asset.duration.timescale)
                    let audiTimeRange = CMTimeRange(start: startTime, duration: duration)
                    let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: audioTrack.trackID)
                    try audioCompositionTrack?.insertTimeRange(audiTimeRange, of: audioTrack, at: .zero)
                }
            }else{
                
                print("do here")
                
                if let audioTrack = videoAsset.tracks(withMediaType: .video).first {
                    
                    
                    let startTime = CMTime.zero
                    let durationSeconds = videoAsset.duration.seconds
                    let duration = CMTime(seconds: durationSeconds , preferredTimescale: asset.duration.timescale)
                    let audiTimeRange = CMTimeRange(start: startTime, duration: duration)
                    let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: audioTrack.trackID)
                    try audioCompositionTrack?.insertTimeRange(audiTimeRange, of: audioTrack, at: .zero)
                    
                    
                }
            }
        } catch { return nil }
        
        ////////////////////////////////////////////////////////////////////
        /////////////End CODE FOR ADDING AUDIO AND VIDEO TRACKS/////////////
        ////////////////////////////////////////////////////////////////////
        
        // Video Composition Instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        let compositonColor = containerBgColor
        let (r, g, b, a) = compositonColor.getComponents()
        let color = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [r, g, b, a])
        mainInstruction.backgroundColor = color
        mainInstruction.timeRange = CMTimeRangeMake(start: .zero, duration: videoAsset.duration)
        // Video Composition Layer Instruction
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
        let trackTransform = videoTrack.preferredTransform
        let assetInfo = VideoManager.shared.orientationFromTransform(transform: trackTransform)
        // Size calulations
        let defaultSize = CGSize(width: 1920, height: 1080)
        var outputSize = CGSize.zero
        var videoSize = videoTrack.naturalSize
        if assetInfo.isPortrait {
            videoSize.width =  videoTrack.naturalSize.height
            videoSize.height = videoTrack.naturalSize.width
        }
        if videoSize.height > outputSize.height {
            outputSize.height = videoSize.height
        }
        if videoSize.width > outputSize.width {
            outputSize.width = videoSize.width
        }
        if outputSize.width == 0 || outputSize.height == 0 {
            outputSize = defaultSize
        }
        let trans = VideoManager.shared.correctTransformWith(asset: videoAsset, standardSize: outputSize)
        layerInstruction.setTransform(trans, at: .zero)
        mainInstruction.layerInstructions = [layerInstruction]
        // video composition
        mixComposition.naturalSize = outputSize
        let videoComposition = AVMutableVideoComposition()
        print("RENDER SIZE>>", outputSize)
        videoComposition.renderSize = outputSize
        videoComposition.instructions = [mainInstruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        // return compositions
        return(mixComposition, videoComposition)
    }
    */
    
    static func getCompositionsFrom(asset: AVAsset, isNewAudio: Bool, audioAssetModel: [AudioModel]) -> (AVMutableComposition, AVMutableVideoComposition)? {
        // Mutable Composition
        let mixComposition = AVMutableComposition()
        
        let videoAsset = asset
        
        
        // For trimming
        let timeRange = CMTimeRange(start: CMTime.zero, duration: videoAsset.duration)
        // Video and Audio tracks
        guard let videoTrack = videoAsset.tracks(withMediaType: .video).first else { return nil }
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        do {
            try videoCompositionTrack?.insertTimeRange(timeRange, of: videoTrack, at: .zero)
            if audioAssetModel.count > 0 {
                let audioModel = audioAssetModel[0]
                let audioAsset = AVAsset(url: audioModel.audioUrl)
                if let audioTrack = audioAsset.tracks(withMediaType: .audio).first {
                    let startTime = CMTime(seconds: Double(audioModel.beginTime), preferredTimescale: asset.duration.timescale)
                    var durationSeconds = Double(audioModel.duration - audioModel.beginTime)
                    if durationSeconds > asset.duration.seconds {
                        let difference = durationSeconds - asset.duration.seconds
                        durationSeconds -= difference
                    }
                    let duration = CMTime(seconds: durationSeconds , preferredTimescale: asset.duration.timescale)
                    let audiTimeRange = CMTimeRange(start: startTime, duration: duration)
                    let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: audioTrack.trackID)
                    try audioCompositionTrack?.insertTimeRange(audiTimeRange, of: audioTrack, at: .zero)
                }
            }else{
                
                print("do here")
                
                if let audioTrack = videoAsset.tracks(withMediaType: .audio).first {
                    
                    
                    let startTime = CMTime.zero
                    let durationSeconds = videoAsset.duration.seconds
                    let duration = CMTime(seconds: durationSeconds , preferredTimescale: asset.duration.timescale)
                    let audiTimeRange = CMTimeRange(start: startTime, duration: duration)
                    let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: audioTrack.trackID)
                    try audioCompositionTrack?.insertTimeRange(audiTimeRange, of: audioTrack, at: .zero)
                    
                    
                }
            }
        } catch { return nil }
        
        ////////////////////////////////////////////////////////////////////
        /////////////End CODE FOR ADDING AUDIO AND VIDEO TRACKS/////////////
        ////////////////////////////////////////////////////////////////////
        
        // Video Composition Instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: .zero, duration: videoAsset.duration)
        // Video Composition Layer Instruction
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
        let trackTransform = videoTrack.preferredTransform
        let assetInfo = VideoManager.shared.orientationFromTransform(transform: trackTransform)
        // Size calulations
        let defaultSize = CGSize(width: 1920, height: 1080)
        var outputSize = CGSize.zero
        var videoSize = videoTrack.naturalSize
        if assetInfo.isPortrait {
            videoSize.width =  videoTrack.naturalSize.height
            videoSize.height = videoTrack.naturalSize.width
        }
        if videoSize.height > outputSize.height {
            outputSize.height = videoSize.height
        }
        if videoSize.width > outputSize.width {
            outputSize.width = videoSize.width
        }
        if outputSize.width == 0 || outputSize.height == 0 {
            outputSize = defaultSize
        }
        let trans = VideoManager.shared.correctTransformWith(asset: videoAsset, standardSize: outputSize)
        layerInstruction.setTransform(trans, at: .zero)
        mainInstruction.layerInstructions = [layerInstruction]
        // video composition
        mixComposition.naturalSize = outputSize
        let videoComposition = AVMutableVideoComposition()
        print("RENDER SIZE>>", outputSize)
        videoComposition.renderSize = outputSize
        videoComposition.instructions = [mainInstruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        // return compositions
        return(mixComposition, videoComposition)
    }
    
    
    
    func generateThumbnail(asset: AVAsset,at time: CMTime = .zero,of size: CGSize = .zero) -> UIImage? {
        do {
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            if size != .zero{
                imgGenerator.maximumSize = size
            }
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    
    func generateThumbnails(asset: AVAsset ,block: @escaping(([UIImage]?)->())) {
        
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        
        let duration: Int = Int(ceil(asset.duration.seconds))
        let arayTiming = NSMutableArray()
        let arayThumbs = NSMutableArray()
        for i in 0..<duration {
            let time: CMTime = CMTimeMake(value: Int64(i), timescale: 1)
            arayTiming.add(NSValue(time: time))
        }
        
        let handler : AVAssetImageGeneratorCompletionHandler? = { requestedTime, im, actualTime, result, error in
            if result != .succeeded {
                if let error = error {
                    debugPrint("couldn't generate thumbnail, error:\(error)")
                }
                arayThumbs.add(UIImage(named: "") as Any)
            }
            if result == .succeeded {
                arayThumbs.add(UIImage(cgImage: im!))
            }
            
            if arayThumbs.count == arayTiming.count{
                block((arayThumbs as! [UIImage]))
            }
        }
        
        let maxSize = CGSize(width: 128, height: 128)
        generator.maximumSize = maxSize
        if let handler2 = handler {
            generator.generateCGImagesAsynchronously(forTimes: arayTiming as! [NSValue], completionHandler: handler2)
        }
    }
    
    func rotateComposition(for naturalTransform: CGAffineTransform, isP: Bool = false, with angle: TrimAngle = .kZero, size: CGSize) -> (CGAffineTransform, CGSize) {
        
        var trans = naturalTransform
        let w = size.width
        let h = size.height
        var outSize = size
        
        switch angle {
        case .kZero: break
        case .k90:
            if isP {
                trans = trans.translatedBy(x: w, y: -w * (w / h))
                trans = trans.rotated(by: angle.cgFloat)
            } else {
                trans = trans.translatedBy(x: h, y: 0)
                trans = trans.rotated(by: angle.cgFloat)
            }
            outSize = CGSize(width: h, height: w)
        case .k180:
            if isP {
                trans = trans.translatedBy(x: h, y: w)
                trans = trans.rotated(by: angle.cgFloat)
            } else {
                trans = trans.translatedBy(x: w, y: h)
                trans = trans.rotated(by: angle.cgFloat)
            }
        case .k270:
            trans = trans.translatedBy(x: 0, y: w)
            outSize = CGSize(width: h, height: w)
            trans = trans.rotated(by: angle.cgFloat)
        }
        return (trans, outSize)
    }
    
   /* static func getCompositionsFrom(asset: AVAsset, containerBgColor: UIColor, containerBounds: CGRect, startTime: CGFloat? = nil, endTime: CGFloat? = nil, trimAngle: TrimAngle = .kZero,isNewAudio: Bool) -> (AVMutableComposition, AVMutableVideoComposition)? {
        // Mutable Composition
        let mixComposition = AVMutableComposition()
        
        let videoAsset = asset
        
        ////////////////////////////////////////////////////////////////////
        /////////////CODE FOR ADDING AUDIO AND VIDEO TRACKS/////////////
        ////////////////////////////////////////////////////////////////////
        
        // For trimming
        var timeRange = CMTimeRange(start: CMTime.zero, duration: videoAsset.duration)
        if startTime != nil && endTime != nil {
            let sTime = CMTime(seconds: Double(startTime!), preferredTimescale: videoAsset.duration.timescale)
            let eTime = CMTime(seconds: Double(endTime!), preferredTimescale: videoAsset.duration.timescale)
            timeRange = CMTimeRange(start: sTime, end: eTime)
        }
        // Video and Audio tracks
        guard let videoTrack = videoAsset.tracks(withMediaType: .video).first else { return nil }
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        do {
            try videoCompositionTrack?.insertTimeRange(timeRange, of: videoTrack, at: .zero)
      
            var audioTrack: AVAssetTrack!

            if audioAssetModel.count > 0 {
                for model in audioAssetModel {
                    let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: Int32(model.trackId))
                    let audioAsset = AVAsset(url: model.audioUrl)
                    audioTrack = audioAsset.tracks(withMediaType: .audio).first
                    let startTime = CMTimeMakeWithSeconds(Float64(model.startTime), preferredTimescale: Int32(NSEC_PER_SEC))
                    let duration = CMTimeMakeWithSeconds(Float64(model.duration), preferredTimescale: Int32(NSEC_PER_SEC))
                    let audioStartTime = CMTimeMakeWithSeconds(Float64(model.audioStart), preferredTimescale: Int32(NSEC_PER_SEC))
                    let audioTR = CMTimeRange(start: audioStartTime, duration: duration)
                    try audioCompositionTrack?.insertTimeRange(audioTR, of: audioTrack, at: startTime)
                }
            }
            
            if isNewAudio {
                audioTrack = asset.tracks(withMediaType: .audio).first
                if audioTrack != nil {
                    let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: Int32(1055))
                    try audioCompositionTrack?.insertTimeRange(timeRange, of: audioTrack, at: .zero)
                }
            }
            
        } catch { return nil }
        
        ////////////////////////////////////////////////////////////////////
        /////////////End CODE FOR ADDING AUDIO AND VIDEO TRACKS/////////////
        ////////////////////////////////////////////////////////////////////
        
        // Video Composition Instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        let compositonColor = containerBgColor
        let (r, g, b, a) = compositonColor.getComponents()
        let color = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [r, g, b, a])
        mainInstruction.backgroundColor = color
        mainInstruction.timeRange = CMTimeRangeMake(start: .zero, duration: videoAsset.duration)
        // Video Composition Layer Instruction
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
        let trackTransform = videoTrack.preferredTransform
        let assetInfo = VideoManager.shared.orientationFromTransform(transform: trackTransform)
        // Size calulations
        let defaultSize = CGSize(width: 1920, height: 1080)
        var outputSize = CGSize.zero
        var videoSize = videoTrack.naturalSize
        if assetInfo.isPortrait {
            videoSize.width =  videoTrack.naturalSize.height
            videoSize.height = videoTrack.naturalSize.width
        }
        if videoSize.height > outputSize.height {
            outputSize.height = videoSize.height
        }
        if videoSize.width > outputSize.width {
            outputSize.width = videoSize.width
        }
        if outputSize.width == 0 || outputSize.height == 0 {
            outputSize = defaultSize
        }
        let trans = VideoManager.shared.correctTransformWith(asset: videoAsset, standardSize: outputSize)
        layerInstruction.setTransform(trans, at: .zero)
        mainInstruction.layerInstructions = [layerInstruction]
        // video composition
        mixComposition.naturalSize = outputSize
        let videoComposition = AVMutableVideoComposition()
        print("RENDER SIZE>>", outputSize)
        videoComposition.renderSize = outputSize
        videoComposition.instructions = [mainInstruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        // return compositions
        return(mixComposition, videoComposition)
    } */
    
    /*
    static func getCompositionsWithStickersAndBackground(asset: AVAsset, containerBgColor: UIColor, videoBounds: CGRect,mainContainerBounds: CGRect, trimAngle: TrimAngle = .kZero,stickersData: [StickerLayerData]) -> (AVMutableComposition, AVMutableVideoComposition)? {
        // Mutable Composition
        let mixComposition = AVMutableComposition()
        let videoAsset = asset
        
        ////////////////////////////////////////////////////////////////////
        /////////////CODE FOR ADDING AUDIO AND VIDEO TRACKS/////////////
        ////////////////////////////////////////////////////////////////////
        
        let timeRange = CMTimeRange(start: .zero, duration: videoAsset.duration)
        // Video and Audio tracks
        guard let videoTrack = videoAsset.tracks(withMediaType: .video).first else { return nil }
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let isAudio = videoAsset.tracks(withMediaType: .audio).count > 0
        do {
            try videoCompositionTrack?.insertTimeRange(timeRange, of: videoTrack, at: .zero)
            var audioTrack: AVAssetTrack!
           if isAudio{
                let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
                audioTrack = videoAsset.tracks(withMediaType: .audio).first
                try audioCompositionTrack?.insertTimeRange(timeRange, of: audioTrack, at: .zero)
            }
        } catch { return nil }
        
        ////////////////////////////////////////////////////////////////////
        /////////////End CODE FOR ADDING AUDIO AND VIDEO TRACKS/////////////
        ////////////////////////////////////////////////////////////////////
        
        // Video Composition Instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        let compositonColor = containerBgColor
        let (r, g, b, a) = compositonColor.getComponents()
        let color = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [r, g, b, a])
        mainInstruction.backgroundColor = color
        mainInstruction.timeRange = CMTimeRangeMake(start: .zero, duration: videoAsset.duration)
        // Video Composition Layer Instruction
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
        let trackTransform = videoTrack.preferredTransform
        let assetInfo = VideoManager.shared.orientationFromTransform(transform: trackTransform)
        // Size calulations
        var videoSize = videoTrack.naturalSize
        if assetInfo.isPortrait {
            videoSize.width =  videoTrack.naturalSize.height
            videoSize.height = videoTrack.naturalSize.width
        }
        
        let ratio = videoSize.width / videoBounds.width
        videoSize.width = videoBounds.width * ratio
        videoSize.height = videoBounds.height * ratio
        // MAKE SIZE DIVISIBLE BY 4
        var newOutputWidth = Int(videoSize.width)
        var newOutputHeight = Int(videoSize.height)
        while newOutputWidth % 4 > 0 {
            newOutputWidth += 1
        }
        while newOutputHeight % 4 > 0 {
            newOutputHeight += 1
        }
    
        let outputSize = CGSize(width: newOutputWidth, height: newOutputHeight)
        
        let trans = VideoManager.shared.correctTransformWith(asset: videoAsset, standardSize: outputSize)
        layerInstruction.setTransform(trans, at: .zero)
        mainInstruction.layerInstructions = [layerInstruction]
        mixComposition.naturalSize = outputSize
        
        //Add Stickers Layer
        let parentUILayer = CALayer()
        parentUILayer.isGeometryFlipped = true
        parentUILayer.frame = CGRect(origin: .zero, size: outputSize)
        let videoLayer = CALayer.init()
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.frame = parentUILayer.frame
        parentUILayer.backgroundColor = UIColor.clear.cgColor
        parentUILayer.addSublayer(videoLayer)
        let videoContainerRect = AVMakeRect(aspectRatio: videoBounds.size, insideRect: mainContainerBounds)
        let widthRatio: CGFloat = outputSize.width / videoContainerRect.width
        for model in stickersData {
            if model.type != .audio {
               // Edited By Vikas
                
//                let newSticker = Helper.sharedInstance.getTextLayer(from: model, scaleFactor: widthRatio, layerOpacity: model.opacity,isExport : true)
//                parentUILayer.addSublayer(newSticker)
                if model.type == .gif {
                    let newSticker = Helper.sharedInstance.getStickerLayer(myModel: model, scaleFactor: widthRatio, layerOpacity: model.opacity)
                    parentUILayer.addSublayer(newSticker)
                } else {
                    let newSticker = Helper.sharedInstance.getTextLayer(from: model, scaleFactor: widthRatio, layerOpacity: model.opacity,isExport : true)
                    parentUILayer.addSublayer(newSticker)
                }
            }
        }
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool.init(postProcessingAsVideoLayer: videoLayer, in: parentUILayer)
        videoComposition.renderSize = outputSize
        videoComposition.instructions = [mainInstruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        // return compositions
        return(mixComposition, videoComposition)
    } */
    
    
    func correctTransformWith(asset: AVAsset, standardSize: CGSize) -> CGAffineTransform {
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        var finalTransform = assetTrack.preferredTransform
        var aspectFillRatio: CGFloat = 1
        var videoTrackSize = assetTrack.naturalSize.applying(assetTrack.preferredTransform)
        videoTrackSize = CGSize(width: abs(videoTrackSize.width), height: abs(videoTrackSize.height))
        let maxAspectRatio = standardSize.width / standardSize.height
        let videoAspectRatio = videoTrackSize.width / videoTrackSize.height
        if maxAspectRatio > videoAspectRatio {
            aspectFillRatio = standardSize.height / videoTrackSize.height
        } else {
            aspectFillRatio = standardSize.width / videoTrackSize.width
        }
        let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
        let posX = standardSize.width/2 - (videoTrackSize.width * aspectFillRatio)/2
        let posY = standardSize.height/2 - (videoTrackSize.height * aspectFillRatio)/2
        let moveFactor = CGAffineTransform(translationX: posX, y: posY)
        let concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
        finalTransform = concat
        return finalTransform
    }
    
    
    
    
    func corrTransform(asset: AVAsset, standardSize: CGSize ) -> CGAffineTransform {
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        var finalTransform = assetTrack.preferredTransform
        var aspectFillRatio: CGFloat = 1
        var videoTrackSize = assetTrack.naturalSize.applying(assetTrack.preferredTransform)
        videoTrackSize = CGSize(width: abs(videoTrackSize.width), height: abs(videoTrackSize.height))
        if videoTrackSize.height > videoTrackSize.width {
            aspectFillRatio = standardSize.height / videoTrackSize.height
        } else {
            aspectFillRatio = standardSize.width / videoTrackSize.width
        }
        let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
        let posX = standardSize.width/2 - (videoTrackSize.width * aspectFillRatio)/2
        let posY = standardSize.height/2 - (videoTrackSize.height * aspectFillRatio)/2
        let moveFactor = CGAffineTransform(translationX: posX, y: posY)
        let concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
        finalTransform = concat
        return finalTransform
    }
    
    func rotateComposition2(for naturalTransform: CGAffineTransform, isP: Bool = false, with angle: TrimAngle = .kZero, size: CGSize, widthRatio: CGFloat = 1.0, heightRatio: CGFloat = 1.0) -> (CGAffineTransform, CGSize) {
        
        let outputSize = size
        let w = size.width
        let h = size.height
        switch angle {
        case .kZero:
            return (naturalTransform, outputSize)
        case .k90:
            if isP {
                let translate = naturalTransform.translatedBy(x: w, y: -w * (w / h))
                let rotate = translate.rotated(by: angle.cgFloat)
                return (rotate, outputSize)
            } else {
                let ratio1 = w / h
                let ratio2 = h / w
                print(ratio1, ratio2, heightRatio)
                let translate = naturalTransform.translatedBy(x: w, y: 0)
                let rotate = translate.rotated(by: angle.cgFloat)
                return (rotate, outputSize)
            }
        case .k180:
            if isP {
                let translate = naturalTransform.translatedBy(x: h * heightRatio, y: w)
                let rotate = translate.rotated(by: angle.cgFloat)
                return (rotate, outputSize)
            } else {
                let translate = naturalTransform.translatedBy(x: w, y: h * heightRatio)
                let rotate = translate.rotated(by: angle.cgFloat)
                return (rotate, outputSize)
            }
        case .k270:
            
            var translate = naturalTransform.scaledBy(x: heightRatio, y: -1.0)
            translate = naturalTransform.translatedBy(x: 0, y: h * 0.5)
            let rotate = translate.rotated(by: angle.cgFloat)
            return (rotate, outputSize)
        }
    }
    
    
}

