

import UIKit

class Filters {

    var type: CustomFilterType?
    var value: Float?
    
    init(type: CustomFilterType?) {
        self.type = type
    }
}
