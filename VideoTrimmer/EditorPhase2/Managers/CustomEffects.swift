
import UIKit

enum CustomEffectsVHSType: Int {
    case None = 00, A1 = 01, A2 = 02, A3 = 03, A4 = 04, A5 = 05, A6 = 06, A7 = 07, A8 = 08, A9 = 09, A10 = 10, A11 = 11, A12 = 12, A13 = 13
    
    static let count: Int = {
        var max: Int = 0
        while let _ = CustomEffectsVHSType(rawValue: max) { max += 1 }
        return max
    }()
}

enum CustomEffectsGlitchType: Int {
    case None = 00, A1 = 01, A2 = 02, A3 = 03, A4 = 04, A5 = 05, A6 = 06, A7 = 07, A8 = 08, A9 = 09, A10 = 10, A11 = 11, A12 = 12, A13 = 13
    
    static let count: Int = {
        var max: Int = 0
        while let _ = CustomEffectsGlitchType(rawValue: max) { max += 1 }
        return max
    }()
}

enum CustomEffectsVINType: Int {
    case None = 00, A1 = 01, A2 = 02, A3 = 03, A4 = 04, A5 = 05, A6 = 06, A7 = 07, A8 = 08, A9 = 09, A10 = 10, A11 = 11, A12 = 12, A13 = 13
    
    static let count: Int = {
        var max: Int = 0
        while let _ = CustomEffectsVINType(rawValue: max) { max += 1 }
        return max
    }()
}

enum CustomEffectsBWType: Int {
    case None = 00, A1 = 01, A2 = 02, A3 = 03, A4 = 04, A5 = 05, A6 = 06, A7 = 07, A8 = 08, A9 = 09, A10 = 10, A11 = 11, A12 = 12, A13 = 13
    
    static let count: Int = {
        var max: Int = 0
        while let _ = CustomEffectsBWType(rawValue: max) { max += 1 }
        return max
    }()
}

enum CustomEffectsMirrorType: Int {
    case None = 00, A1 = 01, A2 = 02, A3 = 03, A4 = 04, A5 = 05, A6 = 06, A7 = 07, A8 = 08, A9 = 09, A10 = 10, A11 = 11, A12 = 12, A13 = 13
    
    static let count: Int = {
        var max: Int = 0
        while let _ = CustomEffectsMirrorType(rawValue: max) { max += 1 }
        return max
    }()
}

class CustomEffects: NSObject {

    static func getFilteredImageWithVHSFilter(_ type: CustomEffectsVHSType,to image: UIImage) -> UIImage? {
        var newImage : UIImage? = image
        var filter: GPUImageFilter?
        if type == .None {
            filter = GPUImageFilter()
        } else {
            filter = getShaderFilterImage(type.rawValue, 1)
            newImage = filter?.image(byFilteringImage: newImage)
        }
        return newImage
    }
    
    static func getFilteredImageWithGLITFilter(_ type: CustomEffectsGlitchType,to image: UIImage) -> UIImage? {
        var newImage : UIImage? = image
        var filter: GPUImageFilter?
        if type == .None {
            filter = GPUImageFilter()
        } else {
            filter = getShaderFilterImage(type.rawValue, 2)
            newImage = filter?.image(byFilteringImage: newImage)
        }
        return newImage
    }
    
    static func getFilteredImageWithVINFilter(_ type: CustomEffectsVINType,to image: UIImage) -> UIImage? {
        var newImage : UIImage? = image
        var filter: GPUImageFilter?
        if type == .None {
            filter = GPUImageFilter()
        } else {
            filter = getShaderFilterImage(type.rawValue, 3)
            newImage = filter?.image(byFilteringImage: newImage)
        }
        return newImage
    }
    
    static func getFilteredImageWithBAWFilter(_ type: CustomEffectsBWType,to image: UIImage) -> UIImage? {
        var newImage : UIImage? = image
        var filter: GPUImageFilter?
        if type == .None {
            filter = GPUImageFilter()
        } else {
            filter = getShaderFilterImage(type.rawValue, 4)
            newImage = filter?.image(byFilteringImage: newImage)
        }
        return newImage
    }
    
    static func getFilteredImageWithMirrorFilter(_ type: CustomEffectsMirrorType,to image: UIImage) -> UIImage? {
        var newImage : UIImage? = image
        var filter: GPUImageFilter?
        if type == .None {
            filter = GPUImageFilter()
        } else {
            filter = getShaderFilterImage(type.rawValue, 5)
            newImage = filter?.image(byFilteringImage: newImage)
        }
        return newImage
    }
    
    static func getShaderFilterImage(_ type: NSInteger, _ category:NSInteger) -> GPUImageFilter {
        
        let filter = GPUImageFilter(fragmentShaderFromFile: "SlideProEffect")
        filter?.setFloat(3.0, forUniformName: "MainTime")
        filter?.setFloat(GLfloat(SCREEN_WIDTH), forUniformName: "width")
        filter?.setFloat(GLfloat(SCREEN_WIDTH), forUniformName: "height")
        filter?.setFloat(0.01, forUniformName: "amplitude")
        filter?.setFloat(1.0, forUniformName: "speed")
        filter?.setFloat(0.6, forUniformName: "power")
        filter?.setInteger(GLint(type), forUniformName: "type")
        filter?.setInteger(GLint(category), forUniformName: "category")
        filter?.setInteger(0, forUniformName: "Zoom")
        filter?.setInteger(0, forUniformName: "zoompower")
        
        return filter!
    }
    
    static func getShaderFilter() -> GPUImageFilter {
        
        let filter = GPUImageFilter(fragmentShaderFromFile: "SlideProEffect")
        filter?.setFloat(3.0, forUniformName: "MainTime")
        filter?.setFloat(GLfloat(SCREEN_WIDTH), forUniformName: "width")
        filter?.setFloat(GLfloat(SCREEN_WIDTH), forUniformName: "height")
        filter?.setFloat(0.01, forUniformName: "amplitude")
        filter?.setFloat(1.0, forUniformName: "speed")
        filter?.setFloat(0.6, forUniformName: "power")
        filter?.setInteger(0, forUniformName: "type")
        filter?.setInteger(0, forUniformName: "category")
        filter?.setInteger(0, forUniformName: "Zoom")
        filter?.setInteger(0, forUniformName: "zoompower")
        
        return filter!
    }
    
}
