varying highp vec2 textureCoordinate;
uniform sampler2D inputImageTexture;

uniform highp float size;
uniform highp float iGlobalTime;
uniform highp float height;
uniform highp float width;
uniform highp float aspectRatio;
uniform highp int   type;

#define r(x)     fract(1e4*sin((x)*541.17))      // rand, signed rand   in 1, 2, 3D.
#define sr2(x)   ( r(vec2(x,x+.1)) *2.-1. )
#define sr3(x)   ( r(vec4(x,x+.1,x+.2,0)) *2.-1. )


highp float hash( highp float n ) { return fract(sin(n)*753.5453123); }

highp float noise( highp  vec2 x )
{
    highp vec2 p = floor(x);
    highp vec2 f = fract(x);
    f = f*f*(3.0-2.0*f);
    
    highp float n = p.x + p.y*157.0;
    return mix(mix( hash(n+  0.0), hash(n+  1.0),f.x),
               mix( hash(n+157.0), hash(n+158.0),f.x),f.y);
}

// 2D rotation matrix by approximately 36 degrees.
highp mat2 m = mat2(0.8, 0.6, -0.6, 0.8);

highp float fbm(highp vec2 r) {
    
    highp float f;
    // rotate every octave to add more variation.
    f  = 0.5000*noise( r ); r = r*m*2.01;
    f += 0.2500*noise( r ); r = r*m*2.02;
    f += 0.1250*noise( r ); r = r*m*2.03;
    f += 0.0625*noise( r ); r = r*m*2.01;
    
    return f;
}

highp float rand(highp float co){
    return fract(sin(dot(vec2(co ,co ) ,vec2(12.9898,78.233))) * 43758.5453);
}

highp float rand_range(highp float seed,highp float low, highp float high) {
    //    return 0.0001;
    return low + (high - low) * rand(seed);
}

highp vec4 rand_color(highp float time){
    return vec4(sin(time)*5.0,0.5,sin(time)*1.0,1.0);
}

highp mat2 rot_matrix(highp float rot) {
    return mat2(
                cos(rot), -sin(rot),
                sin(rot),  cos(rot)
                );
}

highp vec3 rand_color( highp float seed,highp vec3 col,highp vec3 variation) {
    return vec3(
                col.x + rand_range(seed,-variation.x, +variation.x),
                col.y + rand_range(seed,-variation.y, +variation.y),
                col.z + rand_range(seed,-variation.z, +variation.z));
}


// id = particle id
// id = particle id

//***********************************************************************//
//********************** START OF SPARKLE EFFECCT ************************//
//***********************************************************************//

highp vec4 sparkle(highp float time,highp float id,highp vec2 q, int type) {
    
    highp float lifespan = rand_range(id*1232.23232, 3.0, 4.5);
    
    lifespan = 60.0 * 4.0;
    
    // pgen = particle generation
    // every time a particle has outlived its lifespan,
    // it is respawned as a new particle at a new position
    // the generation of this new particle is one plus
    // the generation of the old particle.
    
    highp float pgen = float(int(time / lifespan));
    
    // how long the particle of the current generation has lived.
    highp float lifetime = time - lifespan * pgen;
    
    // pseed is used to determine the random attributes of the particle.
    // two particles with the same id but different generations
    // are essentially different particles.
    highp float pseed = id *12.2 + pgen * 50.3;
    highp vec2 pos = q;
    pos.y *= aspectRatio + 0.3;
    
    // controls the diameter of the superellipse.
    // we vary it over the lifetime to animate the particle.
    highp float timeT = time;
    highp float dx = 0.04 + 0.001*sin(9.0*(pseed*12.5454));
    highp float dy = 0.04 + 0.001*sin(9.0*(pseed*223.323));
    
    // slightly rotate the superellipse randomly.
    highp float rot = rand_range(pseed*23.33+3.4353, -0.10, 0.30);
    
    if(type == 2 || type == 3) {
        rot = rand_range(pseed*23.33+3.4353, -0.10, time*0.30);
    }
    
    pos = rot_matrix(rot) * pos;
    
    highp float sizeT = size * 0.8;
    highp float func = pow(abs(pos.x/(dx)), 0.5) + pow(abs(pos.y/dy), 0.5) - 0.4;
    
    if (type == 5) {
        func = pow(abs(pos.x/(dx)), 0.5) + pow(abs(pos.y/dy), 0.4) - 0.4;
    }
    
    func *= (1.6 - sizeT);
    
    highp vec4 res;
    highp vec3 start_color = rand_color(pseed *39.3232,
                                        vec3(1.0,1.0,1.0),
                                        vec3(1.0,1.0,1.0)
                                        );
    
    if (type == 2) {
        start_color = rand_color(pseed *19.3232,
                                 vec3(0.9,0.9,0),
                                 vec3(0.4,0.4,0.4)
                                 );
    }
    
    if (type == 3) {
        start_color = rand_color(pseed *19.3232,
                                 vec3(time*1.0,time*1.0,time*1.0),
                                 vec3(1.0,1.0,1.0)
                                 );
    }
    
    if (type == 5) {
        start_color = rand_color(pseed *19.3232,
                                 vec3(time*1.0,time*1.0,time*1.0),
                                 vec3(1.0,1.0,1.0)
                                 );
    }
    
    // now rgb-value over 1.0 allowed.
    if(start_color.r > 1.0) {
        start_color.r = 1.0;
    }
    
    if(start_color.g > 1.0) {
        start_color.g = 1.0;
    }
    
    highp vec3 end_color;
    
    if(start_color.r < 0.85 && start_color.r < 0.85) {
        end_color = start_color - vec3(1.0,0.0,1.0);
    } else {
        end_color = start_color - vec3(0.0,0.0,1.0);
    }
    
    // slightly vary color over lifetime;
    // makes for a small blinking effect.
    // *** Colors ***
    highp float f = 1.0/2.0 + (sin(9.0*(12.5454)))/2.0;
    res.xyz = mix(start_color, end_color,f);
    
    // we use this value to combine the particle with the rest
    // of the image.
    res.w = smoothstep(0.0, 0.75, 1.0-func);
    
    //fade out a particle quickly when its about to die.
    // but before that time, leave it mostly unchanged.
    f = 0.000976175 * exp(6.93187* (lifetime/lifespan) );
    res.w = mix(res.w, 0.0, f);
    
    highp vec4 whiteC = vec4(1.0,1.0,1.0,1.0);
    
    if(type == 4) {
        whiteC = rand_color(time);
    }
    
    if (type == 5) {
        whiteC = rand_color(time);
        whiteC.r += textureCoordinate.y * 10.0;
    }
    
    res.rgb = mix(res.rgb, whiteC.rgb, 0.5);
    return res;
}

//***********************************************************************//
//************************END OF SPARKLE EFFECCT************************//
//***********************************************************************//
highp float intensity(highp vec4 color){
    return sqrt((color.x*color.x)+(color.y*color.y)+(color.z*color.z));
}

highp vec2 detectRedColor_InWindow(highp vec2 center,highp float power)
{
    int windowSizeHalf = 14;
    windowSizeHalf = 18;
    
    int doBreak = 0 ;
    
    highp float stepX = 1.0/width;
    highp float stepY = 1.0/height;
    
    highp vec2 processUV = vec2(-1.0,-1.0);
    
    for (int i = -windowSizeHalf ; doBreak == 0 &&  i< windowSizeHalf ; i++) {
        
        for (int j = -windowSizeHalf; doBreak == 0 && j< windowSizeHalf ; j++) {
            
            highp float iT = float(i);
            highp float jT = float(j);
            
            highp vec2 tempProcessUV = vec2( center.x + (iT * stepX) ,center.y + (jT * stepY));
            
            highp vec4 processColor = texture2D(inputImageTexture,tempProcessUV);
            
            if (processColor.r > (power * 0.555))
            {
                processUV = tempProcessUV;
                doBreak = 1;
                break;
            }
        }
    }
    return processUV ;
}

// *********************************************************************** //
// ************* https://www.shadertoy.com/view/4t33D2   ***************** //
// ********************** Highlights from the 80's *********************** //
// *********************************************************************** //


const highp float PI = 3.1415926535897932384626433832795;
const highp float spokes = 0.0;
const highp float bevel_size = 0.1;
highp float spin0 = iGlobalTime * 0.2;
const highp vec2 light_dir = vec2(0.1, 1.0);
const highp float bevelExp = 0.772;
const highp float lineExp = 20.776 ;

const highp float line1Mul = -1.0;
const highp float line2Mul = -1.0;
const highp float bevelMul = 1.8760002;
const highp float lineMul = 1.06;

const highp float glareMul = 0.736;
const highp float sparkleMul = -2.0;


highp mat2 rot2(highp float T) {
    highp mat2 R = mat2(vec2(cos(T), sin(T)), vec2( -sin(T), cos(T)));
    return R;
}

highp float udLine(highp vec2 p,highp vec2 a,highp vec2 b) {
    highp vec2 pa = p- a, ba = b- a;
    return length(pa- ba* dot(pa, ba)/ dot(ba, ba));
}

highp float glyph_dist2(highp vec2 pt, highp float angle) {
    
    highp float len = length(pt);
    highp float rad = 1.0- len;
    return rad- abs(sin(angle* spokes/ 2.0))* 0.6;
}

highp float glyph_dist(highp vec2 pt) {
    highp float angle = atan(pt.y, pt.x)- spin0* 2.0* PI;
    return glyph_dist2(pt, angle);
}

highp vec2 gradient(highp vec2 pt, highp float dist) {
    highp float dfdu = glyph_dist(pt+ vec2(0.01, 0.0))- dist/ 1.01;
    highp float dfdv = glyph_dist(pt+ vec2(0.0, 0.01))- dist/ 1.01;
    highp vec2 grad = normalize(vec2(dfdu, -dfdv));
    return grad;
}

highp float bevelShade(highp vec2 pt) {
    return clamp(1.0* dot(gradient(pt, glyph_dist(pt)), light_dir)* 0.5+ 0.5, 0.0,
                 1.0);
}

highp vec2 sparklyPosition(highp float progress) {
    
    highp vec2 pt = vec2(1, 0);
    highp float angle = 2.0* PI* progress;
    highp float glyph = glyph_dist2(pt, angle);
    highp vec2 adjust = vec2(1.0- 0.5* bevel_size, 0);
    highp vec2 trace = pt* glyph+ adjust;
    highp vec2 pos = trace* rot2( -angle+ -2.0* PI* spin0);
    return pos;
}

highp vec3 newStar (highp vec2 uv ,highp vec2 center, highp float time, highp float power)
{
    highp vec3 color = vec3(0.0,0.0,0.0);
    highp float highlights = 1.0;
    highp float aspect = 1.7777777778 ;
    
    highp vec2 pt = (uv* 2.0- 1.0)* vec2(aspect, 1.0)* 1.0;
    pt = uv ;
    
    for (highp float i = 0.0; i< highlights; i++) {
        
        highp vec2 glintPos = sparklyPosition((i+ 1.0)/ highlights);
        highp float dist = distance(pt, glintPos);
        highp float sparkle = dist* 2.0;
        
        if(sparkle< 1.0) {
            
            highp float line1 = udLine(pt, glintPos, glintPos+ vec2(1, +1))* line1Mul;
            highp float line2 = udLine(pt, glintPos, glintPos+ vec2(1, -1))* line2Mul;
            
            highp float bevel = bevelShade(glintPos)* bevelMul;
            
            highp float lines = 1.0+ (line1+ line2)* lineMul;
            highp float glare = pow(bevel, bevelExp)* pow(lines, lineExp);
            color = color+ glare* glareMul* (1.0- sparkle* sparkleMul);
        }
        
    }
    
    return color;
    
}

// 20 November 2018
// ********************************************************************************* //
// *******************  https://www.shadertoy.com/view/Xl33Wn ********************** //
// ********************************************************************************* //

// Testing new Approach For Stars ..

highp float flare(highp vec2 U ) // rotating hexagon
{
    highp vec2  A  = vec2( 0.7 , 0.7 );
    U = abs( U * mat2(A, -A.y,A.x ) ) * mat2(2,0.0,1.0,1.7);
    return 1.0/max(U.x,U.y);
}


highp vec4 stars(highp vec4 O, highp vec2 U ,highp vec2 point )
{
    highp float time =  iGlobalTime;
    highp vec2 R = vec2(1.0,1.0);
    
    U =  (U+U-R.y) /R.y;
    U += vec2(point.x - 0.5,point.y - 0.5);
    
    O += flare (U)*0.02 * (1.+.2*sr3(1.0));
    return O;
}

void main() {
    
    highp float time =  iGlobalTime;
    highp int PARTICLE_COUNT = 20;
    
    highp vec4 orgColor = texture2D(inputImageTexture,textureCoordinate);
    
    if (type == 0) {
        gl_FragColor = orgColor;
        return;
    }
    
    highp vec4 finalColor;
    
    highp float stepX = 1.0/width;
    highp float stepY = 1.0/height;
    
    // Stars
    highp vec2 p = textureCoordinate.xy;
    highp vec3 col = vec3(0.0);
    
    // Areas Detection on Single Pixel Intensity
    highp float power = 0.1;
    highp vec2 firstRed = detectRedColor_InWindow(textureCoordinate,0.5);
    
    if (firstRed.x != -1.0 && firstRed.y != -1.0 )
    {
        finalColor = texture2D(inputImageTexture,firstRed);
        
        highp float id = float(firstRed.x);
        highp float jd = float(firstRed.y);
        highp vec2 q = p - vec2(id,jd);
        
        for(int K = 1; K<7; K++)
        {
            highp vec4 res = sparkle(time, float(K), q,type);
            finalColor.rgb = mix(col, res.xyz, res.w);
            finalColor.a = res.w;
        }
    }
    else
    {
    
    }
    
    gl_FragColor = finalColor;
    
}
