

import UIKit
import Photos

protocol Filter2ViewDelegate : EffectFiltersDelegate {
    func didSelectFilter()
    func filterEndEditing()
}

class FilterView: UIView , UICollectionViewDelegate ,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var lblCurrentTime: UILabel!
//    @IBOutlet weak var sliderThumbLeading: NSLayoutConstraint!
//    @IBOutlet weak var sliderThumb: UIView!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var timeLineBgView: UIView!
    @IBOutlet weak var timeLineParent: UIView!
    private var imageGenerator: AVAssetImageGenerator!
    @IBOutlet weak var cv_filters: UICollectionView!
    var sharpenFilter: GPUImageSharpenFilter?
    var gpuImageView: GPUImageView?
    var gpuMovie: GPUImageMovie?
    var alphaBlendFilter: GPUImageAlphaBlendFilter?
    var filters: Filters?
    var delegate: Filter2ViewDelegate?
    var selectedFilterIndex: IndexPath? = nil
    var timeLine: VideoRangeSlider?
    
    var asset: AVAsset? = nil {
        didSet {
            initializeTrimmerView()
        }
    }

    var oneSecondWidth : CGFloat = 0.0
    
    let filterNames = ["X","hyk","Reek","parrot","bolt","gold","cherry","doct","craze","bride","common","flexy","uder","vivo","simple","Curd","ammino","Challenger","nigro","blue","Mental","lol","Next","stop","plus","grake","land","innoce","Nothing"]
    
    
    var filterImagesArr = [UIImage]()
    var firstFrame : UIImage! {
        didSet{
            if firstFrame != nil{
               cv_filters.reloadData()
            }
        }
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
        closeSliderView(self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        /* DispatchQueue.main.async {
            self.btnDone.isHidden = false
            self.btnCancel.isHidden = false
            Utility.setGradient(self.btnDone)
            Utility.setGradient(self.btnCancel)
        } */
    }
    
    override func awakeFromNib() {
        cv_filters.register(UINib.init(nibName: String(describing: FilterViewCell.reuseIdentifier), bundle: nil), forCellWithReuseIdentifier: FilterViewCell.reuseIdentifier)
        cv_filters.dataSource = self
        cv_filters.delegate = self
         /* slider.addTarget(self, action: #selector(touchEvent(slider:for:)), for: .valueChanged)
        
        btnDone.layer.cornerRadius = 5.0
        btnDone.clipsToBounds = true
        btnDone.isHidden = true
        
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.clipsToBounds = true
        btnCancel.isHidden = true */
        
    }
    
    @objc func touchEvent(slider: UISlider,for event: UIEvent){
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                print("slider began editing")
            case .moved:
                filters?.value = slider.value
                alphaBlendFilter?.mix = CGFloat(slider.value)
            // handle drag began
            default:
                break
            }
        }
    }
    
    func setupData(with imageView: GPUImageView){
        self.gpuImageView = imageView
        cv_filters.reloadData()
    }
    
    @objc func handleMove(_ gesture: UIPanGestureRecognizer) {
        guard let gestureView = gesture.view else {return}
        guard let superview = gestureView.superview else {return}
        
        let translation = gesture.translation(in: superview)
        // Boundary detection
        var targetPoint = CGPoint(x: center.x + translation.x, y: center.y + translation.y )
        targetPoint.x = max(0, targetPoint.x)
        targetPoint.y = max(0, targetPoint.y)
        targetPoint.x = min(superview.bounds.size.width, targetPoint.x)
        targetPoint.y = min(superview.bounds.size.height, targetPoint.y)
        
        self.center = targetPoint
        gesture.setTranslation(CGPoint.zero, in: superview)
    }
    
    class func initWith(andImageView: GPUImageView,gpuMovie: GPUImageMovie) -> FilterView {
        let view = self.fromNib()
        view.gpuImageView = andImageView
        view.gpuMovie = gpuMovie
        view.initView()
        return view
    }
    
    func initView() {
        cv_filters.reloadData()
    }
    
    func initializeTrimmerView(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.slider.isHidden = false
            self.setMovieFrame()
        }
        
    }
    
    func setMovieFrame(){
        guard let myAsset = asset else {
            return
        }
        oneSecondWidth = timeLineBgView.frame.width / CGFloat(myAsset.duration.seconds)
        imageGenerator = AVAssetImageGenerator(asset: myAsset)
        imageGenerator.maximumSize =  CGSize(width: 60, height: 60)
        let interval = myAsset.duration.seconds / 10.0 //only 10 images will be generated
        var imageArray = [UIImage]()
        for i in 0..<10{
            do {
                let time = CMTime(seconds: Double(Double(i) * interval), preferredTimescale: myAsset.duration.timescale)
                let img = try imageGenerator.copyCGImage(at: time, actualTime: nil)
                let image = UIImage(cgImage: img)
                imageArray.append(image)
            } catch {
                print(error.localizedDescription)
            }
        }
        let picWidth: CGFloat = timeLineBgView.frame.width / 10.0
        var xPos : CGFloat = 0
        for image in imageArray{
            let imgLayer = CALayer()
            imgLayer.frame = CGRect(x: xPos, y: 0, width: picWidth, height: timeLineBgView.frame.size.height)
            xPos += picWidth
            imgLayer.contents = image.cgImage
            self.timeLineBgView.layer.addSublayer(imgLayer)
        }
        imageArray.removeAll()
        timeLineBgView.bringSubviewToFront(slider)
        lblStartTime.text = "00:00.0"
//        lblEndTime.text = getTimeLabel(seconds: Int(myAsset.duration.seconds))
        lblEndTime.text = CGFloat(myAsset.duration.seconds).convertSecondsToDurationString2()
        setupSlider(asset: myAsset)
    }
    
    func setupSlider(asset: AVAsset){
        
//        let imageTint = UIImage(named: "SliderThumb")?.withRenderingMode(.alwaysTemplate)

        self.slider.tintColor = UIColor(named: "tintThemeColor")
        self.slider.setThumbImage(UIImage(named: "SliderThumb"), for: .normal)
        slider.setThumbImage(UIImage(named: "SliderThumb"), for: .highlighted)
        self.slider.minimumValue = 0.0
        self.slider.maximumValue = Float(asset.duration.seconds)
        self.slider.value = 0.0
        self.slider.addTarget(self, action: #selector(sliderAction(slider:for:)), for: .allEvents)
    }
    
    @objc func sliderAction(slider: UISlider,for event: UIEvent){
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                print("slider began editing")
                delegate?.sliderDidBeginMoving()
            case .moved:
                delegate?.sliderMoved(value: slider.value)
            // handle drag began
            default:
                break
            }
        }
    }
    
    func updateCurrentTime(time: CGFloat) {
//        UIView.animate(withDuration: 0.2) {
//            self.sliderThumbLeading.constant = self.oneSecondWidth * time
//            self.sliderThumb.setNeedsLayout()
//        }
        slider.value = Float(time)
        lblCurrentTime.text = CGFloat(time).convertSecondsToDurationString2()
//        lblCurrentTime.text = getTimeLabel(seconds: Int(time))
    }
    
    
    func getTimeLabel(seconds: Int) -> String{
        var timeStr = ""
        let time = secondsToHoursMinutesSeconds(seconds:seconds)
        let hour = time.0 < 10 ? "0" + String(time.0) : String(time.0)
        let minute = time.1 < 10 ? "0" + String(time.1) : String(time.1)
        let seconds = time.2 < 10 ? "0" + String(time.2) : String(time.2)
        timeStr = minute + ":" + seconds
        if hour != "00"{
            timeStr = hour + ":" + timeStr
        }
        return timeStr
    }
    
    func secondsToHoursMinutesSeconds(seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    

    func updateWithModel(_ model: Filters) {
        
        alphaBlendFilter?.removeAllTargets()
        
        alphaBlendFilter = GPUImageAlphaBlendFilter()
        let filter = VideoEdCustomFilter.getACVFilterWith(CustomFilterType(rawValue: model.type!.rawValue)!)
        gpuMovie?.removeAllTargets()
        
        var liveFilter : GPUImageFilter?

        if let controller = self.parentViewController as? ViewController {
            liveFilter = controller.liveFilter
            liveFilter?.removeAllTargets()
        }

        gpuMovie?.addTarget(liveFilter)
        
        liveFilter?.addTarget(filter)
        liveFilter?.addTarget(alphaBlendFilter)
        
        filter.addTarget(alphaBlendFilter)
        
        alphaBlendFilter?.mix = 1.0
        alphaBlendFilter?.addTarget(gpuImageView)
        
        if let controller = self.parentViewController as? ViewController {
            controller.alphaBlend = alphaBlendFilter
            controller.firstFilter = filter
        }
        //update slider
        updateSliderWithType(model)
        filters = model
    }
    
    func updateSliderWithType(_ model: Filters) {
//        slider.minimumValue = 0.0
//        slider.maximumValue = 1.0
//        slider.value = 1.0
//        guard let newValue = model.value else { return }
//        slider.value = newValue
//        alphaBlendFilter?.mix = CGFloat(newValue)
    }
    
    @IBAction func closeSliderView(_ sender: Any) {
//        sliderView.isHidden = true
    }
    
    @IBAction func sliderValueEvents(_ sender: UISlider) {
        filters?.value = sender.value
        alphaBlendFilter?.mix = CGFloat(sender.value)
    }
    
    //MARK:- Collection View Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CustomFilterType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterViewCell.reuseIdentifier, for: indexPath) as! FilterViewCell
        var image = UIImage()
        if firstFrame != nil{
            image = firstFrame
            if filterImagesArr.count > indexPath.row{
                image = filterImagesArr[indexPath.row]
            }else{
                if let filteredImage = VideoEdCustomFilter.getFilteredImageWithACVFilter(CustomFilterType(rawValue: indexPath.row) ?? .A1, to: image){
                     image = filteredImage
                     filterImagesArr.append(image)
                }
            }
        }

        cell.updateData(image: image, name: filterNames[indexPath.row])
        cell.isSelectedCell = (selectedFilterIndex != nil && indexPath.row == selectedFilterIndex?.row) ? true: false
        cell.layer.cornerRadius = 5.0
        cell.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let previousIndex = selectedFilterIndex
        
        if selectedFilterIndex?.item == indexPath.item {
            //sliderView.isHidden = false
           // updateSliderWithType(filters!)
            selectedFilterIndex = indexPath
            return
        }
        
        if gpuImageView != nil {
            let filterModel = Filters(type: CustomFilterType(rawValue: indexPath.row) ?? .A1)
            filterModel.value = 1.0
            delegate?.didSelectFilter()
            updateWithModel(filterModel)
            selectedFilterIndex = indexPath
            if previousIndex != nil {
                collectionView.reloadItems(at: [previousIndex!,selectedFilterIndex!])
            } else {
                collectionView.reloadItems(at: [selectedFilterIndex!])
            }
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        let height = collectionView.frame.size.height * 0.85
//        let width = height / 1.3
//        return CGSize(width: width, height: height)
        return CGSize(width: 60, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16.0, bottom: 0, right: 16.0)
    }

    func reset(){
        filterImagesArr.removeAll()
        gpuMovie?.removeAllTargets()
        alphaBlendFilter?.removeAllTargets()
    }
    
}

extension FilterView {
    class func fromNib<T: FilterView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}


