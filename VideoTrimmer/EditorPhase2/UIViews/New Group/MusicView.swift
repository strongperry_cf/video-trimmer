

import UIKit
import AVFoundation
import MediaPlayer

protocol MusicViewDelegate: class {
    func didSelectMusic(audioModel: AudioModel)
    func didChangeVolume(volumeLevel: Float)
    func pausePlayer()
}

class MusicView: UIView , UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var musicTrimmerView: UIView!
    @IBOutlet weak var waveViewContainer: UIView!
    @IBOutlet weak var volumeSliderView: UIView!
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var cv_music: UICollectionView!
    @IBOutlet weak var changeMusicButton: UIButton!
    
    var musics = [AudioModel]()
    
    var delegate: MusicViewDelegate?
    var selectedMusicIndex: IndexPath? = nil
        
    //MARK: Trim View Props
     var waveformView: VIWaveformView?
    
    class func initWith(with frame: CGRect) -> MusicView {
        let view = self.fromNib()
        view.frame = frame
        return view
    }
    
    override func awakeFromNib() {
        volumeSlider.isHidden = true
        cv_music.register(UINib.init(nibName: String(describing: MusicViewCell.reuseIdentifier), bundle: nil), forCellWithReuseIdentifier: MusicViewCell.reuseIdentifier)
        cv_music.dataSource = self
        cv_music.delegate = self
        setupData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.volumeSlider.isHidden = false
            self.setupViews()
        }
    }
    
    func setupViews() {
        setupVolumeBar()
    }
    
    func setupVolumeBar() {
        volumeSlider.value = 1.0
        volumeSlider.addTarget(self, action: #selector(touchEvent(slider:for:)), for: .valueChanged)
//        volumeSlider.setThumbImage(UIImage(named: "SliderThumb"), for: .normal)
//        volumeSlider.setThumbImage(UIImage(named: "SliderThumb"), for: .highlighted)
//        volumeSlider.setThumbImage(UIImage(named: "emptyImage"), for: .normal)

        volumeSliderView.layer.cornerRadius = 10.0
        volumeSliderView.clipsToBounds = true
    }
    
    func setupData() {
        musics.removeAll()
        
        
        
        guard let localMusics = Bundle.main.urls(forResourcesWithExtension: "mp3", subdirectory: "") else {return}
        let addMusic = AudioModel()
        addMusic.type = "AddMusic"
        musics.append(addMusic)
        for url in localMusics {
            if url.pathExtension.contains("mp3") {
                if !url.path.contains("silenceAudio") {
                    
                    let music = AudioModel(url:url)
                    musics.append(music)
                }
            }
        }
        cv_music.reloadData()
        
        
    }
    
    @IBAction func actionTrimDone(_ sender: UIButton) {
        musicTrimmerView.isHidden = true
    }
    
    
    @IBAction func actionTrimCancel(_ sender: UIButton) {
        musicTrimmerView.isHidden = true
    }
    
    @IBAction func changeMusicAction(_ sender: UIButton) {
        self.delegate?.pausePlayer()
        let ringtoneVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RingtoneListVC") as! RingtoneListVC
        ringtoneVC.delegate = self
        self.parentViewController?.present(ringtoneVC, animated: true, completion: nil)
        return

    }
    
    
    //MARK:- Actions
    @objc func touchEvent(slider: UISlider,for event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                 break
            case .moved:
                delegate?.didChangeVolume(volumeLevel: slider.value)
                break
            default:
                break
            }
        }
    }
    
    //MARK: Collection View Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return musics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MusicViewCell.reuseIdentifier, for: indexPath) as! MusicViewCell
        cell.setupData(with: musics[indexPath.row])
        
        
        if selectedMusicIndex != nil && selectedMusicIndex?.row == indexPath.row {
            cell.isSelectedCell = true
        }else {
            cell.isSelectedCell = false
        }
        
        if indexPath.row == 0{
            cell.imgView.image = UIImage(named: "AddMusic")
        }else{
            cell.imgView.image = UIImage(named: "musicIcon")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 && musics[0].audioUrl == nil{
            self.delegate?.pausePlayer()
            let ringtoneVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RingtoneListVC") as! RingtoneListVC
            ringtoneVC.delegate = self
            self.parentViewController?.present(ringtoneVC, animated: true, completion: nil)
            return
        }
        if selectedMusicIndex?.row == 0{
            changeMusicButton.isHidden = false
        }else{
            changeMusicButton.isHidden = true
        }
        if indexPath.row == selectedMusicIndex?.row {
            musicTrimmerView.isHidden = false
            createWave(from: musics[indexPath.row].audioUrl)
            return
        }
        selectedMusicIndex = indexPath
        delegate?.didSelectMusic(audioModel: musics[indexPath.row])
        collectionView.reloadData()
    }
    
}
//MARK: Trimmer View Extenstion
extension MusicView : TrimDelegate , RingtoneListVCDelegage{
    func createWave(from url: URL) {
        setupWaveformView()
        let asset = AVAsset.init(url: url)
        _ = waveformView?.loadVoice(from: asset, completion: { (asset) in
        })
    }
    
    func setupWaveformView() {
        if let wave = waveformView {
            wave.delegate = nil
            wave.removeFromSuperview()
            waveformView = nil
        }
        waveformView = VIWaveformView()
        waveformView?.delegate = self
        waveViewContainer.addSubview(waveformView!)
        waveformView?.addConstraintToFillSuperview()
        waveformView?.waveformNodeViewProvider = BasicWaveFormNodeProvider(generator: { () -> NodePresentation in
            let view = VIWaveformNodeView()
            view.waveformLayer.strokeColor = UIColor(red: 81.0 / 255.0, green: 81.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.00).cgColor
            return view
        }())
    }
    
    // MARK: Trim delegate method
    func updateTrimValue(startTime: Double) {
        guard let selectedIndex = self.selectedMusicIndex?.row else {return}
        let audioModel = musics[selectedIndex]
        audioModel.beginTime = CGFloat(startTime)
        delegate?.didSelectMusic(audioModel: audioModel)
    }
    
    //MARK: Library Song Delegate
    func didSelectLocalMusic(audioModel: AudioModel) {
//        if musics.count > 1 {
//            if musics[1].type == "LibrarySong" {
//                self.musics[1] = audioModel
//            }
//        }else {
//            self.musics.insert(audioModel, at: 1)
//        }
        self.musics.remove(at: 0)
        self.musics.insert(audioModel, at: 0)
        selectedMusicIndex = IndexPath(row: 0, section: 0)
        self.cv_music.reloadData()
        delegate?.didSelectMusic(audioModel: audioModel)
        musicTrimmerView.isHidden = false

        createWave(from: musics[0].audioUrl)
        changeMusicButton.isHidden = false

        
    }
}

extension MusicView {
    class func fromNib<T: MusicView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
extension FileManager {
    func urls(for directory: FileManager.SearchPathDirectory, skipsHiddenFiles: Bool = true ) -> [URL]? {
        let documentsURL = urls(for: directory, in: .userDomainMask)[0]
        let fileURLs = try? contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil, options: skipsHiddenFiles ? .skipsHiddenFiles : [] )
        return fileURLs
    }
}

