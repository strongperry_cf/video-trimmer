

import UIKit
import AVFoundation

class AudioModel: NSObject {
    var title: String?
    var artWork: UIImage?
    var audioUrl:URL!
    var audioStart:CGFloat = 0.0
    var trackId: Int!
    var updating: Bool = false
    var beginTime: CGFloat = 0.1
    var duration: CGFloat = 0.0
    var volume: CGFloat = 1.0
    var type: String?
    var imageName:String?
    
    
    init(urlStr: String) {
        super.init()
        
        let urlEncodingStr = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let urlstring = urlEncodingStr {
            self.audioUrl = URL(string: urlstring)
        }
        self.title = getSongName(urlString: urlStr)
    }
    
    override init() {
        self.audioUrl = nil
        self.title = ""
    }
    
    
    func getSongName(urlString: String) -> String?{
        let components = urlString.components(separatedBy: "/")
        return components.last
    }
    
    init(url: URL) {
        super.init()
        
        self.audioUrl = url
        self.title = getSongName(url: url)?.capitalized
        
        let asset = AVAsset(url: url)
        self.beginTime = 0.0
        self.duration = CGFloat(asset.duration.seconds)
        
    }
    
    func getSongName(url: URL) -> String?{
        let name = url.lastPathComponent
        return name.replacingOccurrences(of: ".mp3", with: "")
    }
}
