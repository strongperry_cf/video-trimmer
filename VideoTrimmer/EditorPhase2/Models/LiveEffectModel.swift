

import UIKit
import Photos

class LiveEffectModel: NSObject {

    var startTime: CMTime?
    var endTime: CMTime?
    var currentFilter: Int?
    var currentFilterCategory: Int?
    var filterColor: UIColor?
    
    init(startTime: CMTime, endTime: CMTime, currentFilter: Int, currentFilterCategory: Int, filterColor: UIColor) {
        self.startTime = startTime
        self.endTime = endTime
        self.currentFilter = currentFilter
        self.filterColor = filterColor
        self.currentFilterCategory = currentFilterCategory
    }
    
}

class LiveEffectFilter {
    var index: Int?
    var state: UIGestureRecognizer.State?
    
    init(index: Int, state: UIGestureRecognizer.State) {
        self.index = index
        self.state = state
    }
}

