//
//  SettingsViewController.swift
//  FiltersVideo
//
//  Copyright © 2019 iApp. All rights reserved.
//

import UIKit
import MessageUI

class SettingsViewController: UIViewController {

    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        versionLabel.text = "version -  " + appVersion!;
        appNameLabel.text = AppNameGlobal
    }

    @IBAction func feedbackAction(_ sender: Any) {
        

        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["strongperry9@gmail.com"])
        mailComposerVC.setSubject("Feedback")
//        mailComposerVC.setMessageBody("* \(AppNameGlobal) Feedback * \n\n Device Model: \(UIDevice.modelNameString)\n\n iOS Version: \(UIDevice.current.systemVersion)", isHTML: false)
        
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposerVC, animated: true, completion: nil)
        } else {
            //            @"Please Login through Setting"
            
            AlertManager.shared.alertWithTitle(title: "Alert", message: "Please Login first in Settings.")
        }
        
    }
    
    @IBAction func shareAction(_ sender: Any) {

        let appId = NSString(string: "https://itunes.apple.com/us/app/rwdevcon-conference/id\(AppIDGlobal)?mt=8")
        let productURL = URL(string: appId as String)

        let activityViewController = UIActivityViewController(
            activityItems: [productURL as Any],
          applicationActivities: nil)

        // 2.
        present(activityViewController, animated: true, completion: nil)

        
    }
    
    @IBAction func reviewAppstore(_ sender: Any) {
        
        let appId = NSString(string: "https://itunes.apple.com/us/app/rwdevcon-conference/id\(AppIDGlobal)?mt=8")
        let productURL = URL(string: appId as String)

        var components = URLComponents(url: productURL!, resolvingAgainstBaseURL: false)
        components?.queryItems = [
            URLQueryItem(name: "action", value: "write-review")
        ]
        
        // 3.
        guard let writeReviewURL = components?.url else {
            return
        }
        
        // 4.
        UIApplication.shared.open(writeReviewURL)
    }


    @IBAction func restoreAction(_ sender: Any) {
        if IAPManagerLocal.sharedInstance.isInAppPurchased(){
            AlertManager.shared.alertWithMessage(message: "Currently your in App is purchased")
            return
        }

        IAPManagerLocal.sharedInstance.restoreIAP()
    }
    @IBAction func buySubscription(_ sender: Any) {

        IAPManagerLocal.sharedInstance.purchaseProduct(productID: "com.app.monthly") { (status) in
            
            print(status ? "purchased" : "something wrong in inapp purchase")
            
        }
        return;
        SwiftyStoreKit.purchaseProduct("com.app.monthly", quantity: 1, atomically: true) { result in
            switch result {
            case .success(let purchase):
                print("Purchase Success: \(purchase.productId)")
            case .error(let error):
                switch error.code {
                case .unknown: print("Unknown error. Please contact support")
                case .clientInvalid: print("Not allowed to make the payment")
                case .paymentCancelled: break
                case .paymentInvalid: print("The purchase identifier was invalid")
                case .paymentNotAllowed: print("The device is not allowed to make the payment")
                case .storeProductNotAvailable: print("The product is not available in the current storefront")
                case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
                case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
                case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
                default: print((error as NSError).localizedDescription)
                }
            }
        }

    }
}
extension SettingsViewController : MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

}
