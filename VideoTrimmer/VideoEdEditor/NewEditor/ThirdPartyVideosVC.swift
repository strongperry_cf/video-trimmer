//
//  ThirdPartyVideosVC.swift
//  FiltersVideo
//
//  Created by JAGVEER on 08/04/20.
//  Copyright © 2020 iApp. All rights reserved.
//

import UIKit
import SDWebImage

class ThirdPartyVideosVC: UIViewController {
    @IBOutlet weak var serchBar: UISearchBar!
    
    @IBOutlet weak var colView: UICollectionView!
    var pixabaySearching            = false
    var pixabayImageSearchArr    : NSMutableArray?
    var pixabayImageSearchBarStr         =   ""

    override func viewDidLoad() {
        super.viewDidLoad()

        let nibn = UINib(nibName: "FilterViewCell", bundle: nil)
        colView.register(nibn, forCellWithReuseIdentifier: "cellID")
        serchBar.delegate = self
        colView.delegate = self
        colView.dataSource = self
        
        self.serchBar.searchBarStyle = UISearchBar.Style.prominent
        self.serchBar.isTranslucent = false
        let textFieldInsideSearchBar = self.serchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor.white
        textFieldInsideSearchBar?.textColor = .black
        self.serchBar.barTintColor = UIColor.black

        // Do any additional setup after loading the view.
    }


    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func loadPixabaySearchImages(_ searchStr : String) {
        
        let searchString = searchStr.removingWhitespaces() ;
        weak var weakSelf = self
        let pixaBayDefault  = "https://pixabay.com/api/videos/?key=15943079-f7c4747147a25415fe56ee459&q=";
        let categoryString =  searchString;
        let perPage = 30;
        let Page = ((pixabayImageSearchArr?.count)! / perPage) + 1;
        let finalURL = pixaBayDefault + "\(categoryString)&page=\(Page)&per_page=\(perPage)";
        let url = URL(string: finalURL);
        
        PSIndicatorViewController.sharedInstance.startIndicator()
        if (url == nil) {
            PSIndicatorViewController.sharedInstance.stopIndicator()
            return;
        }
        
        if VideoEdHelper.isInternetAvailable() && !pixabaySearching {
            
            pixabaySearching = true;
            let task = URLSession.shared.dataTask(with: url!, completionHandler: {  (data, response, error) in
                
                weakSelf?.pixabaySearching = false ;
                Helper.dispatchMain {
                    PSIndicatorViewController.sharedInstance.stopIndicator()
                }
                if(error != nil){
                    print("error")
                }else{
                    do{
                        if let jsonDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            
                            if !((jsonDict["hits"]) is NSNull)
                            {
                                guard let jsonArray = (jsonDict["hits"]) as? [[String: Any]] else {
                                      return
                                }
                                print(jsonArray)
                                
//                                let dataArray = jsonDict["hits"] as! Array<Any>?
                                if ( (jsonArray.count) > 0)
                                {
                                    //print(dataArray!);
                                    weakSelf?.pixabayImageSearchArr?.addObjects(from: jsonArray as! [Any])
                                    
                                    DispatchQueue.main.async {
                                    weakSelf?.colView.reloadData()
                                    }
                                }
                                else {
                                    weakSelf?.pixabayImageSearchArr?.removeAllObjects()
                                    DispatchQueue.main.async {
                                         Utility.showAlertViewController(title: "Alert", message: "No Result Found", viewController: self)
                                         weakSelf?.colView.reloadData()
                                    }
                                   
                                }
                            }
                        }
                    }catch let error as NSError{
                        print(error)
                    }
                }
            })
            task.resume()
        }
    }

}
extension ThirdPartyVideosVC : UISearchBarDelegate{
   
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        let searchText = searchBar.text!
        
        if(pixabayImageSearchBarStr != searchText && searchText != "" && searchText != " "  && VideoEdHelper.isInternetAvailable() )
        {
            pixabayImageSearchBarStr = searchText ;
            
            if (pixabayImageSearchArr != nil )
            {
                pixabayImageSearchArr  = nil;
                
            }
            
            pixabayImageSearchArr = NSMutableArray();
            let trimmedString :String? = searchText.trimmingCharacters(in: .whitespaces)
            
            if(trimmedString == nil  || trimmedString == "" || trimmedString == " ")            {
                return;
            }else       {
                PSIndicatorViewController.sharedInstance.startIndicator()
                self.loadPixabaySearchImages(trimmedString!);
            }
        }
    }
    
}
extension ThirdPartyVideosVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                 
        if(pixabayImageSearchArr != nil) {
            return  pixabayImageSearchArr!.count;
        }else {
            return  0;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath) as! FilterViewCell
        
        
        var localPixaArr2 = NSMutableArray() ;
        if (pixabayImageSearchArr != nil)   {
            localPixaArr2 = pixabayImageSearchArr! ;
        }
        if localPixaArr2 != nil {
            
            if(localPixaArr2.count > indexPath.item) {
                
                if let tempDic = localPixaArr2[indexPath.item] as? [String: Any]{
                    
                    if let pictureID = tempDic["userImageURL"] {
                        let picUrl:String = pictureID as! String ;
                        cell.lblName.isHidden=true;
                        cell.imgView.sd_setImage(with: URL(string: picUrl));
                    }
                    
                }

            }
        }

        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var localPixaArr2 = NSMutableArray() ;
        if (pixabayImageSearchArr != nil)   {
            localPixaArr2 = pixabayImageSearchArr! ;
        }
        if localPixaArr2 != nil {
            
            if(localPixaArr2.count > indexPath.item) {
                
                if let tempDic = localPixaArr2[indexPath.item] as? [String: Any]{
                    
                    if let pageURL = tempDic["pageURL"] as? String{

//                        downloadVideo(urlString: pageURL)
                    }
                    
                }

            }
        }
    }
    /*
    func downloadVideo(urlString : String){


//        DispatchQueue.global(qos: .background).async {
//            if let url = URL(string: urlString),
//                let urlData = NSData(contentsOf: url) {
//                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
//                let filePath="\(documentsPath)/tempFile.mp4"
//                DispatchQueue.main.async {
//                    urlData.write(toFile: filePath, atomically: true)
//                    PHPhotoLibrary.shared().performChanges({
//                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
//                    }) { completed, error in
//                        if completed {
//                            print("Video is saved!")
//    3
        
//    }
//                    }
//                }
//            }
//        }
        
           // Create destination URL
           let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
           let destinationFileUrl = documentsUrl.appendingPathComponent("downloadedFile.mp4")
           
           //Create URL to the source file you want to download
           let fileURL = URL(string: urlString)
           
           let sessionConfig = URLSessionConfiguration.default
           let session = URLSession(configuration: sessionConfig)
        
           let request = URLRequest(url:fileURL!)
           
           let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
               if let tempLocalUrl = tempLocalUrl, error == nil {
                   // Success
                   if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                       print("Successfully downloaded. Status code: \(statusCode)")
                   }
                   
                   do {
                       try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                   } catch (let writeError) {
                       print("Error creating a file \(destinationFileUrl) : \(writeError)")
                   }
                   
               } else {
                   print("Error took place while downloading a file. Error description: %@", error?.localizedDescription);
               }
           }
           task.resume()
           
         */
    

}
extension String{
    public func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    func SizeOfString( font: UIFont) -> CGSize {
        let fontAttribute = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttribute)  // for Single Line
        return size;
    }
    
    subscript(i: Int) -> Character {
            return self[index(startIndex, offsetBy: i)]
    }
    
}
