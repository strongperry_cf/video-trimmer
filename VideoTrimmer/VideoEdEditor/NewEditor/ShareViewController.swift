//
//  ShareViewController.swift
//  FiltersVideo
//
//  Copyright © 2019 iApp. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
import GoogleMobileAds

enum SharingOptions {
    case saveToGallery
    case shareToFacebook
    case shareToInstagram
}

class ShareViewController: UIViewController {
    
    var videoUrl : URL!
    
    var playerItem:AVPlayerItem?
    var player:AVPlayer?
    var playerLayer: AVPlayerLayer?
    
    var videoIdentifier: String?

    var interstitial: GADInterstitial!

    @IBOutlet weak var playerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playerItem = AVPlayerItem(url: videoUrl)
        player = AVPlayer(playerItem: playerItem)
        playerLayer = AVPlayerLayer(player: player)
        playerView.layer.addSublayer(playerLayer!)
        player?.play()
        NotificationCenter.default.addObserver(self, selector: #selector(finishedPlaying(notification:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        // Do any additional setup after loading the view.
        
        interstitial = createAndLoadInterstitial()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
        
            if self.interstitial.isReady {
                self.interstitial.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
            }

        })
            
        
    }
    func createAndLoadInterstitial() -> GADInterstitial {
      let interstitiallocal = GADInterstitial(adUnitID: GOOGLE_UNIT_AD)
      interstitiallocal.delegate = self
      interstitiallocal.load(GADRequest())
      return interstitiallocal
    }
    @objc func finishedPlaying(notification: Notification) {
        player?.pause()
        player?.seek(to: .zero)
        player?.play()
    }
    
    
    override func viewDidLayoutSubviews() {
        playerLayer?.frame = playerView.bounds
        
    }
    
    @IBAction func actiionBack(_ sender: UIButton) {
        player?.pause()

        NotificationCenter.default.removeObserver(self, name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func galleryOption(_ sender: Any) {
        
        saveVideo(saveType: .saveToGallery)

    }
    @IBAction func whatsappOption(_ sender: Any) {
        
        saveVideo(saveType: .shareToFacebook)
        
//        uploadVideoOnFacebook()
        
//        let video = ShareVideo()
//        video.videoURL = videoUrl
//        let content = ShareVideoContent()
//        content.video = video
//        let dialog = ShareDialog()
//        dialog.fromViewController = self
//        dialog.delegate = self
//        dialog.shareContent = content
//        dialog.mode = .automatic
//        if dialog.canShow{
//            dialog.show()
//        }
    }
    
    @IBAction func instaOption(_ sender: Any) {
        saveVideo(saveType: .shareToInstagram)
    }
    @IBAction func moreOption(_ sender: Any) {
        
        openMoreOptionsForOptionSharing(url: videoUrl)
    }

    func openMoreOptionsForOptionSharing(url: URL){
        //        weak var weakSelf = self
        let vc = UIActivityViewController(activityItems: [url], applicationActivities: [])
        vc.completionWithItemsHandler = { (activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) -> Void in
            var message = ""
            if completed == true {
                if activityType == .postToFacebook
                {
                    message = "Successfully posted to facebook."
                }
                else if activityType == .postToTwitter
                {
                    message = "Successfully posted to twitter."
                }
                else if activityType == .message
                {
                    message = "Successfully sent via message."
                }
                else if activityType == .mail
                {
                    message = "Successfully sent via mail."
                }
                else if activityType == .copyToPasteboard
                {
                    message = "Successfully copy to pasteboard."
                }
                else if activityType == .assignToContact
                {
                    message = "Successfully assigned to contact."
                }
                else if activityType == .saveToCameraRoll
                {
                    message = "Successfully saved to camera roll."
                }
                else if activityType == .postToFlickr
                {
                    message = "Successfully posted to Flickr."
                }
                else if activityType == .airDrop
                {
                    message = "Successfully sent via airDrop."
                }
                else
                {
                    message = ""
                }
            }
            
            if message != "" {
                //                AlertManager.shared.alertWithTitle(title: "", message: "alert.")
                
            }
        }
        
        
        present(vc, animated: true)
        if let popOver = vc.popoverPresentationController {
            popOver.sourceView = self.view
            popOver.permittedArrowDirections = .down
            
        }
    }
    
    func saveVideo(saveType: SharingOptions){

        guard let url = videoUrl else{
            return
        }
        weak var weakSelf = self
        var videoIdentifier = ""
        var placeHolder: PHObjectPlaceholder!
        PHPhotoLibrary.shared().performChanges({
            if let myRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url) {
                placeHolder = myRequest.placeholderForCreatedAsset!
            }
        }, completionHandler: { (isSuccess, error) in
            if isSuccess{
                let fetchResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [placeHolder.localIdentifier], options: nil)
                if fetchResult.count > 0 {
                    videoIdentifier = ((fetchResult.firstObject)?.localIdentifier)!
                    let array = videoIdentifier.components(separatedBy: "/")
                    let stringName = array.first
                    let urlAssetString = "assets-library://asset/asset.MOV?id=" + stringName! + "&ext=MOV"
                    weakSelf?.videoIdentifier = urlAssetString
                    
                    DispatchQueue.main.async {
                        
                        switch saveType {
                        case .saveToGallery:
                            
                            AlertManager.shared.alertWithTitle(title: "", message: isSuccess ? "Successfully saved to photos." : "Oops! Some problem occurred.")

                            break
                        case .shareToFacebook:
                            weakSelf?.faceBookSharingWith(identifier: urlAssetString)
                            break
                        case .shareToInstagram:
                            weakSelf?.loadCameraRollAssetToInstagram(str: urlAssetString)
                            break
                        }
                    }
                }
            }
            
        })
    }
    
    private func faceBookSharingWith(identifier: String) {
       let installed = UIApplication.shared.canOpenURL(URL(string: "fb://")!)
        if installed {
            
            
//            let video = ShareVideo()
//            video.videoURL = URL(string: identifier)
//            let content = ShareVideoContent()
//            content.video = video
//            let dialog = ShareDialog()
//            dialog.fromViewController = self
//            dialog.delegate = self
//            dialog.shareContent = content
//            dialog.mode = .automatic
//            if dialog.canShow{
//                dialog.show()
//            }
            
            let video = ShareVideo()
            video.videoURL = URL(string: identifier)
            let content = ShareVideoContent()
            content.video = video
            
            let dialog = ShareDialog()
            dialog.fromViewController = self
            dialog.delegate = self
            dialog.shareContent = content
            dialog.mode = .automatic
            if dialog.canShow {
                dialog.show()
            }
        } else {
            AlertManager.shared.alertWithTitle(title: "", message: "Facebook not found.")

        }
        
    }
    func testFb(identifier: String){
        
        let video = ShareVideo()
        video.videoURL = URL(string: identifier)
        let content = ShareVideoContent()
        content.video = video

        let dialog = MessageDialog(
          content: content,
          delegate: self
        )

        guard dialog.canShow else {
            print("Facebook Messenger must be installed in order to share to it")
            return
        }

        dialog.show()

    }
//    private func faceBookSharingWith(identifier: String) {
//        let installed = UIApplication.shared.canOpenURL(URL(string: "fb://")!)
//        if installed {
//            let video = ShareVideo()
//            video.videoURL = URL(string: identifier)
//            let content = ShareVideoContent()
//            content.video = video
//
//            let dialog = ShareDialog()
//            dialog.fromViewController = self
//            dialog.delegate = self
//            dialog.shareContent = content
//            dialog.mode = .automatic
//            if dialog.canShow {
//                dialog.show()
//            }
//        } else {
//            Utility.showAlertViewController(title: Globals.kAppName, message: "Facebook not found.", viewController: self)
//
//        }
//    }
//    func saveVideo(saveType: ShareAction , videoUrl:URL) {
//
//        Utility.isGalleryAuthorized { (isAuthorized) in
//            if isAuthorized {
//                var placeHolder: PHObjectPlaceholder!
//                PHPhotoLibrary.shared().performChanges({
//                    if let myRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoUrl) {
//                        placeHolder = myRequest.placeholderForCreatedAsset
//                    }
//                }, completionHandler: { (completed, error) in
//                    if placeHolder != nil {
//                        let fetchResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [placeHolder.localIdentifier], options: nil)
//                        if fetchResult.count > 0 {
//                            let identifierUrl = fetchResult.firstObject?.localIdentifier
//                            let array = identifierUrl?.components(separatedBy: "/")
//                            let stringName = array?.first
//                            let urlAssetString = "assets-library://asset/asset.MOV?id=" + stringName! + "&ext=MOV"
//                            self.videoIdentifier = urlAssetString
//                            DispatchQueue.main.async {
//                                switch saveType {
//                                case .gallerysave:
//                                    Utility.showAlertViewController(title: "Congratulations!!!", message: "Video saved successfully", btnTitle1: "Ok", ok: { (action) in
//                                        Utility.showAppleReviewPopUp()
//                                    }, viewController: self)
//                                default:
//                                    self.shareURL(shareAction: saveType, url: nil)
//                                }
//                            }
//                        }
//                    }
//                })
//            }
//        }
//
//    }

    /*
    func showShareDialog<Content: SharingContent>(
        _ content: C,
        mode: ShareDialog.Mode = .automatic
        ) {
        let dialog = ShareDialog(
            fromViewController: self,
            content: content,
            delegate: self
        )
        dialog.mode = mode
        dialog.show()
    }
*/
    func loadCameraRollAssetToInstagram (str: String) {
        let instagramURL = URL(string: String(format: "instagram://library?LocalIdentifier=@", str))
        if UIApplication.shared.canOpenURL(instagramURL!){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(instagramURL!, options: [:], completionHandler: { (finsih) in
                })
            } else {
                UIApplication.shared.openURL(instagramURL!);
            }
        }else{
            
            let urlStr = "https://itunes.apple.com/in/app/instagram/id389801252?mt=8"
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                
            } else {
                UIApplication.shared.openURL(URL(string: urlStr)!)
            }

        }
    }
}
extension ShareViewController : SharingDelegate{
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
      print(results)
    }
    
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print(error.localizedDescription)

    }
    
    func sharerDidCancel(_ sharer: Sharing) {
        print(sharer)

    }
    
    
}
extension ShareViewController : GADInterstitialDelegate{
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
      interstitial = createAndLoadInterstitial()
    }

}
