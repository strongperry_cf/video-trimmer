//
//  FrontScreenVC.swift
//  camerafilter
//
//

import UIKit
import GoogleMobileAds

class FrontScreenVC: UIViewController {
    var interstitial: GADInterstitial!
    private var shouldDisplayAd = true

    private var isAdReady:Bool = false {
        didSet {
            if isAdReady && shouldDisplayAd {
                displayAd()
            }
        }
    }

    @IBOutlet weak var editButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

//        UserDefaults.standard.set("en", forKey: "lang")
//        let langCheck = UserDefaults.standard.value(forKey: "lang") as! String
//        editButton.setTitle("EDIT_BUTTON_TITLE".localizableString(loc: langCheck), for: .normal)
        
        interstitial = createAndLoadInterstitial()
        
    }


    @IBAction func settingButtonAction(_ sender: UIButton) {
        
        SKStoreReviewController.requestReview()

        let picker = SettingsViewController(nibName: "SettingsViewController", bundle: nil)
        self.navigationController?.pushViewController(picker, animated: true)

    }
    func createAndLoadInterstitial() -> GADInterstitial {
      let interstitiallocal = GADInterstitial(adUnitID: GOOGLE_UNIT_AD)
      interstitiallocal.delegate = self
      interstitiallocal.load(GADRequest())
        shouldDisplayAd = false

      return interstitiallocal
    }

    @IBAction func proButtonAction(_ sender: Any) {
        
    }
    @IBAction func adsButtonAction(_ sender: UIButton) {
        
        displayAd()
        
//        if interstitial.isReady {
//          interstitial.present(fromRootViewController: self)
//        } else {
//          print("Ad wasn't ready")
//        }

        
    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        
        let picker = VideoEdPickerVC(nibName: "VideoEdPickerVC", bundle: nil)
        picker.mediaTypePrefferred = .video
        self.navigationController?.pushViewController(picker, animated: true)
        
        

//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
////        vc.videoUrl = videoUrl
//        //        vc.delegate =  self
//        GlobalHelper.dispatchMainAfter(time: .now() + 0.2, execute: {[weak self] in
//            self?.navigationController?.pushViewController(vc, animated: true)
//        })

        
    }
    @IBAction func recordButtonAction(_ sender: UIButton) {
    }
    @IBAction func videoAction(_ sender: Any) {
        
//        let picker = ThirdPartyVideosVC(nibName: "ThirdPartyVideosVC", bundle: nil)
//        self.navigationController?.pushViewController(picker, animated: true)

        let storybd = UIStoryboard(name: "VideoEdAlbum", bundle: nil)
        let vc = storybd.instantiateViewController(withIdentifier: "CPPhotoPickerVC")
        self.navigationController?.pushViewController(vc, animated: true)

    }
}
extension FrontScreenVC : GADInterstitialDelegate{
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print(#function, "ad ready", interstitial.isReady)
        isAdReady = true
    }

    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
      interstitial = createAndLoadInterstitial()
    }
    func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        
    }
    private func displayAd() {
        print(#function, "ad ready", interstitial.isReady)
        if (interstitial.isReady) {
            shouldDisplayAd = false
            interstitial.present(fromRootViewController: self)
        }
    }

}
