//  VideoEdEditVC.swift
//  VideoTrimmer
//  Created by strongapps on 08/04/19.
//  Copyright © 2019 strongapps. All rights reserved.

import UIKit

enum categoryTools:Int {
    case VIDEOS
    case TRIM
    case CROP
    case COLOR
    case SPEED
    case ROTATE
    case GLITCHFILTER
    case MUSIC
    case SPLIT
    case ADJUST
    case ACVFILTER
}
class VideoEdEditVC: UIViewController, FilterViewDelegate , NVActivityIndicatorViewable,AdjustViewDelegate,CategoryViewDelegate, VideoTransitionViewDelegate,TransitionListVCDelegate ,TimelineDelegate  {

    var orintationValue : videoTrimAngleEnum = .kZero
    
    // MARK: - IBOutlets
    
    var blurTypeGlobal = -1
    var gradColorGlobal : GradientColor?
    @IBOutlet weak var btnSave              :UIButton!
    @IBOutlet weak var btnBack              :UIButton!
    @IBOutlet weak var btnCancelSaving      :UIButton!
    @IBOutlet weak var btnPlay              :UIButton!
    
    @IBOutlet weak var categoryView         :VideoEdEditorCategoryView!
    @IBOutlet weak var videoTransitionView  :VideoEdVideoTransitionView!
    @IBOutlet weak var bottomControlView    :UIView!
    @IBOutlet weak var subCategoryView      :UIView!
    @IBOutlet weak var mainContainer        :UIView!
    @IBOutlet weak var overlayView          :UIView!
    @IBOutlet weak var videoRepresentView   :UIView!
    @IBOutlet weak var viewLoaderBg         :UIView!
    @IBOutlet weak var viewMainLoader       :UIView!
    @IBOutlet weak var seekBarView          :UIView!
    @IBOutlet weak var gpuBackBlurView      :GPUImageView!
    
    var temporaryVisualView : UIVisualEffectView!
    @IBOutlet weak var backBlurView         :UIVisualEffectView!
//    @IBOutlet weak var seekBar              :UISlider!
    @IBOutlet weak var lblLoaderPercentage  :UILabel!
    @IBOutlet weak var viewSelectedThings   :UIView!
    
    @IBOutlet var videoRepresentViewWidthConstraint     : NSLayoutConstraint!
    @IBOutlet var videoRepresentViewHeightConstraint    : NSLayoutConstraint!
    @IBOutlet weak var mainContainerTopConstraint       : NSLayoutConstraint!
    
    var arraySelectedItems                 :NSMutableArray = []
    var compositionFromManager = VideoCompositionModelStruct()
    
//    var backViewThemeColor:UIColor =  UIColor(red: 45.0/255.0, green: 45.0/255.0, blue: 45.0/255.0, alpha: 1.0)
    var backViewThemeColor:UIColor =  .black

    // MARK: - Inherit
    var videoStartTime          :CGFloat = 0.0
    var videoEndTime            :CGFloat = 0.0
    var singleVideoUrl          :URL!
    var singleAvasset           :AVAsset!
    var singlePHAsset           :PHAsset?
    var selectedAssets          :[PHAsset] = []
    var selectedVideoData       :[VideoData] = []

    // MARK: - GPUObjects
    var gpuMovie            :GPUImageMovie?
    var gpuWriter           :GPUImageMovieWriter?
    lazy var gpuImageView   :GPUImageView = {
        return GPUImageView()
    }()
    
    var sharpenFilter       :GPUImageSharpenFilter!
    var adjustFilter        :GPUCustomColorControlFilter!
    var acvFilterGroup      :GPUImageFilterGroup!
    var acvBlendFilter      :GPUImageAlphaBlendFilter!
    var blurFilter          :GPUImageFilter!
    var lastFilter          :GPUImageFilter!
    var firstFilter         :GPUImageFilterGroup!
    var alphaBlend          :GPUImageAlphaBlendFilter!
    var elementBlendFilter  :GPUImageAlphaBlendFilter!
    var gpuElement          :GPUImageUIElement?
    var gpuComposition      :GPUImageMovieComposition?
    
    
    var gpuGlitchFilter                     :GPUImageFilter?

    //MARK:- AVFoundation and AVplayer Objectc
    var player              :AVPlayer?
    var playerItem          :AVPlayerItem?
    var avSyncLayer         :AVSynchronizedLayer?
    var mainItemsLayer      :CALayer?
    var avExporter            :AVAssetExportSession?
    
    
    //MARK:- Properties
    var videoUrl            :URL!
    // var customMusicUrl      :URL? = nil
    var thumbnailImage      :UIImage?
    var audioData           :AudioData?
    var audioComposition    :AVMutableComposition?

    var isPlaying = true
    var videoSize: CGSize = .zero {
        didSet{
            print("sizse changed")
        }
    }
    var videoDuration : CMTime!
    // var topView             :TopView!
    var NVindicator             :NVActivityIndicatorView!
    var timeObserverToken       :Any?

    
    
    let window = UIApplication.shared.keyWindow
    var _topSafeArea:CGFloat = 0.0
    var topSafeArea:CGFloat {
        get        {
            _topSafeArea = 0
            if #available(iOS 11.0, *) {
                _topSafeArea = (window?.safeAreaInsets.top)!
                
            }
            return _topSafeArea
    }}
    var _bottomSafeArea:CGFloat = 0.0
    var bottomSafeArea:CGFloat {
        get        {
            _bottomSafeArea = 0
            if #available(iOS 11.0, *) {
                _bottomSafeArea = (window?.safeAreaInsets.bottom)!
                
            }
            return _bottomSafeArea
    }}


    
    var sPositions = [CGPoint]()

    var adjustView          :VideoEdAdjustView?
    var cropView            :VideoEdCropView!
    var splitView           :VideoEdSplitView?
    var speedView           :SpeedView?
    var trimView            :TrimmingView?
    var glitchFilterView    :GlitchFilterView?

    var timeLineView        :VideoTimeLine?
    var selectedLayerData   :Any?
    
    var selectedItemLayer   :CALayer?
    var itemHandlerView     :StickerView?

    
    var filtersView         :VideoEdFilterView!
    var musicView           :MusicView!
    
    var musicTimelineView   :VideoEdTimelineView!
    weak var gpuImageWidth          : NSLayoutConstraint?
    weak var gpuImageHeight         : NSLayoutConstraint?
    
    var BOTTOM_VIEWS_HEIGHT : CGFloat = 100.0
    var isSeeking = false
    var currentTimeScale :CMTimeScale = 100
    var aspectCropRatio:CGFloat = 0.0
    var videoBackGroundColor:UIColor? =  nil
    
    weak var videoTimer: Timer?
    var isSaving = false
    
    //react camera

    
    var isINAPP_PURCHASE_CALLED_WHEN_SAVE_CLICKED = false
    
    
    var videoMiddleTime : CGFloat = 0.0

    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    //MARK:- viewDidLoad
    
    @IBAction func playFromBegining(_ sender: UIButton) {
        seekPlayer(to: 0.0)
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        videoRepresentView.backgroundColor = .yellow
        initResourceData()
        createUI()
        mainContainer.clipsToBounds = false
        addObservers()
        

        GlobalHelper.dispatchMainAfter(time: .now() + 0.2) {
            
            self.addTrimView()
            self.btnPlay.setImage(UIImage(named: "ic_pause"), for: .normal)
//            self.btnPlay.setImage(UIImage(named: "ic_play"), for: .normal)
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        themeAppearanceSetup()
        
        NotificationCenter.default.addObserver(self, selector: #selector(appWillResignActive(notification:)), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive(notification:)), name: UIApplication.didBecomeActiveNotification, object: nil)

        
    }
    func themeAppearanceSetup(){
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)

    }
    
    func addObservers()  {

        NotificationCenter.default.addObserver(self, selector: #selector(self.IndicatorNotify),  name: NSNotification.Name(rawValue: "IndicatorNotify"), object: nil)

    }
    
    func removeObservers() {
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("IndicatorNotify"), object: nil)

        

    }
    
    func initResourceData() {
        
        if singleVideoUrl != nil {
            
            let videoData = VideoData(url: singleVideoUrl)
            let startTime:CMTime = CMTime(seconds: Double(videoStartTime), preferredTimescale: 100)
            let endTime  :CMTime = CMTime(seconds: Double(videoEndTime), preferredTimescale: 100)

            // videoStartTime
            videoData.trimStartTime =  startTime
            videoData.trimEndTime = endTime
            if singlePHAsset != nil{
                
                selectedAssets.append(singlePHAsset!)
            }
            self.selectedVideoData.append(videoData)
            _ = VideoEdVideoManager().resolutionOfVideo(videoUrl: singleVideoUrl)
            
            let asset = AVURLAsset(url: singleVideoUrl)
            print(asset.fileSize ?? 0)

            
            self.updateVideoTransitionView()
            self.createCompositions()
             self.btnPlay.setImage(UIImage(named: "ic_pause"), for: .normal)
        }
        else{
            
            // Multiple Videos Selected
            for asset in selectedAssets {
                PHAssetManager.getUrlFromPHAsset(asset: asset) { (videoUrlStr) in
                    DispatchQueue.main.async { [weak self] in
                        let videoUrl = URL(string: videoUrlStr!)
                        let videoSize = VideoEdVideoManager().resolutionOfVideo(videoUrl: videoUrl!)
                        print("VideoSize is",videoSize! )
                        
                        let videoData = VideoData(url: videoUrl!)
                        videoData.trimStartTime =  CMTime.zero
                        
                        let duration =  VideoEdVideoManager().getDurationOfUrl(url: videoUrl!)                      //{
                            videoData.trimEndTime = CMTime(seconds: duration, preferredTimescale: 100)
                        // }
                        
                        self!.selectedVideoData.append(videoData)
                        
                        // Last Cycle of For Loop
                        if (self!.selectedVideoData.count == self!.selectedAssets.count)  {
                            self!.updateVideoTransitionView()
                            self!.createCompositions()
                            self!.btnPlay.setImage(UIImage(named: "ic_play"), for: .normal)

                        }
                    }
                }
            }
        }
        print("self!.selectedVideoData count is :",selectedVideoData.count);
        
    }
    
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        btnPlay.addTarget(self, action: #selector(playClicked(sender:)), for: .touchUpInside)
        
        if isINAPP_PURCHASE_CALLED_WHEN_SAVE_CLICKED && IAPManagerLocal.sharedInstance.isInAppPurchased(){
            self.btnSaveClicked(UIButton())
        }
        
        isINAPP_PURCHASE_CALLED_WHEN_SAVE_CLICKED = false
        
    }
    
    
    //MARK:- notification methods
    @objc func appWillResignActive(notification: Notification){
        
        print("appWillResignActive in editor screen")

        if(player != nil)   {
            player?.pause()
            isPlaying = false
            btnPlay.setImage(UIImage(named: "ic_play"), for: .normal)
        }
        if (gpuMovie != nil)    {
            gpuMovie?.endProcessing()
        }
        
        
        
        if isSaving{
            if gpuWriter != nil {
                gpuWriter?.cancelRecording()
                gpuWriter = nil
            }
            videoTimer?.invalidate()
            videoTimer = nil
            hideHud()
        }
        if(gpuComposition != nil)        {
            gpuComposition?.cancelProcessing()
            gpuComposition = nil
        }
        
        if gpuWriter != nil {
            gpuWriter?.cancelRecording()
            gpuWriter = nil
        }
        if avExporter != nil{
            avExporter?.cancelExport()
            avExporter = nil
        }
        
    }
    
    
    
    
    @objc func appDidBecomeActive(notification: Notification){
       
         print("appDidBecomeActive from editor screen")
        if(player != nil  && isPlaying)   {
//            player?.play()
            pauseAVPlayer(pause: false)
        }
        
        
        
        if isSaving{
            isSaving = false
        }else{
            if (gpuMovie != nil)    {
                gpuMovie?.startProcessing()
            }
        }
    }
    
    @objc func IndicatorNotify(notification: NSNotification){
        self.startAnimating(CGSize(width: 50, height: 50), message: "0%", type: .ballClipRotateMultiple)
    }
    


    
    func createUI() {
        
        mainContainerTopConstraint.constant = IS_IPHONE_X_Series ? 30.0 : 0.0
        updateColors()
        if categoryView != nil{
            categoryView.delegate =  self
            
        }
        videoTransitionView.delegate = self
    }
    
    func updateColors()  {
        
        seekBarView.backgroundColor =  backViewThemeColor
        videoTransitionView.backgroundColor = backViewThemeColor
        categoryView.backgroundColor =  backViewThemeColor
        self.view.backgroundColor = backViewThemeColor
    }
    //MARK:-
    //MARK:-
    func categoryViewDidSelect(_ indexPath: IndexPath) {
        
        if indexPath.row  < 3{
            
            hideAllOptionsViews()
        }
        let filter:categoryTools = categoryTools(rawValue: indexPath.item + 1)! // By Default VideoView Show .
        
        switch filter{
        
        case .VIDEOS:
            viewSelectedThings.isHidden = true
        case .MUSIC:
            addMusicView()
            break
        case .ACVFILTER:
            addACVFiltersView()
        case .ADJUST:
            addAdjustView()

        case .CROP:
            addAspectRatioView()
            break
        case .SPLIT:
            addSplitView()
            break
        case .SPEED:
            addSpeedView()
            break
        case .TRIM:
            addTrimView()
            break
        case .GLITCHFILTER:
            addGlitchFilterView()
            break
            
        case .COLOR:
            addColorsView()
            break
            
        case .ROTATE:
            var angle = 0
//            pauseAVPlayer(pause: true)
            if orintationValue == .kZero{
                orintationValue = .k90
                angle = 90
            }else if orintationValue == .k90{
                orintationValue = .k180
                angle = 180

            }else if orintationValue == .k180{
                orintationValue = .k270
                angle = 270

            }else if orintationValue == .k270{
                orintationValue = .kZero
                angle = 0

            }
            
//            reCreateAVCompositions()
//            pauseAVPlayer(pause: false)
            
            gpuImageView.rotate(angle: CGFloat(angle))
            
//            print("gpuImageView.originalFrame",gpuImageView.originalFrame)
//            print("gpuImageView.frame",gpuImageView.frame)
//            print("gpuBackBlurView.frame",gpuBackBlurView.frame)
//            print("mainContainer",mainContainer.frame)

            makeRotationWith(ratio: CGFloat(aspectCropRatio))

            break
        }
    }
    
    func makeRotationWith(ratio: CGFloat) {
        
        
//        aspectCropRatio = ratio
//        let videoSize = compositionFromManager.mixVideoCompStruct.renderSize
        let videoSize = gpuImageView.frame.size

        videoRepresentViewWidthConstraint.isActive = true
        videoRepresentViewHeightConstraint.isActive = true
        
        if ratio == 0 {
            
            let aspectFrame =  AVMakeRect(aspectRatio: videoSize, insideRect: mainContainer.frame) //AspectFit
            videoRepresentViewWidthConstraint.constant = aspectFrame.width
            videoRepresentViewHeightConstraint.constant = aspectFrame.height
            self.view.layoutIfNeeded()
            gpuImageView.frame = videoRepresentView.bounds
            if temporaryVisualView != nil && gpuBackBlurView != nil{
                temporaryVisualView.frame = gpuBackBlurView.bounds
                
            }
            
            return
        }
        
        // var aspectsize = CGSize(width: minSide, height: minSide)
        var aspectSize = CGSize(width: 2000.0, height: 2000.0)
        aspectSize.width *= ratio;
        
        var aspectFrame =  AVMakeRect(aspectRatio: aspectSize, insideRect: mainContainer.frame) //AspectFit
        
        
        videoRepresentViewWidthConstraint.constant = aspectFrame.width
        videoRepresentViewHeightConstraint.constant = aspectFrame.height
        self.view.layoutIfNeeded()
        aspectFrame.origin.x = 0.0
        aspectFrame.origin.y = 0.0
        let representSize =  AVMakeRect(aspectRatio: videoSize, insideRect: aspectFrame) //AspectFit
        // gpuImageView.frame = CGRect(x: 0, y: 0, width: representSize.width, height: representSize.height)
        gpuImageView.frame = representSize
        if temporaryVisualView != nil && gpuBackBlurView != nil{
            temporaryVisualView.frame = gpuBackBlurView.bounds
            
        }
        
    }
    // *********************************************** Video and Video Transition Section ***********************************
    
    // MARK:- Video Transiton View Delegates
    
    
    func transViewVideoDidSelect(index: Int) {
        print("videoDidSelect is ",index);
    
    }
 
    func updateVideoTransitionView()    {
    }
    
    func transViewTransitionDidSelect(index: Int) {
        
        print("transitionDidSelect is ",index);
        let vc = VideoEdTransitionListVC(nibName: "VideoEdTransitionListVC", bundle: nil)
        let videoObj:VideoData = selectedVideoData[index]
        vc.selectedVideoObj = videoObj
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func transitionVCDidSelect(_ videoObj: VideoData, _ indexPath: IndexPath) {
        
        for videoObj in selectedVideoData    {
            print("videoObj.transitionType is :",videoObj.transitionType)
        }
        reCreateAVCompositions()
    }
    
    func transViewAddNewVideo() {
        
        pauseAVPlayer(pause: true)
        print("addNewVideoDidSelct is ",index);
        let pickerVC = VideoEdPickerVC(nibName: "VideoEdPickerVC", bundle: nil)
        pickerVC.mediaTypePrefferred = .video
        pickerVC.delegate = self
        pickerVC.alreadySelectedMediaSeconds = (player?.currentItem?.duration.durationInInt)!
        pickerVC.alreadySelectedMediaCount = selectedAssets.count
        self.navigationController?.present(pickerVC, animated: true, completion: nil)
        
    }
    

    
    
    func transViewCopyVideo(index: Int) {
     
        copyVideoTrack()
    }
    
    func transViewDeleteVideo(index: Int) {
        

        
        AlertManager.shared.alertWithTitle(title: "", mesage: "Do you really want to delete video?", primaryActionText: "Delete", dismissActionText: "Cancel", completionHandler: {
            self.showHUD(progressLabel: "")
            self.selectedAssets.remove(at: index)
            self.selectedVideoData.remove(at: index)
            self.videoTransitionView.videoSeletedIndex.item = index - 1
            self.reCreateAVCompositions()
            self.hideHud()
            
        }) {
            
        }
        
    }
    
    //MARK:- Copy Video Track Module
    func copyVideoTrack() {
        
        if selectedVideoData.count >= Int(VIDEO_MAXIMUM_COUNT_LIMIT) {
            AlertManager.shared.alertWithMessage(message: "Duplicating this video will exceed maximum video count limit")

            return
        }
        let index = videoTransitionView.videoSeletedIndex.item
        if (index >= 0 ){
            
            // selectedAssets.
            var totalSeconds : CGFloat = 0
            let asset = selectedAssets[index]
            
            totalSeconds = (player?.currentItem?.duration.durationInSeconds)! + CGFloat(asset.duration) 
            debugPrint("totalDuration : ", totalSeconds)
            
            if totalSeconds >= VIDEO_MAXIMUM_LIMIT{

                AlertManager.shared.alertWithMessage(message: "Duplicating this video will exceed maximum video limit")

                return
            }
            let assetCopy =  asset.copy()
            selectedAssets.insert(assetCopy as! PHAsset,at: index + 1)
            
            let videoDataObj = selectedVideoData[index]
            let videoDataObjCopy =  videoDataObj.copy()
            selectedVideoData.insert(videoDataObjCopy as! VideoData,at: index + 1)

//            selectedVideoData.forEach{print("VideoData VideoUrl is ",$0.videoUrl)}
//            selectedAssets.forEach{print("Assets videoURL is :",$0.videoURL )}
            
            self.reCreateAVCompositions()
        }
    }
    
  
    
    // *********************************************** Adding Subviews and Their Delegates ***********************************

    //MARK:- Add Views
    
    //MARK:- Add Music View & Its Delegate

    func pauseAVPlayer (pause:Bool) {
        
        if pause {
            isPlaying = false
            player?.pause()
            btnPlay.setImage(UIImage(named: "ic_play"), for: .normal)
        }
        else
        {
            isPlaying = true
            player?.play()
            btnPlay.setImage(UIImage(named: "ic_pause"), for: .normal)
            speedToPlayer()

        }
    }
    
    func addMusicView(){
        
        
        if audioData != nil {
            if musicTimelineView == nil{
                musicTimelineView = VideoEdTimelineView.fromNib()
                musicTimelineView.delegate = self

            }else{
                musicTimelineView.playSelectedSong()
            }
            musicTimelineView.startTimeSlider.maximumValue = Float(videoDuration!.seconds)
            musicTimelineView.isHidden = false
            musicTimelineView.startTimeSlider.value = Float((audioData?.audioAddingTime?.seconds)!)
            var frame = videoTransitionView.frame
            frame.size.height += categoryView.frame.size.height
            musicTimelineView.frame = frame
            bottomControlView.addSubview(musicTimelineView)
            
            musicTimelineView.multiSlider.value = [CGFloat(Float((audioData?.trimStartTime?.durationInFloat)!)),CGFloat(Float((audioData?.trimEndTime?.durationInFloat)!))]
            musicTimelineView.backgroundColor = backViewThemeColor
            musicTimelineView.mainView.backgroundColor =  backViewThemeColor
    

            return
        }

        pauseAVPlayer(pause: true)
        isPlaying = false
        player?.pause()
        btnPlay.setImage(UIImage(named: "ic_play"), for: .normal)

    }
    
    func overlayView_UserInteraction(enabled:Bool)  {
//        overlayView.isUserInteractionEnabled =  enabled
    }
    
    func topButtonShow(show:Bool)
    {
        btnBack.isHidden = !show
        btnSave.isHidden = !show
    }
    

    
    func updateMusicPlayer(with newMusic: URL?,startTime:Double,endTime :Double) {

        self.stopAnimating()
        
        if (newMusic == nil)    {
            audioData = nil
        }
        else {
            audioData?.audioUrl = newMusic
            audioData?.trimStartTime = CMTime(seconds: startTime, preferredTimescale: 10)
            audioData?.trimEndTime =  CMTime(seconds: endTime, preferredTimescale: 10)
            audioComposition = setRepitionofMusic(newMusic: newMusic, trimStartTime: startTime, trimEndTime: endTime, repeatMusic:  true, musicInsertTime: Double(musicTimelineView.startTimeSlider!.value))
            

        }
        

    }
    
    func timelineDoneBtnClicked() {
        
        musicTimelineView.isHidden =  true
//        overlayView_UserInteraction(enabled: true)
        topButtonShow(show: true)
        btnPlay.isUserInteractionEnabled = true
        
        player?.seek(to: CMTime.zero)
        player?.play()
        btnPlay.setImage(UIImage(named: "ic_pause"), for: .normal)
        isPlaying = true
        
    }
    
    func timelineMusicDidChange() {
       
    }
    
    func timelineMusicTrimChange(startTime: Double, endTime: Double) {
        if audioData != nil{
            
            audioData?.trimStartTime = CMTime(seconds: startTime, preferredTimescale: 1000)
            audioData?.trimEndTime = CMTime(seconds: endTime, preferredTimescale: 1000)
            audioData?.audioAddingTime = CMTime(seconds: Double(musicTimelineView.startTimeSlider!.value), preferredTimescale: 1000)
            
        }
//        updateMusicPlayer(with: audioData?.audioUrl, startTime: startTime, endTime: endTime)
//        reCreateAVCompositions()
        reCreateWhenOnlyAudioChages()
        

    }
    
    func insertAudioInSlideshow(composition : AVMutableComposition ) -> AVMutableComposition{
        
//        let calcTimeForVideo = videoDuration.seconds
        let calcTimeForVideo = composition.duration.seconds
//        debugPrint("calcTimeForVideo",calcTimeForVideo)

        let endTime:CMTime = CMTime(seconds: Double(calcTimeForVideo), preferredTimescale: 100)
        
        let audioAsset = AVAsset(url: audioData!.audioUrl!)
        var minTime = CMTimeMinimum(CMTimeSubtract(endTime, audioData!.audioAddingTime!), CMTimeSubtract(audioData!.trimEndTime!, audioData!.trimStartTime!))
        
//        let minTime = audioData?.trimEndTime
        let startTime = audioData?.trimStartTime
        var addingTime = audioData?.audioAddingTime
        
        
        if minTime.seconds < 0 {
            audioData?.audioAddingTime = .zero
            addingTime = audioData?.audioAddingTime
            minTime = composition.duration
        }

        let audioTimeRange = CMTimeRange(start: startTime! , duration:  minTime)
        let assetAudioTrack: AVAssetTrack = audioAsset.tracks(withMediaType: AVMediaType.audio)[0]
        
        if audioData!.repeatMusic{
            
            if minTime<endTime{
                
                let compositionAudioVideo: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
                let divideTimeCount =  endTime.durationInSeconds /  minTime.durationInSeconds
                
                var mutCmtime : CMTime = .zero
                for _ in 0...Int(divideTimeCount) {
                    
                    
                    do {
                        try compositionAudioVideo.insertTimeRange(audioTimeRange, of: assetAudioTrack, at: mutCmtime)
                        
                    } catch {
//                        print("Inserting time range failed. ", error)
                    }
                    
                    mutCmtime = mutCmtime + minTime
//                    debugPrint("mutCmtime=",mutCmtime)
                    
                }
                
            }else{
                let compositionAudioVideo: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
                
                do {
                    try compositionAudioVideo.insertTimeRange(audioTimeRange, of: assetAudioTrack, at: .zero)
                    
                } catch {
                    // Handle the error
                    print("Inserting time range failed. ", error)
                }
            }
        }else{
            
//            let compositionAudioVideo: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
            print("composition.tracks. ", composition.tracks)
            let compositionAudioVideo: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID(audioTrackCustomTrackID))!

            do {
                try compositionAudioVideo.insertTimeRange(audioTimeRange, of: assetAudioTrack, at: addingTime!)
                
            } catch {
                // Handle the error
                print("Inserting time range failed. ", error)
            }
        }
        debugPrint("addingTime ==>>>",addingTime?.seconds ?? 0)
        
        return composition
    }
    let audioTrackCustomTrackID = 1001

    //MARK:- Create ACV Filter View And Delegates

    func addACVFiltersView(){
        
      /*
        if filtersView == nil{
            filtersView = VideoEdFilterView.initWith(andImageView: gpuImageView, gpuMovie: gpuMovie!)
            filtersView.delegate = self
            if let thumb = VideoEdVideoManager().generateThumbnail(asset: compositionFromManager.mixCompStruct,at: .zero, of : CGSize(width: 150, height: 150)){
                filtersView.firstFrame = thumb
            }
        }
        filtersView.frame = CGRect(x: 0, y: SCREEN_HEIGHT - BOTTOM_VIEWS_HEIGHT*1.8, width: SCREEN_WIDTH, height: BOTTOM_VIEWS_HEIGHT)
        self.view.addSubview(filtersView)
 */

        // viewSelectedThings.isHidden = false
        
        if filtersView == nil   {
           // let frame = CGRect(x: 0, y: SCREEN_HEIGHT - BOTTOM_VIEWS_HEIGHT*1.8, width: SCREEN_WIDTH, height: BOTTOM_VIEWS_HEIGHT)
            var frame = videoTransitionView.frame
            frame.size.height += categoryView.frame.height
            filtersView = VideoEdFilterView.initWithFrame(frame: frame)
            filtersView.delegate = self
            
            
            filtersView.backgroundColor = UIColor.black
            // self.view.addSubview(filtersView)
            bottomControlView.addSubview(filtersView)
        }
        if let thumb = VideoEdVideoManager().generateThumbnail(asset: compositionFromManager.mixCompStruct, compositionFromManager.mixVideoCompStruct , at: .zero, of: CGSize(width: 150, height: 150)){
            thumbnailImage = thumb
            filtersView.firstFrame = thumb
            filtersView.cv_filters.reloadData()

        }

        filtersView.isHidden = false
        filtersView.backgroundColor = backViewThemeColor
        filtersView.sliderView.backgroundColor =  backViewThemeColor
    }
    
    //MARK:- Filter view delegate
    func didSelectFilter() {
        if !isPlaying{
            isPlaying = true
            player?.play()
            btnPlay.setImage(UIImage(named: "ic_pause"), for: .normal)
        }
    }
    
    func didSelectACVFilter(filter: GPUImageFilterGroup, alpha: CGFloat) {
        
        print("didSelectACVFilter")
        
        adjustFilter.removeTarget(acvFilterGroup)
        acvFilterGroup.removeAllTargets()
        acvFilterGroup = nil
        acvFilterGroup = filter
        adjustFilter.addTarget(acvFilterGroup)
        acvFilterGroup.addTarget(acvBlendFilter)
        
    }
    
    func updateACVFilter(alpha: CGFloat) {
        print("updateACVFilter alpha is ",alpha)
        acvBlendFilter.mix = alpha
    }
    
    //MARK:- Add Adjust View

    func addAdjustView()  {
        
        if adjustView == nil{
            //let frame = CGRect(x: 0, y: SCREEN_HEIGHT - BOTTOM_VIEWS_HEIGHT*1.8, width: SCREEN_WIDTH, height: BOTTOM_VIEWS_HEIGHT)
            var frame = videoTransitionView.frame
            frame.size.height += categoryView.frame.height
            adjustView = VideoEdAdjustView.initWithFrame(frame: frame)
            adjustView?.delegate =  self
            // self.view.addSubview(adjustView!)
            bottomControlView!.addSubview(adjustView!)
        }
        
        adjustView?.isHidden = false
        
        adjustView?.adjustView.backgroundColor = backViewThemeColor
        adjustView?.adjustCV.backgroundColor   = backViewThemeColor

        
    }
    
    func updateAdjustEffect(filterValues: AdjustFilterValues) {
        
        if !isPlaying {
            pauseAVPlayer(pause: false)
        }
        
        sharpenFilter.sharpness     = filterValues.sharpness.current
        adjustFilter.brightness     = filterValues.brightness.current
        adjustFilter.contrast       = filterValues.contrast.current
        adjustFilter.exposure       = filterValues.exposure.current
        adjustFilter.saturation     = filterValues.saturation.current
        adjustFilter.temperature    = filterValues.temprature.current
        adjustFilter.hue            = filterValues.hue.current
        adjustFilter.vignetteStart  = filterValues.vignette.current
        adjustFilter.vignetteEnd    = filterValues.vignette.current + 0.23

    }

    
    


    //MARK:- denit Views
    deinit {
        player?.pause()
        if gpuMovie != nil  {
            gpuMovie?.cancelProcessing()
        }
        
        removeObservers()
        NotificationCenter.default.removeObserver(self)
        print("edit vc deinit")
    }
    
    func deinitalizeComponenets(){
        

        
        if firstFilter != nil{
            firstFilter.removeAllTargets()
            firstFilter = nil
        }
        
        if alphaBlend != nil{
            alphaBlend.removeAllTargets()
            alphaBlend = nil
        }
        
        if cropView != nil{
            //cropView.reset()
            cropView.removeFromSuperview()
            cropView = nil
        }
        

        
        if musicView != nil{
//            musicView.reset()
            musicView.removeFromSuperview()
            musicView = nil
        }
        

        if filtersView != nil{
            filtersView.reset()
            filtersView.removeFromSuperview()
            filtersView = nil
        }
        
        // stickerLayerArray.removeAll()
        btnPlay.removeTarget(nil, action: nil, for: .allEvents)
        selectedItemLayer = nil
        gpuMovie?.removeAllTargets()
        gpuMovie = nil
        gpuWriter = nil
        
    }
    

    
    
// ********************************************************************************************************************************************* //
// MARK :-  Setup AVPLayer and Compositions
// ********************************************************************************************************************************************* //
    
    func createCompositions()  {
        
        weak var weakSelf = self
        /*
        if videoStartTime != nil { // If comes from trimmer view
            guard let compositionNew = VideoEdCompositionManager.getCompositionbyTriming(videoAsset: videoAssetFromUrl!, audioAsset: safeSongUrl, from: videoStartTime, to: videoEndTime)else{
                return
            }
            compositionFromManager = compositionNew
            weakSelf!.setupPlayer()
        }
        else{
            */
        
            // If selects multiple videos without trimming
            var maximumSize =  CGSize.zero
            let videoDataObj = selectedVideoData[0]
        if let url = videoDataObj.videoUrl{
         
            
            maximumSize = VideoEdVideoManager().resolutionOfVideo(videoUrl: url)!
            VideoEdVideoManager().mergeVideos_WithSpeed(videoArray: selectedVideoData, maxSize: maximumSize,isAspectFit: false, complitionBlock: {
                (mixComposition, videoComposition, audioMix) in
                weakSelf?.compositionFromManager.mixCompStruct = mixComposition
                weakSelf?.compositionFromManager.mixVideoCompStruct = videoComposition
                weakSelf?.setupAVPlayer()
                

                
                
            })
            
        }
        //}
    }
    
    
    func setupAVPlayer(){
        
        if  compositionFromManager.mixCompStruct == nil  {
            // Invalid Video
            return;
        }
        
        if(thumbnailImage == nil)            {
            if let thumb = VideoEdVideoManager().generateThumbnail(asset: compositionFromManager.mixCompStruct, compositionFromManager.mixVideoCompStruct  , at: .zero, of: CGSize(width: 150, height: 150)){
                thumbnailImage = thumb
                
            }
        }
        
        playerItem  = AVPlayerItem(asset: compositionFromManager.mixCompStruct)
        playerItem?.videoComposition = compositionFromManager.mixVideoCompStruct
        
        player  = AVPlayer(playerItem: playerItem!)
        videoDuration = compositionFromManager.mixCompStruct.duration //  composition.videoTimeRangeStruct.end - composition.videoTimeRangeStruct.start
        
        GlobalHelper.dispatchMainAfter(time: .now() + 0.5) {
            
            self.player?.play()
            self.isPlaying = true
        }
        
        btnPlay.setImage(UIImage(named: "ic_pause"), for: .normal)
        initGPUPlayer(item: playerItem!)
//        self.lblDuration.text = compositionFromManager.mixCompStruct!.duration.durationInSeconds.convertSecondsToDurationString()

        // NotificationCenter.default.addObserver(self, selector: #selector(playerDidEndPlaying(notification:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem!)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem!)
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidEndPlaying(notification:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem!)

        addPeriodicTimeObserver()
        
        setupSeekBar()
        currentTimeScale = compositionFromManager.mixCompStruct.duration.timescale
        
        GlobalHelper.dispatchMainAfter(time: .now() + 0.1) {
            self.cropViewChangeRatio(ratio: 0)
        }


    }
    func reCreateWhenOnlyAudioChages(){
        weak var weakSelf = self

        if self.audioData != nil{
            let needAudioTrack = weakSelf?.compositionFromManager.mixCompStruct.track(withTrackID: CMPersistentTrackID(self.audioTrackCustomTrackID))
            if needAudioTrack != nil {
                weakSelf?.compositionFromManager.mixCompStruct.removeTrack(needAudioTrack!)
            }
            weakSelf?.compositionFromManager.mixCompStruct = self.insertAudioInSlideshow(composition: (weakSelf?.compositionFromManager.mixCompStruct!)!)
        }
        if musicTimelineView != nil && !musicTimelineView.isHidden && audioData != nil{
            player?.seek(to: audioData!.audioAddingTime!)
        }else{
            
            player?.seek(to: CMTime.zero)
        }
    }
    func reCreateAVCompositions()   {
        weak var weakSelf = self
        var maximumSize =  CGSize.zero
        let videoDataObj = selectedVideoData[0]
        let url = videoDataObj.videoUrl
        maximumSize = VideoEdVideoManager().resolutionOfVideo(videoUrl: url!)!
        updateVideoTransitionView()
        
        VideoEdVideoManager().mergeVideos_WithSpeed(videoArray: selectedVideoData, maxSize: maximumSize,isAspectFit: false,orintationValue, complitionBlock: {
            (mixComposition, videoComposition, audioMix) in
            
            if self.audioData != nil{
                let needAudioTrack = weakSelf?.compositionFromManager.mixCompStruct.track(withTrackID: CMPersistentTrackID(self.audioTrackCustomTrackID))
                if needAudioTrack != nil {
                    weakSelf?.compositionFromManager.mixCompStruct.removeTrack(needAudioTrack!)
                }
                weakSelf?.compositionFromManager.mixCompStruct = self.insertAudioInSlideshow(composition: mixComposition!)
            }else{
                weakSelf?.compositionFromManager.mixCompStruct = mixComposition // 28junechanges
            }
            weakSelf?.compositionFromManager.mixVideoCompStruct = videoComposition
            

            weakSelf?.updateAVPlayeCompositions()
            weakSelf?.updateGpuFrame(videoSize: maximumSize)//newchangejune19
        })
    }
    
    func reCreateAVCompositionsAfterRatio()   {
        weak var weakSelf = self
        var maximumSize =  CGSize.zero
        let videoDataObj = selectedVideoData[0]
        let url = videoDataObj.videoUrl
        maximumSize = VideoEdVideoManager().resolutionOfVideo(videoUrl: url!)!
        updateVideoTransitionView()
        
        VideoEdVideoManager().mergeVideos_WithSpeed(videoArray: selectedVideoData, maxSize: maximumSize,isAspectFit: false,orintationValue, complitionBlock: {
            (mixComposition, videoComposition, audioMix) in
            
            if self.audioData != nil{
                let needAudioTrack = weakSelf?.compositionFromManager.mixCompStruct.track(withTrackID: CMPersistentTrackID(self.audioTrackCustomTrackID))
                if needAudioTrack != nil {
                    weakSelf?.compositionFromManager.mixCompStruct.removeTrack(needAudioTrack!)
                }
                weakSelf?.compositionFromManager.mixCompStruct = self.insertAudioInSlideshow(composition: mixComposition!)
            }else{
                weakSelf?.compositionFromManager.mixCompStruct = mixComposition // 28junechanges
            }
            weakSelf?.compositionFromManager.mixVideoCompStruct = videoComposition
            
            
            weakSelf?.updateAVPlayeCompositions()
            weakSelf?.updateGpuFrame(videoSize: maximumSize)//newchangejune19
            weakSelf?.cropViewChangeRatio(ratio: self.aspectCropRatio)

        })
    }
    func updateGpuFrame(videoSize : CGSize){
//        let videoSize = compositionFromManager.mixVideoCompStruct.renderSize

//        let mainContainerSize = CGSize(width: SCREEN_WIDTH, height: SCREEN_HEIGHT - 290 - topSafeArea - bottomSafeArea)
//        let calSize =  AVMakeRect(aspectRatio: videoSize, insideRect: CGRect(x: 0, y: 0, width: mainContainerSize.width, height: mainContainerSize.height)) //AspectFit
        let calSize =  AVMakeRect(aspectRatio: videoSize, insideRect: CGRect(x: 0, y: 0, width: mainContainer.frame.size.width, height: mainContainer.frame.size.height)) //18aug2019 change

        
//        aspectCropRatio = 0.0
        if cropView != nil{
//            cropView.updateDefault()
        }
        GlobalHelper.dispatchDelay(deadLine: .now() + 0.1) { [weak self] in
            self!.videoRepresentViewWidthConstraint.isActive = true
            self!.videoRepresentViewHeightConstraint.isActive = true
            self!.videoRepresentViewWidthConstraint.constant = calSize.width
            self!.videoRepresentViewHeightConstraint.constant = calSize.height
        }
        
//        print("videoCompositionSize is :",videoSize)
//        print("calSize is :",calSize)
        
        gpuImageView.frame = CGRect(x: 0, y: 0, width: calSize.width, height: calSize.height)
        if temporaryVisualView != nil && gpuBackBlurView != nil{
            temporaryVisualView.frame = gpuBackBlurView.bounds
            
        }

    }
    func updateAVPlayeCompositions() {
        
        if  compositionFromManager.mixCompStruct == nil  {
            // Invalid Video
            // Show Alert
            return;
        }
        if (player!.isPlaying)  {
            pauseAVPlayer(pause: true)
        }
        
        if player != nil
        {
//            NotificationCenter.default.removeObserver(self, name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem!)
            NotificationCenter.default.removeObserver(self, name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        }
        playerItem  = AVPlayerItem(asset: compositionFromManager.mixCompStruct)
        playerItem?.videoComposition = compositionFromManager.mixVideoCompStruct
        player?.replaceCurrentItem(with: playerItem)
       
        if player?.currentItem != nil{
            
            NotificationCenter.default.addObserver(self, selector: #selector(playerDidEndPlaying(notification:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem!)
        }

        
        videoDuration = compositionFromManager.mixCompStruct.duration //  composition.videoTimeRangeStruct.end - composition.videoTimeRangeStruct.start
        
        if musicTimelineView != nil && !musicTimelineView.isHidden && audioData != nil{
            player?.seek(to: audioData!.audioAddingTime!)
        }else{
            
            player?.seek(to: CMTime.zero)
        }
        
        pauseAVPlayer(pause: false)
        

        // GPU Movie allocation
        if (gpuMovie != nil)    {
            gpuMovie?.cancelProcessing()
            gpuMovie?.removeAllTargets()
            gpuMovie = nil
        }
        
        gpuMovie = GPUImageMovie(playerItem: playerItem)
        gpuMovie?.playAtActualSpeed = true
        gpuMovie?.addTarget(sharpenFilter)
        gpuMovie?.startProcessing()
        
        //addPeriodicTimeObserver()
        //setupSeekBar()
        currentTimeScale = compositionFromManager.mixCompStruct.duration.timescale
//        seekBar.maximumValue = Float(videoDuration.seconds)
    }
    
    // MARK :-  Setup GPUImagePlayer And Filters


    func initGPUPlayer(item: AVPlayerItem) {
        
        // GPU Movie allocation
        if (gpuMovie != nil)    {
            gpuMovie?.cancelProcessing()
            gpuMovie = nil
        }
        
        gpuMovie = GPUImageMovie(playerItem: item)
        gpuMovie?.playAtActualSpeed = true
        
        sharpenFilter =  GPUImageSharpenFilter()
        adjustFilter  = GPUCustomColorControlFilter()
        acvFilterGroup = VideoEdCustomFilter.getACVFilterWith(CustomFilterType(rawValue: 0)!)  // alloc default filter
        acvBlendFilter =  GPUImageAlphaBlendFilter()

        gpuGlitchFilter = VideoEdGlitchHelper().createGlitchFilter(IndexPath(row: 0, section: 0))
        gpuMovie?.addTarget(sharpenFilter)
        sharpenFilter.addTarget(gpuGlitchFilter)
        gpuGlitchFilter!.addTarget(adjustFilter)
        adjustFilter.addTarget(acvFilterGroup)
        adjustFilter.addTarget(acvBlendFilter)
        acvFilterGroup.addTarget(acvBlendFilter)
        acvBlendFilter.addTarget(gpuImageView)
        acvBlendFilter.addTarget(gpuBackBlurView)
        gpuMovie?.startProcessing()
        
        sharpenFilter.forceProcessing(atSizeRespectingAspectRatio: CGSize(width: 700, height: 700))
        
        gpuBackBlurView.fillMode =  kGPUImageFillModePreserveAspectRatioAndFill
        
        let videoSize = compositionFromManager.mixVideoCompStruct.renderSize
//        let mainContainerSize = CGSize(width: SCREEN_WIDTH, height: SCREEN_HEIGHT - 290 - topSafeArea - bottomSafeArea)
//        let calSize =  AVMakeRect(aspectRatio: videoSize, insideRect: CGRect(x: 0, y: 0, width: mainContainerSize.width, height: mainContainerSize.height)) //AspectFit
        let calSize =  AVMakeRect(aspectRatio: videoSize, insideRect: CGRect(x: 0, y: 0, width: mainContainer.frame.size.width, height: mainContainer.frame.size.height)) //23july2019 change

        GlobalHelper.dispatchDelay(deadLine: .now() + 0.1) { [weak self] in
            self!.videoRepresentViewWidthConstraint.isActive = true
            self!.videoRepresentViewHeightConstraint.isActive = true
            self!.videoRepresentViewWidthConstraint.constant = calSize.width
            self!.videoRepresentViewHeightConstraint.constant = calSize.height
        }
        
//        print("videoCompositionSize is :",videoSize)
//        print("calSize is :",calSize)
        
        gpuImageView.frame = CGRect(x: 0, y: 0, width: calSize.width, height: calSize.height)
        gpuImageView.removeFromSuperview()
        if temporaryVisualView == nil{
            
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            temporaryVisualView = UIVisualEffectView(effect: blurEffect)
            temporaryVisualView.frame = gpuBackBlurView.bounds
            let blurLayer = temporaryVisualView.layer.sublayers![0]
            gpuBackBlurView.layer.addSublayer(blurLayer)
            
        }

        self.videoRepresentView.addSubview(self.gpuImageView)
        self.videoRepresentView.clipsToBounds = true
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        gestureRecognizer.cancelsTouchesInView = true
        self.gpuImageView.addGestureRecognizer(gestureRecognizer)
        self.gpuImageView.isUserInteractionEnabled = true
//        self.overlayView.removeFromSuperview()
//        let pinchGesture = UIPinchGestureRecognizer(target: self, action:#selector(pinchedView))
//        self.gpuImageView.addGestureRecognizer(pinchGesture)

    }
    @objc func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            
            let translation = gestureRecognizer.translation(in: self.view)
            // note: 'view' is optional and need to be unwrapped
            gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
            
            print("frame ",self.gpuImageView.frame)
            print("center ",gestureRecognizer.view!.center)
        }
    }

    /*
    
    // MARK: - Add constraints for video container
    func defaultVideoPosition() {
        
        view.layoutIfNeeded() // need to get exact position
        //videoSize = composition.mixCompStruct.naturalSize
        videoSize = compositionFromManager.mixVideoCompStruct.renderSize
        let playerFrame = VideoEdHelper().calculateRectOfImage(isAspectFit: true, parentFrame: mainContainer.frame, childSize: videoSize)
        view.addConstraintSameCenterXY(mainContainer, and: videoContainer)
        if let videoContainerWidth = videoContainerWidth {
            videoContainer.removeConstraint(videoContainerWidth)
        }
        if let videoContainerHeight = videoContainerHeight {
            videoContainer.removeConstraint(videoContainerHeight)
        }
        videoContainerWidth = videoContainer.addConstraintForWidth(playerFrame.width)
        videoContainerHeight = videoContainer.addConstraintForHeight(playerFrame.height)
        videoContainer.layoutIfNeeded()
        shadowView.frame = playerFrame
        print("video container bounds,",videoContainer.bounds)
        resetGpuImagePosition()
    }

     func resetGpuImagePosition(){
        view.layoutIfNeeded()
        guard let parent = gpuImageView.superview else { return }
        let playerFrame = VideoEdHelper().calculateRectOfImage(isAspectFit: true, parentFrame: parent.bounds, childSize: videoSize)
        if let gpuImageWidth = gpuImageWidth {
            gpuImageView.removeConstraint(gpuImageWidth)
        }
        if let gpuImageHeight = gpuImageHeight {
            gpuImageView.removeConstraint(gpuImageHeight)
        }
        
        gpuImageWidth = gpuImageView.addConstraintForWidth(playerFrame.width)
        gpuImageHeight = gpuImageView.addConstraintForHeight(playerFrame.height)
        view.addConstraintSameCenterXY(parent, and: gpuImageView)
        updateSyncLayerPosition()
        shadowView.frame = parent.frame
    }
    
    func updateSyncLayerPosition() {
        parentLayer.frame = videoContainer.bounds
    }
    
    func initLayers(_ item: AVPlayerItem) {
        parentLayer.contentsGravity = .resizeAspect
        parentLayer.removeFromSuperlayer()
        videoContainer.layer.addSublayer(parentLayer)
    }
    
    */
    
    
    func setupSeekBar(){
//        seekBar.isHidden = false
//        seekBar.minimumValue = 0.0
//        seekBar.maximumValue = Float(videoDuration.seconds)
//        seekBar.value = 0.0
//        seekBar.addTarget(self, action: #selector(touchEvent(slider:for:)), for: .valueChanged)
//        seekBar.setThumbImage(UIImage(named: "ic_slider_line"), for: .normal)
//        seekBar.setThumbImage(UIImage(named: "ic_slider_line"), for: .highlighted)
    }
    
    
    @objc func playerDidEndPlaying(notification: Notification){
        if let item = notification.object as? AVPlayerItem, item == playerItem{
//            seekBar.value = 0.0
            player?.pause()
            player?.seek(to: .zero)
            player?.play()
            speedToPlayer()
        }
    }
    
    //MARK:- Actions
    @objc func touchEvent(slider: UISlider,for event: UIEvent){
//        if let touchEvent = event.allTouches?.first {
//            switch touchEvent.phase {
//            case .began:
//                player?.pause()
//                btnPlay.setImage(UIImage(named: "ic_play"), for: .normal)
//                isPlaying = false
//            case .moved:
//                seekPlayer(to: Double(slider.value))
//                player?.seek(to: CMTime(seconds: Double(slider.value), preferredTimescale: 6000))
//
//            case.ended,.cancelled:
//
//                // Update Music TImeline
//                if(audioData != nil && audioData?.audioUrl != nil && musicTimelineView != nil && musicTimelineView.isHidden == false ){
//                    updateMusicPlayer(with: audioData?.audioUrl, startTime: (audioData?.trimStartTime!.seconds)!, endTime: (audioData?.trimEndTime!.seconds)!)
//                }
//
//
//            default:
//                break
//            }
//        }
    }
    
    @IBAction func ActionCancleVideoSaving(_ sender: Any) {
        hideHud()
    }
    
    @objc func playClicked(sender: UIButton){
        isPlaying = !isPlaying
        if isPlaying{
            itemHandlerView?.removeFromSuperview()
            
            DispatchQueue.main.async {
                
                self.player?.play()
                self.speedToPlayer()

                sender.setImage(UIImage(named: "ic_pause"), for: .normal)
            }
        }else{
            player?.pause()
            sender.setImage(UIImage(named: "ic_play"), for: .normal)
        }
    }
    
   
    @IBAction func btnBackClicked(_ sender: UIButton) {
        
        
       AlertManager.shared.alertWithTitle(title: "", mesage: "Would you like to go Dashboard screen?", primaryActionText: "Dashboard", dismissActionText: "Cancel", completionHandler: {
            self.dismissVC()
        }) {
            
        }
    }
    
    func dismissVC()  {
        
        releaseComponents()
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func releaseComponents(){
        
        removePeriodicTimeObserver()
        player?.pause()
        player = nil
        playerItem = nil
        gpuMovie?.removeAllTargets()
        gpuMovie?.endProcessing()
        gpuMovie = nil
        gpuImageView.removeFromSuperview()
        
        self.deinitalizeComponenets()
        NotificationCenter.default.removeObserver(self)
    }
    @IBAction func btnSaveClicked(_ sender: UIButton) {

        self.player?.pause()
        

        if !IS_APP_FREE_TEMP && !IAPManagerLocal.sharedInstance.isInAppPurchased(){
            isINAPP_PURCHASE_CALLED_WHEN_SAVE_CLICKED = true
            IAPManagerLocal.sharedInstance.openInapScreen()
            
//            IAPManagerLocal.sharedInstance.openInapScreen { (status) in
//                self.btnSaveClicked(sender)
//            }
            /*
            if IAPManagerLocal.sharedInstance.productsIAP.count < 1 {
                AlertManager.shared.alertWithMessage(message: "Products not loaded yet ,Please try again")
                IAPManagerLocal.sharedInstance.loadProducts()
                return
            }
           
            let subs  = SubscriptionVC()
            subs.inappStatusHandler = { status in
                
                if status {
                    
                    GlobalHelper.dispatchMainAfter(time: .now() + 0.5, execute: {
                        self.btnSaveClicked(sender)
                        
                    })
                    
                }
            }
            if let topController = UIApplication.topViewController() {
                topController.present(subs, animated: true, completion: nil)
            }
            */
            return
        }
        self.showHUD(progressLabel: "")

        gpuMovie?.cancelProcessing()
        btnPlay.setImage(UIImage(named: "ic_play"), for: .normal)
        isPlaying = false
        itemHandlerView?.removeFromSuperview()
        var maximumSize =  CGSize.zero
        let videoDataObj = selectedVideoData[0]
        let url = videoDataObj.videoUrl
        maximumSize = VideoEdVideoManager().resolutionOfVideo(videoUrl: url!)!
        
        VideoEdVideoManager().mergeVideos_WithSpeed(videoArray: selectedVideoData, maxSize: maximumSize,isAspectFit: false,orintationValue, complitionBlock: { [weak self]
            (mixComposition, videoComposition, audioMix)  in

               self?.saveVideoPart1(mixComposition: mixComposition, videoComposition: videoComposition, audioMix: audioMix)
        })
        
    }
    /*
// only For Testing
    func TestLayersOnView()   {
        
        mainItemsLayer?.removeFromSuperlayer()
        
        mainItemsLayer?.sublayers?.forEach({ (layer) in
            layer.removeAllAnimations()
            layer.removeFromSuperlayer()
        })
        
        
        for (index,modal) in arraySelectedItems.enumerated() {
            
            print("saving arraySelectedItems  Index :",index,"modal is:",modal)
            
            
            if let stickerDataObj = modal as? StickerData {
                
                let stickerLayer:CALayer =  addNewStickerLayer(stickerDataObj: stickerDataObj, parentLayer: mainItemsLayer!, 1.0)
                
                let animation = VideoEdAnimationHelper().layerPreviewOpacityAnimtion(fromValue: 0.0, toValue: 1.0, beginTime: CFTimeInterval(stickerDataObj.startTime), duration: CFTimeInterval(stickerDataObj.duration))
                
                stickerLayer.add(animation, forKey: "opacityAnima")
                
                stickerLayer.isGeometryFlipped = true
                stickerLayer.contentsScale = 3.0
                mainItemsLayer!.addSublayer(stickerLayer)
            }
            else if let gifDataObj = modal as? GifData {
                
                let gifLayer:CALayer =  addNewGifLayer(gifDataObj: gifDataObj, parentLayer: mainItemsLayer!)
                let animation = VideoEdAnimationHelper().layerPreviewOpacityAnimtion(fromValue: 0.0, toValue: 1.0, beginTime: CFTimeInterval(gifDataObj.startTime), duration: CFTimeInterval(gifDataObj.duration))
                
                gifLayer.add(animation, forKey: "opacityAnima")
                gifLayer.isGeometryFlipped = true
                gifLayer.contentsScale = 3.0
                mainItemsLayer!.addSublayer(gifLayer)
            }
              
            else if let gifDataObj = modal as? GifData {
                
                let gifLayer:CALayer =  addNewGifLayer(gifDataObj: gifDataObj, parentLayer: mainItemsLayer!)
                let animation = VideoEdAnimationHelper().layerPreviewOpacityAnimtion(fromValue: 0.0, toValue: 1.0, beginTime: CFTimeInterval(gifDataObj.startTime), duration: CFTimeInterval(gifDataObj.duration))
                
                gifLayer.add(animation, forKey: "opacityAnima")
                gifLayer.isGeometryFlipped = true
                gifLayer.contentsScale = 3.0
                mainItemsLayer!.addSublayer(gifLayer)
            }
                
            else if let textDataObj = modal as? TextData {
                
                print("textDataObj.fontName is :",textDataObj.fontName)
                print("textDataObj.fontSize is :",textDataObj.fontSize)
                print("textDataObj.string is :",textDataObj.string)
                print("textDataObj.centerPt is :",textDataObj.centerPt)
                print("textDataObj.scale is :",textDataObj.scale)
                
                let textLayer:CATextLayer = addNewTextLayer(textLayerDataObj: textDataObj, parentLayer: mainItemsLayer!, 1.0)
                // let stickerLayer:CALayer =  (self?.addNewStickerLayer(stickerDataObj: stickerDataObj, parentLayer: overlayLayer, scaleItems))!
                
                if (textDataObj.duration != 0.0)
                {
                    let animation = VideoEdAnimationHelper().layerPreviewOpacityAnimtion(fromValue: 0.0, toValue: 1.0, beginTime: CFTimeInterval(textDataObj.startTime), duration: CFTimeInterval(textDataObj.duration))
                    textLayer.add(animation, forKey: "opacityAnima")
                }
                textLayer.backgroundColor = UIColor.green.cgColor
                textLayer.isGeometryFlipped = true
                textLayer.contentsScale = 3.0
                mainItemsLayer!.addSublayer(textLayer)
                
            }
        }
        
        avSyncLayer?.addSublayer(mainItemsLayer!)
        
    }
    */
    var savingVideoUrl : URL? {
        get {
            var passUrl = false
            var url: URL?
            
            while !passUrl {
                
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                
                let completePath = documentsPath.appendingFormat(String(format: "/VideoDoc_%lld.mov", Int64(arc4random()) % 1000000000000000))
                url =  URL(fileURLWithPath: completePath)
                
                let exists: Bool = FileManager.default.fileExists(atPath: completePath)
                if !exists {
                    passUrl = true
                }
            }
            return url
        }
        
    }
    func  saveVideoPart1(mixComposition:AVMutableComposition?,videoComposition:AVMutableVideoComposition?,audioMix:AVAudioMix?)  {
        
        let videoDuration:Double = (mixComposition?.duration.seconds)!
        let saveURL = savingVideoUrl // FilePathManager.createNewPath("albumsUrl.mp4")
        let videoSize = videoComposition?.renderSize
        
        var newMixComposition : AVMutableComposition?
        if audioData != nil{
            newMixComposition = self.insertAudioInSlideshow(composition: mixComposition!)
        }

        let isAudio:Bool = newMixComposition != nil ? (newMixComposition?.tracks(withMediaType: .audio).count)! > 0 : (mixComposition?.tracks(withMediaType: .audio).count)! > 0
        
        if(gpuComposition != nil)        {
            gpuComposition?.cancelProcessing()
            gpuComposition = nil
        }
        
        gpuComposition =  GPUImageMovieComposition(composition: newMixComposition != nil ? newMixComposition : mixComposition, andVideoComposition: videoComposition, andAudioMix: audioMix)
        
        if(gpuWriter != nil){
            gpuWriter?.cancelRecording();
            gpuWriter = nil
        }
        
        gpuWriter = GPUImageMovieWriter(movieURL: saveURL, size: videoSize!)
        gpuComposition!.enableSynchronizedEncoding(using: gpuWriter)
        gpuWriter?.shouldPassthroughAudio = isAudio
        gpuWriter?.encodingLiveVideo = false
        gpuComposition!.audioEncodingTarget = isAudio ? gpuWriter : nil
        gpuComposition!.playAtActualSpeed = false
        
        // Chain of Filters
        gpuMovie?.removeAllTargets()
        acvBlendFilter.removeAllTargets()
        gpuComposition?.addTarget(sharpenFilter)
        acvBlendFilter.addTarget(gpuWriter)
        
        videoTimer?.invalidate()
        videoTimer = nil
        
        DispatchQueue.main.async {
            self.videoTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { timer in
                
//                var processedSecond:Double =  0.0
//                if (self.gpuComposition!.processedSeconds != nil && !self.gpuComposition!.processedSeconds.isNaN && !self.gpuComposition!.processedSeconds.isInfinite){
//                    processedSecond = Double(self.gpuComposition!.processedSeconds)
//                }
                
                let percentageRatio :Double =  Double(self.gpuComposition!.compositionProgress) / videoDuration
                // print("progressTimer is:",timer);
                
                if percentageRatio.isNaN  == false
                {
                    let percentage:Int = Int(percentageRatio * 100.0)
                    print("GPU export Timer :",percentage);

                    DispatchQueue.main.async(execute: {
                        NVActivityIndicatorPresenter.sharedInstance.setMessage(String(Int(percentage/2)) + "%")
                    })
                    
                }
                
            }
        }
        
        
        // writer block
        isSaving = true
        gpuComposition!.startProcessing()
        gpuWriter?.startRecording()

        gpuWriter?.failureBlock = { error in
            
            
            print("gpuWriter gpuWriter?.failureBlock =>",error.debugDescription)

        }
        gpuWriter?.completionBlock = {[weak self] in
            
            print("gpuWriter completionBlock Done") 

            GlobalHelper.dispatchMain {[weak self] in
                
                self?.videoTimer?.invalidate()
                self?.videoTimer = nil
                self?.gpuWriter?.finishRecording()
                self?.gpuWriter?.endProcessing()
                if self!.gpuComposition != nil{
                    
                    self!.gpuComposition!.endProcessing()
                    self!.gpuComposition?.removeAllTargets()
                    self!.gpuComposition =  nil
                }
                self?.acvBlendFilter.removeAllTargets()
                
                if self?.gpuWriter != nil{
                    debugPrint("self!.gpuWriter!.movieURL=> ",self!.gpuWriter!.movieURL)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.5, execute: {
                        
                        self?.saveVideoPart2(withUrl: self!.gpuWriter!.movieURL)
                    })
                }else{
                    DispatchQueue.main.async {
                        
//                        self?.stopAnimating()
                        self!.hideHud()
                    }
                }
                
            }
        }
        
        
    }
    
    
    // render     1. SynLayer Items.    2. AspectRatio    3. BackGround Color.    4. Back Video Blur

    func saveVideoPart2(withUrl videoUrl:URL)  {
        
        if(gpuWriter != nil){
            gpuWriter?.cancelRecording();
            gpuWriter = nil
        }
        
        let videoDataObj =  VideoData(url: videoUrl)
        
        weak var weakSelf = self
        var videoSize = VideoEdVideoManager().resolutionOfVideo(videoUrl: videoUrl)
        var maxSize = videoSize

        let floatRatio = aspectCropRatio
        
        if floatRatio != 0{
                if (floatRatio < 1.0) {
                    maxSize!.height = (maxSize!.width / floatRatio);
                } else {
                    maxSize!.width = (maxSize!.height * floatRatio);
            }}
        
        print("maxSize is:",maxSize as Any);
        if maxSize == nil{
            maxSize = CGSize(width: 600, height: 600)
        }
        let _:VideoData = VideoData(url: videoUrl)

        // Some Time Video Cross 2K Resolution.
        let compressRECT  = AVMakeRect(aspectRatio: maxSize!, insideRect: CGRect(x: 0.0, y: 0.0, width: 1080, height: 1080 ))
        maxSize = compressRECT.size;
        
        
        VideoEdVideoManager().mergeVideos_WithSpeed(videoArray: [videoDataObj], maxSize: maxSize!,isAspectFit: true, complitionBlock: { [weak self]
            (mixComposition, videoComposition, audioMix)  in
            
            let finalSize = videoComposition?.renderSize
            let videoViewSize =  self?.videoRepresentView.frame.size
            let mainContainerSize = self?.mainContainer.frame.size
            let compositionDuration = CGFloat((mixComposition?.duration.seconds)!)
            
            print("finalSize is :",finalSize as Any)
            print("compositionDuration is :",compositionDuration)

            let scaleItems:CGFloat = finalSize!.width / (videoViewSize?.width)! // videoRepresentViewHeightConstraint

            
//            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
//            let blurEffectView = UIVisualEffectView(effect: blurEffect)
//            let blurLayer = blurEffectView.layer.sublayers![0]

            var blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            var blurEffectView = UIVisualEffectView(effect: blurEffect)
            var blurLayer = blurEffectView.layer.sublayers![0]

            
            if self!.blurTypeGlobal == 0 {
                
                blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
                blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurLayer = blurEffectView.layer.sublayers![0]
                blurLayer.opacity = 0.5
                
                
            }else if self!.blurTypeGlobal == 1{
                
                blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
                blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurLayer = blurEffectView.layer.sublayers![0]
                blurLayer.opacity = 0.8

                
            }else if self!.blurTypeGlobal == 2{
                
                blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurLayer = blurEffectView.layer.sublayers![0]

                
            }
            
            
            let parentLayer     = CALayer()
            let videoLayer      = CALayer()
            let backVideoLayer  = CALayer()
            let upparVideoMask  = CAShapeLayer()
            let colorLayer      = CAShapeLayer()
            let overlayLayer    = CALayer()

            parentLayer.frame       = CGRect(x: 0, y: 0, width: (finalSize?.width)!, height: (finalSize?.height)!)
            blurLayer.frame         = CGRect(x: 0, y: 0, width: (finalSize?.width)!, height: (finalSize?.height)!)
            videoLayer.frame        = CGRect(x: 0, y: 0, width: (finalSize?.width)!, height: (finalSize?.height)!)
            backVideoLayer.frame    = CGRect(x: 0, y: 0, width: (finalSize?.width)!, height: (finalSize?.height)!)
            upparVideoMask.frame    = CGRect(x: 0, y: 0, width: (finalSize?.width)!, height: (finalSize?.height)!)
            colorLayer.frame        = CGRect(x: 0, y: 0, width: (finalSize?.width)!, height: (finalSize?.height)!)
            overlayLayer.frame      = CGRect(origin: CGPoint.zero, size: CGSize(width: mainContainerSize!.width * scaleItems, height: mainContainerSize!.height * scaleItems))

            
            overlayLayer.position = CGPoint(x: (finalSize!.width) / 2.0, y: (finalSize!.height ) / 2.0)

            
            var newCenter = CGPoint(x: self!.gpuImageView.center.x * scaleItems, y: self!.gpuImageView.center.y * scaleItems)
            newCenter.y = parentLayer.frame.height - newCenter.y
            videoLayer.position = newCenter
            videoLayer.transform = CATransform3DMakeScale(self!.gpuImageView.transform.xscale(), self!.gpuImageView.transform.xscale(), self!.gpuImageView.transform.xscale())
            
            let newPoint = self!.distanceinCGpoints(parentLayer.position, videoLayer.position)

            // parentLayer.isGeometryFlipped = true
            parentLayer.contentsScale = 3.0
            overlayLayer.isGeometryFlipped = true
            overlayLayer.contentsScale = 3.0
            
            if videoSize == nil{
                videoSize = CGSize(width: 600, height: 600)
            }
            var calCulateFrame:CGRect = AVMakeRect(aspectRatio: (videoSize)!, insideRect: videoLayer.frame)
            let newOrigin = CGPoint(x: calCulateFrame.origin.x + newPoint.x , y: calCulateFrame.origin.y + newPoint.y)
            calCulateFrame.origin = newOrigin

            let minLength = min(calCulateFrame.width, calCulateFrame.height)
            let maxLength = max(Float((finalSize?.width)!), Float((finalSize?.height)!))
            let scaleAspectFill = maxLength / Float(minLength)
            
            print("calCulateFrame is:",calCulateFrame)
            let rectShape:UIBezierPath = UIBezierPath(rect: calCulateFrame)
            upparVideoMask.path = rectShape.cgPath
            
            parentLayer.addSublayer(backVideoLayer)
            parentLayer.addSublayer(blurLayer)
            parentLayer.addSublayer(colorLayer)
            parentLayer.addSublayer(videoLayer)
            parentLayer.addSublayer(overlayLayer)
            if(self?.aspectCropRatio != 0)  {
                videoLayer.mask = upparVideoMask
            }
            videoLayer.masksToBounds = true
            
            parentLayer.backgroundColor     = UIColor.black.cgColor
            backVideoLayer.backgroundColor  = UIColor.green.cgColor
            backVideoLayer.contentsGravity  = .resizeAspectFill //.resizeAspect
            videoLayer.backgroundColor      = UIColor.clear.cgColor
            //overlayLayer.backgroundColor    = UIColor.clear.cgColor
            // overlayLayer.backgroundColor    = UIColor.green.cgColor
            overlayLayer.backgroundColor    = UIColor.clear.cgColor
            upparVideoMask.fillColor        = UIColor.gray.cgColor
            colorLayer.backgroundColor      = (self?.videoBackGroundColor != nil) ? self?.videoBackGroundColor?.cgColor : UIColor.clear.cgColor
            
            if  self!.gradColorGlobal != nil{
                
                let gradient: CAGradientLayer = CAGradientLayer()
                gradient.colors = self!.gradColorGlobal?.colorsArray
//                gradient.locations = [0.0 ,0.5, 1.0]
                gradient.startPoint = self!.gradColorGlobal!.startPoint
                gradient.endPoint = self!.gradColorGlobal!.endpoint
                gradient.frame = CGRect(x: 0.0, y: 0.0, width: colorLayer.frame.size.width, height: colorLayer.frame.size.height)
                colorLayer.insertSublayer(gradient, at: 0)
                gradient.isGeometryFlipped = true
            }

            
            // Aspect Fill the Back Video Layer
            backVideoLayer.setAffineTransform(CGAffineTransform.init(scaleX: CGFloat(scaleAspectFill), y: CGFloat(scaleAspectFill)));
            

            // *** Adding CALayer Items , sticker , Gif , text is pending
            // stickerDataObj

            
//            for (index,modal) in (self?.arraySelectedItems.enumerated())! {
//
//                print("saving arraySelectedItems  Index :",index,"modal is:",modal)
//
//                if let stickerDataObj = modal as? StickerData {
//
//                    let stickerLayer:CALayer =  (self?.addNewStickerLayer(stickerDataObj: stickerDataObj, parentLayer: overlayLayer, scaleItems))!
//
//                    if !(stickerDataObj.startTime <= 0.0 && ((compositionDuration ) <= stickerDataObj.duration) )                    {
//                        let animation = VideoEdAnimationHelper().layerPreviewOpacityAnimtion(fromValue: 0.0, toValue: 1.0, beginTime: CFTimeInterval(stickerDataObj.startTime), duration: CFTimeInterval(stickerDataObj.duration))
//                        stickerLayer.add(animation, forKey: "opacityAnima")
//                        stickerLayer.opacity = 0.0
//                    }
//
//                    stickerLayer.isGeometryFlipped = true
//                    stickerLayer.contentsScale = 3.0
//                    overlayLayer.addSublayer(stickerLayer)
//                }
//                else if let gifDataObj = modal as? StickerGifData {
//
//                    let gifLayer:CALayer = (self?.addNewStickerGifLayer(gifDataObj: gifDataObj, parentLayer: overlayLayer, scaleItems, save: true))!
//
//                    //if (gifDataObj.startTime >= 0.0 && ((compositionDuration ) > gifDataObj.duration) )
//                    if !(gifDataObj.startTime <= 0.0 && ((compositionDuration ) <= gifDataObj.duration) )
//                    {
//                        let animation = VideoEdAnimationHelper().layerPreviewOpacityAnimtion(fromValue: 0.0, toValue: 1.0, beginTime: CFTimeInterval(gifDataObj.startTime), duration: CFTimeInterval(gifDataObj.duration))
//                        gifLayer.add(animation, forKey: "opacityAnima")
//                        gifLayer.opacity = 0.0
//                    }
//
//                    gifLayer.isGeometryFlipped = true
//                    gifLayer.contentsScale = 3.0
//                    overlayLayer.addSublayer(gifLayer)
//                }
//                else if let gifDataObj = modal as? VideoGifData {
//
//                    let gifLayer:CALayer = (self?.addNewVideoGifLayer(gifDataObj: gifDataObj, parentLayer: overlayLayer, scaleItems, save: true))!
//
//                    //if (gifDataObj.startTime >= 0.0 && ((compositionDuration ) > gifDataObj.duration) )
//                    if !(gifDataObj.startTime <= 0.0 && ((compositionDuration ) <= gifDataObj.duration) )
//                    {
//                        let animation = VideoEdAnimationHelper().layerPreviewOpacityAnimtion(fromValue: 0.0, toValue: 1.0, beginTime: CFTimeInterval(gifDataObj.startTime), duration: CFTimeInterval(gifDataObj.duration))
//                        gifLayer.add(animation, forKey: "opacityAnima")
//                        gifLayer.opacity = 0.0
//                    }
//
//                    gifLayer.isGeometryFlipped = true
//                    gifLayer.contentsScale = 3.0
//                    overlayLayer.addSublayer(gifLayer)
//                }
//                else if let actionFxDataObj = modal as? ActionFxData {
//
//                    let fxLayer:CALayer = (self?.addNewActionFxLayer(fxDataObj: actionFxDataObj, parentLayer: overlayLayer, scaleItems, save: true))!
//
//                    // if (actionFxDataObj.startTime >= 0.0 && ((compositionDuration ) > actionFxDataObj.duration) )
//                    if !(actionFxDataObj.startTime <= 0.0 && ((compositionDuration ) <= actionFxDataObj.duration) )
//                    {
//                        let animation = VideoEdAnimationHelper().layerPreviewOpacityAnimtion(fromValue: 0.0, toValue: 1.0, beginTime: CFTimeInterval(actionFxDataObj.startTime), duration: CFTimeInterval(actionFxDataObj.duration))
//                        fxLayer.add(animation, forKey: "opacityAnima")
//                        fxLayer.opacity = 0.0
//                    }
//                    fxLayer.isGeometryFlipped = true
//                    fxLayer.contentsScale = 3.0
//                    overlayLayer.addSublayer(fxLayer)
//                }
//
//                else if let textDataObj = modal as? TextData {
//
//                    let textLayer:CATextLayer = (self?.addNewTextLayer(textLayerDataObj: textDataObj, parentLayer: overlayLayer, scaleItems))!
//
//                    //if (textDataObj.startTime >= 0.0 &&  ((compositionDuration ) > textDataObj.duration) )
//                    if !(textDataObj.startTime <= 0.0 &&  ((compositionDuration ) <= textDataObj.duration) )
//                    {
//                        let animation = VideoEdAnimationHelper().layerPreviewOpacityAnimtion(fromValue: 0.0, toValue: 1.0, beginTime: CFTimeInterval(textDataObj.startTime), duration: CFTimeInterval(textDataObj.duration))
//                        textLayer.add(animation, forKey: "opacityAnima")
//                        textLayer.opacity = 0.0
//                    }
//
//                    textLayer.isGeometryFlipped = true
//                    textLayer.contentsScale = 3.0
//                    overlayLayer.addSublayer(textLayer)
//                }
//            }
            
            // Pending
            
            if(self?.aspectCropRatio != 0){
                videoComposition!.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayers: [videoLayer,backVideoLayer], in: parentLayer);
            }
            else if((self?.arraySelectedItems.count)! > 0) {
                videoComposition!.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
            }
          
            if self!.avExporter != nil{
                self!.avExporter?.cancelExport()
                self!.avExporter =  nil
            }
            self!.avExporter = VideoEdVideoManager().exportSession(mixComposition: mixComposition!, videoComposition: videoComposition, audioMix: audioMix, videoName: "saveVideoPart2", progress: { (progress) in
                
                print("progress is :",progress)
                DispatchQueue.main.async(execute: {
                    NVActivityIndicatorPresenter.sharedInstance.setMessage(String((progress/2)+50) + "%")
                })
            }, complitionBlock: { (exportSession) in
                
                DispatchQueue.main.async(execute: {
                    self!.hideHud()
                })
                
                if exportSession?.status == AVAssetExportSession.Status.completed{
                    
                    DispatchQueue.main.async(execute: {
                        self?.openShareView(videoUrl: exportSession!.outputURL!)
                        
                    })
                }
                else if exportSession?.status == AVAssetExportSession.Status.failed {
                    
                    if self!.avExporter != nil{
                        self!.avExporter?.cancelExport()
                        self!.avExporter =  nil
                    }
                    
                    weakSelf!.playPlayerAfterSave()
                    print("exporter?.error : ",exportSession?.error.debugDescription as Any);
                }
            })
        })
        
        /*
         // Tools
         PHPhotoLibrary.shared().performChanges({
         PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoUrl)
         }) { saved, error in
         
             if saved {
                 let fetchOptions = PHFetchOptions()
                    fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                 let fetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions).firstObject
                    print(" video saved Successfully ");
             }
             else{
                print("exporter?.error :",error.debugDescription);
             }
            
        }
        */
    }
    func distanceinCGpoints(_ a: CGPoint, _ b: CGPoint) -> CGPoint {
        let xDist = a.x - b.x
        let yDist = a.y - b.y
        return CGPoint(x: xDist, y: yDist)
        //        return CGFloat(sqrt(xDist * xDist + yDist * yDist))
    }
    func pauseAllPlayers()  {
    
        player?.pause()
        gpuMovie?.cancelProcessing()
        if gpuComposition != nil  {
            gpuComposition?.cancelProcessing()
        }
    }
    
    func playAllPlayers()  {
        
    }
    
    func openShareView(videoUrl:URL)  {
        
        if avExporter != nil{
            avExporter?.cancelExport()
            avExporter =  nil
        }
        
        pauseAllPlayers()
//        let storyboard = UIStoryboard(name: "VideoEdAlbum", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "VideoEdShareVC") as! VideoEdShareVC
//        vc.videoUrl = videoUrl
//        vc.delegate =  self
//        GlobalHelper.dispatchMainAfter(time: .now() + 0.2, execute: {[weak self] in
//            self?.navigationController?.pushViewController(vc, animated: true)
//        })

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.videoUrl = videoUrl
        //        vc.delegate =  self
        GlobalHelper.dispatchMainAfter(time: .now() + 0.2, execute: {[weak self] in
            self?.navigationController?.pushViewController(vc, animated: true)
        })
        
    }
    
    func playPlayerAfterSave()  {
        
        if avExporter != nil{
            avExporter?.cancelExport()
            avExporter =  nil
        }
        gpuMovie?.removeAllTargets()
        acvBlendFilter.removeAllTargets()
        
        // GPU Movie allocation
        if (gpuMovie != nil)    {
            gpuMovie?.cancelProcessing()
            gpuMovie?.removeAllTargets()
            gpuMovie = nil
        }
        
        gpuMovie = GPUImageMovie(playerItem: playerItem)
        gpuMovie?.playAtActualSpeed = true
        gpuMovie?.addTarget(sharpenFilter)
        gpuMovie?.startProcessing()

        acvBlendFilter.addTarget(gpuImageView)
        
        self.player?.play()
        speedToPlayer()

        gpuMovie?.startProcessing()
        btnPlay.setImage(UIImage(named: "ic_pause"), for: .normal)
        isPlaying = true
        
    }
    
    
    @IBAction func btnSaveClicked_old(_ sender: UIButton) {
        
        sender.isUserInteractionEnabled = false
        self.player?.pause()
        btnPlay.setImage(UIImage(named: "ic_play"), for: .normal)
        isPlaying = false
        itemHandlerView?.removeFromSuperview()
        
        self.showHUD(progressLabel: "")
        sender.isUserInteractionEnabled = true
        isSaving = true
        
       /* guard let (mixComp,videoComp) = VideoEdCompositionManager.getCompositionsFrom(asset: composition.videoAssetStruct, containerBgColor: containerBgColor, containerBounds: videoContainer.bounds, startTime: startPoint, endTime: endPoint, trimAngle: angle,isNewAudio: isNewAudio,audioAsset: audioAsset) else {
            hideHud()
            return
        }*/
        
        
        /*
        let gpuComposition = GPUImageMovieComposition(composition: compositionFromManager.mixCompStruct, andVideoComposition: compositionFromManager.mixVideoCompStruct, andAudioMix: nil)
        gpuComposition?.runBenchmark = true
        saveGPUMovie(comp: gpuComposition!)
        
        */
        

        

        // composition.instructions = [mainInstruction]
        //composition.animationTool =  AVVideoCompositionCoreAnimationTool.init(postProcessingAsVideoLayer: videoLayer, in: parentUILayer)
        
        // composition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        // composition.renderSize = outputSize;
        
        let videoLayer = CALayer()
        let parentLayer = CALayer()
        let frame = CGRect(x: 0, y: 0, width: compositionFromManager.mixVideoCompStruct.renderSize.width, height: compositionFromManager.mixVideoCompStruct.renderSize.height)
        videoLayer.frame = frame
        parentLayer.frame =  frame
        parentLayer.addSublayer(videoLayer)
        
        let mutableVideoComposition = compositionFromManager.mixVideoCompStruct
        mutableVideoComposition!.animationTool =  AVVideoCompositionCoreAnimationTool.init(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
        
        let sample = VideoEdVideoManager().exportSession(mixComposition: compositionFromManager.mixCompStruct, videoComposition: mutableVideoComposition, audioMix: nil, videoName: "text", progress: { (progress) in
            
            print("progress progress is :",progress)
            
        }) { (exportSession) in
            
            if exportSession?.status == AVAssetExportSession.Status.completed
            {
                DispatchQueue.main.async(execute: {
                    
                     PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: exportSession!.outputURL!)
                     }) { saved, error in
                         if saved {
//                             let fetchOptions = PHFetchOptions()
//                             fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
//                             let fetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions).firstObject
                             print(" video saved Successfully ");
                         }
                         else{
                            print("exporter?.error :",error.debugDescription);
                         }
                     }
                    
                })
            }
            else if exportSession?.status == AVAssetExportSession.Status.failed {
                print("exporter?.error : ",exportSession?.error.debugDescription as Any);
                //completion(exportURL,exporter?.error)
            }
            
        }
        debugPrint("sample export ",sample as Any)
    }

    func showHUD(progressLabel:String){
        
        if(NVindicator != nil){
            NVindicator.removeFromSuperview()
            NVindicator.stopAnimating()
            NVindicator.layer.removeAllAnimations()
            NVindicator = nil
        }
        
        UIApplication.shared.keyWindow?.isUserInteractionEnabled = false
        
//        viewLoaderBg.isHidden = false
//        viewMainLoader.isHidden = false
//        self.view.bringSubviewToFront(viewLoaderBg)
//        self.view.bringSubviewToFront(viewMainLoader)
        GlobalHelper.dispatchMain { [weak self] in
//            self?.NVindicator = NVActivityIndicatorView(frame: CGRect(x: ((self?.viewMainLoader.frame.width)!/2-25), y: ((self?.viewMainLoader.frame.height)!/2-25), width: 50, height: 50), type: .circleStrokeSpin, color: .white, padding: 50)
//            self?.viewMainLoader.addSubview(self!.NVindicator)
//            self?.view.startAnimating()
            self!.startAnimating(CGSize(width: 50, height: 50), message: "0%", type: .ballClipRotateMultiple)

        }
    }
    
    func hideHud(){
        
        GlobalHelper.dispatchMain { [weak self] in
            self?.stopAnimating()
            UIApplication.shared.keyWindow?.isUserInteractionEnabled = true
        }
    }

    
    // MARK: - Get progress of saving
    @objc func retrievingProgress(timer: Timer) {
        guard let gpuComp = timer.userInfo as? GPUImageMovieComposition else{
            return
        }
        let total = gpuComp.compositon.duration.seconds
        let compProgress = gpuComp.compositionProgress
        if !compProgress.isNaN{
            let progress = compProgress / Float(total)
        }
    }
    
    func saveVideo(url: URL, completion: @escaping (_: Bool, _: String) -> Void) {
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
        }) { saved, error in
            if saved {
                completion(true, "Video saved successfully.")
            } else {
                completion(false, "Some error has been occured.")
            }
        }
    }
    
    @IBAction func categoryActions(_ sender: UIButton) {
        
    }
    
    func removeAllOptionViews() {
        if filtersView != nil{
            filtersView.removeFromSuperview()
        }
        if cropView != nil{
            cropView.removeFromSuperview()
        }


        if musicView != nil{
            musicView.removeFromSuperview()
        }
        if adjustView != nil{
            adjustView?.removeFromSuperview()
        }
    }
    
    func  hideAllOptionsViews()  {
        
        if filtersView != nil{
            filtersView.isHidden = true
        }
        if cropView != nil{
            cropView.isHidden = true
        }


        if musicView != nil{
            musicView.isHidden = true
        }
        if adjustView != nil{
            adjustView?.isHidden = true
        }
        
        for textField in self.view.subviews where textField is UITextField {
            textField.resignFirstResponder()
        }
//        for textField in self.overlayView.subviews where textField is UITextField {
//            textField.resignFirstResponder()
//        }
        if splitView != nil{
            splitView?.isHidden = true
        }
        if speedView != nil{
            speedView?.isHidden = true
        }
        if trimView != nil{
            trimView?.isHidden = true
        }
        
    }
    
   
    
    //MARK:- Crop View Delegate
    func didChangeRatio(ratio: CGFloat) {
        updateTextLayerPosition(with: ratio)
    }
    
    public func updateTextLayerPosition(with ratio: CGFloat) {
       /*
         if parentLayer.sublayers != nil {
            for (index,layer) in parentLayer.sublayers!.enumerated(){
                let newPosition = mainContainer.convert(sPositions[index], to: videoContainer)
                CATransaction.begin()
                CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
                layer.position = newPosition
                CATransaction.commit()
            }
            updateLayerPositionsInModel()
        }
        */
    }
    
    @objc func updateLayerPositionsInModel() {
       /*
         for (index,_) in self.stickerLayerArray.enumerated(){
            let newPosition = self.mainContainer.convert(self.sPositions[index], to: self.videoContainer)
            self.stickerLayerArray[index].poistion = newPosition
            print("newPosition",newPosition)
        }
        */
    }
    
    
    func stickerEndEditing() {
        itemHandlerView?.removeFromSuperview()
        selectedLayerData = nil
    }
    
    
    //MARK:- Stickers VC Delegate
    func didSelectImage(image: UIImage,category: String) {
        

    }
    
    func didCancelled() {
        
    }
   
    
    func setRepitionofMusic(newMusic: URL?,trimStartTime:Double,trimEndTime:Double,repeatMusic:Bool = true,musicInsertTime:Double = 0.0) -> AVMutableComposition
    {
        let audioAsset: AVURLAsset? = AVURLAsset(url: newMusic!, options: nil)
        let composition = AVMutableComposition()
        let audioCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        do {

            let musicStartTime = CMTime(seconds: trimStartTime, preferredTimescale: 10)
            let musicEndTime = CMTime(seconds: trimEndTime, preferredTimescale: 10)
            
            var durationTime = CMTime.zero

            if  (musicStartTime < musicEndTime)            {
                durationTime = CMTimeSubtract(musicEndTime, musicStartTime)
            }
            var musicInsertCMTime = CMTime.zero
            musicInsertCMTime = CMTime(seconds: musicInsertTime, preferredTimescale: 10)
            
            print("musicStartTime is :",musicStartTime.seconds)
            print("musicEndTime is :",musicEndTime.seconds)
            print("durationTime is :",durationTime.seconds)
            print("musicInsertTime is :",musicInsertCMTime.seconds)

            let finalAudioAsset = audioAsset
            
            let finalSoundTrack = finalAudioAsset?.tracks(withMediaType: AVMediaType.audio).first!
                //if video duration is shorter than complete music duration
            
            audioCompositionTrack?.insertEmptyTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: musicInsertCMTime))
            
            /*
            if(compositionFromManager.mixCompStruct!.duration < finalAudioAsset!.duration){
                
                    if let audioTrack = finalSoundTrack {
                        try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime,duration: compositionFromManager.mixCompStruct!.duration),
                                                                   of: audioTrack,
                                                                   at: musicInsertCMTime)
                    }
            }else{
              
                */
                if let audioTrack = finalSoundTrack {
                    
                    try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime, duration: durationTime),
                                                               of: audioTrack,
                                                               at: musicInsertCMTime)
                    musicInsertCMTime = CMTimeAdd(musicInsertCMTime, durationTime)
                }
                
                if let finalAudioDuration = finalAudioAsset?.duration {
                    if (compositionFromManager.mixCompStruct!.duration > finalAudioDuration) // && finalAudioDuration != CMTime.zero)
                    {
                        let remainder =  Double(CMTimeGetSeconds(compositionFromManager.mixCompStruct!.duration).truncatingRemainder(dividingBy: Double(CMTimeGetSeconds(finalAudioDuration))))
                        let quotient = Int(CMTimeGetSeconds(compositionFromManager.mixCompStruct!.duration)/CMTimeGetSeconds(finalAudioDuration))
                        print("Remainder",remainder,quotient)
                        let lock = NSLock()
                        lock.lock()
                        //if isMusicRepetitionStatus {
                            if repeatMusic {
                             for val in 0..<quotient{
                                if(val != (quotient-1)){
                                    if let audioTrack = finalSoundTrack {
                                        try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime, duration: durationTime),
                                                                                   of: audioTrack,
                                                                                   at: musicInsertCMTime)
                                        musicInsertCMTime = CMTimeAdd(musicInsertCMTime, durationTime)
                                    }
                                }
                            }
                            lock.unlock()
                            if(remainder > 0){
                                if let audioTrack = finalSoundTrack {
                                    try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime, duration: CMTimeMakeWithSeconds(remainder, preferredTimescale: finalAudioDuration.timescale)),  of: audioTrack,  at: musicInsertCMTime)
                                }
                            }
                        }
                    }
                }
            // }
        }catch{
        }
        return composition
    }
    

    
    
    //MARK:- Periodic time observers
    func addPeriodicTimeObserver() {
        // Notify every half second
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 0.1, preferredTimescale: timeScale)
        var iTime:Float = 0.0

        timeObserverToken = player?.addPeriodicTimeObserver(forInterval: time, queue: .main) { [weak self] time in
            if self?.player != nil{
                
                if !self!.isPlaying  {
                    return
                }
//                let currentTime = self!.player!.currentTime()

                iTime = (iTime + 0.01).truncatingRemainder(dividingBy: 10.0)

                if self!.gpuGlitchFilter != nil{
                    self!.gpuGlitchFilter?.setFloat(iTime, forUniformName: "iGlobalTime")
                }

//                if self!.trimView != nil && !self!.trimView!.isHidden{
//                    return
//                }

                if (self?.player!.rate)! < Float(1.0) {
                    return
                }
                
                if self!.selectedVideoData.first!.trimStartTime > time{
                    let makeNewtime = self!.selectedVideoData.first!.trimStartTime.seconds + 0.15
                    print("trimstarttime",self!.selectedVideoData.first!.trimStartTime.seconds)
                    print("currentTime",time.seconds)

                    DispatchQueue.main.async {
                        self!.seekPlayer(to: makeNewtime)
//                        self?.player?.seek(to: CMTime(seconds: makeNewtime, preferredTimescale: 1000))
                    }
                }else      if self!.selectedVideoData.first!.trimEndTime < time{
                    
                    self!.seekPlayer(to: self!.selectedVideoData.first!.trimStartTime.seconds)
                    
//                    self?.player?.seek(to: self!.selectedVideoData.first!.trimStartTime)
                }
                
//                self?.seekBar.setValue(Float(currentTime.seconds), animated: true)
//                if(self?.textView != nil && self?.textView.isHidden == false)                {
//                    self?.textView.updateProgressSlider(currentTime: Float(currentTime.seconds))
//                }
            }
        }
    }
    
    func removePeriodicTimeObserver() {
        if let timeObserverToken = timeObserverToken {
            player?.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
    }
    
    
    //MARK:- Seek Player
    func seekPlayer(to seconds: Double){
        let time = CMTime(seconds: seconds, preferredTimescale: currentTimeScale)
        if !self.isSeeking{
            self.isSeeking = true
            player?.seek(to: time, toleranceBefore: .zero, toleranceAfter: .zero, completionHandler: { [weak self]_ in
                self?.isSeeking = false
            })
        }
    }
    func speedToPlayer(){
        
        let videoDataObjFirst :VideoData = selectedVideoData[0];
        player?.playImmediately(atRate: Float(videoDataObjFirst.videoSpeed))
        
        

    }
    

}



func resizableImageOfFrameTemp( sizeOfVideo:CGSize, image : UIImage) -> UIImage? {
    
    
    print(image.size )
    let scale = 1.0//(framesArray![indexPath.section] as! FramesModelClass).scale ?? 0
    let type = 1.0//(framesArray![indexPath.section] as! FramesModelClass).frames![indexPath.item].type!
    let customOffset = 5//(framesArray![indexPath.section] as! FramesModelClass).frames![indexPath.item].customOffset
    
    let j: Float = IS_IPAD ? 2.0 : 1.0
    
    print("FrameImage scale = \(image.scale )")
    print("type = \(type)")
    
    var newImage : UIImage!
    if image.scale != CGFloat(Float(scale) / j) {
        if let CGImage = image.cgImage {
            newImage = UIImage(cgImage: CGImage, scale: CGFloat(Float(scale) / j), orientation: .up)
        }
    }else{
        newImage = image
    }
    
    let k: Float = (type == 1) ? 3.0 : 2.0
    
    let xOffset = Float(customOffset) == 0 ? (Float((newImage.size.width))/j)/k : Float(customOffset)*j
    let yOffset = Float(customOffset) == 0 ? (Float((newImage.size.height))/j)/k : Float(customOffset)*j
    
    if type == 1 {
        return newImage.resizableImage(withCapInsets: UIEdgeInsets(top: CGFloat(yOffset), left: CGFloat(xOffset), bottom: CGFloat(yOffset), right: CGFloat(xOffset)), resizingMode: .tile)
    } else {
        return newImage.resizableImage(withCapInsets: UIEdgeInsets(top: CGFloat(yOffset), left: CGFloat(xOffset), bottom: CGFloat(yOffset), right: CGFloat(xOffset)), resizingMode: .stretch)
    }
    
}

//MARK:- Speed Video
extension VideoEdEditVC : SpeedViewDelegate{
    
    func speedDoneAction(speed: CGFloat) {
        
        speedView?.isHidden =  true
        seekBarView.isUserInteractionEnabled = true
        
        let index = videoTransitionView.videoSeletedIndex.item
        if (index >= 0 ){
            
            let videoDataObjFirst :VideoData = selectedVideoData[index];
            videoDataObjFirst.videoSpeed = Float(speed)
            selectedVideoData.forEach{
                print("VideoData VideoUrl trimStartTime is ",$0.trimStartTime)
                print("VideoData VideoUrl trimEndTime is ",$0.trimEndTime)
            }
//            self.reCreateAVCompositions()
            
            speedToPlayer()
        }
        
    }
    
    func speedCancelAction() {
        seekBarView.isUserInteractionEnabled = true

    }
    
    func speedDidChange(speed: CGFloat) {
        
        let index = videoTransitionView.videoSeletedIndex.item
        if (index >= 0 ){
            
            let videoDataObjFirst :VideoData = selectedVideoData[index];
            videoDataObjFirst.videoSpeed = Float(speed)
            print("speed is ",videoDataObjFirst.videoSpeed)

            selectedVideoData.forEach{
                print("VideoData VideoUrl trimStartTime is ",$0.trimStartTime)
                print("VideoData VideoUrl trimEndTime is ",$0.trimEndTime)
            }
//            self.reCreateAVCompositions()
            speedToPlayer()

        }
        
        if speedView != nil && !speedView!.isHidden{
            
            var previousTracksDuration = CMTime.zero
            if videoTransitionView.videoSeletedIndex.item > 0   {
                for i in  0...(videoTransitionView.videoSeletedIndex.item - 1)   {
                    let videoData = selectedVideoData[i]
                    var duration = CMTimeSubtract(videoData.trimEndTime, videoData.trimStartTime)
                    let durationSeconds = duration.seconds / Double(videoData.videoSpeed)
                    duration = CMTime(seconds: durationSeconds, preferredTimescale: 1000)
                    previousTracksDuration =  CMTimeAdd(previousTracksDuration, duration)
                }
            }
            let avPlayerCurrentTime  = previousTracksDuration.seconds

            seekPlayer(to: Double(avPlayerCurrentTime))

            
        }

    }
    
    
    func addSpeedView()
    {
        if videoTransitionView.videoSeletedIndex.item < 0 || videoTransitionView.videoSeletedIndex.item >= selectedVideoData.count  {
            return
        }
        
        // return if Video
        let videoData = selectedVideoData[videoTransitionView.videoSeletedIndex.item]
        let duration = CMTimeSubtract(videoData.trimEndTime, videoData.trimStartTime).seconds
        print("duration is :",duration)
        if (duration < 2.0) {
            return
            
        }
        
        if speedView == nil   {
            
            let frame = videoTransitionView.bounds
//            frame.size.height += categoryView.frame.height
            speedView = SpeedView.initWithFrame(frame: frame)
            speedView!.delegate = self
            videoTransitionView.addSubview(speedView!)
        }
        
    
        videoTransitionView.bringSubviewToFront(speedView!)
    
    
        speedView?.videoData = selectedVideoData[videoTransitionView.videoSeletedIndex.item]
        speedView?.setupUI()
        speedView?.isHidden =  false
//        pauseAVPlayer(pause: true)
//        seekBarView.isUserInteractionEnabled = false
        
        // let avPlayerCurrentTime  = (player?.currentTime().seconds)! + (duration * 0.5)
        
        var previousTracksDuration = CMTime.zero
        
        if videoTransitionView.videoSeletedIndex.item > 0   {
            for i in  0...(videoTransitionView.videoSeletedIndex.item - 1)   {
                let videoData = selectedVideoData[i]
                var duration = CMTimeSubtract(videoData.trimEndTime, videoData.trimStartTime)
                let durationSeconds = duration.seconds / Double(videoData.videoSpeed)
                duration = CMTime(seconds: durationSeconds, preferredTimescale: 1000)
                previousTracksDuration =  CMTimeAdd(previousTracksDuration, duration)
            }
        }
        let avPlayerCurrentTime  = previousTracksDuration.seconds
        seekPlayer(to: Double(avPlayerCurrentTime))
        
    }
    
}

//MARK:- Trim Video
extension VideoEdEditVC : trimViewDelegate{
    
    
    func addTrimView(){
        
        if videoTransitionView.videoSeletedIndex.item < 0 || videoTransitionView.videoSeletedIndex.item >= selectedVideoData.count  {
            return
            
        }
        
        // return if Video
        let videoData = selectedVideoData[videoTransitionView.videoSeletedIndex.item]
        let duration = CMTimeSubtract(videoData.trimEndTime, videoData.trimStartTime).seconds
//        print("duration is :",duration)
        if (duration < 1.0) {
            return

        }
        if trimView == nil   {
            
            // let frame = CGRect(x: 0, y: SCREEN_HEIGHT - BOTTOM_VIEWS_HEIGHT*1.8, width: SCREEN_WIDTH, height: BOTTOM_VIEWS_HEIGHT)
            var frame = videoTransitionView.bounds
            //            frame.size.height += categoryView.frame.height
            trimView = TrimmingView.initWithFrame(frame: frame)
            trimView!.delegate = self
            videoTransitionView.addSubview(trimView!)
            trimView?.videoData = selectedVideoData[videoTransitionView.videoSeletedIndex.item]
            trimView?.setupUI(avasset: AVAsset(url: (trimView?.videoData!.videoUrl)!))
            
            //        seekBarView.isUserInteractionEnabled = false
            
            var previousTracksDuration = CMTime.zero
            
            if videoTransitionView.videoSeletedIndex.item > 0   {
                for i in  0...(videoTransitionView.videoSeletedIndex.item - 1)   {
                    let videoData = selectedVideoData[i]
                    var duration = CMTimeSubtract(videoData.trimEndTime, videoData.trimStartTime)
                    let durationSeconds = duration.seconds / Double(videoData.videoSpeed)
                    duration = CMTime(seconds: durationSeconds, preferredTimescale: 1000)
                    previousTracksDuration =  CMTimeAdd(previousTracksDuration, duration)
                }
            }
            let avPlayerCurrentTime  = previousTracksDuration.seconds + 0.1
            
            let nextvideoData = selectedVideoData[videoTransitionView.videoSeletedIndex.row]
            
            trimView?.multiSlider.minimumValue = 0
            trimView?.multiSlider.maximumValue = CGFloat(AVAsset(url: nextvideoData.videoUrl!).duration.seconds)
            trimView?.multiSlider.value = [0, CGFloat(AVAsset(url: nextvideoData.videoUrl!).duration.seconds)]
            
            trimView?.trimSliderTimeLine?.updateNewValues(left: nextvideoData.trimStartTime.durationInSeconds, right: nextvideoData.trimEndTime.durationInSeconds)
            
            let index = videoTransitionView.videoSeletedIndex.item
            if (index >= 0 ){
                
                let videoDataObjFirst :VideoData = selectedVideoData[index];
                VideoEdEditVC.tempraryVideodata = videoDataObjFirst.copy() as? VideoData
                
                videoDataObjFirst.trimStartTime = .zero
                videoDataObjFirst.trimEndTime = AVAsset(url: nextvideoData.videoUrl!).duration
                
                selectedVideoData.forEach{
                    print("VideoData VideoUrl trimStartTime is ",$0.trimStartTime)
                    print("VideoData VideoUrl trimEndTime is ",$0.trimEndTime)
                }
                self.reCreateAVCompositions()
                self.pauseAVPlayer(pause: true)
                seekPlayer(to: Double(avPlayerCurrentTime))
                
            }
        }
        videoTransitionView.bringSubviewToFront(trimView!)
        trimView?.isHidden =  false
        pauseAVPlayer(pause: true)


    }
    func trimDidChange(startvalue: CGFloat, endValue: CGFloat) {
        print("=========================================================== ")

        selectedVideoData.first?.trimStartTime = CMTime(seconds: Double(startvalue), preferredTimescale: 1000)
        selectedVideoData.first?.trimEndTime = CMTime(seconds: Double(endValue), preferredTimescale: 1000)

        seekPlayer(to: Double(startvalue))
        
        if isPlaying{
            pauseAVPlayer(pause: false)
        }
    }
    func trimScrubbingChange(startvalue: CGFloat, endValue: CGFloat , isStartPointChanges : Bool) {
        
        player?.pause()
        /*
        var previousTracksDuration = CMTime.zero
        let currentData = selectedVideoData[videoTransitionView.videoSeletedIndex.row]
        if videoTransitionView.videoSeletedIndex.item > 0   {
            for i in  0...(videoTransitionView.videoSeletedIndex.item - 1)   {
                let videoData = selectedVideoData[i]
                var duration = CMTimeSubtract(videoData.trimEndTime, videoData.trimStartTime)
                let durationSeconds = duration.seconds / Double(videoData.videoSpeed)
                duration = CMTime(seconds: durationSeconds, preferredTimescale: 1000)
                previousTracksDuration =  CMTimeAdd(previousTracksDuration, duration)
            }
        }
        let avPlayerCurrentTime  = previousTracksDuration.seconds
        
//        print("avPlayerCurrentTime is ", Double(avPlayerCurrentTime)+Double(startvalue))

        if isStartPointChanges{
            let newSTart = Double(avPlayerCurrentTime)+Double(startvalue/CGFloat(currentData.videoSpeed))
            print("start is ",newSTart)
//            seekPlayer(to: newSTart)

        }else{
            let newEndValue = Double(avPlayerCurrentTime)+Double(endValue/CGFloat(currentData.videoSpeed)) - 0.05
            print("newEndValue is ",newEndValue)
//            seekPlayer(to: newEndValue)


        }
*/
    }
    static var startValueGlobal :  CGFloat = 0
    static var endValueGlobal :  CGFloat = 0
    static var tempraryVideodata : VideoData?
    func isStartValueSame(startvalue: CGFloat, endValue: CGFloat) -> Bool{
        
        if VideoEdEditVC.startValueGlobal == startvalue{
            return true
        }
        VideoEdEditVC.startValueGlobal = startvalue
        return false
        
    }
    
    func trimDoneAction(startvalue: CGFloat, endValue: CGFloat) {
        
        trimView?.isHidden =  true
//        pauseAVPlayer(pause: true)
        seekBarView.isUserInteractionEnabled = true
        let index = videoTransitionView.videoSeletedIndex.item
        if (index >= 0 ){
            
            let videoDataObjFirst :VideoData = selectedVideoData[index];
            
            
            videoDataObjFirst.trimStartTime = CMTime(seconds: Double(startvalue), preferredTimescale: 1000)
            videoDataObjFirst.trimEndTime = CMTime(seconds: Double(endValue), preferredTimescale: 1000)
            
            self.reCreateAVCompositions()
        }

    }
    
    func trimCancelAction() {
        trimView?.isHidden =  true
        pauseAVPlayer(pause: true)
        seekBarView.isUserInteractionEnabled = true
        
        let index = videoTransitionView.videoSeletedIndex.item
        if (index >= 0 ){

            let videoDataObjFirst :VideoData = selectedVideoData[index];

            videoDataObjFirst.trimStartTime = VideoEdEditVC.tempraryVideodata!.trimStartTime
            videoDataObjFirst.trimEndTime = VideoEdEditVC.tempraryVideodata!.trimEndTime

            self.reCreateAVCompositions()
        }

    }

}
extension VideoEdEditVC :SplitViewDelegate
{
    //MARK:- Split Video Track Module
    func addSplitView()
    {
        if cropView == nil
        {
            if(thumbnailImage == nil)            {
                if let thumb = VideoEdVideoManager().generateThumbnail(asset: compositionFromManager.mixCompStruct, compositionFromManager.mixVideoCompStruct , at: .zero, of: CGSize(width: 150, height: 150)){
                    thumbnailImage = thumb
                    
                }
            }
            let blurImage =  GPUImageGaussianBlurFilter().image(byFilteringImage: thumbnailImage)
            cropView = VideoEdCropView.fromNib()
            cropView.delegate =  self
            cropView.thumbnailImage = blurImage
            var frame = videoTransitionView.frame
            frame.size.height +=  categoryView.frame.size.height
            cropView.frame = frame
            bottomControlView.addSubview(cropView)
        }
        
        cropView.isHidden                       = false
        cropView.backgroundColor                = backViewThemeColor
        cropView.upperView.backgroundColor      = backViewThemeColor
        cropView.bottomView.backgroundColor     = backViewThemeColor
        cropView.aspectRatioCV.backgroundColor  = backViewThemeColor
        cropView.colorCV.backgroundColor        = backViewThemeColor
        cropView.showColors = true
        cropView.updateFeature()
        
       
    }
    
    func SplitView_HideBtnAction()  {
        
        splitView?.isHidden =  true
        pauseAVPlayer(pause: true)
        seekBarView.isUserInteractionEnabled = true
    }
    
    func SplitView_SliderDidChange(ratio: CGFloat) {
        //            let newSTart = Double(avPlayerCurrentTime)+Double(startvalue/CGFloat(currentData.videoSpeed))

        let videoData = selectedVideoData[videoTransitionView.videoSeletedIndex.item]
        let duration = CMTimeSubtract(videoData.trimEndTime, videoData.trimStartTime).seconds
        
        let currentTime = ((splitView?.previousTracksDuration.seconds)! + (duration / Double(videoData.videoSpeed) * Double(ratio)) )
        
        debugPrint("currentTime  / speed =====",currentTime)
        player?.seek(to: CMTime(seconds: currentTime, preferredTimescale: 6000) )
        seekPlayer(to: Double(currentTime))
    }
    
    func SplitView_SplitAction(ratio: CGFloat) {
        
        splitView?.isHidden =  true
        seekBarView.isUserInteractionEnabled = true
        
        let index = videoTransitionView.videoSeletedIndex.item
        if (index >= 0 ){
            
            // Split Video Data Modle
            let videoDataObjFirst :VideoData = selectedVideoData[index];
            let videoDataObjSecond:VideoData =  videoDataObjFirst.copy() as! VideoData
            
            var trackDuration =  CMTimeSubtract(videoDataObjFirst.trimEndTime, videoDataObjFirst.trimStartTime)
            trackDuration = CMTimeMultiplyByFloat64(trackDuration, multiplier: Float64(ratio))
            videoDataObjFirst.trimEndTime = CMTimeAdd(videoDataObjFirst.trimStartTime, trackDuration)
            videoDataObjSecond.trimStartTime = videoDataObjFirst.trimEndTime
            
            selectedVideoData.insert(videoDataObjSecond ,at: index + 1)
            
            let asset = selectedAssets[index]
            let assetCopy =  asset.copy()
            selectedAssets.insert(assetCopy as! PHAsset,at: index + 1)
            self.reCreateAVCompositions()
        }
    }
    
    
    
}

extension VideoEdEditVC :CropViewDelegate
{
    
    
    func addColorsView()
    {
        if cropView == nil
        {
            if(thumbnailImage == nil)            {
                if let thumb = VideoEdVideoManager().generateThumbnail(asset: compositionFromManager.mixCompStruct, compositionFromManager.mixVideoCompStruct , at: .zero, of: CGSize(width: 150, height: 150)){
                    thumbnailImage = thumb
                    
                }
            }
            let blurImage =  GPUImageGaussianBlurFilter().image(byFilteringImage: thumbnailImage)
            cropView = VideoEdCropView.fromNib()
            cropView.delegate =  self
            cropView.thumbnailImage = blurImage
            var frame = videoTransitionView.bounds
//            frame.size.height +=  categoryView.frame.size.height
            cropView.frame = frame
            videoTransitionView.addSubview(cropView)
        }
        videoTransitionView.bringSubviewToFront(cropView)
        cropView.isHidden                       = false
        cropView.backgroundColor                = backViewThemeColor
        cropView.upperView.backgroundColor      = backViewThemeColor
        cropView.aspectRatioCV.backgroundColor  = backViewThemeColor
        cropView.colorCV.backgroundColor        = backViewThemeColor
        cropView.showColors = true
        cropView.updateFeature()
        
       
    }
    func addAspectRatioView(){
        
        if cropView == nil
        {
            if(thumbnailImage == nil)            {
                if let thumb = VideoEdVideoManager().generateThumbnail(asset: compositionFromManager.mixCompStruct, compositionFromManager.mixVideoCompStruct , at: .zero, of: CGSize(width: 150, height: 150)){
                    thumbnailImage = thumb
                    
                }
            }
            let blurImage =  GPUImageGaussianBlurFilter().image(byFilteringImage: thumbnailImage)
            cropView = VideoEdCropView.fromNib()
            cropView.delegate =  self
            cropView.thumbnailImage = blurImage
            var frame = videoTransitionView.bounds
//            frame.size.height +=  categoryView.frame.size.height
            cropView.frame = frame
            videoTransitionView.addSubview(cropView)
        }
        videoTransitionView.bringSubviewToFront(cropView)
        cropView.isHidden                       = false
        cropView.backgroundColor                = backViewThemeColor
        cropView.upperView.backgroundColor      = backViewThemeColor
        cropView.aspectRatioCV.backgroundColor  = backViewThemeColor
        cropView.colorCV.backgroundColor        = backViewThemeColor
        cropView.showColors = false
        cropView.updateFeature()

    }
    
    func cropViewChangeRatio(ratio: CGFloat) {
        
        aspectCropRatio = ratio
        let videoSize = compositionFromManager.mixVideoCompStruct.renderSize
        
        videoRepresentViewWidthConstraint.isActive = true
        videoRepresentViewHeightConstraint.isActive = true
        
        if ratio == 0 {
            
            let aspectFrame =  AVMakeRect(aspectRatio: videoSize, insideRect: mainContainer.frame) //AspectFit
            videoRepresentViewWidthConstraint.constant = aspectFrame.width
           videoRepresentViewHeightConstraint.constant = aspectFrame.height
            self.view.layoutIfNeeded()
            gpuImageView.frame = videoRepresentView.bounds
            if temporaryVisualView != nil && gpuBackBlurView != nil{
                temporaryVisualView.frame = gpuBackBlurView.bounds
                
            }

            return
        }
        
        // var aspectsize = CGSize(width: minSide, height: minSide)
        var aspectSize = CGSize(width: 2000.0, height: 2000.0)
        aspectSize.width *= ratio;
        
        var aspectFrame =  AVMakeRect(aspectRatio: aspectSize, insideRect: mainContainer.frame) //AspectFit
        videoRepresentViewWidthConstraint.constant = aspectFrame.width
        videoRepresentViewHeightConstraint.constant = aspectFrame.height
        self.view.layoutIfNeeded()
        aspectFrame.origin.x = 0.0
        aspectFrame.origin.y = 0.0
        let representSize =  AVMakeRect(aspectRatio: videoSize, insideRect: aspectFrame) //AspectFit
        // gpuImageView.frame = CGRect(x: 0, y: 0, width: representSize.width, height: representSize.height)
        gpuImageView.frame = representSize
        if temporaryVisualView != nil && gpuBackBlurView != nil{
            temporaryVisualView.frame = gpuBackBlurView.bounds
            
        }
        
        if gradColorGlobal != nil{
            
            videoRepresentView.layer.sublayers!.forEach{ layer in
                
                if let newSafelayer = layer as? CAGradientLayer{
                    newSafelayer.removeFromSuperlayer()
                }
            }
            
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.colors = gradColorGlobal?.colorsArray
            // gradient.locations = [0.0 ,0.5, 1.0]
            gradient.startPoint = gradColorGlobal!.startPoint
            gradient.endPoint = gradColorGlobal!.endpoint
            gradient.frame = CGRect(x: 0.0, y: 0.0, width: videoRepresentView.frame.size.width, height: videoRepresentView.frame.size.height)
            videoRepresentView.layer.insertSublayer(gradient, at: 0)
        }

    }

    func cropViewChangeBlurColor(blurType: Int) {
        
        blurTypeGlobal = blurType
        
        gpuBackBlurView.isHidden    = false
        backBlurView.isHidden       = true
        videoBackGroundColor =  nil
        
        

        gpuBackBlurView.layer.sublayers?.forEach { $0.removeFromSuperlayer() }

        temporaryVisualView.removeFromSuperview()
        temporaryVisualView = nil

        if blurType == 0 {
            
            
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            temporaryVisualView = UIVisualEffectView(effect: blurEffect)
            temporaryVisualView.frame = gpuBackBlurView.bounds
            let blurLayer = temporaryVisualView.layer.sublayers![0]
            blurLayer.opacity = 0.5
            gpuBackBlurView.layer.addSublayer(blurLayer)

            
        }else if blurType == 1{
            
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
            temporaryVisualView = UIVisualEffectView(effect: blurEffect)
            temporaryVisualView.frame = gpuBackBlurView.bounds
            let blurLayer = temporaryVisualView.layer.sublayers![0]
            blurLayer.opacity = 0.8
            gpuBackBlurView.layer.addSublayer(blurLayer)

            
        }else{
           
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
            temporaryVisualView = UIVisualEffectView(effect: blurEffect)
            temporaryVisualView.frame = gpuBackBlurView.bounds
            let blurLayer = temporaryVisualView.layer.sublayers![0]
            gpuBackBlurView.layer.addSublayer(blurLayer)

            
        }
    }
    func cropViewChangeColor(color: UIColor?) {
        
        videoRepresentView.layer.sublayers!.forEach{ layer in
            if let newSafelayer = layer as? CAGradientLayer{
                newSafelayer.removeFromSuperlayer()
            }
        }
        gradColorGlobal = nil
        blurTypeGlobal = -1
        
        videoBackGroundColor =  color
        if color ==  nil        {
            // Blur Effect
            gpuBackBlurView.isHidden    = false
//            backBlurView.isHidden       = false
            

        }
        else{
            gpuBackBlurView.isHidden    = true
            backBlurView.isHidden       = true
            videoRepresentView.backgroundColor =  color
            
            

        }

//        blurTypeGlobal
    }

    func cropViewChangeGradColor(color: GradientColor?) {
        
        gradColorGlobal = color
        if color ==  nil        {
            gpuBackBlurView.isHidden    = false
            
            
        }
        else{
            gpuBackBlurView.isHidden    = true
            backBlurView.isHidden       = true
            
            videoRepresentView.layer.sublayers!.forEach{ layer in
                
                if let newSafelayer = layer as? CAGradientLayer{
                    newSafelayer.removeFromSuperlayer()
                }
            }
            
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.colors = gradColorGlobal?.colorsArray
            // gradient.locations = [0.0 ,0.5, 1.0]
            gradient.startPoint = gradColorGlobal!.startPoint
            gradient.endPoint = gradColorGlobal!.endpoint
            gradient.frame = CGRect(x: 0.0, y: 0.0, width: videoRepresentView.frame.size.width, height: videoRepresentView.frame.size.height)
            videoRepresentView.layer.insertSublayer(gradient, at: 0)
            
            
        }
        
        
    }

}

extension VideoEdEditVC : PickerVCDelegate  {

    //MARK:- PickerVCDelegate

    
    func pickerVCSelectVideos(_ phassetArray: [PHAsset]) {
        selectedAssets = selectedAssets +  phassetArray
        for asset in phassetArray {
            PHAssetManager.getUrlFromPHAsset(asset: asset) { (videoUrlStr) in
                DispatchQueue.main.async { [weak self] in
                    let videoUrl = URL(string: videoUrlStr!)
                    let videoSize = VideoEdVideoManager().resolutionOfVideo(videoUrl: videoUrl!)
                    print("videoSize is :",videoSize as Any)
                    let videoData = VideoData(url: videoUrl!)
                    videoData.trimStartTime = CMTime.zero
                    let duration = VideoEdVideoManager().getDurationOfUrl(url: videoUrl!)
                    videoData.trimEndTime = CMTime(seconds: duration, preferredTimescale: 100)
                    
                    self!.selectedVideoData.append(videoData)
                    
                    if (self!.selectedVideoData.count == self!.selectedAssets.count)  {
                        print("calles")
                        self!.reCreateAVCompositions()
                    }
                }
            }
        }
    }
}

extension UIApplication {
    class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return ((rate != 0) && (error == nil))
    }
}


//MARK:- GLITCH VIEW


extension VideoEdEditVC : GlitchFilterViewDelegate {
    
    func dismissGlitchView(){
        glitchFilterView?.isHidden =  true
        pauseAVPlayer(pause: false)
        seekBarView.isUserInteractionEnabled = true

    }
    func didSelectGlitchFilter(index: Int) {
        
        sharpenFilter.removeTarget(gpuGlitchFilter)
        gpuGlitchFilter = nil
        
        gpuGlitchFilter = VideoEdGlitchHelper().createGlitchFilter(IndexPath(row: index, section: 0))
        gpuGlitchFilter?.setFloat(0.0, forUniformName: "width")
        gpuGlitchFilter?.setFloat(0.0, forUniformName: "height")
        gpuGlitchFilter?.setFloat(0.0, forUniformName: "aspectRatio")
        gpuGlitchFilter?.setFloat(3.7, forUniformName: "iGlobalTime")
        sharpenFilter.addTarget(gpuGlitchFilter)
        gpuGlitchFilter!.addTarget(adjustFilter)

        pauseAVPlayer(pause: false)


        
    }
    
    
    
    func addGlitchFilterView(){
        
        
        if glitchFilterView == nil   {
            
            var frame = videoTransitionView.frame
            frame.size.height += categoryView.frame.height
            glitchFilterView = GlitchFilterView.initWithFrame(frame: frame)
            glitchFilterView!.delegate = self
            bottomControlView.addSubview(glitchFilterView!)
        }
        
        glitchFilterView?.setupUI()
        glitchFilterView?.isHidden =  false
        pauseAVPlayer(pause: true)
        seekBarView.isUserInteractionEnabled = false
        
        var previousTracksDuration = CMTime.zero
        
        if videoTransitionView.videoSeletedIndex.item > 0   {
            for i in  0...(videoTransitionView.videoSeletedIndex.item - 1)   {
                let videoData = selectedVideoData[i]
                var duration = CMTimeSubtract(videoData.trimEndTime, videoData.trimStartTime)
                let durationSeconds = duration.seconds / Double(videoData.videoSpeed)
                duration = CMTime(seconds: durationSeconds, preferredTimescale: 1000)
                previousTracksDuration =  CMTimeAdd(previousTracksDuration, duration)
            }
        }
        let avPlayerCurrentTime  = previousTracksDuration.seconds
        seekPlayer(to: Double(avPlayerCurrentTime))
        
    }
}

extension UIView {
    
    /**
     Rotate a view by specified degrees
     - parameter angle: angle in degrees
     */
    func rotate(angle angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat.pi
        let rotation = CGAffineTransform.init(rotationAngle: radians) //CGAffineTransformRotate(self.transform, radians);
        self.transform = rotation
    }
    /// Helper to get pre transform frame
    var originalFrame: CGRect {
        let currentTransform = transform
        transform = .identity
        let originalFrame = frame
        transform = currentTransform
        return originalFrame
    }
    
    
}
extension VideoEdEditVC {
    
    
    func testing(ratio: CGFloat) {
        
        aspectCropRatio = ratio
        let videoSize = compositionFromManager.mixVideoCompStruct.renderSize
        
        videoRepresentViewWidthConstraint.isActive = true
        videoRepresentViewHeightConstraint.isActive = true
        
        if ratio == 0 {
            
            let aspectFrame =  AVMakeRect(aspectRatio: videoSize, insideRect: mainContainer.frame) //AspectFit
            videoRepresentViewWidthConstraint.constant = aspectFrame.width
            videoRepresentViewHeightConstraint.constant = aspectFrame.height
            self.view.layoutIfNeeded()
            gpuImageView.frame = videoRepresentView.bounds
            if temporaryVisualView != nil && gpuBackBlurView != nil{
                temporaryVisualView.frame = gpuBackBlurView.bounds
                
            }
            
            return
        }
        
        // var aspectsize = CGSize(width: minSide, height: minSide)
        var aspectSize = CGSize(width: 2000.0, height: 2000.0)
        aspectSize.width *= ratio;
        
        var aspectFrame =  AVMakeRect(aspectRatio: aspectSize, insideRect: mainContainer.frame) //AspectFit
        videoRepresentViewWidthConstraint.constant = aspectFrame.width
        videoRepresentViewHeightConstraint.constant = aspectFrame.height
        self.view.layoutIfNeeded()
        aspectFrame.origin.x = 0.0
        aspectFrame.origin.y = 0.0
        let representSize =  AVMakeRect(aspectRatio: videoSize, insideRect: aspectFrame) //AspectFit
        // gpuImageView.frame = CGRect(x: 0, y: 0, width: representSize.width, height: representSize.height)
        gpuImageView.frame = representSize
        if temporaryVisualView != nil && gpuBackBlurView != nil{
            temporaryVisualView.frame = gpuBackBlurView.bounds
            
        }
        
    }
}

extension AVURLAsset {
    var fileSize: Int? {
        let keys: Set<URLResourceKey> = [.totalFileSizeKey, .fileSizeKey]
        let resourceValues = try? url.resourceValues(forKeys: keys)
        return resourceValues?.fileSize ?? resourceValues?.totalFileSize
    }
}
