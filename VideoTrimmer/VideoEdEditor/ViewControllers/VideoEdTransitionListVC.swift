//
//  VideoEdTransitionListVC.swift
//  VideoTrimmer
//
//  Created by strongapps on 30/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

protocol TransitionListVCDelegate: class {
    
    // func transitionDidSelect(_ videoObj:VideoData,_ indexPath:IndexPath);
    func transitionVCDidSelect(_ videoObj:VideoData,_ indexPath:IndexPath);
    // func transitionVCDidSelect( videoObj:VideoData)


}

class VideoEdTransitionListVC: UIViewController {
    
    public weak var delegate            :TransitionListVCDelegate?
    @IBOutlet weak var mainBackView     :UIView!
    @IBOutlet weak var topView          :UIView!
    @IBOutlet weak var representView    :UIView!
    @IBOutlet weak var backBtn          :UIButton!
    @IBOutlet weak var transitionCV     :UICollectionView!
    
    @IBOutlet weak var transitionTitle: UILabel!
    var itemSpacing                     :CGFloat = 22

    struct Title {
        var topLabel            :String = ""
        var bottomLabel         :String = ""
        init(topLabel:String,bottomLabel:String) {
            self.topLabel = topLabel
            self.bottomLabel = bottomLabel
        }
    }
    
    var titleNames = [Title(topLabel: "", bottomLabel: "None"),
                     Title(topLabel: "Fade", bottomLabel: ""),
                     Title(topLabel: "Slide", bottomLabel: "Left"),
                     Title(topLabel: "Slide", bottomLabel: "Right"),
                     Title(topLabel: "Slide", bottomLabel: "Top"),
                     Title(topLabel: "Slide", bottomLabel: "Bottom"),
                     Title(topLabel: "Zoom", bottomLabel: "out")
                     ]
    
    public var selectedVideoObj         :VideoData? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        transitionTitle.font = UIFont(name: FONT_BOLD, size: 23)
        createUI()
    }

    func createUI()  {
        
        transitionCV.delegate = self
        transitionCV.dataSource = self
        transitionCV.register(UINib(nibName: "TransitionCVCell", bundle: nil), forCellWithReuseIdentifier: "TransitionCVCell")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    @IBAction func backBtnAct(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension VideoEdTransitionListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (SCREEN_WIDTH - (itemSpacing * 3.0))/2.0
        return CGSize(width: side, height: side)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: itemSpacing, bottom: 0, right: itemSpacing)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TransitionCVCell", for: indexPath as IndexPath) as! TransitionCVCell

        if selectedVideoObj?.transitionType == indexPath.item {
            cell.mainBackView.layer.borderColor = AppThemeManager.shared.currentTheme().SelectedStrokeColor.cgColor
            cell.mainBackView.layer.borderWidth = 2.0
        }   else        {
            cell.mainBackView.layer.borderColor = UIColor.clear.cgColor
            cell.mainBackView.layer.borderWidth = 0.0
        }
 
        cell.titleTopLbl.text = titleNames[indexPath.item].topLabel
        cell.titleTopLbl.font = UIFont(name: FONT_REGULAR, size: 15)
        cell.titleBottomLbl.text = titleNames[indexPath.item].bottomLabel
        cell.titleBottomLbl.font = UIFont(name: FONT_REGULAR, size: 15)
        
        GlobalHelper.dispatchMainAfter(time: .now() + 0.3, execute: {
            self.showAnimationsOnCell(view: cell.thumbnailView, indexPath: indexPath)
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        selectedVideoObj?.transitionType = indexPath.item
        collectionView.reloadData()
        
        //collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
        delegate?.transitionVCDidSelect(selectedVideoObj!, indexPath)
        backBtnAct(backBtn)
    }
 
    func showAnimationsOnCell(view:UIView,indexPath:IndexPath) {
        
        view.layer.removeAllAnimations()
        view.layer.sublayers = nil
        
        let imageLayer1 = CALayer()
        imageLayer1.contents = UIImage(named: "TransImage1")?.cgImage
        imageLayer1.frame = view.bounds
        imageLayer1.backgroundColor  = UIColor.clear.cgColor
        imageLayer1.contentsGravity =  .resizeAspectFill
        
        let imageLayer2 = CALayer()
        imageLayer2.contents = UIImage(named: "TransImage2")?.cgImage
        imageLayer2.frame = view.bounds
        imageLayer2.backgroundColor  = UIColor.clear.cgColor
        imageLayer2.contentsGravity =  .resizeAspectFill
        
        view.layer.addSublayer(imageLayer2)
        view.layer.addSublayer(imageLayer1)

        if indexPath.item == 1  {
            VideoEdAnimationHelper().applyOpacityAnimation(layer: imageLayer1, fromValue: 1.0, toValue: 0.0, duration: 1.5, repeatMode: true)
        }
        else if indexPath.item == 2 {
            VideoEdAnimationHelper().applyPushAnimation(layer: imageLayer1, toPoint: CGPoint(x: -imageLayer1.bounds.maxX, y: 0), beginTime: 0.0, duration: 1.5, repeatMode: true)

        }
        else if indexPath.item == 3 {
            VideoEdAnimationHelper().applyPushAnimation(layer: imageLayer1, toPoint: CGPoint(x: imageLayer1.bounds.maxX, y: 0), beginTime: 0.0, duration: 1.5, repeatMode: true)

        }
        else if indexPath.item == 4 {
            VideoEdAnimationHelper().applyPushAnimation(layer: imageLayer1, toPoint: CGPoint(x: 0, y: -imageLayer1.bounds.maxY), beginTime: 0.0, duration: 1.5, repeatMode: true)

        }
        else if indexPath.item == 5 {
            VideoEdAnimationHelper().applyPushAnimation(layer: imageLayer1, toPoint: CGPoint(x: 0, y: imageLayer1.bounds.maxY), beginTime: 0.0, duration: 1.5, repeatMode: true)
        }
        
    }
    
    
}
