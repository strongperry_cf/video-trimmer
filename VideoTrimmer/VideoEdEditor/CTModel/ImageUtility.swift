//
//  ImageUtility.swift
//  Video Editorial
//
//  Created by strongapps on 15/03/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit
import ImageIO

class ImageUtility {
    
    static let shared = ImageUtility()
    
    func convert(toJPEGFormat inputImage: UIImage?) -> UIImage? {
        ///For ios 11 ,We are converting image to JPEG since PNG is not supported while Exporting in ios11.
        if #available(iOS 11.0, *) {
            if let pngData = inputImage?.jpegData(compressionQuality: 1){
                return UIImage(data: pngData)
                
            }
            return nil
        }
        return inputImage
    }
}


struct ImageHeaderData{
    static var PNG: [UInt8] = [0x89]
    static var JPEG: [UInt8] = [0xFF]
    static var GIF: [UInt8] = [0x47]
    static var TIFF_01: [UInt8] = [0x49]
    static var TIFF_02: [UInt8] = [0x4D]
}

enum ImageFormat{
    case Unknown, PNG, JPEG, GIF, TIFF
}

extension Data{
    var imageFormat: ImageFormat{
        var buffer = [UInt8](repeating: 0, count: 1)
        
        guard let range = Range(NSRange(location: 0,length: 1)) else{
            return .JPEG
        }
        self.copyBytes(to: &buffer, from: range)
        if buffer == ImageHeaderData.PNG
        {
            return .PNG
        } else if buffer == ImageHeaderData.JPEG
        {
            return .JPEG
        } else if buffer == ImageHeaderData.GIF
        {
            return .GIF
        } else if buffer == ImageHeaderData.TIFF_01 || buffer == ImageHeaderData.TIFF_02{
            return .TIFF
        } else{
            return .Unknown
        }
    }
}

