//
//  VideoEdAnimationHelper.swift
//  VideoTrimmer
//
//  Created by strongapps on 15/05/19.
//  Copyright © 2019 strongapps. All rights reserved.

import UIKit

class VideoEdAnimationHelper: NSObject {

    
    func layerPreviewOpacityAnimtion(fromValue:CGFloat , toValue:CGFloat , beginTime:CFTimeInterval , duration:CFTimeInterval) -> CAKeyframeAnimation {
        
        let animation       = CAKeyframeAnimation(keyPath: "opacity")
        animation.fillMode  = .forwards
        animation.values    = [fromValue,toValue,toValue,fromValue]
        animation.keyTimes  = [0.0,0.0001,0.9999,1.0]
        animation.duration  = CFTimeInterval(duration)
        animation.beginTime = AVCoreAnimationBeginTimeAtZero + beginTime
        animation.isRemovedOnCompletion = false
        animation.timingFunctions = []
        
        animation.timingFunctions = [CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut),
                                             CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut),
                                             CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut),
                                             CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)]
        
        return animation;
    }
    
    func applyPushAnimation(layer:CALayer,toPoint:CGPoint,beginTime:CGFloat = 0.0,duration:CGFloat = 0.3,repeatMode:Bool = true) {

        let animation       = CABasicAnimation(keyPath: "transform.translation")
        animation.fromValue = NSValue(cgPoint: CGPoint(x: 0.0, y: 0.0))
        animation.toValue   = NSValue(cgPoint: toPoint)
        //animation.fillMode  = .forwards
        animation.repeatCount = (repeatMode == true) ? HUGE : 1
        animation.isRemovedOnCompletion = false
        animation.duration  = CFTimeInterval(duration)
        animation.beginTime = AVCoreAnimationBeginTimeAtZero + Double(beginTime)
        layer.add(animation, forKey: "push")
    }

    func applyOpacityAnimation(layer:CALayer,fromValue:Double,toValue:Double,beginTime:CGFloat = 0.0,duration:CGFloat = 0.3,repeatMode:Bool = true) {
        
        let animation       = CABasicAnimation(keyPath: "opacity")
        animation.fromValue = fromValue
        animation.toValue   = toValue
        //animation.fillMode  = .forwards
        animation.repeatCount = (repeatMode == true) ? HUGE : 1
        animation.isRemovedOnCompletion = false
        animation.duration  = CFTimeInterval(duration)
        animation.beginTime = AVCoreAnimationBeginTimeAtZero + Double(beginTime)
        layer.add(animation, forKey: "opacity")
    }
    
    func createGIFAnimation(data:NSData?) -> CAKeyframeAnimation? {

        if data == nil {
            return nil
        }
        
        let source = CGImageSourceCreateWithData((data)  as! CFData, nil)
        let frameCount = Int(CGImageSourceGetCount(source!))
        
        if(frameCount<1){
            return nil
        }
        
        var totalTime: Float = 0
        
        var framesArray: [AnyHashable] = []
        var tempTimesArray: [AnyHashable] = []
        
        if frameCount < 1 {
            return nil        }
      
        for i in 0..<frameCount {
            
            // Frame default duration
            var eachFrameDuration : Float = 0.1;
            
            let cfFrameProperties = CGImageSourceCopyPropertiesAtIndex(source!, i, nil)
            guard let frame_Prpoerties = cfFrameProperties as? [String:AnyObject] else {return nil}
            guard let gif_Properties = frame_Prpoerties[kCGImagePropertyGIFDictionary as String] as? [String:AnyObject]
                else { return nil }

            // Use kCGImagePropertyGIFUnclampedDelayTime or kCGImagePropertyGIFDelayTime
            if let delayTimeUnclampedProp = gif_Properties[kCGImagePropertyGIFUnclampedDelayTime as String] as? NSNumber {
                eachFrameDuration = delayTimeUnclampedProp.floatValue
            }
            else{
                if let delayTimeProp = gif_Properties[kCGImagePropertyGIFDelayTime as String] as? NSNumber {
                    eachFrameDuration = delayTimeProp.floatValue
                }
            }
            
            if (eachFrameDuration < 0.011 ){
                eachFrameDuration = 0.100;
            }
            
            // Adding image to array
            if let frame = CGImageSourceCreateImageAtIndex(source!, i, nil) {
                tempTimesArray.append(NSNumber(value: eachFrameDuration))
                framesArray.append(frame)
            }
            // Total  time of Images
            totalTime = totalTime + eachFrameDuration
        }
        
        var timesArray = [NSNumber]()
        var base : Float = 0
        timesArray.append(NSNumber(value: 0.0))

        for case let duration as NSNumber in tempTimesArray {
            base = base + ( duration.floatValue / totalTime )
            timesArray.append(NSNumber(value: base))
        }
        
        let gifAnimation = CAKeyframeAnimation(keyPath: "contents")
        gifAnimation.keyTimes = timesArray
        gifAnimation.values = framesArray
        gifAnimation.beginTime = AVCoreAnimationBeginTimeAtZero
        gifAnimation.duration = CFTimeInterval(totalTime)
        gifAnimation.repeatCount = HUGE;
        gifAnimation.isRemovedOnCompletion = false
        gifAnimation.fillMode = CAMediaTimingFillMode.forwards
        gifAnimation.fillMode = .both

        //animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        gifAnimation.calculationMode = CAAnimationCalculationMode.discrete
        
        return  gifAnimation
    }
    
    
}
