//
//  PHAssetManager.swift
//  Video Editorial
//
//  Created by strongapps on 29/11/18.
//  Copyright © 2018 strongapps. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices

enum RequestType: Int {
    case video = 1, live = 2, photo = 3, burst = 4, gif = 5
}
//struct FilePathManager {
//    static var saveURL: URL {
//        let path = NSTemporaryDirectory().appendingFormat("SlideVideo.mov")
//        let videoURL = URL(fileURLWithPath: path)
//        try? FileManager.default.removeItem(atPath: path)
//        return videoURL
//    }
//}

class PHAssetData {
    
    var isSelected = false
    var asset: PHAsset
    var count = 0
    var indexPath: IndexPath?
    
    init(asset: PHAsset) {
        self.asset = asset
    }
}

class PHAssetManager: NSObject {
    
    var options: PHFetchOptions?
    static let shared = PHAssetManager()
    
    override init() {
        super.init()
    }
//    private override init() {}
    
    func fetchAssetWithType(type: RequestType = .video, completion: @escaping (_ : Array<Any>) -> Void) {
        options = PHFetchOptions()
        options?.includeAllBurstAssets = false
        options?.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        switch type {
        case .video:
            fetchVideos { (items) in
                completion(items)
            }
        case .live:
            fetchLivePhoto { (items) in
                completion(items)
            }
        case .photo:
            fetchImages { (items) in
                completion(items)
            }
        case .burst:
            fetchBurstImage { (items) in
                completion(items)
            }
        case .gif:
            fetchGIFImage { (items) in
                completion(items)
            }
       
        }
    }
    
    func fetchMoments(completion: @escaping (_: Array<Any>) -> Void) {
        options = PHFetchOptions()
        guard let fetchOptions = options else { return }
        var imageArray = Array<Any>()
        let result = PHAssetCollection.fetchAssetCollections(with: .moment, subtype: .albumRegular, options: fetchOptions)
        result.enumerateObjects { (collection, index, flag) in
            let asset = PHAsset.fetchAssets(in: collection, options: fetchOptions)
            imageArray.append(asset)
        }
        completion(imageArray)
    }
    
    func fetchImages(completion: @escaping (_: Array<Any>) -> Void) {
        guard let fetchOptions = options else { return }
        let result = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        var imageArray = Array<Any>()
        result.enumerateObjects { (asset, index, flag) in
            if asset.burstIdentifier != nil {
                return //here we remove burst photos
            }
            if asset.mediaSubtypes == PHAssetMediaSubtype.photoLive {
                return //here we remove live photos
            }
            if let identifier = asset.value(forKey: "uniformTypeIdentifier") as? String {
                if identifier == kUTTypeJPEG as String || identifier == kUTTypePNG as String || identifier == "public.heic" {
                    let model = PHAssetData(asset: asset)
                    imageArray.append(model)
                }
            }
        }
        completion(imageArray)
    }
    

    func fetchVideos(completion: @escaping (_: Array<Any>) -> Void) {
        guard let fetchOptions = options else { return }
        let result = PHAsset.fetchAssets(with: .video, options: fetchOptions)
        var imageArray = Array<Any>()
        result.enumerateObjects { (asset, index, flag) in
            if asset.burstIdentifier != nil {
                return //here we remove burst photos
            }
        
            imageArray.append(asset)
        }
        completion(imageArray)
    }
    
    func fetchLivePhoto(completion: @escaping (_: Array<Any>) -> Void) {
        guard let fetchOptions = options else { return }
        fetchOptions.predicate = NSPredicate(format: "mediaSubtype = %d", (PHAssetMediaSubtype.photoLive.rawValue))
        let result = PHAsset.fetchAssets(with: fetchOptions)
        var imageArray = Array<Any>()
        result.enumerateObjects { (asset, index, flag) in
            imageArray.append(asset)
        }
        completion(imageArray)
    }
    
    func fetchBurstImage(completion: @escaping (_: Array<Any>) -> Void) {
        guard let fetchOptions = options else { return }
        fetchOptions.includeAllBurstAssets = true
        let result = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        var imageArray = Array<Any>()
        result.enumerateObjects { (asset, index, flag) in
            if asset.representsBurst {
                imageArray.append(asset)
            }
        }
        completion(imageArray)
    }
    
    func fetchGIFImage(completion: @escaping (_: Array<Any>) -> Void) {
        guard let fetchOptions = options else { return }
        let result = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        var imageArray = Array<Any>()
        result.enumerateObjects { (asset, index, flag) in
            if let identifier = asset.value(forKey: "uniformTypeIdentifier") as? String {
                if identifier == kUTTypeGIF as String {
                    imageArray.append(asset)
                }
            }
        }
        completion(imageArray)
    }
    
    
    static func getUrlFromPHAsset(asset: PHAsset, callBack: @escaping (_ url: String?) -> Void) {
        let inputRequestOptions = PHContentEditingInputRequestOptions()
        inputRequestOptions.isNetworkAccessAllowed = true

        asset.requestContentEditingInput(with: inputRequestOptions, completionHandler: { (contentEditingInput, dictInfo) in
            if let strURL = (contentEditingInput!.audiovisualAsset as? AVURLAsset)?.url.absoluteString {
                callBack(strURL)
            }
        })
    }
    
    static func videoFromLive(asset: PHAsset, callBack: @escaping (_ url: String?) -> Void) {
//        let videoURL = FilePathManager.saveURL
        let videoURL = FilePathManager.createNewPath("videoFromLive.mp4")

        let res = PHAssetResource.assetResources(for: asset)
        if res.count > 0 {
            PHAssetResourceManager.default().writeData(for: res[1], toFile: videoURL, options: nil) { (error) in
                if error == nil {
                    callBack(videoURL.absoluteString)
                }
            }
        }
    }
    
    /*
     PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
     options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat; //I only want the highest possible quality
     options.synchronous = NO;
     options.networkAccessAllowed = YES;
     options.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {
     NSLog(@"%f", progress); //follow progress + update progress bar
     };
     
     [[PHImageManager defaultManager] requestImageForAsset:myPhAsset targetSize:self.view.frame.size contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage *image, NSDictionary *info) {
     NSLog(@"reponse %@", info);
     NSLog(@"got image %f %f", image.size.width, image.size.height);
     }];
    */
    
    
    public func getOriginalImage(asset: PHAsset,clouser: @escaping((UIImage?)->())) {
        let options = PHImageRequestOptions()
        options.isNetworkAccessAllowed = true
        options.deliveryMode = .highQualityFormat
        

        options.progressHandler = { (progress, err, stop,dic) in
            print("progress for image", progress)

//            let percent : Int = (Int((exporter?.progress)!*100))
            let str = String(format: "%d%%", Int(progress * 100))
            NVActivityIndicatorPresenter.sharedInstance.setMessage(str)

        }
//        let imageSize = CGSize(width:SCREEN_WIDTH, height:SCREEN_WIDTH)
        PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: options) { (image, info) in
            if image != nil {
                clouser(image)
            } else {
                clouser(nil)
            }
        }
//        PHImageManager.default().requestImageData(for: asset, options: options, resultHandler: { (data, str, orientation, keys) in
//            if let imageData = data {
//                if let image = UIImage(data: imageData) {
//                    clouser(image)
//                } else {
//                    clouser(nil)
//                }
//            }
//        })
    }

}



