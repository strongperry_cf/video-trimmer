//
//  VideoEdCompositionManager.swift
//  Video Editorial
//
//  Created by strongapps on 04/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

enum videoTrimAngleEnum: Double {
    
    case kZero = 0, k90 = 90, k180 = 180, k270 = 270
    
    var cgFloat: CGFloat {
        switch self {
        case .kZero:
            return 0
        case .k90:
            return .pi / 2
        case .k180:
            return .pi
        case .k270:
            return .pi * 1.5
        }
    }
}


class VideoEdCompositionManager: NSObject {
    
    public static let shared = VideoEdCompositionManager()
    
    static func getCompositionbyTriming(videoAsset: AVAsset,audioAsset: AVAsset?,from startTime: CGFloat,to endTime: CGFloat,rotation: videoTrimAngleEnum = .kZero) -> VideoCompositionModelStruct?{
        let asset = videoAsset
        guard let videoTrack = asset.tracks(withMediaType: AVMediaType.video).first else {
            return nil
        }
        var audioTrack: AVAssetTrack?
        if let aTrack = asset.tracks(withMediaType: AVMediaType.audio).first{
            audioTrack = aTrack
        }
        let mixComposition = AVMutableComposition()
        let sTime = CMTime(seconds: Double(startTime), preferredTimescale: videoAsset.duration.timescale)
        let eTime = CMTime(seconds: Double(endTime), preferredTimescale: videoAsset.duration.timescale)
        let timeRange = CMTimeRange(start: sTime, end: eTime)
        
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        do {
            if audioAsset == nil {
                // Add video track to video composition at specific time
                try videoCompositionTrack?.insertTimeRange(timeRange, of: videoTrack, at: .zero)
                // Add video track to video composition at specific time
                if audioTrack != nil{
                    try audioCompositionTrack?.insertTimeRange(timeRange, of: audioTrack!, at: .zero)
                }
            }
            else
            {
                let musicStartTime = CMTime.zero
                var musicInsertTime = CMTime.zero
                var musicEndTime = CMTime.zero
                
                let finalAudioAsset = audioAsset
                let finalSoundTrack = finalAudioAsset?.tracks(withMediaType: AVMediaType.audio).first!
                musicEndTime = finalAudioAsset!.duration
                
                try videoCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime,duration: videoAsset.duration),
                                                           of: videoTrack,
                                                           at: musicInsertTime)
                
                
                
                //if video duration is shorter than complete music duration
                if(mixComposition.duration < finalAudioAsset!.duration){
                    if let audioTrack = finalSoundTrack {
                        try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime,duration: mixComposition.duration),
                                                                   of: audioTrack,
                                                                   at: musicInsertTime)
                    }
                }else{
                    if let audioTrack = finalSoundTrack {
                        try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime, duration: musicEndTime),
                                                                   of: audioTrack,
                                                                   at: musicInsertTime)
                        
                        musicInsertTime = CMTimeAdd(musicInsertTime, musicEndTime)
                    }
                    if let finalAudioDuration = finalAudioAsset?.duration {
                        //                    finalAudioDuration = CMTime.zero
                        if (mixComposition.duration > finalAudioDuration) // && finalAudioDuration != CMTime.zero)
                        {
                            let remainder =  Double(CMTimeGetSeconds(mixComposition.duration).truncatingRemainder(dividingBy: Double(CMTimeGetSeconds(finalAudioDuration))))
                            let quotient = Int(CMTimeGetSeconds(mixComposition.duration)/CMTimeGetSeconds(finalAudioDuration))
                            print("Remainder",remainder,quotient)
                            let lock = NSLock()
                            lock.lock()
                            if isMusicRepetitionStatus {
                                // Edit by harpreet - play when music repeats
                                for val in 0..<quotient{
                                    if(val != (quotient-1)){
                                        if let audioTrack = finalSoundTrack {
                                            try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime, duration: musicEndTime),
                                                                                       of: audioTrack,
                                                                                       at: musicInsertTime)
                                            musicInsertTime = CMTimeAdd(musicInsertTime, musicEndTime)
                                        }
                                    }
                                }
                                lock.unlock()
                                if(remainder > 0){
                                    if let audioTrack = finalSoundTrack {
                                        try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime, duration: CMTimeMakeWithSeconds(remainder, preferredTimescale: finalAudioDuration.timescale)),  of: audioTrack,  at: musicInsertTime)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch {
            print("Error to load track")
        }
        var naturalSize = videoTrack.naturalSize
        naturalSize = naturalSize.applying(videoTrack.preferredTransform)
        naturalSize = CGSize(width: abs(naturalSize.width), height: abs(naturalSize.height))
        
        
        let frontLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
        let (transform,  isPortrait) = VideoEdCompositionManager.videoTransformAffineAndPortrait(asset: asset, standardSize: naturalSize)
        let newTransform = VideoEdCompositionManager.videoRotateComposition(for: transform, isP: isPortrait, with: rotation, size: naturalSize)
        frontLayerInstruction.setTransform(newTransform.0, at: .zero)
        frontLayerInstruction.setOpacity(1.0, at: CMTime.zero)
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        videoComposition.renderScale  = 1.0
        videoComposition.renderSize =  newTransform.1
        mixComposition.naturalSize = newTransform.1
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange =  (videoCompositionTrack?.timeRange)!
        mainInstruction.layerInstructions = [frontLayerInstruction]
        videoComposition.instructions = [mainInstruction]
        
        
     /*         To export video with gif
         self.applyVideoEffectsToComposition(composition: videoComposition, size: naturalSize)// videoComposition, size: naturalSize ) // (videoComposition)
        
        VideoEdCompositionManager.shared.exportVideoWith(mixComposition: mixComposition, videoComposition: videoComposition, range: timeRange, progressBlock: { (percentttt) in
            print(percentttt)
        }) { (urlll) in
            print(urlll)
        }*/
        var composition = VideoCompositionModelStruct()
        composition.mixCompStruct = mixComposition
        composition.videoTimeRangeStruct = timeRange
        composition.mixVideoCompStruct = videoComposition
        composition.videoRotationAngleStruct = rotation.cgFloat
        composition.videoAssetStruct =  asset
        return composition
    }
   /* static func getMultipleVideoComp(videoURLArray: Array<Any> ,from startTime: CGFloat,to endTime: CGFloat,rotation: videoTrimAngleEnum = .kZero) -> VideoCompositionModelStruct?{
        
        let mixComposition = AVMutableComposition()
        var totalTime = CMTime.zero
        var audioTime = CMTime.zero
        let layerInstructionsArray : [AVMutableVideoCompositionLayerInstruction]
        
        for videoAsset in videoURLArray {
            
            let videoTrack =
                mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                               preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
            do {
                try videoTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: (videoAsset as AnyObject).duration),
                                                of: (videoAsset as AnyObject).tracks(withMediaType: AVMediaType.video).first!,
                                                at: totalTime)
               var videoSize = (videoTrack?.naturalSize)!
            } catch let error as NSError {
                print("error: \(error)")
            }
            let trackArray = (videoAsset as AnyObject).tracks(withMediaType: .audio)
            if trackArray.count > 0 {
                let audioTrack =
                    mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
                do {
                    try audioTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: (videoAsset as AnyObject).duration), of: (videoAsset as AnyObject).tracks(withMediaType: AVMediaType.audio).first!, at: audioTime)
                    audioTime = audioTime + (videoAsset as AnyObject).duration
                }
                catch {
                    
                }
            }
            totalTime = totalTime + (videoAsset as AnyObject).duration
            let videoInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack!)
            
            var naturalSize = videoTrack!.naturalSize
            naturalSize = naturalSize.applying(videoTrack!.preferredTransform)
            naturalSize = CGSize(width: abs(naturalSize.width), height: abs(naturalSize.height))
            
            let (transform,  isPortrait) = VideoEdCompositionManager.videoTransformAffineAndPortrait(asset: ((videoAsset as AnyObject) as! AVAsset), standardSize: naturalSize)
            let newTransform = VideoEdCompositionManager.videoRotateComposition(for: transform, isP: isPortrait, with: rotation, size: naturalSize)
            videoInstruction.setTransform(newTransform.0, at: .zero)
//            if videoAsset != videoURLArray.last{
                videoInstruction.setOpacity(0.0, at: totalTime)
//            }
            layerInstructionsArray.append(videoInstruction)
        }
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: totalTime)
        mainInstruction.layerInstructions = layerInstructionsArray
        
        let mainComposition = AVMutableVideoComposition()
        mainComposition.instructions = [mainInstruction]
        mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        mainComposition.renderSize = newTransform.1
        
        var videoCompostionStruct : VideoCompositionModelStruct
        videoCompostionStruct.mixCompStruct = mixComposition
        videoCompostionStruct.mixVideoCompStruct = mainComposition
        videoCompostionStruct.videoRotationAngleStruct = rotation.cgFloat
        videoCompostionStruct.videoAssetStruct =  asset
    }*/
    
    static func resolutionOfVideo(videoUrl:URL) -> CGSize? {
        
        guard let track = AVAsset(url: videoUrl as URL).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
    
    static func isUrlContainsAudioTrack(url:URL) -> Bool {
        let asset: AVURLAsset? = AVURLAsset(url: url as URL, options: nil)
        if ((asset)?.tracks(withMediaType: .audio))!.count != 0 {
            return true;
        }
        return false;
    }
    
    static func isUrlContainsVideoTrack(url:URL) -> Bool {
        let asset: AVURLAsset? = AVURLAsset(url: url as URL, options: nil)
        if ((asset)?.tracks(withMediaType: .video))!.count != 0 {
            return true;
        }
        return false;
    }
    
    static func getAssetFromUrl(videoArray : [PHAsset] ,complitionBlock:@escaping (_ arr : NSMutableArray) -> ()) {        
        let arrayVideoURLNew : NSMutableArray = []
        for url in videoArray {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: url, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                DispatchQueue.main.async {
                    if let urlAsset = asset as? AVURLAsset {

                        // let assetNew: AVURLAsset? = AVURLAsset(url: url as URL, options: nil)
                        print("urlAsset.url >>>>>> " ,urlAsset.url)
                        var vdoStruct = VideoAssetURLStruct()
                        vdoStruct.videoAssetURL = urlAsset
                        vdoStruct.videoStartTime = 0.0
                        vdoStruct.videoEndTime = Double(urlAsset.duration.durationInSeconds)

                        arrayVideoURLNew.add(vdoStruct)
                        if videoArray.count == arrayVideoURLNew.count {
                            complitionBlock(arrayVideoURLNew)
                        }
                    }
                }
            })
        }
    }
    
 /*   static func getMultipleVideoComp(videoArrayNew : [PHAsset]) ->  VideoCompositionModelStruct? {
        
       
        VideoEdCompositionManager.getAssetFromUrl(videoArray: videoArrayNew) { (ass) in
            
            
            
        let videoArray = ass
        // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        let mixComposition = AVMutableComposition()
        var arrayLayerInstructions:[AVMutableVideoCompositionLayerInstruction] = []
        
        // Calculate Final Average Size Of Videos
        var outputSize = CGSize.init(width: 1920, height: 1080)
      var isAudioContains = false
        var isVideoContains = false
          for videoModal in videoArray
        {
            let url:URL = videoModal as! URL
//            if url == nil {  continue }
            
            let videoSize: CGSize =  VideoEdCompositionManager.resolutionOfVideo(videoUrl: url )! //.resolutionOfVideo(videoUrl: url )!
            if (videoSize.height) > outputSize.height {
                outputSize.height = (videoSize.height)
            }
            if (videoSize.width) > outputSize.width {
                outputSize.width = (videoSize.width)
            }
            if (isUrlContainsVideoTrack(url: url) == false)    {
                
              //  complitionBlock (nil,nil,nil);
                break
            }
            else{     isVideoContains = true     }
            
            if (isUrlContainsAudioTrack(url: url) == true)    {
                isAudioContains = true     }
        }
        
        // Init video & audio composition track
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        var audioCompositionTrack:AVMutableCompositionTrack? = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                                              preferredTrackID: Int32(kCMPersistentTrackID_Invalid));
        if !isAudioContains {
            audioCompositionTrack = nil
        }
        
        /*
         let assetInfo = orientationFromTransform(transform: (videoTrack.preferredTransform))
         outputSize = videoTrack.naturalSize
         if assetInfo.isPortrait == true {
         outputSize.width = (videoTrack.naturalSize.height)
         outputSize.height = (videoTrack.naturalSize.width)
         }
         */
        // ------- Adding Multiple Video Tracks With Different Different Speed ---------
        var currentTime:CMTime = CMTime.zero;
        
        for i in 0 ..< videoArray.count {
            
            //            let videoSpeedObj:VideoWithSpeed  =  videoArray.object(at: i) as! VideoWithSpeed;
            let videoUrl  =  videoArray[i] ;
//            debugPrint("videoSpeedObj.videoUrl is ",videoSpeedObj.videoUrl as Any);
//            let videoUrl = videoSpeedObj.videoUrl;
            
            let videoAsset: AVURLAsset?  = AVURLAsset(url: videoUrl as! URL, options: nil)
            
            // Get video track
            guard let videoTrack = videoAsset?.tracks(withMediaType: AVMediaType.video).first else { continue }
            
            // Get audio track
            var audioTrack:AVAssetTrack?
            if (videoAsset?.tracks(withMediaType: AVMediaType.audio).count)! > 0 {
                audioTrack = videoAsset?.tracks(withMediaType: AVMediaType.audio).first
            }
            
            // **** calculation Timings
            let SpeedMuliplier:Float =  1.0 //videoSpeedObj.videoSpeed ;
            let currentVideoDuration:CMTime = (videoAsset?.duration)!;
            
            let durationFloat :Float =  Float(CMTimeGetSeconds(currentVideoDuration));
            let calDuration:Float = durationFloat/SpeedMuliplier;
            let calDurationInt:Int64 = Int64(calDuration*100);
            let calDurationCMTime:CMTime = CMTimeMake(value: calDurationInt, timescale: 100);
            _ =  CMTimeGetSeconds(calDurationCMTime);
            
            
            // Init video & audio composition track
            do {
                
                // ***** 1
                // Add video track to video composition at specific time
                
                try videoCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: currentVideoDuration),
                                                           of: videoTrack,
                                                           at: currentTime)
                // Add audio track to audio composition at specific time
                if let audioTrack = audioTrack {
                    
                    try audioCompositionTrack!.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: currentVideoDuration),
                                                               of: audioTrack,
                                                               at: currentTime)
                    
                    audioCompositionTrack!.scaleTimeRange(CMTimeRangeMake(start: CMTimeSubtract((audioCompositionTrack?.timeRange.duration)!, currentVideoDuration) , duration: currentVideoDuration), toDuration: calDurationCMTime); // Org
                    
                }
                
                // ---  scale  ----
                videoCompositionTrack?.scaleTimeRange(CMTimeRangeMake(start: CMTimeSubtract((videoCompositionTrack?.timeRange.duration)!, currentVideoDuration) , duration: currentVideoDuration), toDuration: calDurationCMTime); // ORG
                
                let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
                
                // ----- Opacity -----
                layerInstruction.setOpacity(0, at: (videoCompositionTrack?.timeRange.duration)!)
                
                
                /*
                 let timeRange:CMTimeRange = CMTimeRange(start: ((videoCompositionTrack?.timeRange.duration)!), duration: CMTime(value: 2, timescale: 1 ))
                 layerInstruction.setOpacityRamp(fromStartOpacity: 1.0, toEndOpacity: 0.0, timeRange: timeRange);
                 
                 */
                
                // ----- Transform ---
                /*var transOrg = videoTrack.preferredTransform;
                 let assetInfo = orientationFromTransform(transform: transOrg)
                 let trans = correctTransform(asset: videoAsset!, standardSize: outputSize);*/
                // layerInstruction.setTransform((videoAsset?.preferredTransform)!, at: currentTime)
                
                layerInstruction.setTransform((videoTrack.preferredTransform), at: currentTime)
                arrayLayerInstructions.append(layerInstruction)
                
            } // Do
            catch {
                print("Load track error")
            }
            
            
            currentTime = (videoCompositionTrack?.timeRange.duration)!;
            
            //  prevTime = CMTimeAdd(prevTime, trimDuration);
            //  debugPrint("current Total Video Duratoion is",prevTime);
            
            let duration = videoCompositionTrack?.timeRange.duration;
            debugPrint("current Total Video Duratoion is",duration as Any);
            
        }
        
        // **** Video Composition
        
        // Main video composition instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: mixComposition.duration)
        // mainInstruction.timeRange = (videoCompositionTrack?.timeRange)!
        mainInstruction.layerInstructions = arrayLayerInstructions
        
        // Main video composition
        let videoComposition = AVMutableVideoComposition()
        videoComposition.instructions = [mainInstruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        videoComposition.renderSize = outputSize;
        
        /*
         // 2 Get video track
         guard let videoTrack = asset.tracks(withMediaType: AVMediaType.video).first else {
         complitionBlock (nil,nil,nil);
         return;
         }
         */
        
       
        videoCompostionStruct.mixCompStruct = mixComposition
        videoCompostionStruct.mixVideoCompStruct = videoComposition
//        videoCompostionStruct.videoAssetStruct =  asset
            composition = videoCompostionStruct
       // complitionBlock (mixComposition,videoComposition,nil);
//        return videoCompostionStruct
        }
        return videoCompostionStruct
    }
    static var videoCompostionStruct = VideoCompositionModelStruct()*/
    
     func VideoCorrectTransform(asset: AVAsset, standardSize: CGSize) -> CGAffineTransform {
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        var finalTransform = assetTrack.preferredTransform
        var aspectFillRatio: CGFloat = 1
        var videoTrackSize = assetTrack.naturalSize.applying(assetTrack.preferredTransform)
        videoTrackSize = CGSize(width: abs(videoTrackSize.width), height: abs(videoTrackSize.height))
        let maxAspectRatio = standardSize.width / standardSize.height
        let videoAspectRatio = videoTrackSize.width / videoTrackSize.height
        if maxAspectRatio > videoAspectRatio {
            aspectFillRatio = standardSize.height / videoTrackSize.height
        } else {
            aspectFillRatio = standardSize.width / videoTrackSize.width
        }
        let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
        let posX = standardSize.width/2 - (videoTrackSize.width * aspectFillRatio)/2
        let posY = standardSize.height/2 - (videoTrackSize.height * aspectFillRatio)/2
        let moveFactor = CGAffineTransform(translationX: posX, y: posY)
        let concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
        finalTransform = concat
        return finalTransform
    }
    
    static func getTransformForPortraitVideo(for videoTrack: AVAssetTrack) -> CGAffineTransform {
        
        let natSize = CGSize(width: videoTrack.naturalSize.width,
                             height: videoTrack.naturalSize.height)
        
        let cropFrame = CGRect(x: 0, y: 0, width: natSize.width, height: natSize.height)
        let renderScale = cropFrame.width / cropFrame.width
        let offset = CGPoint(x: -cropFrame.origin.x, y: -cropFrame.origin.y)
        let rotation = atan2(videoTrack.preferredTransform.b, videoTrack.preferredTransform.a)
        
        var rotationOffset = CGPoint(x: 0, y: 0)
        
        if videoTrack.preferredTransform.b == -1.0 {
            rotationOffset.y = videoTrack.naturalSize.width
        } else if videoTrack.preferredTransform.c == -1.0 {
            rotationOffset.x = videoTrack.naturalSize.height
        } else if videoTrack.preferredTransform.a == -1.0 {
            rotationOffset.x = videoTrack.naturalSize.width
            rotationOffset.y = videoTrack.naturalSize.height
        }
        
        var transform = CGAffineTransform.identity
        transform = transform.scaledBy(x: renderScale, y: renderScale)
        transform = transform.translatedBy(x: offset.x + rotationOffset.x, y: offset.y + rotationOffset.y)
        transform = transform.rotated(by: rotation)
        
        print("track size \(videoTrack.naturalSize)")
        print("preferred Transform = \(videoTrack.preferredTransform)")
        print("rotation angle \(rotation)")
        print("rotation offset \(rotationOffset)")
        print("actual Transform = \(transform)")
        return transform
    }
    
    
    static func videoTransformAffineAndPortrait(asset: AVAsset, standardSize:CGSize ) -> (CGAffineTransform, Bool) {
        
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        let transform = assetTrack.preferredTransform
        var finalTransform = assetTrack.preferredTransform
        
        let assetInfo = VideoEdCompositionManager.shared.orientationFromTransform(transform: transform)
        
        var aspectFillRatio:CGFloat = 1
        
        var videoSize = assetTrack.naturalSize;
        videoSize = videoSize.applying(assetTrack.preferredTransform)
        videoSize = CGSize(width: abs(videoSize.width), height: abs(videoSize.height))
        
        //  if videoSize.height > videoSize.width //Aspect Fit
        if videoSize.height < videoSize.width  //Aspect Fill
        {
            aspectFillRatio = standardSize.height / videoSize.height
        }
        else {
            aspectFillRatio = standardSize.width / videoSize.width
        }
        
        if assetInfo.isPortrait {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            let posX = standardSize.width/2 - (videoSize.width * aspectFillRatio)/2
            let posY = standardSize.height/2 - (videoSize.height * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor);
            
        } else {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            let posX = standardSize.width/2 - (videoSize.width * aspectFillRatio)/2
            let posY = standardSize.height/2 - (videoSize.height * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            let concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
            if assetInfo.orientation == .down {
                // let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                //concat = fixUpsideDown.concatenating(scaleFactor).concatenating(moveFactor)
            }
            finalTransform = concat;
        }
        
        return (finalTransform,assetInfo.isPortrait) ;
    }
    
    func orientationFromTransform(transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
            assetOrientation = .up
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
        }
        return (assetOrientation, isPortrait)
    }
    
    static func videoRotateComposition(for naturalTransform: CGAffineTransform, isP: Bool = false, with angle: videoTrimAngleEnum = .kZero, size: CGSize) -> (CGAffineTransform, CGSize) {
        
        var trans = naturalTransform
        let w = size.width
        let h = size.height
        var outSize = size
        
        switch angle {
        case .kZero: break
        case .k90:
            if isP {
                trans = trans.translatedBy(x: w, y: w - h)
                trans = trans.rotated(by: angle.cgFloat)
//                trans = trans.translatedBy(x: w, y: -w * (w / h))
            } else {
                print(trans)
                if trans.tx < 1{
                    trans = trans.translatedBy(x: h, y: 0)
                }else{
                    trans = trans.translatedBy(x: w, y:  h - w )
                }
                trans = trans.rotated(by: angle.cgFloat)
            }
            outSize = CGSize(width: h, height: w)
        case .k180:
            if isP {
                trans = trans.translatedBy(x: h, y: w)
                trans = trans.rotated(by: angle.cgFloat)
            } else {
                trans = trans.translatedBy(x: w, y: h)
                trans = trans.rotated(by: angle.cgFloat)
            }
        case .k270:
            trans = trans.translatedBy(x: 0, y: w)
            outSize = CGSize(width: h, height: w)
            trans = trans.rotated(by: angle.cgFloat)
        }
        return (trans, outSize)
    }
    
    
    var exportSession : AVAssetExportSession!
    func exportVideoWith(mixComposition: AVMutableComposition,videoComposition: AVVideoComposition,range: CMTimeRange,progressBlock:((_ progress : Float) -> ())?,complitionBlock:@escaping (_ url : URL?) -> ()){

        let outputPath = DirectoryManager().createFileWith(fileName: .finalVideo)
        exportSession = AVAssetExportSession.init(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exportSession?.outputFileType = AVFileType.mp4
        exportSession?.outputURL = URL.init(fileURLWithPath: outputPath)
        exportSession?.videoComposition = videoComposition
        exportSession?.timeRange = range
//      exportSession.cancelExport()
        var exportProgress: Float = 0
        let queue = DispatchQueue(label: "Export Progress Queue")
        var exportProgressBarTimer = Timer() // initialize timer
        queue.async {[weak self] in
            exportProgressBarTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
                exportProgress = self?.exportSession?.progress ?? 0.0
                progressBlock?(exportProgress)
            })
        }
        exportSession?.exportAsynchronously(completionHandler: {[weak self] in
            switch(self?.exportSession?.status){
            case .failed?:
                exportProgressBarTimer.invalidate()
                print("Failed \(String(describing: self!.exportSession?.error))")
                self?.exportSession?.cancelExport()
                complitionBlock(nil)
                break
            case .completed?:
                exportProgressBarTimer.invalidate()
                
                self?.exportSession = nil
                print("Completed \(String(describing: outputPath))")
                let videoURL = URL(fileURLWithPath: outputPath)
                complitionBlock(videoURL)
                break
            case .exporting?:
                print("exporting")
                break
            case .cancelled?:
                exportProgressBarTimer.invalidate()
                complitionBlock(nil)
                break
            case .waiting?:
                break
            default:
                break
            }
        })
    }
    
    func cancelExport(){
        if exportSession != nil{
            exportSession.cancelExport()
            exportSession = nil
        }
    }

    
    static func videoCompositionWith(videoUrl: URL,playerFrame: CGRect,willShowWaterMark: Bool,startTime: CGFloat,duration: CGFloat,rotationAngle: CGFloat,
                                     complitionBlock:@escaping (_ url : URL?) -> ())  {
        
        let visibleRect = playerFrame
        let videoAsset = AVURLAsset(url: videoUrl, options: nil)
        // Get video track
        guard let videoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).first else {
            return
        }
        
        let finalSeconds = duration
        let finalDurationWithSpeed = CMTime(seconds: Double(finalSeconds), preferredTimescale: videoAsset.duration.timescale)
        
        // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        let mixComposition = AVMutableComposition()
        // Init video & audio composition track
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        let currentTime:CMTime = CMTime.zero;
        let previewTime:CMTime =  videoAsset.duration
        do {
            // ***** 1
            // Add video track to video composition at specific time
            try videoCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: currentTime, duration: previewTime), of: videoTrack, at: currentTime)
            
        } catch {
            print("Load track error")
        }
        
        let naturalSize = videoTrack.naturalSize
        let videoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        videoComposition.renderScale  = 1.0
        videoComposition.renderSize = naturalSize
        let frontLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
        var customTrans = videoAsset.tracks(withMediaType: .video).first?.preferredTransform
        //for Time-lapse videos
        if (customTrans?.b == 1.0 && (customTrans?.c)! == -1.0 &&  (customTrans?.tx)! == 0.0 &&   (customTrans?.ty)! == 0.0 )
        {
            customTrans =  customTrans?.translatedBy(x: 0.0, y: -naturalSize.width);
            frontLayerInstruction.setTransform(customTrans!.rotated(by: rotationAngle), at: CMTime.zero)
        }else{
            frontLayerInstruction.setTransform(videoTrack.preferredTransform.rotated(by: rotationAngle), at: CMTime.zero)
        }
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: finalDurationWithSpeed)
        mainInstruction.layerInstructions = [frontLayerInstruction]
        videoComposition.instructions = [mainInstruction]
        let videoLayer = CALayer.init()
        videoLayer.frame = CGRect(x: 0, y: 0, width: naturalSize.width, height: naturalSize.height)
        if willShowWaterMark{
            let textLayer  = CenterTextLayer()
            let ratio = naturalSize.height / visibleRect.height
            var textFrame : CGRect = .zero
            let size = 18.0 * ratio
            textLayer.font = UIFont(name: FONT_BOLD, size: size)
            if let font =  UIFont(name: FONT_BOLD, size: size){
                let size = NSAttributedString(string: "Drunk Cam", attributes:
                    [NSAttributedString.Key.font: font]).size()
                textFrame = CGRect(x: naturalSize.width - (size.width * 1.05),y: naturalSize.height - (size.height * 1.05), width: size.width, height:size.height)
            }
            textLayer.frame = textFrame
            textLayer.string = "Drunk Cam"
            textLayer.foregroundColor = VideoEdColorObject.themeColor1.cgColor
            textLayer.fontSize = size
            textLayer.alignmentMode = .center
            textLayer.shouldRasterize = true
            textLayer.rasterizationScale = UIScreen.main.scale
            videoLayer.addSublayer(textLayer)
        }
        mixComposition.scaleTimeRange(CMTimeRange(start: .zero, end: videoAsset.duration), toDuration: finalDurationWithSpeed)
        let outputPath = DirectoryManager().createFileWith(fileName: .finalVideo)
        var exportSession = AVAssetExportSession.init(asset: mixComposition, presetName: AVAssetExportPreset1920x1080)
        exportSession?.outputFileType = AVFileType.mov
        exportSession?.outputURL = URL.init(fileURLWithPath: outputPath)
        exportSession?.videoComposition = videoComposition
        var exportProgress: Float = 0
        let queue = DispatchQueue(label: "Export Progress Queue")
        queue.async(execute: {() -> Void in
            while exportSession != nil {
                exportProgress = (exportSession?.progress)!
                print("current progress == \(exportProgress)")
            }
        })
        
        exportSession?.exportAsynchronously(completionHandler: {
            switch(exportSession?.status){
            case .failed?:
                print("Failed \(String(describing: exportSession?.error))")
                exportSession?.cancelExport()
                complitionBlock(nil)
                break
            case .completed?:
                exportSession = nil
                print("Completed \(String(describing: outputPath))")
                let videoURL = URL(fileURLWithPath: outputPath)
                complitionBlock(videoURL)
                break
            case .exporting?:
                break
            case .cancelled?:
                complitionBlock(nil)
                break
            case .waiting?:
                break
            default:
                break
            }
        })
    }
    
    
    static func getVideoSize(asset: AVAsset) -> CGSize {
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        var videoSize = assetTrack.naturalSize
        videoSize = videoSize.applying(assetTrack.preferredTransform)
        videoSize = CGSize(width: abs(videoSize.width), height: abs(videoSize.height))
        return videoSize
    }
    
    
    static func getCompositionsFrom(asset: AVAsset, containerBgColor: UIColor, containerBounds: CGRect, startTime: CGFloat? = nil, endTime: CGFloat? = nil, trimAngle: videoTrimAngleEnum = .kZero,isNewAudio: Bool, audioAsset: AVAsset?) -> (AVMutableComposition, AVMutableVideoComposition)? {
        // Mutable Composition
        let mixComposition = AVMutableComposition()
        
        let videoAsset = asset
        
        ////////////////////////////////////////////////////////////////////
        ///////////// CODE FOR ADDING AUDIO AND VIDEO TRACKS ///////////////
        ////////////////////////////////////////////////////////////////////
        
        // For trimming
        var timeRange = CMTimeRange(start: CMTime.zero, duration: videoAsset.duration)
        if startTime != nil && endTime != nil {
            let sTime = CMTime(seconds: Double(startTime!), preferredTimescale: videoAsset.duration.timescale)
            let eTime = CMTime(seconds: Double(endTime!), preferredTimescale: videoAsset.duration.timescale)
            timeRange = CMTimeRange(start: sTime, end: eTime)
        }
        // Video and Audio tracks
        guard let videoTrack = videoAsset.tracks(withMediaType: .video).first else { return nil }
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let isAudio = videoAsset.tracks(withMediaType: .audio).count > 0
        do {
            
            if audioAsset == nil {
                try videoCompositionTrack?.insertTimeRange(timeRange, of: videoTrack, at: .zero)
                var audioTrack: AVAssetTrack!
                if isNewAudio && audioAsset != nil{
                    let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
                    audioTrack = audioAsset?.tracks(withMediaType: .audio).first
                    let audioTR = CMTimeRange(start: .zero, duration: timeRange.duration)
                    try audioCompositionTrack?.insertTimeRange(audioTR, of: audioTrack, at: .zero)
                }else if isAudio{
                    let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
                    audioTrack = videoAsset.tracks(withMediaType: .audio).first
                    try audioCompositionTrack?.insertTimeRange(timeRange, of: audioTrack, at: .zero)
                }
            }
                else {
                    let musicStartTime = CMTime.zero
                    var musicInsertTime = CMTime.zero
                    var musicEndTime = CMTime.zero
                    
                    let finalAudioAsset = audioAsset
                    let finalSoundTrack = finalAudioAsset?.tracks(withMediaType: AVMediaType.audio).first!
                    musicEndTime = finalAudioAsset!.duration
                    
                    try videoCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime,duration: videoAsset.duration),
                                                               of: videoTrack,
                                                               at: musicInsertTime)
                    
                    
                    
                    //if video duration is shorter than complete music duration
                    if(mixComposition.duration < finalAudioAsset!.duration){
                        if let audioTrack = finalSoundTrack {
                            try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime,duration: mixComposition.duration),
                                                                       of: audioTrack,
                                                                       at: musicInsertTime)
                        }
                    }else{
                        if let audioTrack = finalSoundTrack {
                            try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime, duration: musicEndTime),
                                                                       of: audioTrack,
                                                                       at: musicInsertTime)
                            
                            musicInsertTime = CMTimeAdd(musicInsertTime, musicEndTime)
                        }
                        if let finalAudioDuration = finalAudioAsset?.duration {
                            //                    finalAudioDuration = CMTime.zero
                            if (mixComposition.duration > finalAudioDuration) // && finalAudioDuration != CMTime.zero)
                            {
                                let remainder =  Double(CMTimeGetSeconds(mixComposition.duration).truncatingRemainder(dividingBy: Double(CMTimeGetSeconds(finalAudioDuration))))
                                let quotient = Int(CMTimeGetSeconds(mixComposition.duration)/CMTimeGetSeconds(finalAudioDuration))
                                print("Remainder",remainder,quotient)
                                let lock = NSLock()
                                lock.lock()
                                if isMusicRepetitionStatus {
                                    // Edit by harpreet - play when music repeats
                                    for val in 0..<quotient{
                                        if(val != (quotient-1)){
                                            if let audioTrack = finalSoundTrack {
                                                try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime, duration: musicEndTime),
                                                                                           of: audioTrack,
                                                                                           at: musicInsertTime)
                                                musicInsertTime = CMTimeAdd(musicInsertTime, musicEndTime)
                                            }
                                        }
                                    }
                                    lock.unlock()
                                    if(remainder > 0){
                                        if let audioTrack = finalSoundTrack {
                                            try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: musicStartTime, duration: CMTimeMakeWithSeconds(remainder, preferredTimescale: finalAudioDuration.timescale)),  of: audioTrack,  at: musicInsertTime)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch { return nil }
        
        ////////////////////////////////////////////////////////////////////
        /////////////End CODE FOR ADDING AUDIO AND VIDEO TRACKS/////////////
        ////////////////////////////////////////////////////////////////////
        
        // Video Composition Instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        let compositonColor = containerBgColor
        let (r, g, b, a) = compositonColor.getComponents()
        let color = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [r, g, b, a])
        mainInstruction.backgroundColor = color
        mainInstruction.timeRange = CMTimeRangeMake(start: .zero, duration: videoAsset.duration)
        // Video Composition Layer Instruction
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
        let trackTransform = videoTrack.preferredTransform
        let assetInfo = VideoEdCompositionManager.shared.orientationFromTransform(transform: trackTransform)
        // Size calulations
        let defaultSize = CGSize(width: 1920, height: 1080)
        var outputSize = CGSize.zero
        var videoSize = videoTrack.naturalSize
        if assetInfo.isPortrait {
            videoSize.width =  videoTrack.naturalSize.height
            videoSize.height = videoTrack.naturalSize.width
        }
        if videoSize.height > outputSize.height {
            outputSize.height = videoSize.height
        }
        if videoSize.width > outputSize.width {
            outputSize.width = videoSize.width
        }
        if outputSize.width == 0 || outputSize.height == 0 {
            outputSize = defaultSize
        }
        let trans = VideoEdCompositionManager.shared.VideoCorrectTransform(asset: videoAsset, standardSize: outputSize)
        layerInstruction.setTransform(trans, at: .zero)
        mainInstruction.layerInstructions = [layerInstruction]
        // video composition
        mixComposition.naturalSize = outputSize
        let videoComposition = AVMutableVideoComposition()
        print("RENDER SIZE>>", outputSize)
        videoComposition.renderSize = outputSize
        videoComposition.instructions = [mainInstruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        // return compositions
        return(mixComposition, videoComposition)
    }
    
    
    static func getCompositionsWithStickersAndBackground(asset: AVAsset, containerBgColor: UIColor, videoBounds: CGRect,mainContainerBounds: CGRect, trimAngle: videoTrimAngleEnum = .kZero,stickersData: [StickerData]) -> (AVMutableComposition, AVMutableVideoComposition)? {
        // Mutable Composition
        let mixComposition = AVMutableComposition()
        let videoAsset = asset
        
        ////////////////////////////////////////////////////////////////////
        /////////////CODE FOR ADDING AUDIO AND VIDEO TRACKS/////////////
        ////////////////////////////////////////////////////////////////////
        
        let timeRange = CMTimeRange(start: .zero, duration: videoAsset.duration)
        // Video and Audio tracks
        guard let videoTrack = videoAsset.tracks(withMediaType: .video).first else { return nil }
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let isAudio = videoAsset.tracks(withMediaType: .audio).count > 0
        do {
            try videoCompositionTrack?.insertTimeRange(timeRange, of: videoTrack, at: .zero)
            var audioTrack: AVAssetTrack!
            if isAudio{
                let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
                audioTrack = videoAsset.tracks(withMediaType: .audio).first
                try audioCompositionTrack?.insertTimeRange(timeRange, of: audioTrack, at: .zero)
            }
        } catch { return nil }
        
        ////////////////////////////////////////////////////////////////////
        /////////////End CODE FOR ADDING AUDIO AND VIDEO TRACKS/////////////
        ////////////////////////////////////////////////////////////////////
        
        // Video Composition Instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        let compositonColor = containerBgColor
        let (r, g, b, a) = compositonColor.getComponents()
        let color = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [r, g, b, a])
        mainInstruction.backgroundColor = color
        mainInstruction.timeRange = CMTimeRangeMake(start: .zero, duration: videoAsset.duration)
        // Video Composition Layer Instruction
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
        let trackTransform = videoTrack.preferredTransform
        let assetInfo = VideoEdCompositionManager.shared.orientationFromTransform(transform: trackTransform)
        // Size calulations
        var videoSize = videoTrack.naturalSize
        if assetInfo.isPortrait {
            videoSize.width =  videoTrack.naturalSize.height
            videoSize.height = videoTrack.naturalSize.width
        }
        
        let ratio = videoSize.width / videoBounds.width
        videoSize.width = videoBounds.width * ratio
        videoSize.height = videoBounds.height * ratio
        // MAKE SIZE DIVISIBLE BY 4
        var newOutputWidth = Int(videoSize.width)
        var newOutputHeight = Int(videoSize.height)
        while newOutputWidth % 4 > 0 {
            newOutputWidth += 1
        }
        while newOutputHeight % 4 > 0 {
            newOutputHeight += 1
        }
        
        let outputSize = CGSize(width: newOutputWidth, height: newOutputHeight)
        
        let trans = VideoEdCompositionManager.shared.VideoCorrectTransform(asset: videoAsset, standardSize: outputSize)
        layerInstruction.setTransform(trans, at: .zero)
        mainInstruction.layerInstructions = [layerInstruction]
        mixComposition.naturalSize = outputSize
        
        //Add Stickers Layer
        let parentUILayer = CALayer()
        parentUILayer.isGeometryFlipped = true
        parentUILayer.frame = CGRect(origin: .zero, size: outputSize)
        let videoLayer = CALayer.init()
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.frame = parentUILayer.frame
        parentUILayer.backgroundColor = UIColor.clear.cgColor
        parentUILayer.addSublayer(videoLayer)
//        let videoContainerRect = AVMakeRect(aspectRatio: videoBounds.size, insideRect: mainContainerBounds)
//        let widthRatio: CGFloat = outputSize.width / videoContainerRect.width
       
        /* for model in stickersData{
            let newSticker = VideoEdHelper.sharedInstance.getTextLayer(from: model, scaleFactor: widthRatio, layerOpacity: model.opacity)
            parentUILayer.addSublayer(newSticker)
        }
        */
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool.init(postProcessingAsVideoLayer: videoLayer, in: parentUILayer)
        videoComposition.renderSize = outputSize
        videoComposition.instructions = [mainInstruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        // return compositions
        return(mixComposition, videoComposition)
    }
    
    
    
    //// ***********//// ***********//// ***********//// ***********//// ***********//// ***********
    //// *********** NEW GIF CODE //// ***********//// ***********//// ***********//// ***********
    //// ***********//// ***********//// ***********//// ***********//// ***********//// ***********
    
/*   static func applyVideoEffectsToComposition(composition: AVMutableVideoComposition?, size: CGSize) {
        var size = size
        // - set up the parent layer
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        parentLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        parentLayer.addSublayer(videoLayer)
        size.width = 100
        size.height = 100
        // - set up the overlay
        let overlayLayer = CALayer()
        overlayLayer.frame = CGRect(x: 0, y: 100, width: size.width, height: size.height)
        let fileUrl: URL? = Bundle.main.url(forResource: "flower", withExtension: "gif")
        startGifAnimation(with: fileUrl, in: overlayLayer)
        parentLayer.addSublayer(overlayLayer)
        // - apply magic
        composition?.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)

    }
   static func startGifAnimation(with url: URL?, in layer: CALayer?) {
        let animation: CAKeyframeAnimation? = animationForGif(with: url)
        if let animation = animation {
            layer?.add(animation, forKey: "contents")
        }
    }
   static func animationForGif(with url: URL?) -> CAKeyframeAnimation? {
        let animation = CAKeyframeAnimation(keyPath: "contents")
        let frames : NSMutableArray = []
        let delayTimes : NSMutableArray = []
        var totalTime: CGFloat = 0.0
        var gifWidth: CGFloat
        var gifHeight: CGFloat
        let gifSource = CGImageSourceCreateWithURL(url! as CFURL, nil)
        let frameCount = CGImageSourceGetCount(gifSource!)
        for i in 0..<frameCount {
            let frame = CGImageSourceCreateImageAtIndex(gifSource!, i, nil)
            if let frame = frame {
                frames.add(frame)
            }
            
            let dict = CGImageSourceCopyPropertiesAtIndex(gifSource!, i, nil) as? [AnyHashable : Any]
            if let value = dict?[kCGImagePropertyGIFDictionary as String] {
                print("kCGImagePropertyGIFDictionary \(value)")
            }
            gifWidth = CGFloat((dict?[kCGImagePropertyPixelWidth as String] as? NSNumber)?.floatValue ?? 0.0)
            gifHeight = CGFloat((dict![kCGImagePropertyPixelHeight as String] as? NSNumber)?.floatValue ?? 0.0)
            var gifDict = dict![kCGImagePropertyGIFDictionary as String] as? [AnyHashable : Any]
            if let value = gifDict?[kCGImagePropertyGIFDelayTime as String] {
                delayTimes.add(value)
            }
            totalTime = totalTime + CGFloat((gifDict?[kCGImagePropertyGIFDelayTime as String] as? NSNumber)!.floatValue)
        }
            var times = [AnyHashable](repeating: 0, count: 3)
            var currentTime: CGFloat = 0
            let count: Int = delayTimes.count
            for i in 0..<count {
                times.append(NSNumber(value: Float((currentTime / totalTime))))
                currentTime += CGFloat((delayTimes[i] as? NSNumber)!.floatValue)
            }
            var images = [AnyHashable](repeating: 0, count: 3)
            for i in 0..<count {
                images.append(frames[i] as! AnyHashable)
            }
            animation.keyTimes = times as? [NSNumber]
            animation.values = images
            animation.timingFunction = CAMediaTimingFunction(name: .linear)
            animation.duration = CFTimeInterval(totalTime)
//            animation.repeatCount = HUGE_VALF
            animation.beginTime = AVCoreAnimationBeginTimeAtZero
            animation.isRemovedOnCompletion = false
            return animation
        }*/
}
enum TransformType : Int {
    case RotateDown = 0
    case centre
    case Top
    case Bottom
    case left
    case right
    case Fade
}
