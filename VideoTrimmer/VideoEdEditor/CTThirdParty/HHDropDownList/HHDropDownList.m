//
//  HHDropDownList.m
//  HHDropDownList
//
//  Created by Herbert Hu on 16/8/4.
//  Copyright © 2016年 Herbert Hu. All rights reserved.
//

#import "HHDropDownList.h"
#import <QuartzCore/QuartzCore.h>
#import "ListCell.h"


#define FONT_NAME @"GeorgeRoundedRegular"
#define FONT_SIZE 22
@implementation HHDropDownList {
    
    NSMutableArray *_indicatorsArray;
    CAShapeLayer *_indicatorLayer;
    CATextLayer *_textLayer;
    UITableView *_tableView;
    NSArray *_data;
    UIVisualEffect *_blurEffect;
    UIVisualEffectView *_visualEffectView;
}

#pragma mark - Init

- (instancetype)initWithFrame:(CGRect)frame {

    self = [super initWithFrame:frame];
    
    if (self) {
        
        /**< 默认属性 */
        _indicatorsArray = [[NSMutableArray alloc] init];
        _isShow = NO;
        _isExclusive = NO;
        _haveBorderLine = YES;
        _highlightColor = [UIColor colorWithRed:252/255.0 green:141/255.0 blue:137/255.0 alpha:1.0];
        
        [self setUpUI];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ADropDownListHasDroppedDown:) name:@"thisDropDownListHasDroppedDown"
                                                   object:nil];
    }
    return self;
}

- (void)setUpUI {
    
//    [[self layer] setBorderWidth:0.7];
    [[self layer] setBorderColor:[[UIColor colorWithRed:209/255.0 green:209/255.0 blue:209/255.0 alpha:1.0] CGColor]];
    
    UIBlurEffect *localblurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:localblurEffect];
    visualEffectView.frame = self.bounds;
//    [self addSubview:visualEffectView];
    
    _blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    _visualEffectView = [[UIVisualEffectView alloc] initWithEffect:_blurEffect];

    _indicatorLayer = [self createIndicatorWithColor:_highlightColor
                                         andPosition:CGPointMake(self.frame.size.width*6.0/7.0, self.frame.size.height/2.0)];
    
    [self.layer addSublayer:_indicatorLayer];
    
    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    [self addGestureRecognizer:tapGesture];
    
    CGPoint position = CGPointMake(self.frame.size.width/2.0+8, self.frame.size.height/2.0);
    _textLayer = [self createTextLayerWithText:@"•P720" color:[UIColor whiteColor] withPosition:position];
    [self.layer addSublayer:_textLayer];
    
    _visualEffectView.layer.cornerRadius = 10;

    _tableView = [self createTableViewAtPosition:CGPointMake(0, self.frame.origin.y+self.frame.size.height)];
//    [_tableView setBackgroundColor:[UIColor blackColor]];
//    [_tableView.layer setBorderWidth:0.7];
//    [_tableView.layer setBorderColor:[UIColor colorWithRed:209/255.0 green:209/255.0 blue:209/255.0 alpha:1.0].CGColor];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
    _tableView.backgroundColor = [UIColor clearColor];
    [_tableView registerNib:[UINib nibWithNibName:@"ListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"cell"];
    _tableView.layer.cornerRadius = 10;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    _indicatorLayer.fillColor = _highlightColor.CGColor;
    
    if (!_haveBorderLine) {
        
        [self.layer setBorderWidth:0.0];
        [_tableView.layer setBorderWidth:0.0];
    }
}

#pragma mark - Layer Drawing
- (CAShapeLayer *)createIndicatorWithColor:(UIColor *)color andPosition:(CGPoint)point {
    
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(16, 0)];
    [path addLineToPoint:CGPointMake(8, 8)];
    [path closePath];
    
    layer.path = path.CGPath;
    layer.lineWidth = 1.0;
    layer.fillColor = color.CGColor;
    
    CGPathRef bound = CGPathCreateCopyByStrokingPath(layer.path, nil, layer.lineWidth, kCGLineCapButt, kCGLineJoinMiter, layer.miterLimit);
    layer.bounds = CGPathGetBoundingBox(bound);
    
    layer.position = point;
    
    return layer;
}

- (UITableView *)createTableViewAtPosition:(CGPoint)point {
    
    UITableView *tableView = [[UITableView alloc] init];
    
    [tableView setFrame:CGRectMake(point.x, point.y, self.frame.size.width, 0)];
    [tableView setRowHeight:36.0];
    
    return tableView;
}

- (CATextLayer *)createTextLayerWithText:(NSString *)text color:(UIColor *)color withPosition:(CGPoint)point {


    CGFloat height = [self heightForString:text font:[UIFont fontWithName:FONT_NAME size:FONT_SIZE] maxWidth:self.frame.size.width];
    CATextLayer *layer = [[CATextLayer alloc] init];
    
    CGFloat sizeWidth = self.frame.size.width;// (size.width < (self.frame.size.width - 10)) ? size.width : self.frame.size.width - 10;
    
    layer.bounds = CGRectMake(12, 0, sizeWidth-24, height);
    layer.bounds = CGRectMake(20, 0, sizeWidth-40, height);
    layer.bounds = CGRectMake(0, 0, sizeWidth, height);

    layer.string = text;
    
    layer.fontSize = 22;
    
    layer.font = (__bridge CFTypeRef _Nullable)(FONT_NAME);

    layer.alignmentMode = kCAAlignmentCenter;
    
    layer.foregroundColor = color.CGColor;
    
//    layer.backgroundColor = [UIColor blackColor].CGColor;
    
    layer.contentsScale = [[UIScreen mainScreen] scale];
    
    layer.position = point;
    
    return layer;
}
- (CGFloat)heightForAttributedString:(NSAttributedString *)text maxWidth:(CGFloat)maxWidth {
    if ([text isKindOfClass:[NSString class]] && !text.length) {
        // no text means no height
        return 0;
    }
    
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    CGSize size = [text boundingRectWithSize:CGSizeMake(maxWidth, CGFLOAT_MAX) options:options context:nil].size;
    
    CGFloat height = ceilf(size.height) + 1; // add 1 point as padding
    
    return height;
}

- (CGFloat)heightForString:(NSString *)text font:(UIFont *)font maxWidth:(CGFloat)maxWidth {
    if (![text isKindOfClass:[NSString class]] || !text.length) {
        // no text means no height
        return 0;
    }
    
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    NSDictionary *attributes = @{ NSFontAttributeName : font };
    CGSize size = [text boundingRectWithSize:CGSizeMake(maxWidth, CGFLOAT_MAX) options:options attributes:attributes context:nil].size;
    CGFloat height = ceilf(size.height) + 1; // add 1 point as padding
    
    return height;
}

#pragma mark - Animation
- (void)animateIndicator:(CAShapeLayer *)ind isforward:(BOOL)isForward withCompletion:(void(^)(void))completion {
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.25];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithControlPoints:0.4 :0.0 :0.2 :1.0]];
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation"];
    animation.values = isForward ? @[@0, @(M_PI)] : @[@(M_PI), @0];
    
    if (!animation.removedOnCompletion) {
        
        [ind addAnimation:animation forKey:animation.keyPath];
    }
    else {
        
        [ind addAnimation:animation andValue:animation.values.lastObject forKeyPath:animation.keyPath];
    }
    
    [CATransaction commit];
    
    completion();
}

- (void)animateTableView:(UITableView *)tableView show:(BOOL)isShow completion:(void(^)(void))completion {
    
    if (isShow) {

        CGFloat tableView_Height = tableView.rowHeight * [tableView numberOfRowsInSection:0];
        [tableView setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y+self.frame.size.height, self.frame.size.width, 0)];
        _visualEffectView.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y+self.frame.size.height, self.frame.size.width, tableView_Height);
        [self.superview addSubview:_visualEffectView];
        [self.superview addSubview:tableView];
        
        _visualEffectView.layer.cornerRadius = 10;

        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            [tableView setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y+self.frame.size.height, self.frame.size.width, tableView_Height)];
            
        } completion:^(BOOL finished) {
            
        }];
    }
    else {
        
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            [tableView setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y+self.frame.size.height, self.frame.size.width, 0)];
            
        } completion:^(BOOL finished) {
            
            [_visualEffectView removeFromSuperview];
            [tableView removeFromSuperview];
        }];
        
    }
    
    completion();
}

- (void)animateTextLayer:(CATextLayer *)textLayer isShow:(BOOL)isShow completion:(void(^)(void))completion {
    
    CGFloat height = [self heightForString:textLayer.string font:[UIFont fontWithName:FONT_NAME size:FONT_SIZE] maxWidth:self.frame.size.width];
    CGFloat sizeWidth = self.frame.size.width ;
    textLayer.bounds = CGRectMake(20, 0, sizeWidth-40, height);
    textLayer.bounds = CGRectMake(0, 0, sizeWidth-30, height);

//    [textLayer setBounds:CGRectMake(0, 0, sizeWidth, size.height)];
    
    completion();
}

#pragma mark - Action
- (void)tapped:(UIGestureRecognizer *)gr {
    
    if (_isShow == NO) {
        
        [self animateIndicator:_indicatorLayer isforward:YES withCompletion:^{
            
           self.isShow = YES;
        }];
        
        [self animateTableView:_tableView show:YES completion:^{
            
            [self PostNoti_thisDropDownListHasDroppedDown];
        }];
    }
    else {
        
        [self animateIndicator:_indicatorLayer isforward:NO withCompletion:^{
            
            self.isShow = NO;
        }];
        
        [self animateTableView:_tableView show:NO completion:^{
            
        }];
    }
}

- (void)reloadListData {
        
    if ([self.delegate respondsToSelector:@selector(listDataForDropDownList:)]) {
        
        _data = [self.dataSource listDataForDropDownList:self];
    }
    
    [_textLayer setString:[_data objectAtIndex:0]];
    
    [self animateTextLayer:_textLayer isShow:YES completion:^{
        
    }];
    
    [_tableView reloadData];
}

- (void)dropDown {
    
    [self animateIndicator:_indicatorLayer isforward:YES withCompletion:^{
        
        self.isShow = YES;
    }];
    
    [self animateTableView:_tableView show:YES completion:^{
        
        [self PostNoti_thisDropDownListHasDroppedDown];
    }];
}

- (void)pullBack {
    
    [self animateIndicator:_indicatorLayer isforward:NO withCompletion:^{
        
        self.isShow = NO;
    }];
    
    [self animateTableView:_tableView show:NO completion:^{
        
    }];
}

#pragma mark Noti-Action
- (void)ADropDownListHasDroppedDown:(NSNotification *)noti {
    
    id theDropDownList = [noti.userInfo objectForKey:@"dropDownList"];
    
    if (![theDropDownList isEqual:self]) {
        
        [self pullBack];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_data count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"cell";

    ListCell *listCell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    listCell.selectedBackgroundView = [[UIView alloc] initWithFrame:listCell.frame];
    listCell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    
    listCell.centerLabel.text = _data[indexPath.row];
    listCell.centerLabel.textColor = [[UIColor whiteColor]colorWithAlphaComponent:0.6];
    listCell.centerLabel.font =  [UIFont fontWithName:FONT_NAME size: 15];
    
    return listCell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *itemName = [_data objectAtIndex:indexPath.row];
    
    [_textLayer setString:itemName];
    
    [self animateTableView:_tableView show:NO completion:^{
        
        self.isShow = NO;
    }];
    
    [self animateIndicator:_indicatorLayer isforward:NO withCompletion:^{
    
    }];
    
    [self animateTextLayer:_textLayer isShow:YES completion:^{
    
    }];
    
    if ([self.delegate respondsToSelector:@selector(dropDownList:didSelectItemName:atIndex:)]) {
        
        [self.delegate dropDownList:self didSelectItemName:itemName atIndex:indexPath.row];
    }
    [_textLayer displayIfNeeded];
}

#pragma mark - Notification
- (void)PostNoti_thisDropDownListHasDroppedDown {
    
    if (_isExclusive) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"thisDropDownListHasDroppedDown"
                                                            object:nil
                                                          userInfo:@{@"dropDownList":self}];
    }
}


@end

#pragma mark - CALayer Category
@implementation CALayer (HHAddAnimationAndValue)

- (void)addAnimation:(CAAnimation *)animation andValue:(NSValue *)value forKeyPath:(NSString *)keyPath {
    
    [self addAnimation:animation forKey:keyPath];
    [self setValue:value forKeyPath:keyPath];
}

@end
