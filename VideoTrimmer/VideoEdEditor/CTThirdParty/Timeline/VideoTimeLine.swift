
//  VideoTimeLine.swift
//  VideoEditorRed

//  Created by strongapps on 06/09/18.
//  Copyright © 2018 strongapps. All rights reserved.


import UIKit


@objc  protocol VideoTimeLine_Delegate : class {
    
    func timeLineBeginDragging(scrollView:UIScrollView,time:CGFloat);
    func timeLineDidScroll(scrollView:UIScrollView,time:CGFloat);
    func timeLineEndScrolling(scrollView:UIScrollView,time:CGFloat);
    func timeLineBarsChange(startTime:CGFloat,duration:CGFloat);
    func timeLineBarBeginChange()
}

@objcMembers class VideoTimeLine: UIView,UIScrollViewDelegate {

    weak public var delegate: VideoTimeLine_Delegate?

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var leftBlankView: UIView!
    @IBOutlet weak var timeLineView: UIView!
    @IBOutlet weak var rightBlankView: UIView!
    @IBOutlet weak var timeLineImagesView: UIView!
    
    @IBOutlet weak var leftBar: UIImageView!
    @IBOutlet weak var rightBar: UIImageView!
    @IBOutlet weak var middleGradientView: UIImageView!
    @IBOutlet weak var middleImageShowView: UIImageView!
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var panGestureLeft: UIPanGestureRecognizer!
    @IBOutlet weak var panGestureMiddle: UIPanGestureRecognizer!
    @IBOutlet weak var panGestureRight: UIPanGestureRecognizer!
    
    @IBOutlet weak var mainViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var timeLineHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var timeLineWidthConstraint: NSLayoutConstraint!   // Minimum Width is  40
    @IBOutlet weak var leftBarLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightBarTrailingConstraint: NSLayoutConstraint!
    
    
    
    var timeLineImagesArr : NSArray? =  nil
    var videoDuration :CGFloat =  0;
    var currentTime :CGFloat = 0;
    var currentTImeStr: NSString? = "";
    var gradientLayer :CAGradientLayer? = nil
    var itemStartTime:CGFloat = 0.0;
    var itemDuration :CGFloat = 0.0;
    var pitch:Int = 60;  // 60 UI Pixels for one Second of Video.
    var gradientLeftColor :UIColor  = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0);
    var gradientRightColor :UIColor  = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0);
    
    var _initialize:Bool = false;
    var scrollMannually = false;
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        debugPrint("awake From nib Called");
        if (!_initialize) {
            _initialize = true;
            setupView()
        }
    }
    
    override func draw(_ rect: CGRect) {

        
    }
    
    func updateHeight(with value: CGFloat){
       mainViewHeightConstraint.constant = value
       self.layoutIfNeeded()
    }
    
    func setupView()  {
        
        gradientLeftColor =  UIColor(red: 195.0/255.0, green: 60.0/255.0, blue: 190.0/255.0, alpha: 1.0);
        gradientRightColor =  UIColor(red: 0.0/255.0, green: 207.0/255.0, blue: 245.0/255.0, alpha: 1.0);
        
        self.backgroundColor = UIColor.clear;
        self.scrollView.backgroundColor =  UIColor.clear;
        self.contentView.backgroundColor =  UIColor.clear;
        self.timeLineView.backgroundColor = UIColor.clear;
        self.leftBlankView.backgroundColor = UIColor.clear;
        self.rightBlankView.backgroundColor = UIColor.clear;
        self.scrollView.delegate = self;
        hideTrimBars();
        applyGradient();
        
        middleGradientView.addObserver(self, forKeyPath: #keyPath(UIView.bounds), options: .new, context: nil);
//      middleGradientView.removeObserver(self, forKeyPath: #keyPath(UIView.bounds));
        
    }

    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        
        if let objectView = object as? UIView,objectView === middleGradientView,keyPath == #keyPath(UIView.bounds) {
            gradientLayer?.frame = objectView.bounds
           // gradientLayer?.frame = middleGradientView.bounds
        }
        CATransaction.commit()

    }
    
    func setUpTimeLine(newImages:NSArray,currentTime:CGFloat,duration:CGFloat)  {
        var finalDuration:CGFloat =  duration;
        if duration == CGFloat.nan {
            finalDuration = 0.0;
        }
        videoDuration =  finalDuration;
        pitch = Int(timeLineImagesView.frame.height)
        let timeLineWidth = floor(videoDuration) * CGFloat(pitch);
        timeLineWidthConstraint.constant =  timeLineWidth;
        let numberofImages = Int(videoDuration);
        timeLineImagesArr = newImages;
        var x:Int = 0;
        let height:Int = Int(timeLineImagesView.frame.height);
        let width :Int = height
        for index in 0..<numberofImages {
            debugPrint("index is :",index);

            var image:UIImage = UIImage();
            if(timeLineImagesArr != nil && index < (timeLineImagesArr?.count)!)
            {
                image = timeLineImagesArr?.object(at: index) as! UIImage;
            }
            
            let imageView = UIImageView.init(image: image)
            imageView.frame = CGRect(x: x, y: 0, width: width, height: height);
            imageView.contentMode = .scaleAspectFill
            x += width;
            timeLineImagesView.addSubview(imageView);
        }
    
    }
    
    
    func updateTimeLine(_ newTime:CGFloat,duration:CGFloat)
    {
        
//        debugPrint("Update Time For TimeLine :",currentTime);
        currentTime = newTime;
        videoDuration = duration;
        
        let ratio:CGFloat = newTime / duration;
        let calX = timeLineWidthConstraint.constant * ratio;
        
        let offset :CGPoint =  CGPoint(x: calX, y: scrollView.contentOffset.y);
        scrollView.setContentOffset(offset, animated: false);
        
        updateTimerLabel(seconds: Int(currentTime))
    }
    
    func updateTimerLabel(seconds:Int)  {
        
        let String =  createTimerString(time: seconds);
        timerLabel.text = String;
    }
    
    func showHandels(_ newImage:UIImage,newStartTime:CGFloat,newItemDuration:CGFloat) {

        itemStartTime = newStartTime;
        itemDuration = newItemDuration;
        
        // **** Rearrange Left Bar ********
        var ratio = itemStartTime / videoDuration;
        var calX = timeLineWidthConstraint.constant * ratio;
        
        if calX > timeLineWidthConstraint.constant - 41  {
            calX = timeLineWidthConstraint.constant - 41;
        }
        
        leftBarLeadingConstraint.constant = calX;
        // **** Rearrange Right Bar *******
        let endTime = itemStartTime + itemDuration;
        ratio = endTime / videoDuration;
        ratio = 1.0 - ratio;
        var xTrailing = timeLineWidthConstraint.constant * ratio;
        if xTrailing > timeLineWidthConstraint.constant - 41  {
            xTrailing = timeLineWidthConstraint.constant - 41;
        }
        rightBarTrailingConstraint.constant = xTrailing;
        
        middleImageShowView.image = newImage;
        showTrimBars();
        
    }
    
    @IBAction func panGestureLeftAct(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: sender.view)
        if ( leftBarLeadingConstraint.constant  + translation.x) >= 0.0  && ( leftBarLeadingConstraint.constant  + translation.x + 20 + 10) < (rightBar.frame.origin.x )   {
            leftBarLeadingConstraint.constant  += translation.x
        }
        sender.setTranslation(CGPoint.zero, in: sender.view)
        if sender.state == .began{
            delegate?.timeLineBarBeginChange()
        }
        if sender.state ==  .ended || sender.state == .cancelled || sender.state == UIGestureRecognizer.State.failed {
            self.relaodTimings();
        }
        
    }
    
//    @IBAction func panGestureLeftAct(_ sender: UIPanGestureRecognizer) {
//        var view = sender.view;
//        //let translation:CGPoint = [sender .translation(in: sender.view)]
//        //let translation = sender.translation(in: self.view)
//
//        let translation = sender.translation(in: sender.view)
//        debugPrint("translation is ",translation);
//
//        if ( leftBarLeadingConstraint.constant  + translation.x) >= 0.0 {
//            leftBarLeadingConstraint.constant  += translation.x
//        }
//        sender.setTranslation(CGPoint.zero, in: sender.view)
//
//        if sender.state == .began{
//            delegate?.timeLineBarBeginChange()
//        }
//
//        if sender.state ==  .ended || sender.state == .cancelled || sender.state == UIGestureRecognizer.State.failed {
//            self.relaodTimings();
//        }
//
//    }
    
    @IBAction func panGestureMiddleAct(_ sender: UIPanGestureRecognizer) {
    
    }
    @IBAction func panGestureRightAct(_ sender: UIPanGestureRecognizer) {
        
        let translation = sender.translation(in: sender.view)
        debugPrint("translation is ",translation);
        
        
        //        ( leftBarLeadingConstraint.constant  + translation.x + 20 + 10) < (rightBar.frame.origin.x )   {
        //            leftBarLeadingConstraint.constant  += translation.x
        //        }
        
        /* let trailNewConstraint = rightBarTrailingConstraint.constant  -= translation.x ;
         trailNewConstraint*/
        
        let leftValue:Float =  Float(leftBarLeadingConstraint.constant + 20 + 10) ;
        let rightValue:Float = Float(rightBar.frame.origin.x - translation.x) ;
        
        debugPrint("leftValue ",leftValue);
        debugPrint("rightValue ",rightValue);
        debugPrint("translation X ",translation.x);
        
        
        
        if ( rightBarTrailingConstraint.constant  - translation.x) >= 0.0
        {
            if(translation.x>0)  {
                rightBarTrailingConstraint.constant  -= translation.x
            }
            else if (leftValue < rightValue)
            {
                rightBarTrailingConstraint.constant  -= translation.x
            }
            
        }
        
        sender.setTranslation(CGPoint.zero, in: sender.view)
        if sender.state == .began{
            delegate?.timeLineBarBeginChange()
        }
        
        if sender.state ==  .ended || sender.state == .cancelled || sender.state == UIGestureRecognizer.State.failed {
            self.relaodTimings();
        }
        
    }
//    @IBAction func panGestureRightAct(_ sender: UIPanGestureRecognizer) {
//
//        let translation = sender.translation(in: sender.view)
//
//        debugPrint("translation is ",translation);
//
//        if ( rightBarTrailingConstraint.constant  - translation.x) >= 0.0 {
//            rightBarTrailingConstraint.constant  -= translation.x
//        }
//
//        sender.setTranslation(CGPoint.zero, in: sender.view)
//
//
//        if sender.state == .began{
//            delegate?.timeLineBarBeginChange()
//        }
//        if sender.state ==  .ended || sender.state == .cancelled || sender.state == UIGestureRecognizer.State.failed {
//            self.relaodTimings();
//        }
//
//    }
    
    func relaodTimings ()  {
        
        var  ratio = leftBarLeadingConstraint.constant / timeLineWidthConstraint.constant;
        itemStartTime = ratio * videoDuration;
        
        ratio = rightBarTrailingConstraint.constant / timeLineWidthConstraint.constant;
        ratio = 1.0 - ratio;
        let endTime = ratio * videoDuration;

        itemDuration = endTime - itemStartTime;
        //delegate?.changeTimeLineBars(startTime: itemStartTime, duration: itemDuration)
        delegate?.timeLineBarsChange(startTime: itemStartTime, duration: itemDuration);

    }
    
    
    func createTimerString(time:Int) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        //return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    func showTrimBars() {
        leftBar.isHidden = false;
        rightBar.isHidden = false;
        middleGradientView.isHidden = false;
        middleImageShowView.isHidden = false;
    }
    
    func hideTrimBars() {
        
        leftBar.isHidden = true;
        rightBar.isHidden = true;
        middleGradientView.isHidden = true;
        middleImageShowView.isHidden = true;
    }
    
    func applyGradient()  {
        
        if(gradientLayer != nil)
        {
            gradientLayer?.removeAllAnimations();
            gradientLayer?.removeFromSuperlayer();
            gradientLayer = nil;
        }
        
        let calFrame =  CGRect(x: 0, y: 0, width: middleGradientView.frame.width , height: middleGradientView.frame.height);
        
        gradientLayer      = CAGradientLayer()
        gradientLayer?.frame     = calFrame
        gradientLayer?.colors    = [gradientLeftColor.cgColor, gradientRightColor.cgColor];
        
        gradientLayer?.startPoint = CGPoint(x:0.0,y: 0.5);
        gradientLayer?.endPoint = CGPoint(x:1.0,y: 0.5);
        middleGradientView.layer.addSublayer(gradientLayer!)
        
        // ******* CALayer
        gradientLayer?.name = "gradientLayer";
        //middleGradientView.layer = "middleGradientView";
        
    }
 
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        scrollMannually = true;
        let offset :CGPoint = scrollView.contentOffset;
        let ratio =  offset.x / timeLineWidthConstraint.constant;
        let newTime = ratio * videoDuration;
        delegate?.timeLineBeginDragging(scrollView: scrollView, time: newTime)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset :CGPoint = scrollView.contentOffset;
        let ratio =  offset.x / timeLineWidthConstraint.constant;
        let newTime = ratio * videoDuration;

        if scrollMannually {

            delegate?.timeLineDidScroll(scrollView: scrollView, time: newTime);
            // updateTimerLabel(seconds: Int(currentTime))
            updateTimerLabel(seconds: Int(newTime))

        }

    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if !decelerate {
            self.scrollViewEndScrolling(scrollView);
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
      
        self.scrollViewEndScrolling(scrollView);
    }
    
    func scrollViewEndScrolling(_ scrollView:UIScrollView) {
        
        let offset :CGPoint = scrollView.contentOffset;
        let ratio =  offset.x / timeLineWidthConstraint.constant;
        let newTime = ratio * videoDuration;
        delegate?.timeLineEndScrolling(scrollView: scrollView, time: newTime)
    }
    
    
    /*
     -(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
     self.manualScrolling=NO;
     }
     
     -(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
     if(velocity.x==0){
     self.manualScrolling=NO;
     }
     }
     */
    
    
}


/*
extension UIImageView {
    
    func gradated(gradientPoints: [GradientPoint]) {
        
        let gradientMaskLayer       = CAGradientLayer()
        gradientMaskLayer.frame     = frame
        gradientMaskLayer.colors    = gradientPoints.map { $0.color.cgColor }
        gradientMaskLayer.locations = gradientPoints.map { $0.location as NSNumber }
        self.layer.insertSublayer(gradientMaskLayer, at: 0)
}
*/
