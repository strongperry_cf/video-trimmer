//
//  VideoRangeSlider.swift
//  Video Editorial
//
//  Created by strongapps on 03/04/19.
//  Copyright © 2019 strongapps. All rights reserved.


import UIKit
import AVKit

protocol VideoRangeSliderDelegate: class {
    func videoRange(_ videoRange: VideoRangeSlider, didChangeLeftPosition leftPosition: CGFloat)
    func videoRange(_ videoRange: VideoRangeSlider, didChangeRightPosition rightPosition: CGFloat)
    func videoRange(_ videoRange: VideoRangeSlider, didGestureStateEndedLeftPosition leftPosition: CGFloat, rightPosition: CGFloat)
    func videoRange(didBeginMoving videoRange: VideoRangeSlider)
    func videoRange(_ videoRange: VideoRangeSlider,didMovePlayerSlider value: CGFloat)
    func videoRange(_ videoRange: VideoRangeSlider, leftPosition: CGFloat,rightPosition: CGFloat)
}


class VideoRangeSlider: UIView , UIGestureRecognizerDelegate{
    
    weak var delegate: VideoRangeSliderDelegate?
    
    var leftValueGetter: CGFloat{
        get{
//            print("left Position",leftPosition)
//            print("durationSeconds",durationSeconds)
//            print("frame_width",frame_width)
            let duration = leftPosition * CGFloat(durationSeconds) / frame_width
            
//            print("duration",duration)
            return duration
        }
    }
    
    var rightValueGetter: CGFloat{
        get{
            
//            print("right Position",rightPosition)
//            print("durationSeconds",durationSeconds)
//            print("frame_width",frame_width)
            let duration = rightPosition * CGFloat(durationSeconds) / frame_width
//            print("duration",duration)
            return duration
        }
    }
    
    var lineViewTimings: CGFloat{
        get{
            return lineView.frame.midX * CGFloat(durationSeconds) / frame_width
        }
    }
    

    
    var leftPosition: CGFloat = 0.0
    var rightPosition: CGFloat = 0.0
    var bubleText: UILabel!
    var topBorder: UIView!
    var bottomBorder: UIView!
    var maxGap: CGFloat = 0
    var minGap: CGFloat = 60
    var minSeconds : CGFloat = 3.0
    var maxSeconds : CGFloat = 300.0
    
    var lineView: UIView!
    var lineParent: UIView!
    
    
    
    private var imageGenerator: AVAssetImageGenerator!
    private var bgView: UIView!
    private var centerView: UIView!
    private var avAsset: AVAsset?
    private var leftThumb: SASliderLeft!
    private var rightThumb: SASliderRight!
    private var frame_width: CGFloat = 0.0
    private var durationSeconds: Double = 0
    private var popoverBubble: SAResizibleBubble!


    let SLIDER_BORDERS_SIZE : CGFloat = 1.0
    let BG_VIEW_BORDERS_SIZE : CGFloat = 0.5
    
    //Following func updates the timeline on the basis of given duration of seconds 
    func updateWidth(with duration:CGFloat){
       let durationWidth =   frame_width * duration / CGFloat(durationSeconds)
        rightPosition = leftPosition + durationWidth
        if rightPosition > frame_width{
            let diff = rightPosition - frame_width
            leftPosition -= diff
            rightPosition -= diff
        }
        if leftPosition < 0{
            leftPosition = 0.0
        }
        if rightPosition > frame_width{
            rightPosition = frame_width
        }
        delegate?.videoRange(self, didGestureStateEndedLeftPosition: leftValueGetter, rightPosition: rightValueGetter)
        setNeedsLayout()
    }
    
    func updateNewValues(left : CGFloat , right : CGFloat){
        
        let durationleft =   frame_width * left / CGFloat(durationSeconds)
        leftPosition = durationleft
        let durationright =   frame_width * right / CGFloat(durationSeconds)
        rightPosition = durationright

        debugPrint(durationleft,"<<<<<<<>>>>>>>>>",durationright)
        setNeedsLayout()

    }
    func reset(){
        bgView = nil
        imageGenerator = nil
    }

    init(frame: CGRect, avAsset: AVAsset) {

//        let themeColor =  UIColor(red: 255.0/255.0, green: 180.0/255.0, blue: 0.0, alpha: 1.0)
        let themeColor =  UIColor(named: "tintThemeColor")

        super.init(frame: frame)
        frame_width = frame.size.width
//        let thumbWidth : CGFloat = 15//frame.size.width * 0.05
        let thumbWidth : CGFloat = 15.0
        durationSeconds = avAsset.duration.seconds
        bgView = UIView(frame: CGRect(x: thumbWidth - BG_VIEW_BORDERS_SIZE, y: 0, width: frame.size.width - thumbWidth * 2 + BG_VIEW_BORDERS_SIZE * 2, height: frame.size.height))
//        bgView.layer.cornerRadius = 10.0
        bgView.clipsToBounds = true
        bgView.layer.borderColor = UIColor.black.cgColor
        bgView.layer.borderWidth = CGFloat(BG_VIEW_BORDERS_SIZE)
        addSubview(bgView)
        self.avAsset = avAsset
        
        topBorder = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: SLIDER_BORDERS_SIZE))
        // topBorder.backgroundColor = UIColor(red: 162.0/255.0, green: 72.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        topBorder.backgroundColor = themeColor
        addSubview(topBorder)
        
        bottomBorder = UIView(frame: CGRect(x: 0, y: frame.size.height - SLIDER_BORDERS_SIZE, width: frame.size.width, height: SLIDER_BORDERS_SIZE))
        // bottomBorder.backgroundColor = UIColor(red: 162.0/255.0, green: 72.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        bottomBorder.backgroundColor = themeColor
        addSubview(bottomBorder)
        
        leftThumb = SASliderLeft(frame: CGRect(x: 0, y: 0, width: thumbWidth, height: frame.size.height))
        leftThumb.contentMode = .left
        leftThumb.isUserInteractionEnabled = true
        leftThumb.clipsToBounds = true
        leftThumb.backgroundColor = UIColor.clear
        leftThumb.layer.borderWidth = 0
        addSubview(leftThumb)
        
        
        let leftimageView = UIImageView()
        leftimageView.frame = CGRect(x: thumbWidth * 0.15, y: 0, width: thumbWidth * 0.7, height: frame.size.height)
        leftimageView.image = #imageLiteral(resourceName: "leftTrim")
        leftimageView.contentMode = .scaleAspectFit
        leftThumb.addSubview(leftimageView)
        
        let leftPan = UIPanGestureRecognizer(target: self, action: #selector(self.handleLeftPan(_:)))
        leftThumb.addGestureRecognizer(leftPan)
        rightThumb = SASliderRight(frame: CGRect(x: 0, y: 0, width: thumbWidth, height: frame.size.height))
        rightThumb.contentMode = .right
        rightThumb.isUserInteractionEnabled = true
        rightThumb.clipsToBounds = true
        rightThumb.backgroundColor = UIColor.clear
        //rightThumb.backgroundColor = themeColor
        addSubview(rightThumb)
        
        
        let rightimageView = UIImageView()
        rightimageView.frame = CGRect(x:thumbWidth * 0.15, y: 0 , width: thumbWidth * 0.7, height: frame.size.height)
        rightimageView.image = #imageLiteral(resourceName: "rightTrim")
        rightimageView.contentMode = .scaleAspectFit
        rightThumb.addSubview(rightimageView)

        
        let rightPan = UIPanGestureRecognizer(target: self, action: #selector(self.handleRightPan(_:)))
        rightThumb.addGestureRecognizer(rightPan)
        rightPosition = frame.size.width
        leftPosition = 0.0
        
        centerView = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        centerView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        addSubview(centerView)
        centerView.tag = 101
        
        let centerPan = UIPanGestureRecognizer(target: self, action: #selector(self.handleCenterPan(_:)))
        centerPan.delegate = self
        centerView.addGestureRecognizer(centerPan)
        popoverBubble = SAResizibleBubble(frame: CGRect(x: 0, y: -50, width: 100, height: 50))
        popoverBubble.alpha = 0
        popoverBubble.backgroundColor = UIColor.clear
        addSubview(popoverBubble)
        bubleText = UILabel(frame: popoverBubble.frame)
        bubleText.font = UIFont.boldSystemFont(ofSize: 20)
        bubleText.backgroundColor = UIColor.clear
        bubleText.textColor = UIColor.black
        bubleText.textAlignment = .left
        popoverBubble.addSubview(bubleText)
        
        minGap = ((bgView.frame.size.width / CGFloat(avAsset.duration.seconds)) * minSeconds ) - (2 * thumbWidth)
        if CGFloat(durationSeconds) > maxSeconds {
            updateWidth(with: maxSeconds)
            maxGap = rightPosition
//            maxGap = ((bgView.frame.size.width / CGFloat(avAsset.duration.seconds)) * maxSeconds
//                )
//            rightPosition = maxGap
        }
      
        
//        lineView = UIView(frame: CGRect(x: 0, y: 0, width: 40.0, height: frame.height))
//        lineParent = UIView(frame: CGRect(x: 0, y: 0, width: 3.0, height: frame.height))
//        lineParent.center.x = lineView.frame.width / 2

//        lineView.backgroundColor = UIColor.white
//        lineView.layer.borderWidth = 20.0
//        lineView.layer.borderColor = UIColor.clear.cgColor
//        lineView.isUserInteractionEnabled = true
//        centerView.addSubview(lineView)
//        centerView.clipsToBounds = true
//        lineView.tag = 105
//        lineParent.backgroundColor = UIColor.white
//        lineView.mask = lineParent
        
//        let lineViewPan = UIPanGestureRecognizer(target: self, action: #selector(handleLinePan(_:)))
//        lineViewPan.delegate = self
//        lineView.addGestureRecognizer(lineViewPan)
        
        getMovieFrame()
    }
    
    func updateTimer(currentTime: CGFloat){
        let widthPS = (bgView.frame.size.width) / CGFloat(durationSeconds)
        let startingDiff = centerView.frame.origin.x
        let xPos = widthPS * currentTime
        self.lineView.frame.origin.x = xPos - 18.5 - startingDiff
        lineView.setNeedsLayout()
    }

   
    
    override func layoutSubviews() {
        let inset: CGFloat = leftThumb.frame.size.width / 2
        leftThumb.center = CGPoint(x: leftPosition + inset, y: leftThumb.frame.size.height / 2)
        rightThumb.center = CGPoint(x: rightPosition - inset, y: rightThumb.frame.size.height / 2)
        topBorder.frame = CGRect(x: leftThumb.frame.origin.x + leftThumb.frame.size.width, y: 0, width: rightThumb.frame.origin.x - leftThumb.frame.origin.x - leftThumb.frame.size.width / 2, height: SLIDER_BORDERS_SIZE)
        bottomBorder.frame = CGRect(x: leftThumb.frame.origin.x + leftThumb.frame.size.width, y: bgView.frame.size.height - SLIDER_BORDERS_SIZE, width: rightThumb.frame.origin.x - leftThumb.frame.origin.x - leftThumb.frame.size.width / 2, height: SLIDER_BORDERS_SIZE)
        centerView.frame = CGRect(x: leftThumb.frame.origin.x + leftThumb.frame.size.width, y: centerView.frame.origin.y, width: rightThumb.frame.origin.x - leftThumb.frame.origin.x - leftThumb.frame.size.width, height: centerView.frame.size.height)
    }
    
    
    func getMovieFrame(){
        guard let myAsset = avAsset else {
            return
        }
      
        imageGenerator = AVAssetImageGenerator(asset: myAsset)
        imageGenerator.appliesPreferredTrackTransform = true
        imageGenerator.maximumSize =  CGSize(width: 200, height: 200)
        let interval = durationSeconds / 10.0 //only 10 images will be generated
        var imageArray = [UIImage]()
        for i in 0..<10{
            do {
                let time = CMTime(seconds: Double(Double(i) * interval), preferredTimescale: myAsset.duration.timescale)
                let img = try imageGenerator.copyCGImage(at: time, actualTime: nil)
                let image = UIImage(cgImage: img)
                imageArray.append(image)
            } catch {
                print(error.localizedDescription)
            }
        }
        let picWidth: CGFloat = bgView.frame.width / 10.0
        var xPos : CGFloat = 0
        for image in imageArray{
            let imgView = UIImageView(image: image)
            imgView.contentMode = .scaleAspectFill
            imgView.frame = CGRect(x: xPos, y: 0, width: picWidth, height: bgView.frame.size.height)
            xPos += picWidth
            self.bgView.addSubview(imgView)
        }
        imageArray.removeAll()
    }
    
    func hideBubble(_ popover: UIView?) {
        delegate?.videoRange(self, didGestureStateEndedLeftPosition: leftValueGetter, rightPosition: rightValueGetter)
      
    }
    
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if (gestureRecognizer.view?.tag == 105) && (otherGestureRecognizer.view?.tag == 101){
            return false
        }
        return true
    }
    
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if (gestureRecognizer.view?.tag == 105) && (otherGestureRecognizer.view?.tag == 101){
            return false
        }
        return true
    }
    
    //MARK:- Gesture Actions
   @objc func handleLeftPan(_ gesture: UIPanGestureRecognizer) {
    if gesture.state == .began{
        delegate?.videoRange(didBeginMoving: self)
    }
    if gesture.state == .changed {
            let translation: CGPoint = gesture.translation(in: self)
            leftPosition += translation.x
            if leftPosition < 0 {
                leftPosition = 0
            }
            if (rightPosition - leftPosition <= leftThumb.frame.size.width + rightThumb.frame.size.width) || ((maxGap > 0) && (rightPosition - leftPosition > maxGap)) || ((minGap > 0) && (rightPosition - leftPosition <= minGap)) {
                leftPosition -= translation.x
            }
            gesture.setTranslation(CGPoint.zero, in: self)
            setNeedsLayout()
            delegate?.videoRange(self, didChangeLeftPosition: leftValueGetter)
        }
        popoverBubble.alpha = 1
        setTimeLabel()
        if gesture.state == .ended {
            hideBubble(popoverBubble)
        }
    }
    
    
   @objc func handleRightPan(_ gesture: UIPanGestureRecognizer) {
    if gesture.state == .began{
        delegate?.videoRange(didBeginMoving: self)
    }
    if gesture.state == .changed {
            let translation: CGPoint = gesture.translation(in: self)
            rightPosition += translation.x
            if rightPosition < 0 {
                rightPosition = 0
            }
        
            if rightPosition > frame_width {
                rightPosition = frame_width
            }
            if rightPosition - leftPosition <= 0 {
                rightPosition -= translation.x
            }
        
            //***** ********
        if (rightPosition - leftPosition <= leftThumb.frame.size.width + rightThumb.frame.size.width){
            rightPosition -= translation.x
        }
        if (maxGap > 0 && (rightPosition - leftPosition > maxGap)) {
            rightPosition -= translation.x
        }
        if ((minGap > 0) && (rightPosition - leftPosition < minGap)) {
           rightPosition -= translation.x
        }
        
//            if (rightPosition - leftPosition <= leftThumb.frame.size.width + rightThumb.frame.size.width) || ((maxGap > 0) && (rightPosition - leftPosition > maxGap)) || ((minGap > 0) && (rightPosition - leftPosition < minGap)) {
//                rightPosition -= translation.x
//            }
            gesture.setTranslation(CGPoint.zero, in: self)
            setNeedsLayout()
            delegate?.videoRange(self, didChangeRightPosition: rightValueGetter)
            popoverBubble.alpha = 1
            setTimeLabel()
        }
        if gesture.state == .ended {
            hideBubble(popoverBubble)
        }
    }
    
    
    @objc func handleLinePan(_ gesture: UIPanGestureRecognizer){
        if gesture.state == .began{
            delegate?.videoRange(didBeginMoving: self)
        }
        if gesture.state == .changed{
            let translation: CGPoint = gesture.translation(in: self)
            lineView.frame.origin.x += translation.x
            gesture.setTranslation(CGPoint.zero, in: self)
            delegate?.videoRange(self, didMovePlayerSlider: lineViewTimings)
        }
    }
    
    @objc func handleCenterPan(_ gesture: UIPanGestureRecognizer) {
        
        if gesture.state == .began{
            delegate?.videoRange(didBeginMoving: self)
        }
        
        if gesture.view == lineView{
            print("line called")
            let translation: CGPoint = gesture.translation(in: self)
            lineView.frame.origin.x += translation.x

            gesture.setTranslation(CGPoint.zero, in: self)

            return
        }
        if gesture.state == .changed {
            let translation: CGPoint = gesture.translation(in: self)
            leftPosition += translation.x
            rightPosition += translation.x
            
            if rightPosition > frame_width || leftPosition < 0 {
                leftPosition -= translation.x
                rightPosition -= translation.x
            }
            gesture.setTranslation(CGPoint.zero, in: self)
            setNeedsLayout()
            delegate?.videoRange(self, leftPosition: leftValueGetter, rightPosition: rightValueGetter)
        }
        popoverBubble.alpha = 1
        setTimeLabel()
        if gesture.state == .ended {
            hideBubble(popoverBubble)
        }
    }
    func setTimeLabel() {
        bubleText.text = trimIntervalStr()
    }
    
    func trimIntervalStr() -> String? {
        let from    =   String(describing: leftPosition)
        let to      =   String(describing: rightPosition)
        return "\(from) - \(to)"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func isRetina() -> Bool {
        return UIScreen.main.responds(to: #selector(UIScreen.displayLink(withTarget:selector:))) && (UIScreen.main.scale == 2.0)
    }
    
    func setPopoverBubbleSize(_ width: CGFloat, height: CGFloat) {
        var currentFrame: CGRect = popoverBubble.frame
        currentFrame.size.width = width
        currentFrame.size.height = height
        currentFrame.origin.y = -height
        popoverBubble.frame = currentFrame
        currentFrame.origin.x = 0
        currentFrame.origin.y = 0
        bubleText.frame = currentFrame
        
    }
}
