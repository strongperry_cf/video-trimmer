//
//  Filters.swift
//  Video Editorial
//
//  Created by strongapps on 18/01/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

enum CropType: String {
    case original = "Original", ins1r1 = "Ins 1:1", ins4r5 = "Ins 4:5", cam4r3 = "Camera 4:3", cam3r4 = "Camera 3:4", you16r9 = "Youtube 16:9", fb191r1 = "Facebook 1.91:1"
    
//    , full9r16 = "Fullscreen 9:16"
    static var count: Int { return CropType.fb191r1.hashValue + 1}
}

class Crop {

    var type: CropType?
    
    init(type: CropType) {
        self.type = type
    }
}
