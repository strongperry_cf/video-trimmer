//
//  TrimmingView.swift
//  VideoTrimmer
//
//  Created by strongapps on 02/07/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit
protocol trimViewDelegate : class{
    
    func trimScrubbingChange(startvalue:CGFloat, endValue : CGFloat, isStartPointChanges : Bool)
    func trimDidChange(startvalue:CGFloat, endValue : CGFloat)
    func trimDoneAction(startvalue:CGFloat, endValue : CGFloat)
    func trimCancelAction()
    
}
class TrimmingView: UIView {

    @IBOutlet weak var startTrimingLabel: UILabel!
    @IBOutlet weak var durationTrimingLabel: UILabel!
    @IBOutlet weak var endTrimingLabel: UILabel!

    @IBOutlet var sliderSuperView: UIView!
    @IBOutlet var multiSlider: MultiSlider!
    public var videoData:VideoData?
    weak var delegate               :trimViewDelegate?
    var trimSliderTimeLine: VideoRangeSlider?

    var videoStartTime: CGFloat = 0.0
    var videoEndTime : CGFloat = 0.0
    var videoMiddleTime : CGFloat = 0.0

    
    class func initWithFrame(frame:CGRect) -> TrimmingView {
        let view = self.fromNib()
        view.frame = frame
        return view
    }
    class func fromNib<T: TrimmingView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    func setupUI(avasset : AVAsset) {
        
        self.videoEndTime = (avasset.duration.durationInSeconds)
        self.startTrimingLabel.text = self.videoStartTime.convertSecondsToDurationString2()
        self.durationTrimingLabel.text = self.videoEndTime.convertSecondsToDurationString2()
        self.endTrimingLabel.text = self.videoEndTime.convertSecondsToDurationString2()
        

        self.multiSlider.addTarget(self, action: #selector(self.sliderChanged(_:)), for: .valueChanged)
        self.multiSlider.addTarget(self, action: #selector(self.sliderExitTouch(_:)), for: .touchUpInside)
        self.multiSlider.orientation = .horizontal
        self.multiSlider.outerTrackColor = .lightGray
        self.multiSlider.trackWidth = 2
        self.multiSlider.valueLabelPosition = .top // .notAnAttribute = don't show labels
        self.multiSlider.value = [0,5]
        self.multiSlider.tintColor = AppThemeManager.shared.currentTheme().TintColor
        
//        self.sliderSuperView.subviews.forEach{
//            $0.removeFromSuperview()
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
//            let frame = CGRect(origin: self.multiSlider.frame.origin, size: CGSize(width: self.sliderSuperView.frame.width-80, height: self.multiSlider.frame.height))
            var origin =  self.multiSlider.frame.origin
            origin.y = self.frame.size.height / 2 - self.multiSlider.frame.height / 2
            let frame = CGRect(origin:origin , size: CGSize(width: self.sliderSuperView.frame.width-60, height: self.multiSlider.frame.height))
            self.trimSliderTimeLine = VideoRangeSlider(frame: frame, avAsset: avasset)
            self.trimSliderTimeLine?.minSeconds = 2.0
            self.trimSliderTimeLine?.delegate = self
            self.sliderSuperView.addSubview(self.trimSliderTimeLine!)
            self.trimSliderTimeLine?.bubleText.isHidden = true
            self.trimSliderTimeLine?.setPopoverBubbleSize(0, height: 0)
            
//            self.sliderSuperView.addSubview(self.startTrimingLabel!)
//            self.sliderSuperView.addSubview(self.durationTrimingLabel!)
//            self.sliderSuperView.addSubview(self.endTrimingLabel!)

        }

    }


    @IBAction func cancelBtnAct(_ sender: UIButton) {
        self.isHidden = true
        if delegate != nil {
            delegate?.trimCancelAction()

        }
    }
    
    @IBAction func doneBtnAct(_ sender: UIButton) {
        self.isHidden = true
        if delegate != nil {
            delegate?.trimDoneAction(startvalue: multiSlider.value[0],endValue: multiSlider.value[1])
        }
    }
    @objc func sliderChanged(_ slider: MultiSlider) {
        
        
        debugPrint("isLeftThumbSelectedFuntion " + (slider.isLeftThumbSelectedFuntion() ? "true" : "false"))
        
        if delegate != nil {
            delegate?.trimScrubbingChange(startvalue: multiSlider.value[0],endValue:multiSlider.value[1], isStartPointChanges: slider.isLeftThumbSelectedFuntion())
        }
        
    }
    @objc func sliderExitTouch(_ slider: MultiSlider) {
        
        print("sliderExitTouch\(slider.value)")
        if delegate != nil {
            delegate?.trimDidChange(startvalue: multiSlider.value[0],endValue:multiSlider.value[1])
        }
        
    }
}
extension TrimmingView : VideoRangeSliderDelegate{
    
    func videoRange(_ videoRange: VideoRangeSlider, didChangeLeftPosition leftPosition: CGFloat) {
//        print("didChangeLeftPosition")
        print("leftPosition", leftPosition)
        videoStartTime = leftPosition
        videoMiddleTime = videoEndTime - videoStartTime

        self.startTrimingLabel.text = self.videoStartTime.convertSecondsToDurationString2()
        self.durationTrimingLabel.text = self.videoMiddleTime.convertSecondsToDurationString2()
        self.endTrimingLabel.text = self.videoEndTime.convertSecondsToDurationString2()

        
        multiSlider.value[0] = leftPosition
        if delegate != nil {
            delegate?.trimScrubbingChange(startvalue: multiSlider.value[0],endValue:multiSlider.value[1], isStartPointChanges: true)
        }


    }
    
    func videoRange(_ videoRange: VideoRangeSlider, didChangeRightPosition rightPosition: CGFloat) {
//        print("didChangeRightPosition")
        print("rightPosition",  rightPosition)

        videoEndTime = rightPosition
        videoMiddleTime = videoEndTime - videoStartTime

        self.startTrimingLabel.text = self.videoStartTime.convertSecondsToDurationString2()
        self.durationTrimingLabel.text = self.videoMiddleTime.convertSecondsToDurationString2()
        self.endTrimingLabel.text = self.videoEndTime.convertSecondsToDurationString2()

        multiSlider.value[1] = rightPosition
        if delegate != nil {
            delegate?.trimScrubbingChange(startvalue: multiSlider.value[0],endValue:multiSlider.value[1], isStartPointChanges: false)
        }


    }
    
    func videoRange(_ videoRange: VideoRangeSlider, didGestureStateEndedLeftPosition leftPosition: CGFloat, rightPosition: CGFloat) {
//        print(leftPosition, "<<>>>", rightPosition)
        
        multiSlider.value = [leftPosition,rightPosition]
        if delegate != nil {
            delegate?.trimDidChange(startvalue: multiSlider.value[0],endValue:multiSlider.value[1])
        }

    }
    
    func videoRange(didBeginMoving videoRange: VideoRangeSlider) {
        
    }
    
    func videoRange(_ videoRange: VideoRangeSlider, didMovePlayerSlider value: CGFloat) {

    }
    
    func videoRange(_ videoRange: VideoRangeSlider, leftPosition: CGFloat, rightPosition: CGFloat) {
        
        videoStartTime = leftPosition
        videoEndTime = rightPosition
        videoMiddleTime = videoEndTime - videoStartTime

        self.startTrimingLabel.text = self.videoStartTime.convertSecondsToDurationString2()
        self.durationTrimingLabel.text = self.videoMiddleTime.convertSecondsToDurationString2()
        self.endTrimingLabel.text = self.videoEndTime.convertSecondsToDurationString2()

        multiSlider.value = [leftPosition,rightPosition]
        if delegate != nil {
            delegate?.trimScrubbingChange(startvalue: multiSlider.value[0],endValue:multiSlider.value[1], isStartPointChanges: true)
        }

    }
    
    
}
