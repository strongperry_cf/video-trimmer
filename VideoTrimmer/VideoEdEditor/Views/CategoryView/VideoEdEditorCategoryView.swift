//
//  VideoEdEditorCategoryView.swift
//  VideoTrimmer
//
//  Created by strongapps on 29/04/19.
//  Copyright © 2019 strongapps. All rights reserved.


import UIKit

//MARK: HSCenterSliderDelegate Protocol
// This protocol used to provide update progress value changes to outside world

public protocol CategoryViewDelegate: class {
    func categoryViewDidSelect(_ indexPath:IndexPath)
}

class VideoEdEditorCategoryView: UIView {
    
    public weak var delegate: CategoryViewDelegate?
    @IBOutlet weak var categoryCV: UICollectionView!
    @IBOutlet internal var contentView: UIView!
    
    var selectedIndex = 0
    var itemSpacing :CGFloat = 10
    var sectionInsect = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
    
    
    let imageArr :[String] = ["bottom_cut","bottom_ratio","bottom_background","bottom_speed","bottom_Rotate"]
    let nameArr :[String] = ["TRIM","RATIO","BACKGROUND","SPEED","ROTATE"]
    
    
    private func nibSetup() {
        debugPrint(" nibSetup() ")
        backgroundColor = .clear
        let view = loadViewFromNib()
        view.frame = bounds
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        debugPrint(" awakeFromNib ")
        nibSetup()
        //        self.backgroundColor = UIColor.clear
        categoryCV.delegate = self
        categoryCV.dataSource = self
        //        categoryCV.register(UINib(nibName: "CategoryCVCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCVCell")
        categoryCV.register(UINib.init(nibName: "EditorMenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EditorMenuCollectionViewCell")
        
    }
    
}
extension VideoEdEditorCategoryView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArr.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (SCREEN_WIDTH - CGFloat(imageArr.count + 1) * itemSpacing) / 5, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsect
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditorMenuCollectionViewCell", for: indexPath as IndexPath) as! EditorMenuCollectionViewCell
        let imageStr = imageArr[indexPath.row]
        cell.thumbnail.tintColor =  UIColor.white
        cell.thumbnail.contentMode = .scaleAspectFit
        cell.textLabel.text = nameArr[indexPath.row].uppercased()
        
        let image = UIImage(named: imageStr)
        if image != nil{
            //            cell.thumbnail.image = image
            cell.thumbnail.image = image?.withRenderingMode(.alwaysTemplate)
            
        } else  {
            cell.thumbnail.image = nil
        }
        cell.textLabel.isHighlighted = false
        if selectedIndex == indexPath.row{
            
            cell.thumbnail.tintColor = colorGrey252
            cell.textLabel.textColor = colorGrey252
            
        }else{
            cell.thumbnail.tintColor = colorGrey140
            cell.textLabel.textColor = colorGrey140
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if indexPath.row != imageArr.count-1{

            selectedIndex =  indexPath.row
            collectionView .reloadData()
        }
        if delegate != nil {
            delegate?.categoryViewDidSelect(indexPath)
        }
    }
    
}


