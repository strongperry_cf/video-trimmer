//
//  VideoEdSplitView.swift
//  VideoTrimmer
//
//  Created by strongapps on 31/05/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

protocol SplitViewDelegate : class{
    
    func SplitView_SliderDidChange(ratio:CGFloat)
    func SplitView_SplitAction(ratio:CGFloat)
    func SplitView_HideBtnAction()

}

class VideoEdSplitView: UIView {

    @IBOutlet weak var doneBtn      :UIButton!
    @IBOutlet weak var backView     :UIView!
    @IBOutlet weak var timeLineView :UIView!
    weak var delegate               :SplitViewDelegate?
    @IBOutlet weak var splitSlider  :UISlider!
    
    public var previousTracksDuration :CMTime!

    public var videoData:VideoData?
    
    
    class func initWithFrame(frame:CGRect) -> VideoEdSplitView {
        let view = self.fromNib()
        view.frame = frame
        view.setupUI()
        return view
    }
    
    func setupUI() {
        createBackTimeLine()
    }
    
    func createBackTimeLine()  {
        
        if videoData != nil {
  
            
            splitSlider.setThumbImage(UIImage(named: "ic_slider_line"), for: .normal)
            splitSlider.setThumbImage(UIImage(named: "ic_slider_line"), for: .highlighted)
            splitSlider.setValue(0.5, animated: false)

            
            let imageSize = CGSize(width:timeLineView.height , height: timeLineView.height)
            let timeLineWidth  = self.width * 0.9
            let count = Int(ceil(timeLineWidth / imageSize.width))
            
            let duration = CMTimeSubtract(videoData!.trimEndTime, videoData!.trimStartTime).seconds
            let step = duration / Double(count)
            let stepTime = CMTime(seconds: step, preferredTimescale: 100)
            var currentTime = videoData!.trimStartTime
            var xDistance:CGFloat = 0.0
            
            for n in 1...count   {
                
                print("image count is : ",n)
                print("currentTime is ",currentTime.seconds)
                
                let asset = AVAsset(url: videoData!.videoUrl!)
                let image = VideoEdVideoManager().generateThumbnail(asset: asset, nil, at: currentTime, of: CGSize(width: imageSize.width * 2.0 , height: imageSize.height * 2.0 ))

                let imageView = UIImageView(image: image)
                imageView.frame = CGRect(x: xDistance, y: CGFloat(0.0), width: imageSize.width, height: imageSize.height)
                imageView.contentMode = .scaleAspectFill
                
                timeLineView.addSubview(imageView)
                xDistance = xDistance + imageSize.width
                currentTime = CMTimeAdd(currentTime, stepTime)
                
            }
        }
    }
    
    @IBAction func cancelBtnAct(_ sender: UIButton) {
        self.isHidden = true
        if delegate != nil {
            delegate?.SplitView_HideBtnAction()
        }
    }
    
    @IBAction func doneBtnAct(_ sender: UIButton) {
        self.isHidden = true
        if delegate != nil {
            delegate?.SplitView_SplitAction(ratio: CGFloat(splitSlider!.value))
        }
    }
    
    @IBAction func splitSliderAct(_ sender: UISlider) {
        
        if delegate != nil {
            delegate?.SplitView_SliderDidChange(ratio: CGFloat(splitSlider!.value))
        }
    }
  
    
}


extension VideoEdSplitView {
    class func fromNib<T: VideoEdSplitView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
