//  TransitionCVCell.swift
//  VideoTrimmer
//
//  Created by strongapps on 30/04/19.
//  Copyright © 2019 strongapps. All rights reserved.

import UIKit

class TransitionCVCell: UICollectionViewCell {

    @IBOutlet weak var mainBackView     :UIView!
    @IBOutlet weak var thumbnailView    :UIImageView!
    
    @IBOutlet weak var titleTopLbl      :UILabel!
    @IBOutlet weak var titleBottomLbl   :UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

}
