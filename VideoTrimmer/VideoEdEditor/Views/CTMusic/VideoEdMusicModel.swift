//
//  MusicModel.swift
//  SlideMakerPro
//
//  Created by strongapps on 25/02/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

enum MusicCategoryType :Int {
    case library = 0
    case itunes
    case findMusic
}

class VideoEdMusicModel: NSObject{
    
    var urlString: String = ""
    var name : String? = nil
    var audioImage : String?
    var image : UIImage?
    var musicType : MusicCategoryType?
    
    override init() {
        
    }
    init(urlString: String) {
        super.init()
        self.urlString = urlString
        self.name = getSongName(urlString: urlString)
    }
    
    
    func getSongName(urlString: String) -> String?{
        let components = urlString.components(separatedBy: "/")
        return components.last
    }
    
    
    
    
    

    
}
