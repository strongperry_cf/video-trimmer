//
//  VideoEdMusicFileTVC.swift
//  VideoTrimmer
//
//  Created by strongapps on 16/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class VideoEdMusicFileTVC: UITableViewCell {

    @IBOutlet weak var LblTimeAndArtist: UILabel!
    @IBOutlet weak var imgSongCat: UIImageView!
    @IBOutlet weak var lblSongTitle: UILabel!
    @IBOutlet weak var c_view_width: NSLayoutConstraint!
    
    @IBOutlet weak var imgPlayPause: UIImageView!
    @IBOutlet weak var selectBtn: UIButton!
    
    var btnSelected : ((Int) -> ())? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgSongCat.layer.cornerRadius = 5
        self.imgSongCat.layer.masksToBounds = true
        
        
        
        self.selectBtn.layer.cornerRadius = 30
        self.selectBtn.layer.borderColor = AppThemeManager.shared.currentTheme().SelectedStrokeColor.cgColor
        self.selectBtn.layer.borderWidth = 1.0;
        self.selectBtn.titleLabel?.font = UIFont(name: FONT_SEMIBOLD, size: 16)
    }

    @IBAction func selectBtnAct(_ sender: UIButton) {
        
        btnSelected!(selectBtn.tag)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
