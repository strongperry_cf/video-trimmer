//
//  VideoEdMusicCell.swift
//  Video Editorial
//
//  Created by strongapps on 5/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

protocol MusicCellDelegate : class{
    func didSelectMusic(with url: URL,at indexPath: IndexPath)
}


class VideoEdMusicCell: UICollectionViewCell {
    
    var indicate : NVActivityIndicatorView!
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var imgSave: UIImageView!
    static let reuseIdentifier = "VideoEdMusicCell"
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var actionBtn: UIButton!
    weak var delegate: MusicCellDelegate?
    
    var indexPath: IndexPath!
//    var indi = NVActivityIndicatorView()
    @IBOutlet weak var lblName: UILabel!
    var musicData: MusicObjectData!
    var isSelectedCell = false {
        didSet{
            if musicData.type != MusicObjectData.NO_MUSIC{
                GlobalHelper.dispatchMain { [weak self] in
                    if self?.isSelectedCell ?? false{
//                        self?.actionBtn.setImage(UIImage(named: "play_song"), for: .normal)
                    }else{
                        self?.actionBtn.setImage((self!.isDownloadable ? UIImage(named: "ic_download_music") : nil), for: .normal)
                    }
                    self?.imgSave.isHidden = self!.isSelectedCell ? false : true
                }
            }else{
                self.imgSave.isHidden = true
                self.actionBtn.setImage(nil, for: .normal)
            }
        }
    }
    
    var downloadSession: URLSession!
    var downloadTask: URLSessionDownloadTask!
    
    var isDownloadable = false{
        didSet{
            GlobalHelper.dispatchMain {[weak self] in
                self?.actionBtn.setImage(self!.isDownloadable ? UIImage(named: "ic_download_music") : nil, for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func setupData(music: MusicObjectData) {
        self.musicData = music
        if let image = music.artwork {
            self.imgView.image = image
        }
        self.lblName.text = music.title
        isDownloadable = false
        if music.type == MusicObjectData.LIBRARY,let urlStr = music.url_path?.absoluteString{
        isDownloadable = DirectoryManager.isFileExist(with: urlStr).0 ? false : true
            
        } else if music.type == MusicObjectData.NO_MUSIC{
            self.imgView.image = UIImage(named: "ic_none")
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
            self.imgView.layer.cornerRadius = self.imgView.frame.width / 2
            self.imgView.clipsToBounds = true
            self.shadowView.layer.borderColor = UIColor(red: 244 / 255.0, green: 168 / 255.0, blue: 26 / 255.0, alpha: 1.0).cgColor
            self.shadowView.addAllShadow(3.0)
        }
    }

    func downloadSong(with Url: String){
        // Create destination URL
       
        let (isExist,destinationUrl) = DirectoryManager.isFileExist(with: Url)
        if isExist{
            delegate?.didSelectMusic(with: destinationUrl, at: indexPath)
            return
        }
        
        let fileURL = URL(string: Url)
        let sessionConfig = URLSessionConfiguration.default
        downloadSession = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
        downloadSession.sessionDescription = destinationUrl.path
        let request = URLRequest(url:fileURL!)
        ShowProgressBar()
        downloadTask = downloadSession.downloadTask(with: request)
        downloadTask.resume()
        
    }
}

extension VideoEdMusicCell: URLSessionDelegate, URLSessionDownloadDelegate, NVActivityIndicatorViewable{
    
    
    
    //MARK:- Download Session Delegates
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        print("downloaded \(100*totalBytesWritten/totalBytesExpectedToWrite)")
        
        if ( session.sessionDescription!.contains("mp3") )  {
            GlobalHelper.dispatchMain {
                let progress  =  (totalBytesWritten * 100/totalBytesExpectedToWrite)
                print("progreesss >> ",progress)
                 NVActivityIndicatorPresenter.sharedInstance.setMessage(String(Int(progress)) + "%")
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        GlobalHelper.dispatchMainAfter(time: .now() + 0.1) {[weak self] in
            self?.HideProgresBar()
        }
        let fileName = session.sessionDescription!
        let downloadsPath = URL(fileURLWithPath: fileName)
        do {
            if FileManager.default.fileExists(atPath: downloadsPath.path){
                try FileManager.default.removeItem(at: downloadsPath)
            }
            try FileManager.default.copyItem(at: location, to: downloadsPath)
            isDownloadable = false
            delegate?.didSelectMusic(with: downloadsPath, at: indexPath)
        } catch (let writeError) {
            print("Error creating a file \(downloadsPath) : \(writeError)")
        }
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        print("Error",error?.localizedDescription ?? "")
    }
    
    
    //MARK:- Custom UIActivity Delegate
    func progressDidCancel() {
        if downloadSession != nil && downloadTask != nil{
            downloadSession.invalidateAndCancel()
            downloadTask.suspend()
            downloadSession = nil
            downloadTask = nil
        }
//        HideProgresBar()
    }
    
    func ShowProgressBar() {
        NotificationCenter.default.post(name: Notification.Name("IndicatorNotify"), object: nil, userInfo: nil)
    }
    func HideProgresBar() {
    }
}


extension VideoEdMusicCell {
    class func fromNib<T: VideoEdMusicCell>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

