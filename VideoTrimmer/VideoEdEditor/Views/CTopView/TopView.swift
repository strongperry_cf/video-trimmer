//
//  TopView.swift
//  Video Editorial
//
//  Created by strongapps on 06/02/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

protocol TopViewDelegate:class {
    func leftButtonClicked(sender: UIButton)
    func rightButtonClicked(sender: UIButton)
}

public enum CornerType: Int{
    case roundCorner
    case squareRound
    
}
class TopView: UIView {

   weak var delegate: TopViewDelegate?
    var willAnimate = false
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    override var backgroundColor: UIColor? {
        didSet{
            for view in subviews{
                view.backgroundColor = UIColor.clear
            }
        }
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()

            var cornerRadius : CGFloat = 12.0 //reference from xib
            if IS_IPAD{
                cornerRadius = 17.0
            }
            self.rightButton.layer.cornerRadius = cornerRadius//self.rightButton.frame.size.height / 2
            self.rightButton.clipsToBounds = true
            
            self.leftButton.layer.cornerRadius = cornerRadius//self.leftButton.frame.size.height / 2
            self.leftButton.clipsToBounds = true
//        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let blurRight = UIVisualEffectView(effect: UIBlurEffect(style:.regular))
            blurRight.frame = self.rightButton.bounds
            blurRight.isUserInteractionEnabled = false
            blurRight.clipsToBounds = true
            blurRight.layer.cornerRadius = blurRight.frame.height / 2
            self.rightButton.insertSubview(blurRight, at: 0)
//            self.rightButton.addSubview(blurRight)
            
            let blurLeft = UIVisualEffectView(effect: UIBlurEffect(style:.regular))
            blurLeft.frame = self.leftButton.bounds
            blurLeft.isUserInteractionEnabled = false
            self.leftButton.insertSubview(blurLeft, at: 0)

        }
        
        
    }
    
    
    @IBAction func rightButtonClicked(_ sender: UIButton) {
        delegate?.rightButtonClicked(sender: sender)
    }
    
    @IBAction func leftButtonClicked(_ sender: UIButton) {
        delegate?.leftButtonClicked(sender: sender)
    }
    
    //animation top view buttons
    func animateFromLeftAndRight(){
        leftButton.frame.origin.x -= 100
        rightButton.frame.origin.x += 100
        UIView.animate(withDuration: 0.1) {
            self.leftButton.frame.origin.x += 100
            self.rightButton.frame.origin.x -= 100
        }
    }
    
    
    func setupButton(button: UIButton,text:String?,image: UIImage?,bgColor: UIColor,corner: CornerType,contentHAlignment: UIControl.ContentHorizontalAlignment = .center){
        button.setTitle(text, for: .normal)
        button.setImage(image, for: .normal)
        button.backgroundColor = bgColor
        button.contentHorizontalAlignment = contentHAlignment
        
        switch corner {
        case .roundCorner:
            button.layer.cornerRadius = button.frame.height / 2
        case .squareRound:
            button.layer.cornerRadius = 5.0
        }
    }
    
    
    
    

}

extension TopView {
    class func fromNib<T: TopView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
