//
//  VideoEdFilters.swift
//  Video Editorial
//
//  Created by strongapps on 18/01/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class VideoEdFilters {

    var type: CustomFilterType?
    var value: Float?
    
    init(type: CustomFilterType?) {
        self.type = type
    }
}
