
import UIKit
import HSCenterSlider
import HSRange

protocol AdjustViewDelegate : class{
    func updateAdjustEffect(filterValues:AdjustFilterValues)
}

struct Parameters {
    var defaultValue:CGFloat = 0.0 ,min:CGFloat = 0.0 , max:CGFloat = 2.0 , current:CGFloat = 0.0
    init(defaultValue:CGFloat,min:CGFloat,max:CGFloat,current:CGFloat) {
        self.defaultValue = defaultValue
        self.min = min
        self.max = max
        self.current = current
    }
}

struct AdjustFilterValues {
    var brightness:Parameters   = Parameters.init(defaultValue: 0.0, min: -0.5, max: 0.5, current: 0.0)
    var contrast:Parameters     = Parameters.init(defaultValue: 1.0, min: 0.5, max: 1.5, current: 1.0)
    var exposure:Parameters     = Parameters.init(defaultValue: 0.0, min: -2.0, max: 2.0, current: 0.0)
    var saturation:Parameters   = Parameters.init(defaultValue: 1.0, min: 0.0, max: 2.0, current: 1.0)
    var temprature:Parameters   = Parameters.init(defaultValue: 5000.0, min: 4000.0, max: 6000.0, current: 5000.0)
    var sharpness:Parameters    = Parameters.init(defaultValue: 0.0, min: -3.0, max: 3.0, current: 0.0)
    var hue:Parameters          = Parameters.init(defaultValue: 0.0, min: -180.0, max: 180.0, current: 0.0)
    //var vignette:Parameters     = Parameters.init(defaultValue: 1.3, min: 0.3, max: 1.3, current: 1.3)
    var vignette:Parameters     = Parameters.init(defaultValue: 1.3, min: 0.3, max: 1.3, current: 0.8)
}

class VideoEdAdjustView: UIView,HSCenterSliderDelegate {
    
    weak var delegate                       :AdjustViewDelegate?
    @IBOutlet weak var adjustCV             :UICollectionView!
    @IBOutlet weak var adjustView           :UIView!
    @IBOutlet weak var adjustSlider         :UISlider!
    @IBOutlet weak var adjustCenterSlider   :HSHorizontalCenterSlider!
    //@IBOutlet weak var adjustCenterSlider :UIView!
    @IBOutlet weak var closeBtn             :UIButton!
    @IBOutlet weak var doneBtn              :UIButton!
    
    var adjustFilterValuesObj               = AdjustFilterValues()
    var selectedIndex                       = IndexPath(item: 0, section: 0)
    var adjustFilterNameArr:[String]        = ["Brightness","Contrast","Exposure","Saturation","Temperature","Sharpness","Hue","Vignette"]
    var adjustIcons:[String]                = ["brightness","contrast","exposure","saturation","temperature","sharpness","hue","vignette"]

    class func initWithFrame(frame:CGRect) -> VideoEdAdjustView {
        let view = self.fromNib()
        view.frame = frame
        view.setupUI()
        return view
    }
    
    
    func setupUI() {
     
        adjustCV.delegate =  self
        adjustCV.dataSource = self
        
        let nibname = UINib(nibName: "CropCVCell", bundle: nil)
        // let nibname = UINib(nibName: "VideoEdAdjustCVCell", bundle: nil)
        adjustCV.register(nibname, forCellWithReuseIdentifier: "CropCVCell")
        adjustCenterSlider.delegate =  self

        
        adjustCenterSlider.thumbImageView.backgroundColor =  UIColor.clear
        adjustCenterSlider.thumbImageView.isHidden = false
        adjustCenterSlider.thumb.backgroundColor =  UIColor.clear
        adjustCenterSlider.thumb.clipsToBounds = false
        adjustCenterSlider.thumbImageView.image = UIImage(named: "ic_slider_line")
        adjustCenterSlider.lblProgressValue.isHidden = true
        let selectEffects:Parameters = adjustFilterValuesObj.brightness
        adjustCenterSlider.rangeValue =  HSRange(low: Double(selectEffects.min), high: Double(selectEffects.max))
        adjustCenterSlider.tintColor = AppThemeManager.shared.currentTheme().TintColor
        
        adjustSlider.minimumTrackTintColor = AppThemeManager.shared.currentTheme().TintColor
    }
    

    func updateSelectedSlider(_ indexPath:IndexPath)  {
     
        let effectName:String = adjustFilterNameArr[indexPath.item]
        
        var selectEffects:Parameters = adjustFilterValuesObj.brightness
        switch effectName {
        case "Brightness":
            selectEffects = adjustFilterValuesObj.brightness
        case "Contrast":
            selectEffects = adjustFilterValuesObj.contrast
        case "Exposure":
            selectEffects = adjustFilterValuesObj.exposure
        case "Saturation":
            selectEffects = adjustFilterValuesObj.saturation
        case "Temperature":
            selectEffects = adjustFilterValuesObj.temprature
        case "Sharpness":
            selectEffects = adjustFilterValuesObj.sharpness
        case "Hue":
            selectEffects = adjustFilterValuesObj.hue
        case "Vignette":
            selectEffects = adjustFilterValuesObj.vignette
        default:
            print("Default Value")
            selectEffects = adjustFilterValuesObj.brightness
        }
        
        adjustSlider.minimumValue = Float(selectEffects.min)
        adjustSlider.maximumValue = Float(selectEffects.max)
        adjustSlider.setValue(Float(selectEffects.current), animated: true)
        //adjustSlider.value = Float(selectEffects.current)
        
        adjustCenterSlider.rangeValue =  HSRange(low: Double(selectEffects.min), high: Double(selectEffects.max))
        //adjustCenterSlider.set(value: Double(selectEffects.current), animated: false)
        
        if effectName == "Vignette" {
            adjustCenterSlider.set(value: Double(1.0 - selectEffects.current + 0.3 + 0.3), animated: false)
        } else {
            adjustCenterSlider.set(value: Double(selectEffects.current), animated: false)
        }
    }

    @IBAction func adjustSliderAct(_ sender: UISlider) {
        adjustSliderDidChange(value: CGFloat(sender.value))
    }
    
    func adjustSliderDidChange(value :CGFloat) {
     
        let effectNamre:String = adjustFilterNameArr[selectedIndex.item]
        
        // ["Brightness","Contrast","Exposure","Saturation","Temperature","Sharpness","Hue","Vignette"]
        switch effectNamre {
        case "Brightness":
            adjustFilterValuesObj.brightness.current = value
        case "Contrast":
            adjustFilterValuesObj.contrast.current = value
        case "Exposure":
            adjustFilterValuesObj.exposure.current = value
        case "Saturation":
            adjustFilterValuesObj.saturation.current = value
        case "Temperature":
            adjustFilterValuesObj.temprature.current = value
        case "Sharpness":
            adjustFilterValuesObj.sharpness.current = value
        case "Hue":
            adjustFilterValuesObj.hue.current = value
        case "Vignette":
            
            let vignetteValue =  (1.0 - value + 0.3 + 0.3)
            adjustFilterValuesObj.vignette.current = vignetteValue
            
        default:
            print("Default Value")
        }
        
        // adjustFilterValuesObj.
        delegate?.updateAdjustEffect(filterValues: adjustFilterValuesObj)
    }
    
    func centerSlider(slider: HSCenterSlider, didChange value: Double) {
        debugPrint("HSCenterSlider did change");
        adjustSliderDidChange(value: CGFloat(value))
    }
    
    @IBAction func closeBtnAct(_ sender: UIButton) {
        
        self.isHidden = true
    }
    
    @IBAction func doneBtnAct(_ sender: UIButton) {
        self.isHidden = true

    }
    
}

extension VideoEdAdjustView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 30.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30.0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return adjustFilterNameArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let minLength = (collectionView.frame.size.height * 0.5)  -  14.0
        let length =  44.0 < minLength ? minLength : 44.0
        
        return CGSize(width: length, height: length)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:CropCVCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CropCVCell", for: indexPath) as! CropCVCell
        cell.backgroundColor = UIColor.clear
        cell.imageView.backgroundColor = UIColor.clear
        cell.mainView.backgroundColor  = UIColor.clear
        
        cell.titleLbl.isHidden = true
        cell.imageView.isHidden = false
        
        let imageStr =  adjustIcons[indexPath.item]
        let image = UIImage(named: imageStr)
        cell.imageView.image = image
        cell.imageView.tintColor =  UIColor.gray
        
        if indexPath.item == selectedIndex.item {
             cell.imageView.tintColor =  UIColor.white
        }
        
        cell.titleLbl.text = adjustFilterNameArr[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex =  indexPath
        updateSelectedSlider(indexPath)
        adjustCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        adjustCV.reloadData()
    }
    
}

extension VideoEdAdjustView {
    class func fromNib<T: VideoEdAdjustView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
