//
//  TextsTableViewCell.swift
//  VideoTrimmer
//
//  Created by strongapps on 27/06/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class TextsTableViewCell: UITableViewCell {

    @IBOutlet var localTextLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
