//
//  DualPickerVC.swift
//  VideoTrimmer
//
//  Created by strongapps on 07/06/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class DualPickerVC: UIViewController {

    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var pickerCV: UICollectionView!
    var galleryAssetsResults = PHFetchResult<PHAsset>()
    var selectedAssets : [PHAsset] = []
    var selectedImages : [UIImage] = []
    var arrayForSelectedIndexPaths : [IndexPath] = []
    var mediaTypePrefferred : PHAssetMediaType = .image
    var isPresented : Bool = false
    var alreadySelectedMediaCount : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "VideoEdPickerCollectionViewCell", bundle: nil)
        self.pickerCV.register(nib, forCellWithReuseIdentifier: "VideoEdPickerCollectionViewCell")
        self.pickerCV.delegate = self
        self.pickerCV.dataSource = self
        checkAuthorizationForPhotoLibrary()

        // Do any additional setup after loading the view.
    }
    func checkAuthorizationForPhotoLibrary(){
        
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.loadAlbums()
            })
        }else if(status == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    
                    DispatchQueue.main.async {
                        // Access has been granted.
                        self.loadAlbums()
                    }
                }else{
                    
                }
            })
        }else if(status == PHAuthorizationStatus.denied) {

            AlertManager.shared.alertWithTitle(title: " ", message: "Permission is denied. Please Go to Settings and enable permission.")

        }
    }
    @IBAction func backBtnAct(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    func loadAlbums() {
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate",ascending: false)]
        if mediaTypePrefferred != .unknown{
            fetchOptions.predicate = NSPredicate(format: "mediaType = %d",  mediaTypePrefferred.rawValue)
        }
        galleryAssetsResults = PHAsset.fetchAssets(with: fetchOptions)
        
        DispatchQueue.main.async {
            
            self.pickerCV.reloadData()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DualPickerVC : UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryAssetsResults.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoEdPickerCollectionViewCell", for: indexPath) as! VideoEdPickerCollectionViewCell
        let asset = galleryAssetsResults.object(at: indexPath.row)
        
        DispatchQueue.main.async {
            PHImageManager.default().requestImage(for: asset, targetSize: cell.frame.size, contentMode: PHImageContentMode.aspectFill, options: nil) { (image, userInfo) -> Void in
                DispatchQueue.main.async {
                    if let safeImage = image{
                        cell.thumbImage.image = safeImage
                    }
                }
            }
        }
        
        
        if asset.mediaType == .video{
            let timeString = CGFloat(asset.duration).convertSecondsToDurationString()
            cell.durationLabel.text = timeString
        }else{
            cell.durationLabel.isHidden = true
        }
        
        if arrayForSelectedIndexPaths.contains(indexPath){
            cell.selectedView.isHidden = false
        }else{
            cell.selectedView.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var itemInLine  : CGFloat = 3
        if(IS_IPAD){
            itemInLine = itemInLine * 2
        }
        let  gapInCells : CGFloat = 0.5
        let widthFactor = CGFloat(itemInLine)
        let screenTotalWidth = collectionView.bounds.size.width
        let width = (screenTotalWidth - widthFactor * (gapInCells+1))/itemInLine
        return CGSize.init(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if mediaTypePrefferred == .image{
            
            
            if arrayForSelectedIndexPaths.contains(indexPath){
                
                if let index = arrayForSelectedIndexPaths.index(of:indexPath) {
                    arrayForSelectedIndexPaths.remove(at: index)
                    selectedImages.remove(at: index)
                }
                self.pickerCV.reloadItems(at: [indexPath])
                
                if self.arrayForSelectedIndexPaths.count > 0 {
//                    self.doneButtonBottomCons.isActive = true
                }else{
//                    self.doneButtonBottomCons.isActive = false
                }
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
            else{
                
                if self.selectedImages.count + self.alreadySelectedMediaCount >= IMAGES_LIMIT{
                    debugPrint("Images reached to selection limit")
                    return
                }
                
                let asset = galleryAssetsResults.object(at: indexPath.row)
                
                //                DispatchQueue.main.async {
                //                    self.startAnimating( type: NVActivityIndicatorType.circleStrokeSpin)
                //                }
                getOriginalImage(asset: asset) { (image) in
                    
                    
                    if let safeImage = image{
                        
                        if let imageJpeg = safeImage.jpegData(compressionQuality: 1){
                            if let newImage = UIImage(data: imageJpeg){
                                self.arrayForSelectedIndexPaths.append(indexPath)
                                self.selectedImages.append(newImage)
                                
                            }
                        }
                        
                        
                    }else{
                        debugPrint("userInfo=> error")
                    }
                    self.pickerCV.reloadItems(at: [indexPath])
                    
                    if self.arrayForSelectedIndexPaths.count > 0 {
//                        self.doneButtonBottomCons.isActive = true
                    }else{
//                        self.doneButtonBottomCons.isActive = false
                    }
                    UIView.animate(withDuration: 0.5) {
                        self.view.layoutIfNeeded()
                    }
                    
                    
                }
                /*
                 let options = PHImageRequestOptions()
                 options.deliveryMode = .fastFormat
                 options.isSynchronous = true
                 
                 PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.aspectFill, options: options) { (image, userInfo) -> Void in
                 
                 DispatchQueue.main.async {
                 
                 if let safeImage = image{
                 
                 if let imageJpeg = safeImage.jpegData(compressionQuality: 1){
                 if let newImage = UIImage(data: imageJpeg){
                 self.arrayForSelectedIndexPaths.append(indexPath)
                 self.selectedImages.append(newImage)
                 
                 }
                 }
                 
                 
                 }else{
                 debugPrint("userInfo=>",userInfo as Any)
                 }
                 
                 self.pickerCV.reloadItems(at: [indexPath])
                 
                 if self.arrayForSelectedIndexPaths.count > 0 {
                 self.doneButtonBottomCons.isActive = true
                 }else{
                 self.doneButtonBottomCons.isActive = false
                 }
                 UIView.animate(withDuration: 0.5) {
                 self.view.layoutIfNeeded()
                 }
                 
                 }
                 }
                 */
                
                
                //                arrayForSelectedIndexPaths.append(indexPath)
                //                selectedAssets.append(galleryAssetsResults[indexPath.row])
                
            }
            
            
            //            if arrayForSelectedIndexPaths.count == limit{
            //
            //                    let slideVC = VideoEdSlideshowVC(nibName: "VideoEdSlideshowVC", bundle: nil)
            //                    slideVC.galleryAssetsResults = selectedImages
            //                    self.navigationController?.pushViewController(slideVC, animated: true)
            //
            //            }
            
        }else if mediaTypePrefferred == .video{
            if arrayForSelectedIndexPaths.contains(indexPath){
                
                if let index = arrayForSelectedIndexPaths.index(of:indexPath) {
                    arrayForSelectedIndexPaths.remove(at: index)
                    selectedAssets.remove(at: index)
                }
                
            }else{
                
                arrayForSelectedIndexPaths.append(indexPath)
                selectedAssets.append(galleryAssetsResults[indexPath.row])
            }
            
            self.pickerCV.reloadItems(at: [indexPath])
            
            
            if arrayForSelectedIndexPaths.count > 0 {
//                doneButtonBottomCons.isActive = true
            }else{
//                doneButtonBottomCons.isActive = false
            }
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            debugPrint("indexPath",indexPath.row)
            
            collectionView.reloadData()
            
            
        }
        
    }
    public func getOriginalImage(asset: PHAsset,clouser: @escaping((UIImage?)->())) {
        
        let options = PHImageRequestOptions()
        options.isNetworkAccessAllowed = true
        options.deliveryMode = .highQualityFormat
        
        var startedAnimation = false
        options.progressHandler = { (progress, err, stop,dic) in
            print("progress for image", progress)
            
            if !startedAnimation{
                startedAnimation = true
                DispatchQueue.main.async {
//                    self.startAnimating( type: NVActivityIndicatorType.circleStrokeSpin)
                }
            }
            let str = String(format: "%d%%", Int(progress * 100))
            NVActivityIndicatorPresenter.sharedInstance.setMessage(str)
            
        }
        PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: options) { (image, info) in
            if image != nil {
                DispatchQueue.main.async {
//                    self.stopAnimating()
                }
                clouser(image)
            } else {
                DispatchQueue.main.async {
//                    self.stopAnimating()
                }
                clouser(nil)
            }
        }
        
    }
}
