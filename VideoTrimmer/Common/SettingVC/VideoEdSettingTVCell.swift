//
//  VideoEdSettingTVCell.swift
//  VideoTrimmer
//
//  Created by strongapps on 19/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class VideoEdSettingTVCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var viewBg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        createUI()
    }
    
    func createUI() {
        
        iconImageView?.layer.shadowRadius = 8.0
//        iconImageView?.layer.shadowColor = UIColor.red.cgColor
        iconImageView?.layer.shadowOpacity = 0.6
        iconImageView?.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        iconImageView?.clipsToBounds = false
        iconImageView?.layer.masksToBounds = false
        
//        var layer = CALayer()
//        layer.backgroundColo
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        
        
    }
    
}
