//
//  VideoEdSettingVC.swift
//  VideoTrimmer
//  Created by strongapps on 19/04/19.
//  Copyright © 2019 strongapps. All rights reserved.

import UIKit
import MessageUI


class VideoEdSettingVC: UIViewController,MFMailComposeViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var lblSettingHeader: UILabel!
    
    var backColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 240.0/255.0)
    
    
    var imageArray  :[String] = ["lock","reload-arrow","star","sent-mail"]
    var titleArray  :[String] = ["Unlock All features","Restore Purchases","Rate App","Send Feedback"]
    var selectors   :[String] = ["gotoInAppScreen","restorePurhases","rateApp","sendFeedbackMail"]

    var colorArray  :[UIColor] = [UIColor(red: 255.0/255.0, green: 25.0/255.0, blue: 78.0/255.0, alpha: 1.0),
                                  UIColor(red: 31.0/255.0, green: 169.0/255.0, blue: 248.0/255.0, alpha: 1.0),
                                  UIColor(red: 248.0/255.0, green: 160.0/255.0, blue: 36.0/255.0, alpha: 1.0),
                                  UIColor(red: 32.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0),
                                  UIColor(red: 254.0/255.0, green: 30.0/255.0, blue: 121.0/255.0, alpha: 1.0),]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblSettingHeader.font = UIFont(name: FONT_SEMIBOLD, size: 20)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        createUI()
    }
    
    func themeAppearanceSetup(){
//        self.view.backgroundColor =  AppThemeManager().currentTheme().VCBackGroundColor
//        lblSettingHeader.textColor =  AppThemeManager().currentTheme().TextColor
    }
    
    @IBAction func backBtnAct(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    func createUI()  {
        
        if IAPManagerLocal.sharedInstance.isInAppPurchased(){
            
            imageArray   = ["reload-arrow","star","sent-mail"]
            titleArray   = ["Restore Purchases","Rate App","Send Feedback"]
            selectors    = ["restorePurhases","rateApp","sendFeedbackMail"]
            
            
            colorArray   = [ UIColor(red: 31.0/255.0, green: 169.0/255.0, blue: 248.0/255.0, alpha: 1.0),
            UIColor(red: 248.0/255.0, green: 160.0/255.0, blue: 36.0/255.0, alpha: 1.0),
            UIColor(red: 32.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0),
            UIColor(red: 254.0/255.0, green: 30.0/255.0, blue: 121.0/255.0, alpha: 1.0),]

        }
        let nibname = UINib(nibName: "VideoEdSettingTVCell", bundle: nil)
        tableView.register(nibname, forCellReuseIdentifier: "VideoEdSettingTVCell")
        tableView.delegate =  self
        tableView.dataSource = self
        
        topView.layer.shadowColor = UIColor.black.cgColor
        topView.layer.shadowRadius = 6.0
        
    }
    
    // Buttons Functions
    
    @objc func gotoInAppScreen()  {
        
        if IAPManagerLocal.sharedInstance.isInAppPurchased(){
            AlertManager.shared.alertWithMessage(message: "Currently your in App is purchased")
            return
        }
        
        if IAPManagerLocal.sharedInstance.productsIAP.count < 1 {
            AlertManager.shared.alertWithMessage(message: "Products not loeaded yet ,Please try again")
            IAPManagerLocal.sharedInstance.loadProducts()
            return
        }
        print("gotoInAppScreen")
    }
    
    @objc func restorePurhases()  {
        print("restorePurhases")
        if IAPManagerLocal.sharedInstance.isInAppPurchased(){
            AlertManager.shared.alertWithMessage(message: "Currently your in App is purchased")
            return
        }

        IAPManagerLocal.sharedInstance.restoreIAP()

    }
    
    @objc func rateApp()  {


    }
    
    @objc func sendFeedbackMail() {
        
        print("sendFeedbackMail")
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["sadads@gmail.com"])
        mailComposerVC.setSubject("Feedback")
        mailComposerVC.setMessageBody("*  App Feedback Note * \n\n Device Model: \(UIDevice.modelNameString)\n\n iOS Version: \(UIDevice.current.systemVersion)", isHTML: false)

//        mailComposerVC.setToRecipients(["you@yoursite.com"])
//        mailComposerVC.setMessageBody("<p>You're so awesome!</p>", isHTML: true)

        
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposerVC, animated: true, completion: nil)
        } else {
//            @"Please Login through Setting"
            
            AlertManager.shared.alertWithTitle(title: "Alert", message: "Please Login through Setting.")
        }
        
        
    }


    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @objc func otherApps()  {
        print("otherApps")

    }
    
}

extension VideoEdSettingVC:UITableViewDataSource,UITableViewDelegate
{
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 80.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return  imageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoEdSettingTVCell", for: indexPath) as! VideoEdSettingTVCell
        cell.iconImageView.image = UIImage(named: "\(imageArray[indexPath.row])")
        cell.titleLbl.text = titleArray[indexPath.row]
        cell.titleLbl.font = UIFont(name: FONT_REGULAR, size: 18)
        cell.iconImageView.layer.shadowColor = colorArray[indexPath.row].cgColor
        cell.viewBg.backgroundColor = .clear
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectorStr = selectors[indexPath.row]
        let selector = Selector(selectorStr)
        self.perform(selector)
        
    }
    
}

