//  AppDelegate.swift
//  VideoTrimmer
//  Created by strongapps on 03/04/19.
//  Copyright © 2019 strongapps. All rights reserved.


import UIKit
import CoreData
import COSTouchVisualizer
import GoogleMobileAds
import Firebase

@_exported import Photos
@_exported import NVActivityIndicatorView
@_exported import SwiftyStoreKit
@_exported import StoreKit
@_exported import Photos
@_exported import MultiSlider

public var isMusicRepetitionStatus = false


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,COSTouchVisualizerWindowDelegate {
    
    static var sharedInstance = AppDelegate()
        var window: UIWindow?
    
//    lazy var window: UIWindow? = {
//        var customWindow = COSTouchVisualizerWindow(frame: UIScreen.main.bounds)
//        customWindow.touchVisualizerWindowDelegate = self
//        return customWindow
//    }()
    
    func touchVisualizerWindowShouldAlwaysShowFingertip(_ window: COSTouchVisualizerWindow!) -> Bool {
        return false
    }
    func touchVisualizerWindowShouldShowFingertip(_ window: COSTouchVisualizerWindow!) -> Bool {
        return false
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }

        UIApplication.shared.isIdleTimerDisabled = true
        DirectoryManager.shared.cleanDocumentVideos()
        DirectoryManager.shared.clearTmpDirectory()
        window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = .white
        
        let vc = FrontScreenVC(nibName: "FrontScreenVC", bundle: nil)
        let nav = UINavigationController(rootViewController: vc)
        nav.isNavigationBarHidden = true
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
        
        //        completeOldTransaction()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
//            IAPManagerLocal.sharedInstance.loadProducts()

            
        }
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        FirebaseApp.configure()
        return true
    }
    func completeOldTransaction(){
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                // Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
    }
    func setupIAP() {
        
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    print("\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                case .failed, .purchasing, .deferred:
                break // do nothing
                @unknown default:
                    break // do nothing
                }
            }
        }
        
        SwiftyStoreKit.updatedDownloadsHandler = { downloads in
            
            // contentURL is not nil if downloadState == .finished
            let contentURLs = downloads.compactMap { $0.contentURL }
            if contentURLs.count == downloads.count {
                print("Saving: \(contentURLs)")
                SwiftyStoreKit.finishTransaction(downloads[0].transaction)
            }
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    
    
    
    
    
    func getTodayString() -> String{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
        let currentDateStr = formatter.string(from: Date())
        print(currentDateStr)
        return currentDateStr
    }
    
}


