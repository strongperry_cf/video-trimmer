
import Foundation
import UIKit

@objc class ConstantsVideoEd: NSObject {
    
    static let musicData = "musicData"
    static let stickersData = "StickersData"
    static let actionFxData = "ActionFxData"

}
let IS_APP_FREE_TEMP = true


class VideoEdColorObject: NSObject {
    static let themeColor1 = UIColor(red: 71.0 / 255.0, green: 64.0 / 255.0, blue: 236.0 / 255.0, alpha: 1.0)
    static let themeColor2 = UIColor(red: 119.0 / 255.0, green: 44.0 / 255.0, blue: 231.0 / 255.0, alpha: 1.0)
}
let colorGrey140 = UIColor(displayP3Red: 140/255.0, green: 140/255.0, blue: 140/255.0, alpha: 1)
let colorGrey252 = UIColor(displayP3Red: 252/255.0, green: 252/255.0, blue: 252/255.0, alpha: 1)

struct Urls {
    
    static let baseUrlNew = "http://52.22.52.18/VideoEditorial/"
    
    static let stickerListAPINew: String = baseUrlNew + "VideoEditorialSticker/Stickersdata.php"
    static let musicListAPINew:String = baseUrlNew + "VideoEditorialMusic/Soundsdata.php"
    static let actionFxListAPINew: String = baseUrlNew + "VideoEditorialFX/fxdata.php"

    
}

class Source: NSObject {
    static var allFonts: [UIFont] {
        var fonts: [UIFont] = []
        let fontNames = ["BodoniFLF-Bold", "ShorelinesScriptBold", "Alittlesunshine", "AcryleScriptPersonalUse", "BaronNeueBlackItalic", "BaronNeueBlack", "BaronNeueBoldItalic", "BaronNeueBold", "BaronNeue"]
        for font in fontNames {
            fonts.append(UIFont(name: font, size: 12.0) ?? UIFont.systemFont(ofSize: 12.0))
        }
        return fonts
    }
    
    static var colors: [UIColor] {
        return [.white, UIColor.almondColor, UIColor.antiqueWhiteColor, UIColor.beigeColor, UIColor.bananaColor(), UIColor.brickRedColor(), UIColor.babyBlueColor(), UIColor.blueberryColor(), UIColor.beigeColor, UIColor.coralColor(), UIColor.dustColor(), UIColor.denimColor(), UIColor.emeraldColor(), UIColor.eggplantColor(), UIColor.easterPinkColor()]
    }
    

    
    //MARK:- Font file form bundle
    
    static func getAllColorPaletteFromPlist() -> NSArray {
        // Get colorPalette array from plist file
        var colorsArray:NSArray = []
        if let path = Bundle.main.path(forResource: "colorPalettes", ofType: "plist"){
            guard  let colorListArray = NSArray(contentsOfFile: path) else {
                return colorsArray
            }
            colorsArray = colorListArray
        }
        return colorsArray
    }
    
    
    
    static let  NETWORK_CONNECTION_BACKNOTIFICATION = "NetWorkConnectionWithBackgoundNotification"
}

