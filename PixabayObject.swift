/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct PixabayObject : Mappable {
    
	var id : Int?
	var pageURL : String?
	var type : String?
	var previewURL : String?
	var largeImageURL : String?
    var imageSize : Int?
    var picture_id : String?
    var largeVideo : String?
    var mediumVideo : String?
    
	init?(map: Map) {

	}
	mutating func mapping(map: Map) {

		id <- map["id"]
		pageURL <- map["pageURL"]
		type <- map["type"]
		previewURL <- map["previewURL"]
		largeImageURL <- map["largeImageURL"]
        imageSize <- map["imageSize"]
        picture_id <- map["picture_id"]
        largeVideo <- map["videos.large.url"]
        mediumVideo <- map["videos.medium.url"]

	}

}
