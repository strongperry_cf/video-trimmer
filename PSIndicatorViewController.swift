//
//  PSIndicatorViewController.swift
//  scanner
//
//  Created by iapp on 01/05/19.
//  Copyright © 2019 iApp. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

public class PSIndicatorViewController: NSObject ,NVActivityIndicatorViewable {

    
    static var sharedInstance = PSIndicatorViewController()

    func startIndicator()  {
        Helper.dispatchMain {
//            KVNProgress.show()
            
            let activityData = ActivityData(type: .circleStrokeSpin)
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, .none)
            
        }
    }
    
        func startIndicatorWithMessage(message:String)  {
            
//            KVNProgress.show(withStatus: message)
            print("message\(message)")
            
            DispatchQueue.main.async(execute: {
                NVActivityIndicatorPresenter.sharedInstance.setMessage(message)
            })

        }
    
    func startIndicatorWithMessage(message:String,size:CGSize)  {
//        self.startAnimating(size, message: message)
        
//        KVNProgress.show(withStatus: message)
        print("message\(message)")
    }
    
    func stopIndicator() {
        Helper.dispatchMain {
//            KVNProgress.dismiss()
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(.none)
        }
    }
}
