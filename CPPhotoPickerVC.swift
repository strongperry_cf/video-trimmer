//
//  CPPhotoPickerVC.swift
//  ConverterPro
//
//  Created by iApp on 27/07/20.
//  Copyright © 2020 iApp. All rights reserved.
//

import UIKit
import Photos
import PhotosUI
import MobileCoreServices
import ObjectMapper
import Alamofire
import SDWebImage
enum PICKER_SOURCE : String, CaseIterable{
    case GALLERY = "GALLERY"
    case PIXABAY = "PIXABAY"
    case UNSPLASH = "UNSPLASH"
}
class CPPhotoPickerVC: UIViewController {

    @IBOutlet var filesSearchBar: UITextField!
    @IBOutlet var imagesCV: UICollectionView!
    @IBOutlet var albumsTitle: UIButton!
    @IBOutlet var donePickerButton: UIButton!
    
    var pixabayImageSearchArr    = [PixabayObject]()
    var unsplashImageSearchArr   = [UnsplashModel]()

    var isLoadingFetchApi = false{
        didSet{
//            print("isLoadingFetchApi value ",isLoadingFetchApi)
        }
    }
    var searchTextGlobal = "love"
    var currentPageOffsetPixabay = 1
    var currentPageOffsetUnsplash = 1

    private var selectedSource: PICKER_SOURCE = .PIXABAY {

        didSet{
            
            if selectedSource == .GALLERY{
                
            }else{
            }
        }
    }
    private var photoLibrary = TLPhotoLibrary()
    private var collections = [TLAssetsCollection]()
    public var configure = TLPhotosPickerConfigure()
    private var focusedCollection: TLAssetsCollection? = nil

    open var selectedAssets = [TLPHAsset]()
    private var requestIDs = SynchronizedDictionary<IndexPath,PHImageRequestID>()
    private var queue = DispatchQueue(label: "tilltue.photos.pikcker.queue")
    private var playRequestID: (indexPath: IndexPath, requestID: PHImageRequestID)? = nil
    private var queueForGroupedBy = DispatchQueue(label: "tilltue.photos.pikcker.queue.for.groupedBy", qos: .utility)

    private var thumbnailSize = CGSize.zero
    private var usedPrefetch: Bool {
        get {
            return self.configure.usedPrefetch
        }
        set {
            self.configure.usedPrefetch = newValue
        }
    }
    private var allowedLivePhotos: Bool {
        get {
            return self.configure.allowedLivePhotos
        }
        set {
            self.configure.allowedLivePhotos = newValue
        }
    }
    private var allowedVideo: Bool {
        return self.configure.allowedVideo
    }

    private let numberOfItemPerRow = IS_IPAD ? 5 : 3
    private let screenWidth = UIScreen.main.bounds.width
    private let sectionInset = UIEdgeInsets(top: 10, left: 30, bottom: 10, right: 30)
    private let minimumInteritemSpacing =  CGFloat(20)
    private let minimumLineSpacing = CGFloat(20)
    // Calculate the item size based on the configuration above
    private var itemSize: CGSize {
        let interitemSpacesCount = numberOfItemPerRow - 1
        let interitemSpacingPerRow = minimumInteritemSpacing * CGFloat(interitemSpacesCount)
        let rowContentWidth = screenWidth - sectionInset.right - sectionInset.left - interitemSpacingPerRow
        let width = rowContentWidth / CGFloat(numberOfItemPerRow)
        let height = width // feel free to change the height to whatever you want

        return CGSize(width: width, height: height)
    }
    private var ImageTypeItemSize: CGSize {
        let interitemSpacesCount = numberOfItemPerRow - 1
        let interitemSpacingPerRow = minimumInteritemSpacing * CGFloat(interitemSpacesCount)
        let rowContentWidth = screenWidth - sectionInset.right - sectionInset.left - interitemSpacingPerRow
        let width = rowContentWidth / CGFloat(numberOfItemPerRow)
        let height = 40.0.cgf
        return CGSize(width: width, height: height)
    }
    private var SelctedItemSize: CGSize {
        let numberOfItemPerRow = IS_IPAD ? 7 : 5.5
        let minimumInteritemSpacing =  CGFloat(20)
        let sectionInset = UIEdgeInsets(top: 2, left: 30, bottom: 2, right: 30)
        let interitemSpacesCount = numberOfItemPerRow - 1
        let interitemSpacingPerRow = minimumInteritemSpacing * CGFloat(interitemSpacesCount)
        let rowContentWidth = screenWidth - sectionInset.right - sectionInset.left - interitemSpacingPerRow
        let width = rowContentWidth / CGFloat(numberOfItemPerRow)
        let height = width
        return CGSize(width: width, height: height)
    }
    private var imageArray = [UIImage]() {
        didSet{
            if imageArray.count == 0{
                donePickerButton.isEnabled = false
            }else{
                donePickerButton.isEnabled = true
            }
        }
    }
    override open func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if self.thumbnailSize == CGSize.zero {
            self.thumbnailSize = itemSize
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectedSource = .PIXABAY
        
        self.configure.allowedVideo = false
        self.configure.usedCameraButton = false
        self.configure.allowedLivePhotos = false
        
        filesSearchBar.delegate = self
        filesSearchBar.leftViewMode = .always
        filesSearchBar.setIcon(#imageLiteral(resourceName: "AddMusic"))
        filesSearchBar.borderStyle = .none
        filesSearchBar.layer.masksToBounds = false
        filesSearchBar.layer.cornerRadius = 9.0;
        filesSearchBar.layer.backgroundColor = UIColor.white.cgColor
        filesSearchBar.layer.borderColor = UIColor.clear.cgColor
        filesSearchBar.layer.shadowColor = UIColor.black.cgColor
        filesSearchBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        filesSearchBar.layer.shadowOpacity = 0.04
        filesSearchBar.layer.shadowRadius = 14
        filesSearchBar.attributedPlaceholder = NSAttributedString(string: "Search Video",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.black,
                                                                               NSAttributedString.Key.font : UIFont.mediumFont(size: 15)])
        let celNib = UINib(nibName: "TLPhotoCollectionViewCell", bundle: nil)
        imagesCV.register(celNib, forCellWithReuseIdentifier: "TLPhotoCollectionViewCell")
        
        
        let layout = imagesCV.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = minimumInteritemSpacing
        layout.minimumLineSpacing = minimumLineSpacing
        layout.sectionInset = sectionInset
        imagesCV.collectionViewLayout = layout
        imagesCV.delegate = self
        imagesCV.dataSource = self
    
        pixabaySearch()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
//            self.unsplashSearch()
//        }
        
    }
    func checkAuthorization() {
        switch PHPhotoLibrary.authorizationStatus() {
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { [weak self] status in
                switch status {
                case .authorized:
                    self?.initPhotoLibrary()
                default:
                    self?.handleDeniedAlbumsAuthorization()
                }
            }
        case .authorized:
            self.initPhotoLibrary()
        case .restricted: fallthrough
        case .denied:
            handleDeniedAlbumsAuthorization()
        @unknown default:
            break
        }
    }
    private func initPhotoLibrary() {
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            self.photoLibrary.delegate = self
            self.photoLibrary.fetchCollection(configure: self.configure)
        }else{
            //self.dismiss(animated: true, completion: nil)
        }
    }
    private func handleDeniedAlbumsAuthorization() {
//        self.delegate?.handleNoAlbumPermissions(picker: self)
//        self.handleNoAlbumPermissions?(self)
    }

//    class AlbumModel {
//      let name:String
//      let count:Int
//      let collection:PHAssetCollection
//      init(name:String, count:Int, collection:PHAssetCollection) {
//        self.name = name
//        self.count = count
//        self.collection = collection
//      }
//    }
//
//    func listAlbums() {
//        var album:[AlbumModel] = [AlbumModel]()
//
//        let options = PHFetchOptions()
//        let userAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .any, options: options)
//        userAlbums.enumerateObjects{ (object: AnyObject!, count: Int, stop: UnsafeMutablePointer) in
//            if object is PHAssetCollection {
//                let obj:PHAssetCollection = object as! PHAssetCollection
//
//                let fetchOptions = PHFetchOptions()
//                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
//                fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
//
//                let newAlbum = AlbumModel(name: obj.localizedTitle!, count: obj.estimatedAssetCount, collection:obj)
//                album.append(newAlbum)
//            }
//        }
//
//        for item in album {
//            print(item.name)
//
//            let YourAlbumTitle = item.name
//            let fetchOptions = PHFetchOptions()
//            fetchOptions.predicate = NSPredicate(format: "title = %@", YourAlbumTitle)
//            let resultCollections = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
//            print(resultCollections)
//
//
//        }
//
//    }
    
//    private func setupPhotos() {
//        let fetchOptions = PHFetchOptions()
//        let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .any, options: fetchOptions)
//        let topLevelUserCollections = PHCollectionList.fetchTopLevelUserCollections(with: fetchOptions)
//        let allAlbums = [topLevelUserCollections, smartAlbums] as! PHFetchResult<AnyObject>
//
//
//
//        allAlbums.enumerateObjects {(assetCollection, index, stop) in
//
//            if #available(iOS 9.0, *) {
//                fetchOptions.fetchLimit = 1
//            }
//
//            let assets = PHAsset.fetchAssets(in: assetCollection as! PHAssetCollection, options: fetchOptions)
//            if let tt = assets.firstObject {
//                print(tt)
//            }
//       }
//    }
    //MARK:- Buttons Actions

    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func donePickerAction(_ sender: Any) {
        
    }
    @IBAction func albumsTitleAction(_ sender: UIButton) {

    }
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier != nil{
        }
    }
}
extension CPPhotoPickerVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedSource == .PIXABAY{
            return pixabayImageSearchArr.count
        }else if selectedSource == .UNSPLASH{
            return self.unsplashImageSearchArr.count
        }
        
        guard let collection = self.focusedCollection else {
            return 0
        }
        return self.focusedCollection?.sections?[safe: section]?.assets.count ?? collection.count
    }
    

    private func getSelectedAssets(_ asset: TLPHAsset) -> TLPHAsset? {
           if let index = self.selectedAssets.firstIndex(where: { $0.phAsset == asset.phAsset }) {
               return self.selectedAssets[index]
           }
           return nil
       }

    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        func makeCell(nibName: String) -> TLPhotoCollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: nibName, for: indexPath) as! TLPhotoCollectionViewCell
            cell.configure = self.configure
            cell.imageView?.image = #imageLiteral(resourceName: "insertPhotoMaterial@3x.png")
            cell.liveBadgeImageView?.image = nil
            return cell
        }
        
        if selectedSource == .PIXABAY{
            
            let nibName = self.configure.nibSet?.nibName ?? "TLPhotoCollectionViewCell"
            let cell = makeCell(nibName: nibName)

            let data: PixabayObject =  self.pixabayImageSearchArr[indexPath.row]

            if let previewUrlPath = data.picture_id{
//                let url = URL(string: previewUrlPath)
                
                Helper.dispatchMain {
                    let url = URL(string: String(format:"https://i.vimeocdn.com/video/\(previewUrlPath)_295x166.jpg"))
                    cell.imageView?.sd_setImage(with: url, completed: nil)

                }
            }
            return cell
            
        }else if selectedSource == .UNSPLASH{
            
            let nibName = self.configure.nibSet?.nibName ?? "TLPhotoCollectionViewCell"
            let cell = makeCell(nibName: nibName)

            let data: UnsplashModel =  self.unsplashImageSearchArr[indexPath.row]

            if let previewUrlPath = data.urls?.thumb{
                let url = URL(string: previewUrlPath)
                DispatchQueue.main.async {
                    
                    cell.imageView?.sd_setImage(with: url, completed: nil)
                }
            }
            return cell
            
        }
        

        let nibName = self.configure.nibSet?.nibName ?? "TLPhotoCollectionViewCell"
        let cell = makeCell(nibName: nibName)
        guard let collection = self.focusedCollection else { return cell }
        cell.isCameraCell = collection.useCameraButton && indexPath.section == 0 && indexPath.row == 0

        guard let asset = collection.getTLAsset(at: indexPath) else { return cell }
        cell.asset = asset.phAsset
        
        if let selectedAsset = getSelectedAssets(asset) {
            cell.selectedAsset = true
            cell.orderLabel?.text = "\(selectedAsset.selectedOrder)"
        }else{
            cell.selectedAsset = false
        }
        if asset.state == .progress {
            cell.indicator?.startAnimating()
        }else {
            cell.indicator?.stopAnimating()
        }
        if let phAsset = asset.phAsset {
            if self.usedPrefetch {
                let options = PHImageRequestOptions()
                options.deliveryMode = .opportunistic
                options.resizeMode = .exact
                options.isNetworkAccessAllowed = true
                let requestID = self.photoLibrary.imageAsset(asset: phAsset, size: self.thumbnailSize, options: options) { [weak self, weak cell] (image,complete) in
                    guard let `self` = self else { return }
                    DispatchQueue.main.async {
                        if self.requestIDs[indexPath] != nil {
                            cell?.imageView?.image = image
                            cell?.update(with: phAsset)
                            if self.allowedVideo {
                                cell?.durationView?.isHidden = asset.type != .video
                                cell?.duration = asset.type == .video ? phAsset.duration : nil
                            }
                            if complete {
                                self.requestIDs.removeValue(forKey: indexPath)
                            }
                        }
                    }
                }
                if requestID > 0 {
                    self.requestIDs[indexPath] = requestID
                }
            }else {
                queue.async { [weak self, weak cell] in
                    guard let `self` = self else { return }
                    let requestID = self.photoLibrary.imageAsset(asset: phAsset, size: self.thumbnailSize, completionBlock: { (image,complete) in
                        DispatchQueue.main.async {
                            if self.requestIDs[indexPath] != nil {
                                cell?.imageView?.image = image
                                cell?.update(with: phAsset)
                                if self.allowedVideo {
                                    cell?.durationView?.isHidden = asset.type != .video
                                    cell?.duration = asset.type == .video ? phAsset.duration : nil
                                }
                                if complete {
                                    self.requestIDs.removeValue(forKey: indexPath)
                                }
                            }
                        }
                    })
                    if requestID > 0 {
                        self.requestIDs[indexPath] = requestID
                    }
                }
            }
            if self.allowedLivePhotos {
                cell.liveBadgeImageView?.image = asset.type == .livePhoto ? PHLivePhotoView.livePhotoBadgeImage(options: .overContent) : nil
                cell.livePhotoView?.delegate = asset.type == .livePhoto ? self : nil
            }
        }
        cell.alpha = 0
        UIView.transition(with: cell, duration: 0.1, options: .curveEaseIn, animations: {
            cell.alpha = 1
        }, completion: nil)
        
        cell.layer.cornerRadius = 5
        cell.clipsToBounds = true

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return itemSize
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedSource == .PIXABAY{
            if !self.isLoadingFetchApi{
                
                self.isLoadingFetchApi = true
                
                let data: PixabayObject =  self.pixabayImageSearchArr[indexPath.row]

                if let previewUrlPath = data.pageURL{
                    PSIndicatorViewController.sharedInstance.startIndicator()
                    
                    ApiCaller.sharedInstance.requestPixabayLargeImage(previewUrlPath) { (videoUrl) in
                        
                        if let validUrl = videoUrl{

                            let storyboard : UIStoryboard = UIStoryboard(name: "VideoEdAlbum", bundle: nil)
                                  let vc : VideoEdEditVC = storyboard.instantiateViewController(withIdentifier: "VideoEdEditVC") as! VideoEdEditVC
                                  vc.singleVideoUrl = validUrl
                                  self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                        self.isLoadingFetchApi = false
                        PSIndicatorViewController.sharedInstance.stopIndicator()
                    }
                }
                

            }
            
            return
        }else if selectedSource == .UNSPLASH{
            
            if !self.isLoadingFetchApi{
                
                self.isLoadingFetchApi = true
                
                PSIndicatorViewController.sharedInstance.startIndicator()
                let data: UnsplashModel =  self.unsplashImageSearchArr[indexPath.row]

                if let previewUrlPath = data.urls?.regular{

                    ApiCaller.sharedInstance.requestPixabayLargeImage(previewUrlPath) { (imageNew) in

                        if let fullResoImage = imageNew{
//                            self.imageArray.append(fullResoImage)

                        }
                        self.isLoadingFetchApi = false
                        PSIndicatorViewController.sharedInstance.stopIndicator()
                    }
                }
                

            }
            
            return
        }
        
        guard let collection = focusedCollection, let asset = collection.getTLAsset(at: indexPath), let _ = asset.phAsset else { return }
        if let fullResoImage = asset.fullResolutionImage{
            imageArray.append(fullResoImage)


        }
//        if let image  =  self.getAssetThumbnail(asset:phAsset) {
//            imageArray.append(image)
//        }

    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if selectedSource == .PIXABAY && collectionView == imagesCV{
            if indexPath.row == self.pixabayImageSearchArr.count-1 && !self.isLoadingFetchApi  {
                self.loadMoreData()
            }
        }else if selectedSource == .UNSPLASH && collectionView == imagesCV{
            if indexPath.row == self.unsplashImageSearchArr.count-1 && !self.isLoadingFetchApi  {
                self.loadMoreData()
            }
        }

        
    }

    func loadMoreData() {
        if !self.isLoadingFetchApi {
            DispatchQueue.global().async {
                if self.selectedSource == .PIXABAY{
                    self.pixabaySearch()
                }else if self.selectedSource == .UNSPLASH{
                    self.unsplashSearch()
                }
                
            }
        }
    }

    func getAssetThumbnail(asset: PHAsset) -> UIImage? {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize , contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            if result != nil{
                thumbnail = result!
                
            }
        })
        return thumbnail
    }
}
// MARK: - TLPhotoLibraryDelegate
extension CPPhotoPickerVC: TLPhotoLibraryDelegate {
    func loadCameraRollCollection(collection: TLAssetsCollection) {
        self.collections = [collection]
        self.focusFirstCollection()
//        self.indicator.stopAnimating()
//        self.reloadTableView()
    }
    
    func loadCompleteAllCollection(collections: [TLAssetsCollection]) {
        self.collections = collections
        self.focusFirstCollection()
//        let isEmpty = self.collections.count == 0
//        self.subTitleStackView.isHidden = isEmpty
//        self.emptyView.isHidden = !isEmpty
//        self.emptyImageView.isHidden = self.emptyImageView.image == nil
//        self.indicator.stopAnimating()
//        self.reloadTableView()
//        self.registerChangeObserver()
    }

}


// MARK: - Video & LivePhotos Control PHLivePhotoViewDelegate
extension CPPhotoPickerVC: PHLivePhotoViewDelegate {
    private func stopPlay() {
        guard let playRequest = self.playRequestID else { return }
        self.playRequestID = nil
        guard let cell = self.imagesCV.cellForItem(at: playRequest.indexPath) as? TLPhotoCollectionViewCell else { return }
        cell.stopPlay()
    }
    
    private func playVideo(asset: TLPHAsset, indexPath: IndexPath) {
        stopPlay()
        guard let phAsset = asset.phAsset else { return }
        if asset.type == .video {
            guard let cell = self.imagesCV.cellForItem(at: indexPath) as? TLPhotoCollectionViewCell else { return }
            let requestID = self.photoLibrary.videoAsset(asset: phAsset, completionBlock: { (playerItem, info) in
                DispatchQueue.main.async { [weak self, weak cell] in
                    guard let `self` = self, let cell = cell, cell.player == nil else { return }
                    let player = AVPlayer(playerItem: playerItem)
                    cell.player = player
                    player.play()
                    player.isMuted = self.configure.muteAudio
                }
            })
            if requestID > 0 {
                self.playRequestID = (indexPath,requestID)
            }
        }else if asset.type == .livePhoto && self.allowedLivePhotos {
            guard let cell = self.imagesCV.cellForItem(at: indexPath) as? TLPhotoCollectionViewCell else { return }
            let requestID = self.photoLibrary.livePhotoAsset(asset: phAsset, size: self.thumbnailSize, completionBlock: { [weak cell] (livePhoto,complete) in
                cell?.livePhotoView?.isHidden = false
                cell?.livePhotoView?.livePhoto = livePhoto
                cell?.livePhotoView?.isMuted = true
                cell?.livePhotoView?.startPlayback(with: self.configure.startplayBack)
            })
            if requestID > 0 {
                self.playRequestID = (indexPath,requestID)
            }
        }
    }
    
    public func livePhotoView(_ livePhotoView: PHLivePhotoView, didEndPlaybackWith playbackStyle: PHLivePhotoViewPlaybackStyle) {
        livePhotoView.isMuted = true
        livePhotoView.startPlayback(with: self.configure.startplayBack)
    }
    
    public func livePhotoView(_ livePhotoView: PHLivePhotoView, willBeginPlaybackWith playbackStyle: PHLivePhotoViewPlaybackStyle) {
    }
}
extension CPPhotoPickerVC {
    private func focusFirstCollection() {
        if self.focusedCollection == nil, let collection = self.collections.first {
            self.focusedCollection = collection
            self.updateTitle()
            self.reloadCollectionView()
        }
    }
    private func updateTitle() {
        guard self.focusedCollection != nil else { return }
//        self.albumsTitle.setTitle(self.focusedCollection?.title, for: .normal)
    }
    private func reloadCollectionView() {
        guard self.focusedCollection != nil else {
            return
        }
        if let groupedBy = self.configure.groupByFetch, self.usedPrefetch == false {
            queueForGroupedBy.async { [weak self] in
                self?.focusedCollection?.reloadSection(groupedBy: groupedBy)
                DispatchQueue.main.async {
                    self?.imagesCV.reloadData()
                }
            }
        }else {
            self.imagesCV.reloadData()
        }
    }
}
public protocol TLPhotosPickerLogDelegate: class {
    func focused(collection: TLAssetsCollection)

}
extension CPPhotoPickerVC: TLPhotosPickerLogDelegate {
    
    func focused(collection: TLAssetsCollection) {
        func resetRequest() {
            cancelAllImageAssets()
        }
        resetRequest()
        self.collections[getfocusedIndex()].recentPosition = self.imagesCV.contentOffset
        var reloadIndexPaths = [IndexPath(row: getfocusedIndex(), section: 0)]
        self.focusedCollection = collection
        self.focusedCollection?.fetchResult = self.photoLibrary.fetchResult(collection: collection, configure: self.configure)
        reloadIndexPaths.append(IndexPath(row: getfocusedIndex(), section: 0))
//        self.albumPopView.tableView.reloadRows(at: reloadIndexPaths, with: .none)
//        self.albumPopView.show(false, duration: self.configure.popup.duration)
        self.updateTitle()
        self.reloadCollectionView()
        self.imagesCV.contentOffset = collection.recentPosition

    }
    private func cancelAllImageAssets() {
        self.requestIDs.forEach{ (indexPath, requestID) in
            self.photoLibrary.cancelPHImageRequest(requestID: requestID)
        }
        self.requestIDs.removeAll()
    }
    private func getfocusedIndex() -> Int {
        guard let focused = self.focusedCollection, let result = self.collections.firstIndex(where: { $0 == focused }) else { return 0 }
        return result
    }
    
}
extension CPPhotoPickerVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let searchText = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if !(searchText!.isEmpty){
            
            guard let search = searchText else { return false}

            if selectedSource == .PIXABAY{
                
                pixabayImageSearchArr.removeAll()
                searchTextGlobal = search
                currentPageOffsetPixabay = 1
                pixabaySearch()
            }else if selectedSource == .UNSPLASH{
                unsplashImageSearchArr.removeAll()
                searchTextGlobal = search
                currentPageOffsetUnsplash = 1
                unsplashSearch()

            }
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    
    func pixabaySearch(){
        
        if !NetworkReachabilityManager()!.isReachable{
            return
        }
        self.isLoadingFetchApi = true

        ApiCaller.sharedInstance.requestPixabayData(searchTextGlobal,page: currentPageOffsetPixabay) { (results) in

            if let gotResult = results{
                self.pixabayImageSearchArr.append(contentsOf: gotResult)
                if self.selectedSource == .PIXABAY{
                    self.imagesCV.reloadData()
                }
                self.currentPageOffsetPixabay = self.currentPageOffsetPixabay + 1

            }
            self.isLoadingFetchApi = false
        }
    }
    
    func unsplashSearch(){
        
        if !NetworkReachabilityManager()!.isReachable{
            return
        }
        self.isLoadingFetchApi = true

        ApiCaller.sharedInstance.requestUnsplashData(searchTextGlobal,page: currentPageOffsetUnsplash) { (results) in

            if let gotResult = results{
                self.unsplashImageSearchArr.append(contentsOf: gotResult)
                if self.selectedSource == .UNSPLASH{
                    self.imagesCV.reloadData()
                }
                self.currentPageOffsetUnsplash = self.currentPageOffsetUnsplash + 1

            }
            self.isLoadingFetchApi = false
        }
    }
    

    
}
