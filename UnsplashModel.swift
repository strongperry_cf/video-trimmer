/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class UnsplashModel {
	public var urls : UrlsUnsplash?
    public class func modelsFromDictionaryArray(array:NSArray) -> [UnsplashModel]
    {
        var models:[UnsplashModel] = []
        for item in array
        {
            models.append(UnsplashModel(dictionary: item as! Dictionary)!)
        }
        return models
    }

	required public init?(dictionary: Dictionary<String,Any>) {

		if (dictionary["urls"] != nil) { urls = UrlsUnsplash(dictionary: dictionary["urls"] as! Dictionary) }
	}

	public func dictionaryRepresentation() -> Dictionary<String,Any> {

		var dictionary = Dictionary<String,Any>()
        dictionary["urls"] = self.urls
        
        return dictionary
        
	}

}

public class UrlsUnsplash {
    public var raw : String?
    public var full : String?
    public var regular : String?
    public var small : String?
    public var thumb : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let urls_list = Urls.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Urls Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [UrlsUnsplash]
    {
        var models:[UrlsUnsplash] = []
        for item in array
        {
            models.append(UrlsUnsplash(dictionary: item as! Dictionary<String,Any>)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let urls = Urls(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Urls Instance.
*/
    required public init?(dictionary: Dictionary<String,Any>) {

        raw = dictionary["raw"] as? String
        full = dictionary["full"] as? String
        regular = dictionary["regular"] as? String
        small = dictionary["small"] as? String
        thumb = dictionary["thumb"] as? String
    }

        
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.raw, forKey: "raw")
        dictionary.setValue(self.full, forKey: "full")
        dictionary.setValue(self.regular, forKey: "regular")
        dictionary.setValue(self.small, forKey: "small")
        dictionary.setValue(self.thumb, forKey: "thumb")

        return dictionary
    }

}
